<?php
namespace App\Helper;

use Auth;
use App\User;
use App\Model\Module;
use App\Model\Rights;
use App\Model\Admin_assign_branch;
use App\Model\Teacher_assign_class;
use App\Model\ClassTbl;


class UserRights {

	public static function rights()
	{
		if(Auth::user()){
			if(Auth::user()->Use_Type=="1"){
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
				$data['CURight'] = array('Admin');
			}elseif(Auth::user()->Use_Type=="5"){
				$user_right = Rights::where('Usr_Use_Id',Auth::user()->Use_Id)->select("Usr_Mod_Id")->get()->toArray();
				// $data['menu'] = Module::with('children')->whereIn('Mod_Id',$user_right)->get();
				$right = array();
				foreach ($user_right as $r) {
					array_push($right, $r["Usr_Mod_Id"]);
				}
				$data['CURight'] = array_unique($right);
			}elseif(Auth::user()->Use_Type=="2"){
				$user_right = Rights::where('Usr_Use_Id',Auth::user()->Use_Id)->select("Usr_Mod_Id")->get()->toArray();
				// $data['menu'] = Module::with('children')->whereIn('Mod_Id',$user_right)->get();
				// $user_right = Rights::where('Usr_Use_Id',Auth::user()->Use_Id)->get();
				$right = array();
				foreach ($user_right as $r) {
					array_push($right, $r["Usr_Mod_Id"]);
				}
				$data['CURight'] = array_unique($right);
			}
		}else{
			$data['CURight'] = array();
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		}
		return $data['CURight'];
	}
	public static function branchAdmin()
	{
		if(Auth::user()){
			if(Auth::user()->Use_Type==5){
				$data["branchAccess"] = Admin_assign_branch::where("Aab_Use_Id",Auth::user()->Use_Id)
						->select("Aab_Brn_Id")->get()->toArray();
				$data["branchClass"] = ClassTbl::whereIn("Cla_Bra_Id",$data["branchAccess"])->select("Cla_Id")->get()->toArray();
			}else{
				$data = "";
			}
		}
		return $data;
	}
	public static function branchAdminApi($id,$rights)
	{
		$data = "";
		if($id!=""){
			if($rights === "branchAccess"){
				$branchAccess = Admin_assign_branch::where("Aab_Use_Id",$id)->select("Aab_Brn_Id")->get()->toArray();
				return $branchAccess;
			}else if($rights === "branchClass"){
				$branchAccess = Admin_assign_branch::where("Aab_Use_Id",$id)->select("Aab_Brn_Id")->get()->toArray();
				$branchClass = ClassTbl::whereIn("Cla_Bra_Id",$branchAccess)->select("Cla_Id")->get()->toArray();
				return $branchClass;
			}else{
				return "Please Send Second Paremeter in string like ( branchAccess | branchClass ) ";
			}
		}
		return $data;
	}	
	public static function teacher(){
		if(Auth::user()){
			if(Auth::user()->Use_Type=="2"){
				$main_class_id = User::where("Use_Id",Auth::user()->Use_Id)->first()->Use_Cla_Id;
				$data["branchAccess"] = ClassTbl::where('Cla_Id',$main_class_id)->first()->Cla_Bra_Id;
				$data["branchClass"] = ClassTbl::where("Cla_Bra_Id",$data["branchAccess"])->select("Cla_Id")->get()->toArray();
				$data["AssignClass"] = Teacher_assign_class::where("Tac_Use_Id",Auth::user()->Use_Id)->select("Tac_Cla_Id")->get()->toArray();
			}else{
				$data = "";
			}
		}
		return $data;
	}
	public static function teacherApi($id,$rights){
		$data = "";
		if($id!=""){
			$main_class_id = User::where("Use_Id",$id)->first()->Use_Cla_Id;
			if($rights === "branchAccess"){
				$branchAccess = ClassTbl::where('Cla_Id',$main_class_id)->first()->Cla_Bra_Id;
				return $branchAccess;
			}elseif($rights === "branchClass" ){
				$branchAccess = ClassTbl::where('Cla_Id',$main_class_id)->first()->Cla_Bra_Id;
				$branchClass = ClassTbl::where("Cla_Bra_Id",$branchAccess)->select("Cla_Id")->get()->toArray();
				return $branchClass;
			}elseif( $rights === "AssignClass" ){
				$AssignClass = Teacher_assign_class::where("Tac_Use_Id",$id)->select("Tac_Cla_Id")->get()->toArray();
				return $AssignClass;
			}else{
				return "Please Send Second Paremeter in string like ( branchAccess | branchClass | AssignClass ) ";
			}
		}
		return $data;
	}
	public static function classTeacherList($class_id){
		$AssignClass = Teacher_assign_class::where("Tac_Cla_Id",$class_id)->select("Tac_Use_Id")->get()->toArray();
		$teacherList = User::whereIn('Use_Id',$AssignClass)->get();
		return $teacherList;
	}
	public static function classTeacherIdList($class_id){
		$teacherIdList = Teacher_assign_class::where("Tac_Cla_Id",$class_id)->select(["Tac_Use_Id"])->get()->toArray();
		// $teacherList = User::whereIn('Use_Id',$AssignClass)->get();
		return $teacherIdList;
	}
	public static function classesTeacherList($class_ids){
		$AssignClass = Teacher_assign_class::whereIn("Tac_Cla_Id",$class_ids)->select("Tac_Use_Id")->get()->toArray();
		$teacherList = User::whereIn('Use_Id',$AssignClass)->get();
		return $teacherList;
	}
}