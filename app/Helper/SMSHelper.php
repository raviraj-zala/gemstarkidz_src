<?php
namespace App\Helper;

use Auth;
use App\User;
use App\Model\Module;
use App\Model\Rights;
use App\Model\Admin_assign_branch;
use App\Model\Teacher_assign_class;
use App\Model\ClassTbl;


class SMSHelper 
{
	public static function sendSMS($number,$message)
	{
		$url = 'http://erp.aerosol.io/api/sendmsg.php';
		$fields = array(
			'user' => urlencode('gemstarkidz'),
			'pass' => urlencode('123456'),
			'sender' => urlencode('GEMKID'),
			'phone' => $number,
			'text' => $message,
			'priority' => urlencode('ndnd'),
			'stype' => urlencode('normal')	
		);

		$fields_string = "";
		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);
		//close connection
		curl_close($ch);
		return $result;
	}
}