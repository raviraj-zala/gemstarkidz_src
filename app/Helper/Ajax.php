<?php 
namespace App\Helper;

use Illuminate\Database\Eloquent\Helper;
use Illuminate\Http\Request;
use App\Model\Exceptions;
use App\Model\Zone;	
use App\Model\ClassTbl;
use App\Model\Branch;
use App\Model\Attendence;
use App\User;
use App\Model\Homework;
use App\Model\Event;
use App\Model\Module;
use App\Model\Report;
use App\Model\Student;
use App\Model\Socialactivity;
use App\Model\Payment;
use App\Helper\UserRights;
use DB;
use App\Model\RouteTbl;
use Auth;
use Exception;

class Ajax	
{
	//Zone Search
	public static function searchZone(Request $request)
	{
		try {
			if($request->Zon_Name!=""){
				$data['zone'] = Zone::where('Zon_Name','like','%'.$request->Zon_Name.'%')->get();
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
				return $data;
			}
		}
	    catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Branch Search 
	public static function searchBranch(Request $request)
	{
		try {
			if($request->Brn_Name){
				if( Auth::user()->Use_Type == "1" ){
					$data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
								      ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
								      ->where('branch_tbl.Brn_Name','like','%'.$request->Brn_Name.'%')
								      ->get();
				}else if(Auth::user()->Use_Type == "2"){
					$teacher = UserRights::teacher();
					$data['branch'] = Branch::where("Brn_Id",$teacher["branchAccess"])
									  ->join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
								      ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
								      ->where('branch_tbl.Brn_Name','like','%'.$request->Brn_Name.'%')
								      ->get();
				}else if(Auth::user()->Use_Type == "5"){
					$branch = UserRights::branchAdmin();
					$data['branch'] = Branch::where("Brn_Id",$branch["branchAccess"])
									  ->join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
								      ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
								      ->where('branch_tbl.Brn_Name','like','%'.$request->Brn_Name.'%')
								      ->get();
				}
	            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            return $data;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//User Search 
	public static function searchUser(Request $request)
	{
		try {
			if($request->Use_Name){
				if(Auth::user()->Use_Type=="1")
				{
					if($request->Use_Name == "Teacher" || $request->Use_Name == "teacher")
					{
	                    $data['user'] =   $data['user'] =DB::table('user_tbl as u')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
	                		->whereIn('cBy.Use_Type',["2"])
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
	            		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            		return $data;
					}elseif($request->Use_Name == "Driver" || $request->Use_Name == "driver"){
						$data['user'] =DB::table('user_tbl as u')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
	                		->whereIn('cBy.Use_Type',["3"])
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
		            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		            	return $data;
					}else{
                        $data['user'] =DB::table('user_tbl as u')
	                		->whereIn('cBy.Use_Type',["1","2","3","5"])
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
                        	->orwhere('cBy.Use_City','like','%'.$request->Use_Name.'%')
                        	->orwhere('cBy.Use_Mobile_No','like','%'.$request->Use_Name.'%')
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
		            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		            	return $data;
					}
				}else if(Auth::user()->Use_Type=="2"){
					$teacher = UserRights::teacher();
					$teacher_ids = User::whereIn("Use_Cla_Id",$teacher["branchClass"])->select(["Use_Id"])->get()->toArray();
					$teacher_driver_ids = User::whereIn('Use_Cla_Id',$teacher['branchClass'])->get();
					if($request->Use_Name == "Teacher" || $request->Use_Name == "teacher")
					{
	                    $data['user'] =   $data['user'] =DB::table('user_tbl as u')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
                        	->whereIn('cBy.Use_Cla_Id',$teacher["branchClass"])
	                		->whereIn('cBy.Use_Type',["2"])
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
	            		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            		return $data;
					}elseif($request->Use_Name == "Driver" || $request->Use_Name == "driver"){
						$data['user'] =DB::table('user_tbl as u')
							->join('route_tbl','route_tbl.Rou_Use_Id','=','u.Use_Id')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
	                		->whereIn('cBy.Use_Type',["3"])
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
		            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		            	return $data;
					}else{
                        $data['user'] =DB::table('user_tbl as u')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
                        	->whereIn('cBy.Use_Id',$teacher_ids)
	                		->whereIn('cBy.Use_Id',$driver)
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
		            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		            	return $data;
					}
				}else if(Auth::user()->Use_Type=="5"){
					$branch = UserRights::branchAdmin();
                	$driver = RouteTbl::where("Rou_Brn_Id",$branch["branchAccess"])->select("Rou_Use_Id")->get()->toArray();
                	$teacher_ids = User::whereIn("Use_Cla_Id",$branch["branchClass"])->select(["Use_Id"])->get()->toArray();
					if($request->Use_Name == "Teacher" || $request->Use_Name == "teacher")
					{
	                    $data['user'] =   $data['user'] =DB::table('user_tbl as u')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
                        	->whereIn('cBy.Use_Cla_Id',$branch["branchClass"])
	                		->whereIn('cBy.Use_Type',["2"])
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
	            		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            		return $data;
					}elseif($request->Use_Name == "Driver" || $request->Use_Name == "driver"){
						$data['user'] =DB::table('user_tbl as u')
							->join('route_tbl','route_tbl.Rou_Use_Id','=','u.Use_Id')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
	                		->whereIn('cBy.Use_Type',["3"])
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
		            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		            	return $data;
					}else{
                        $data['user'] =DB::table('user_tbl as u')
                        	->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
                        	->whereIn('cBy.Use_Id',$teacher_ids)
	                		->whereIn('cBy.Use_Id',$driver)
	                		->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
	                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
	                        ->orderBy('cBy.Use_Id','DESC')
	                        ->get();
		            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		            	return $data;
					}
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}	

	// Search Parent
	public static function searchParent(Request $request)
	{
		try {

			if(Auth::user()->Use_Type=="2")
			{
				if($request->Use_Name)
				{
					$teacher = UserRights::teacher();
					$parent_ids = Student::whereIn("Std_Cla_Id",$teacher["branchClass"])->select('Std_Parent_Id')->get()->toArray();

					$data['parent'] = DB::table('user_tbl as user')
	                    ->where('cBy.Use_Type',4)
						->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
	                    ->whereIn('cBy.Use_Id',$parent_ids)
	                    ->leftjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
	                    ->leftjoin('student_tbl','student_tbl.Std_Parent_Id','=','cBy.Use_Id')
	                    ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
	                    ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
	                    ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
	                    ->get();
	            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();

	            	return $data;
				}
			}
			if(Auth::user()->Use_Type=="1")
			{
				if($request->Use_Name)
				{
					$userList = User::where('Use_Type',4)->select('Use_Id')->get()->toArray();
					$data['parent'] = DB::table('user_tbl as user')
						// ->where('cBy.Use_Type',4)
						->whereIn('cBy.Use_Type',$userList)
						->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
						->orwhere('cBy.Use_Mother_Name','like','%'.$request->Use_Name.'%')
						->orwhere('student_tbl.Std_Name','like','%'.$request->Use_Name.'%')
						->orwhere('student_tbl.Std_Gr_No','like','%'.$request->Use_Name.'%')
						->orwhere('branch_tbl.Brn_Name','like','%'.$request->Use_Name.'%')
						->orwhere('cBy.Use_City','like','%'.$request->Use_Name.'%')
						->orwhere('cBy.Use_Mobile_No','like','%'.$request->Use_Name.'%')
						->orwhere('cBy.Use_Mother_Mobile_No','like','%'.$request->Use_Name.'%')
	                    ->leftjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
	                    ->leftjoin('student_tbl','student_tbl.Std_Parent_Id','=','cBy.Use_Id')
	                    ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
	                    ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
	                    ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
	                    ->get();
	            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();

	            	return $data;
				}
			}

			if(Auth::user()->Use_Type=="5")
			{
				if($request->Use_Name)
				{
					$branch = UserRights::branchAdmin();
					$parent_ids = Student::whereIn("Std_Cla_Id",$branch["branchClass"])->select('Std_Parent_Id')->get()->toArray();

					$data['parent'] = DB::table('user_tbl as user')
	                    ->whereIn('cBy.Use_Id',$parent_ids)
	                    ->where('cBy.Use_Type',4)
						->where('cBy.Use_Name','like','%'.$request->Use_Name.'%')
	                    ->leftjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
	                    ->leftjoin('student_tbl','student_tbl.Std_Parent_Id','=','cBy.Use_Id')
	                    ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
	                    ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
	                    ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
	                    ->get();

	            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();

	            	return $data;
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Class Search
	public static function searchClass(Request $request)
	{
		try {
			if($request->Cla_Name){
				$data['class'] = ClassTbl::where('Cla_Class','like','%'.$request->Cla_Name.'%')
				->Orwhere('branch_tbl.Brn_Name','like','%'.$request->Cla_Name.'%')
				->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
				->select('branch_tbl.*','class_tbl.*')
				->get();
            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}	
	
	// Search Report
    public static function searchReport(Request $request) {
        try {
            if ($request->Std_Name) {
                $data['students'] = Report::where('student_tbl.Std_Name','LIKE','%'.$request->Std_Name.'%')
				->Orwhere('student_tbl.Std_Gr_No','like','%'.$request->Std_Name.'%')
				->Orwhere('class_tbl.Cla_Class','like','%'.$request->Std_Name.'%')
                ->leftjoin('student_tbl','Std_Id','=','Rpt_Std_Id')
                ->leftjoin('class_tbl','Cla_Id','=','Std_Cla_Id')
                ->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                return $data;
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }


	//Event Search
	public static function searchEvent(Request $request)
	{
		try {

			if(Auth::user()->Use_Type=="2")
			{
				$teacherBrnId = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->select("Cla_Bra_Id")->first()->Cla_Bra_Id;
				if($request->Eve_Name){
					$data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
							     ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
							      ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName')
							      ->where('event_tbl.Eve_Name','like','%'.$request->Eve_Name.'%')
							      ->where('event_tbl.Eve_Brn_Id',$teacherBrnId)
							      ->whereDate('event_tbl.Eve_Date','>=',date('Y-m-d'))
								  ->orderBy('event_tbl.Eve_Date', 'ASC')
							      ->get();

	            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            	return $data;
				}
			}

			if(Auth::user()->Use_Type=="1")
			{
				if($request->Eve_Name){
					$data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
							     ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
							     ->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')
							      ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.*')
							      ->where('event_tbl.Eve_Name','like','%'.$request->Eve_Name.'%')
							      ->whereDate('event_tbl.Eve_Date','>=',date('Y-m-d'))
								  ->orderBy('event_tbl.Eve_Date', 'ASC')
							      ->get();

	            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            	return $data;
				}
			}
			if(Auth::user()->Use_Type=="5")
			{
				if($request->Eve_Name){
					$data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
							     ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
							     ->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.*')
							      ->where('event_tbl.Eve_Name','like','%'.$request->Eve_Name.'%')
							      ->Orwhere('class_tbl.Cla_Class','like','%'.$request->Eve_Name.'%')
							      ->Orwhere('event_tbl.Eve_Date','like','%'.$request->Eve_Name.'%')
							      ->whereDate('event_tbl.Eve_Date','>=',date('Y-m-d'))
								  ->orderBy('event_tbl.Eve_Date', 'ASC')
							      ->get();

	            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            	return $data;
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Search Student

	public static function searchStudent(Request $request)
	{
		try {

			if(Auth::user()->Use_Type=="2")
			{
				$teacherBrnId = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->select("Cla_Bra_Id")->first()->Cla_Bra_Id;

				$class_id = ClassTbl::where("Cla_Bra_Id",$teacherBrnId)->select('Cla_Id')->get()->toArray();

				if($request->Std_Name){
				$data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                            ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                            ->join('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                            ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->where('Std_Name','like','%'.$request->Std_Name.'%')
                            ->whereIn('Std_Cla_Id',$class_id)
                            ->orWhere('Std_Gr_No',$request->Std_Name)
                            ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name')
                            ->get();

            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		 }

		 if(Auth::user()->Use_Type=="1")
		 {
		 	if($request->Std_Name){
				$data['student'] = Student::where('student_tbl.Std_Name','like','%'.$request->Std_Name.'%')
						->Orwhere('student_tbl.Std_Gr_No','like','%'.$request->Std_Name.'%')
						->Orwhere('branch_tbl.Brn_Name','like','%'.$request->Std_Name.'%')
						->Orwhere('user_tbl.Use_Mobile_No','like','%'.$request->Std_Name.'%')
						->Orwhere('class_tbl.Cla_Class','like','%'.$request->Std_Name.'%')
						->Orwhere('user_tbl.Use_Name','like','%'.$request->Std_Name.'%')
						->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                        ->leftjoin('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','user_tbl.Use_Mobile_No as parentMobileno','branch_tbl.Brn_Name')
                        ->get();

            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		 }
		 if(Auth::user()->Use_Type=="5")
		 {
		 	if($request->Std_Name){
			$data['student'] = Student::where('student_tbl.Std_Name','like','%'.$request->Std_Name.'%')
						->Orwhere('student_tbl.Std_Gr_No','like','%'.$request->Std_Name.'%')
						->Orwhere('branch_tbl.Brn_Name','like','%'.$request->Std_Name.'%')
						->Orwhere('user_tbl.Use_Mobile_No','like','%'.$request->Std_Name.'%')
						->Orwhere('class_tbl.Cla_Class','like','%'.$request->Std_Name.'%')
						->Orwhere('user_tbl.Use_Name','like','%'.$request->Std_Name.'%')
						->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                        ->leftjoin('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','user_tbl.Use_Mobile_No as parentMobileno','branch_tbl.Brn_Name')
                        ->get();

            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		 }
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Search Student in Payment

	public static function searchSocial(Request $request)
	{
		try {
			/* admin side search social */
			if(Auth::user()->Use_Type=="1")
			{
				if($request->Soa_Title){

	            $data['socialActivitys'] = Socialactivity::join('branch_tbl', 'socialactivity_tbl.Soa_Branch', '=', 'branch_tbl.Brn_Id')->where('Soa_Title','LIKE','%'.$request->Soa_Title.'%')->orderBy('Soa_Id','DESC')->get();
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            	return $data;
				}
			}

			if(Auth::user()->Use_Type=="2")
			{
				$teacherBrnId = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->select("Cla_Bra_Id")->first()->Cla_Bra_Id;

				$class_id = ClassTbl::where("Cla_Bra_Id",$teacherBrnId)->select('Cla_Id')->get()->toArray();

				if($request->Soa_Title){

	            $data['socialActivitys'] = Socialactivity::join('branch_tbl', 'socialactivity_tbl.Soa_Branch', '=', 'branch_tbl.Brn_Id')->where('Soa_Title','LIKE','%'.$request->Soa_Title.'%')->whereIn('Soa_Cla_Id',$class_id)->orderBy('Soa_Id','DESC')->get();
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            	return $data;
				}
			}

			if(Auth::user()->Use_Type=="5")
			{
				if($request->Soa_Title){

	            $data['socialActivitys'] = Socialactivity::where('Soa_Title','LIKE','%'.$request->Soa_Title.'%')->orderBy('Soa_Id','DESC')->get();
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            	return $data;
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public static function searchPayment(Request $request)
	{
		try {
			if($request->Pay_Stu_Name){
				$data['payment'] = Payment::select('payment_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName', 'u.Use_Name as createdByName', 'branch_tbl.Brn_Name', 'student_tbl.Std_Name')
                    ->join('class_tbl', 'payment_tbl.Pay_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl as u', 'payment_tbl.Pay_CreatedBy', '=', 'u.Use_Id')
                    ->join('branch_tbl', 'class_tbl.Cla_Bra_Id', '=', 'branch_tbl.Brn_Id')
                    ->join('student_tbl', 'student_tbl.Std_Id', '=', 'payment_tbl.Pay_Std_Id')
                    ->join('user_tbl', 'user_tbl.Use_Id', '=', 'student_tbl.Std_Parent_Id')
                	->where('Std_Name','LIKE','%'.$request->Pay_Stu_Name.'%')
                    ->Orwhere('user_tbl.Use_Name','LIKE','%'.$request->Pay_Stu_Name.'%')
                    ->groupby('Pay_Std_Id')
                    ->orderBy('Pay_Id', 'DESC')
                    ->get();
            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	// Search Route
	public static function searchRoute(Request $request)
	{
		try {
			if($request->Rou_Name){
				$data['route'] = RouteTbl::where('route_tbl.Rou_Name','LIKE','%'.$request->Rou_Name.'%')
				->Orwhere('branch_tbl.Brn_Name','like','%'.$request->Rou_Name.'%')
				->Orwhere('user_tbl.Use_Name','like','%'.$request->Rou_Name.'%')
				->join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
				->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
				->orderBy('Rou_Id','DESC')
				->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
				->get();
            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	// Search For Diary
	public static function searchAssignment(Request $request)
	{
		try {
			if($request->Hmw_Subject){
				if(Auth::user()->Use_Type == "1"){
					$data['homework'] = Homework::where('Hmw_Subject','LIKE','%'.$request->Hmw_Subject.'%')->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')->select(['homework_tbl.*','class_tbl.Cla_Class','user_tbl.Use_Name'])->orderBy('Hmw_Id','DESC')->get();
				}
            	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            	return $data;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Display Class Wise Section 
	public static function clsChange(Request $request)
	{
		try {
			if($request->Cla_Id){
				$cls = ClassTbl::select('Cla_Section')->where('Cla_Id',$request->Cla_Id)->get();
				echo '<option>-- Select --</option>';
				foreach ($cls as $key => $value) {
					echo '<option value="'.$value['Cla_Section'].'">'.$value['Cla_Section'].'</option>';
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Zone Status Change 
	public static function zoneStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$zone = Zone::whereIn('Zon_Id',$request->input('id'))->update(['Zon_Status'=> 1]);
				if($zone) return true; else return false;
			}elseif($request->input('status') == 0){
				$zone = Zone::whereIn('Zon_Id',$request->input('id'))->update(['Zon_Status'=> 0]);
				if($zone) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
	
	public static function branchStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$branch = Branch::whereIn('Brn_Id',$request->input('id'))->update(['Brn_Status'=> 1]);
				if($branch) return true; else return false;
			}elseif($request->input('status') == 0){
				$branch = Branch::whereIn('Brn_Id',$request->input('id'))->update(['Brn_Status'=> 0]);
				if($branch) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
	public static function userStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$user = User::whereIn('Use_Id',$request->input('id'))->update(['Use_Status'=> 1]);
				if($user) return true; else return false;
			}elseif($request->input('status') == 0){
				$user = User::whereIn('Use_Id',$request->input('id'))->update(['Use_Status'=> 0]);
				if($user) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
	public static function classStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$class = ClassTbl::whereIn('Cla_Id',$request->input('id'))->update(['Cla_Status'=> 1]);
				if($class) return true; else return false;
			}elseif($request->input('status') == 0){
				$class = ClassTbl::whereIn('Cla_Id',$request->input('id'))->update(['Cla_Status'=> 0]);
				if($class) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public static function studentStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$class = Student::whereIn('Std_Id',$request->input('id'))->update(['Std_Status'=> 1]);
				if($class) return true; else return false;
			}elseif($request->input('status') == 0){
				$class = Student::whereIn('Std_Id',$request->input('id'))->update(['Std_Status'=> 0]);
				if($class) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public static function routeStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$route = RouteTbl::whereIn('Rou_Id',$request->input('id'))->update(['Rou_Status'=> 1]);
				if($route) return true; else return false;
			}elseif($request->input('status') == 0){
				$route = RouteTbl::whereIn('Rou_Id',$request->input('id'))->update(['Rou_Status'=> 0]);
				if($route) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	// Social Activity Active InActive

	public static function socialStatus(Request $request)
	{
		try {
			if($request->input('status') == 1){
				$route = Socialactivity::whereIn('Soa_Id',$request->input('id'))->update(['Soa_Status'=> 1]);
				if($route) return true; else return false;
			}elseif($request->input('status') == 0){
				$route = Socialactivity::whereIn('Soa_Id',$request->input('id'))->update(['Soa_Status'=> 0]);
				if($route) return true; else return false;
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	// Search Report
    public static function searchRecord(Request $request) 
    {
        try {
        	$reports = Attendence::select('class_tbl.Cla_Class','class_tbl.Cla_Section','class_tbl.Cla_Id','branch_tbl.Brn_Name','student_tbl.Std_Name','student_tbl.Std_Id','attendance_tbl.Att_Pre_Abs','attendance_tbl.Att_Date')
            			->leftjoin('class_tbl','class_tbl.Cla_Id', '=', 'Att_Cla_Id')
            			->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','class_tbl.Cla_Bra_Id')
            			->leftjoin('student_tbl','student_tbl.Std_Id','=','Att_Stu_Id');
 			if(count($request->all())!=0){
	          	$from_date = trim(strip_tags($request->from_date));
            	$to_date = trim(strip_tags($request->to_date));
	            $branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));

		            if($from_date!="" ){
		              $date = str_replace('/', '-', $from_date);
		              $from_date = date('Y-m-d', strtotime($date));
		              $reports->whereDate('attendance_tbl.Att_Date','>=',$from_date);
		            }
		            if($to_date!=""){
		              $date = str_replace('/', '-', $to_date);
		              $to_date = date('Y-m-d', strtotime($date));
		              $reports->whereDate('attendance_tbl.Att_Date','<=',$to_date);		             
		            }
		            if($branch!="" && $branch!='none'){
		            	$reports->where('branch_tbl.Brn_Id',$branch);
		            	
		            }
		            if($class!="" && $class!='none'){
		            	$reports->where('class_tbl.Cla_Class',$class);
		            	
		            }
		            if($section!="" && $section!='none'){
		            	$reports->where('class_tbl.Cla_Section',$section);
		            	
		            }
		    }
	            	$data['reports'] = $reports->get();
	            	$data['CURight'] = UserRights::rights();
					$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	            	return $data;
	            
                } catch (\Exception $e) {
			Exception::exception($e);
		}
    }

    //Search Fees Report
    public static function searchFeesRecord(Request $request)
    {
    	try{
    		$fees_report = Payment::join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl as u','payment_tbl.Pay_CreatedBy','=','u.Use_Id')
                                ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->join('student_tbl','student_tbl.Std_Id','=','payment_tbl.Pay_Std_Id')
                                ->join('user_tbl','user_tbl.Use_Id','=','student_tbl.Std_Parent_Id')
                                ->select('payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name','student_tbl.Std_Name','student_tbl.Std_Gr_No as Grno','student_tbl.Std_Gr_No')
                                ->orderBy('Pay_Id','DESC');
                              

            if(count($request->all())!=0){
	          	$from_date = trim(strip_tags($request->from_date));
            	$to_date = trim(strip_tags($request->to_date));
	            $branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));
	            $status = trim(strip_tags($request->status));

	    
		            if($from_date!="" ){
		              $date = str_replace('/', '-', $from_date);
		              $from_date = date('Y-m-d', strtotime($date));
		              $fees_report->whereDate('payment_tbl.Pay_Date','>=',$from_date);

		            }
		            if($to_date!=""){
		              $date = str_replace('/', '-', $to_date);
		              $to_date = date('Y-m-d', strtotime($date));
		              $fees_report->whereDate('payment_tbl.Pay_Date','<=',$to_date);		             
		            }
		            if($branch!="" && $branch!='none'){
		            	$fees_report->where('branch_tbl.Brn_Id',$branch);
		            	
		            }
		            if($class!="" && $class!='none'){
		            	$fees_report->where('class_tbl.Cla_Class',$class);
		            	
		            }
		            if($section!="" && $section!='none'){
		            	$fees_report->where('class_tbl.Cla_Section',$section);
		            	
		            }
		            if($status!="" && $status!='none'){
		            	$fees_report->where('payment_tbl.Pay_Status',$status);
		            	
		            }
		    }        
		    		$data['fees_report'] = $fees_report->get();
	            	$data['CURight'] = UserRights::rights();
					$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	            	return $data;            

    	}
    	catch (\Exception $e) {
			// Exception::exception($e);
		}

    }

    public static function searchUserreport(Request $request){
    	try{
    		$user_report = DB::table('user_tbl')
                        ->where('cBy.Use_Type',4)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user_tbl.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user_tbl.Use_Id', 'user_tbl.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section')
                        ->orderBy('cBy.Use_Id','DESC');
			if(count($request->all())!=0){
            	$branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));
	            $status = trim(strip_tags($request->status));
		        if($branch!="" && $branch!='none'){
		            $user_report->where('branch_tbl.Brn_Id',$branch);
	            }
	            if($class!="" && $class!='none'){
	            	$user_report->where('class_tbl.Cla_Class',$class);
	            }
	            if($section!="" && $section!='none'){
	            	$user_report->where('class_tbl.Cla_Section',$section);
	            }
	            if ($status==0 && $status!="") {
	            	$user_report->where('cBy.Use_Status','=',0);
            	} elseif ($status==1 && $status!="") {
            		$user_report->where('cBy.Use_Status','=',1);
            	}
            }
            $data['user_report'] = $user_report->get();
        	$data['CURight'] = UserRights::rights();
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
        	return $data;         
    	}
    	catch (\Exception $e) {
			// Exception::exception($e);
		}
	}

	public static function searchStudentReport(Request $request){
		
			$student= Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->leftjoin('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->leftjoin('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                                ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','user_tbl.Use_Mobile_No as parentMobileno','branch_tbl.Brn_Name')
                                ->orderBy('Std_Id','DESC');

	          	// $from_date = trim(strip_tags($request->from_date));
            // 	$to_date = trim(strip_tags($request->to_date));
	            $branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));
				$status = trim(strip_tags($request->status));
		            // if($from_date!="" ){
		            //   $date = str_replace('/', '-', $from_date);
		            //   $from_date = date('Y-m-d', strtotime($date));
		            //   $reports->whereDate('attendance_tbl.Att_Date','>=',$from_date);
		            // }
		            // if($to_date!=""){
		            //   $date = str_replace('/', '-', $to_date);
		            //   $to_date = date('Y-m-d', strtotime($date));
		            //   $reports->whereDate('attendance_tbl.Att_Date','<=',$to_date);		             
		            // }
		            if($branch!="" && $branch!='none'){
		            	$student->where('branch_tbl.Brn_Id',$branch);
		            }
		            if($class!="" && $class!='none'){
		            	$student->where('class_tbl.Cla_Class',$class);
		            }
		            if($section!="" && $section!='none'){
		            	$student->where('class_tbl.Cla_Section',$section);
		            }
		    		if ($status==0 && $status!="") {
		    			$student->where('student_tbl.Std_Status','=',0);	
		    		}
		    		elseif ($status==1 && $status!="") {
		    			$student->where('student_tbl.Std_Status','=',1);	
		    		}
		    $data['student'] = $student->get();
        	$data['CURight'] = UserRights::rights();
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
        	return $data;         
    }                            

    public static function searchSocialActivity(Request $request) 
    {
        try {
        	$socialActivitys = Socialactivity::join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')
                            ->leftjoin('class_tbl','socialactivity_tbl.Soa_Cla_Id','=','class_tbl.Cla_Id')
                            ->select('socialactivity_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class','class_tbl.Cla_Section')
                            ->orderBy('Soa_Id','DESC');

 			if(count($request->all())!=0){
	          	$from_date = trim(strip_tags($request->from_date));
            	$to_date = trim(strip_tags($request->to_date));
	            $branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));
		            if($from_date!="" ){
		              $date = str_replace('/', '-', $from_date);
		              $from_date = date('Y-m-d', strtotime($date));
		              $socialActivitys->whereDate('socialactivity_tbl.Soa_CreatedAt','>=',$from_date);
		            }
		            if($to_date!=""){
		              $date = str_replace('/', '-', $to_date);
		              $to_date = date('Y-m-d', strtotime($date));
		              $socialActivitys->whereDate('socialactivity_tbl.Soa_CreatedAt','<=',$to_date);		             
		            }
		            if($branch!="" && $branch!='none'){
		            	$socialActivitys->where('branch_tbl.Brn_Id',$branch);
		            	
		            }
		            if($class!="" && $class!='none'){
		            	$socialActivitys->where('class_tbl.Cla_Class',$class);
		            	
		            }
		            if($section!="" && $section!='none'){
		            	$socialActivitys->where('class_tbl.Cla_Section',$section);
		            	
		            }
		    }
	            	$data['socialActivitys'] = $socialActivitys->get();
	            	$data['CURight'] = UserRights::rights();
					$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	            	return $data;
	            
                } catch (\Exception $e) {
			// Exception::exception($e);
		}
    }
    public static function searchEventReport(Request $request)
    {
    	try{
    		$event = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
                        ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
                        ->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')
                        ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.*')
                        ->orderBy('Eve_Id','DESC');

            if(count($request->all())!=0){
	          	$from_date = trim(strip_tags($request->from_date));
            	$to_date = trim(strip_tags($request->to_date));
	            $branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));
		            if($from_date!="" ){
		              $date = str_replace('/', '-', $from_date);
		              $from_date = date('Y-m-d', strtotime($date));
		              $event->whereDate('event_tbl.Eve_Date','>=',$from_date);
		            }
		            if($to_date!=""){
		              $date = str_replace('/', '-', $to_date);
		              $to_date = date('Y-m-d', strtotime($date));
		              $event->whereDate('event_tbl.Eve_Date','<=',$to_date);		             
		            }
		            if($branch!="" && $branch!='none'){
		            	$event->where('branch_tbl.Brn_Id',$branch);
		            	
		            }
		            if($class!="" && $class!='none'){
		            	$event->where('class_tbl.Cla_Class',$class);
		            	
		            }
		            if($section!="" && $section!='none'){
		            	$event->where('class_tbl.Cla_Section',$section);
		            	
		            }
		    }
	            	$data['event'] = $event->get();
	            	$data['CURight'] = UserRights::rights();
					$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	            	return $data;            

    	}
    	catch (\Exception $e) {
			// Exception::exception($e);
		}
    }
    
    public static function searchAssignmentReport(Request $request)
    {
    	try{
    		$homework =  Homework::orderBy('Hmw_Id','DESC')
			                ->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')
			                ->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')
			                ->leftjoin('branch_tbl','class_tbl.Cla_Section','=','branch_tbl.Brn_Id')
			                ->select(['homework_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section','user_tbl.Use_Name','branch_tbl.Brn_Name','branch_tbl.Brn_Id']);
            if(count($request->all())!=0){
	          	$from_date = trim(strip_tags($request->from_date));
            	$to_date = trim(strip_tags($request->to_date));
	            $branch = trim(strip_tags($request->branch));
	            $class = trim(strip_tags($request->class));
	            $section = trim(strip_tags($request->section));
		            if($from_date!="" ){
		              $date = str_replace('/', '-', $from_date);
		              $from_date = date('Y-m-d', strtotime($date));
		              $homework->whereDate('homework_tbl.Hmw_Date','>=',$from_date);
		            }
		            if($to_date!=""){
		              $date = str_replace('/', '-', $to_date);
		              $to_date = date('Y-m-d', strtotime($date));
		              $homework->whereDate('homework_tbl.Hmw_Date','<=',$to_date);		             
		            }
		            if($branch!="" && $branch!='none'){
		            	$homework->where('branch_tbl.Brn_Id',$branch);
		            	
		            }
		            if($class!="" && $class!='none'){
		            	$homework->where('class_tbl.Cla_Class',$class);
		            	
		            }
		            if($section!="" && $section!='none'){
		            	$homework->where('class_tbl.Cla_Section',$section);
		            	
		            }
		    }
	            	$data['homework'] = $homework->get();
	            	$data['CURight'] = UserRights::rights();
					$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	            	return $data;            

    	}
    	catch (\Exception $e) {
			// Exception::exception($e);
		}
    }
}
?>