<?php 
namespace App\Helper;

use Illuminate\Database\Eloquent\Helper;

/**
* 
*/
class ResponseMessage
{
	public static function success($msg,$data)
	{
		echo json_encode(['status' => true, 'error' => 200, 'message' => $msg, 'data' => $data]);
	}

	public static function error($msg)
	{
		echo json_encode(['status' => false, 'error' => 401, 'message' => $msg]);	
	}
}
?>