<?php 
namespace App\Helper;

use Illuminate\Database\Eloquent\Helper;
use Illuminate\Http\Request;
use App\Model\Exceptions;
use App\Model\Zone;	
use App\Model\ClassTbl;
use App\Model\Branch;
use App\User;
use App\Model\Event;
use App\Model\Module;
use App\Model\Report;
use App\Model\Student;
use App\Model\Socialactivity;
use App\Model\Payment;
use App\Helper\UserRights;
use DB;
use App\Model\RouteTbl;
use Auth;

class Notification
{
	public static function sendNotification($token,$title,$message,$type="",$action="")
	{
		$fcmUrl = 'https://fcm.googleapis.com/fcm/send';
		$token = $token;
		$title = $title;
		$message = $message;
		$type = $type;
		$temp = array("TYPE" => $type);

		    $notification = [
		    	'title'=> $title,
		        'body' => $message,
		        'sound' => true,
		        'data' => $temp
		    ];
		    
		    $extraNotificationData = ["message" => $notification,"moredata" =>'dd' ];

		    $fcmNotification = [
		        // 'registration_ids' => $tokenList, //multple token array
		        'to'  => $token, //single token
		        'notification' => $notification,
		        'data' => $extraNotificationData
		    ];

		    $headers = [
		        'Authorization: key=AAAAbFypH7M:APA91bFRq_uU5F9ANeilCqIMh-UjkIVtEqasvqJnJ-xiJzMDgX52T8uytdSvi0L8uwxF004Kw5w3R9jb0v395rAdikVEj9pAW3MoflcFDhXdvSQbhP5TIHJMErrYLfruqqQnizhbE36c',
		        'Content-Type: application/json'
		    ];


		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
		    $result = curl_exec($ch);
		    curl_close($ch);

		    return "Notification Send";
	}

}