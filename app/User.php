<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'Use_CreatedAt';
    const UPDATED_AT = 'Use_UpdatedAt';

    protected $table = "user_tbl";

    protected $primaryKey = 'Use_Id';

    protected $username = 'Use_Name';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Use_Name', 'Use_Password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Use_Password', 'remember_token',
    ];

    public function getAuthPassword()
    {
        return $this->Use_Password;
    }
    public function setAttribute($key, $value)
  {
    $isRememberTokenAttribute = $key == $this->getRememberTokenName();
    if (!$isRememberTokenAttribute)
    {
      parent::setAttribute($key, $value);
    }
  }


}
