<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Helper\Ajax;
use App\Model\Zone;
use App\Model\Branch;
use App\Model\Module;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use URL;

class ZoneController extends Controller
{
    protected $rules = ([
                    'name' => 'required',
                    'status' => 'required',
                ]);

    //Zone Information
    public function index()
    {
    	try {
    		$data['i'] = 1;
    		$data['zone'] = Zone::orderBy('Zon_Id','DESC')->paginate(10);
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.Zone.index',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Zone Search
    public function search(Request $request)
    {
    	try {
            return view('auth.Zone.search',Ajax::searchZone($request));
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Zone Creation Form
    public function create()
    {
    	try {
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.Zone.create',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);	
    	}
    }

    //Zone Creation 
    public function store(Request $request)
    {
    	try {
    		$validator = Validator::make($request->all(), $this->rules);

			if($validator->fails()){
				return Response::json(array('error' => $validator->getMessageBag()->toArray()));
			}else{
        		$zone = new Zone;

        		$zone->Zon_Name = $request->name;;
        		$zone->Zon_Description = $request->description;
        		$zone->Zon_Status = $request->status;
        		$zone->Zon_CreatedBy = Auth::user()->Use_Id;
        		$zone->Zon_CreatedAt = date('Y-m-d H:i:s');
        		$zone->Zon_UpdatedBy = Auth::user()->Use_Id;
        		$zone->Zon_UpdatedAt = date('Y-m-d H:i:s');

        		if($zone->save()){
        			return redirect('Zone Master');
        		}else{
        			return redirect('create_zone')->with('error','Zone Creation Fail');
        		}
            }
    	} catch (\Exception $e) {
    		Exceptions::exception($e);		
    	}
    }

    //Zone Edit Form
    public function edit($id)
    {
    	try {	
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	    	$data['zone'] = Zone::where('Zon_Id',$id)->first();
            $data['CURight']=UserRights::rights();
	    	return view('auth.Zone.edit',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Zone Data Update
    public function update(Request $request)
    {
    	try {
    		$validator = Validator::make($request->all(), $this->rules);

			if($validator->fails()){
				return Response::json(array('error' => $validator->getMessageBag()->toArray()));
			}else{
    			$zone['Zon_Name'] = $request->name;
    			$zone['Zon_Description'] = $request->description;
    			$zone['Zon_Status'] = $request->status;
    			$zone['Zon_UpdatedBy'] = Auth::user()->Use_Id;
    			$zone['Zon_UpdatedAt'] = date('Y-m-d H:i:s');

    			if(Zone::where('Zon_Id',$request->id)->update($zone)){
    				return redirect('Zone Master');
    			}else{
    				return redirect('edit_zone/{id}')->with('error','Zone Updation Fail');
    			}
            }

    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Zone Deletion
    public function destroy(Request $request)
    {
    	try 
    	{
    		$all_data = $request->except('_token');
			
			foreach($all_data as $ids) 
			{
				foreach($ids as $id) 
				{
					Zone::where('Zon_Id',$id)->delete();
					Branch::where('Brn_Zon_Id',$id)->delete();
				}
			}
			return redirect()->back();
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Zone Activation 
    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::zoneStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['zone'] = Zone::paginate(10,['*'],'page',$whatIWant);
            else
                $data['zone'] = Zone::paginate(10);
            return view('Auth.Zone.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);   
        }
    }
}
