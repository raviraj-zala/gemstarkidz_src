<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use Illuminate\Support\Facades\Validator;
use App\Helper\Exceptions;
use File;
use App\Helper\Ajax;
use App\Model\Branch;
use App\Model\Report;
use App\Model\ClassTbl;
use App\Model\Subject;
use App\Model\Student;
use Auth;

class ReportController extends Controller {

    protected $rules = [
        '_token' => 'required',
        'reportType' => 'required'
    ];

    public function index() {
        try {
            if(Auth::user()->Use_Type == "1")
            {
                $data['students'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->orderBy('Rpt_CreatedAt', 'DESC')->paginate(10);
            }
            elseif(Auth::user()->Use_Type == "2")
            {
                $teacher = UserRights::teacher();
                $data['students'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->whereIn('Std_Cla_Id',$teacher['AssignClass'])->orderBy('Rpt_CreatedAt', 'DESC')->paginate(10);   
            }
            elseif(Auth::user()->Use_Type == "5")
            {
                $branch = UserRights::branchAdmin();
                $data['students'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->whereIn('Std_Cla_Id',$branch['branchClass'])->orderBy('Rpt_CreatedAt', 'DESC')->paginate(10);
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $data['CURight'] = UserRights::rights();
            return view('auth.Report.index', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function create() {
        try {
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            if(Auth::user()->Use_Type == "1"){
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
            }
            $data['CURight'] = UserRights::rights();
            return view('auth.Report.create', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function search(Request $request) {
        try {
            return view('auth.Report.search', Ajax::searchReport($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function store(Request $request) {
        try {
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                return back()->with('errors', $validator->errors());
            } else {
                if ($request->reportType === "none") {
                    
                } else if ($request->reportType === "2") {
                    $json_data = array(
                        'student_id' => $request->student_id,
                        'student_gr_no' => $request->student_gr_no,
                        'student_name' => $request->studentName,
                        'student_class' => $request->studentClass,
                        'student_section' => $request->studentSection,
                        'Maths' => [
                            'number1to100' => $request->get('number1to100', 0),
                            'number1to1000' => $request->get('number1to1000', 0),
                            'beforeAfterBeteen' => $request->get('beforeAfterBeteen', 0),
                            'biggestSmallestNumber' => $request->get('biggestSmallestNumber', 0),
                            'expandedForm' => $request->get('expandedForm', 0),
                            'ascDesOrder' => $request->get('ascDesOrder', 0),
                            'greLesThan' => $request->get('greLesThan', 0),
                            'equalNotEqual' => $request->get('equalNotEqual', 0),
                            'montessoriBoardAddition' => $request->get('montessoriBoardAddition', 0),
                            'tallyLinesAddition' => $request->get('tallyLinesAddition', 0),
                            'wordPicturesProblemsAddition' => $request->get('wordPicturesProblemsAddition', 0),
                            'fourDigitAdditionWithout' => $request->get('fourDigitAdditionWithout', 0),
                            'fourDigitAdditionWith' => $request->get('fourDigitAdditionWith', 0),
                            'montessoriBoardSub' => $request->get('montessoriBoardSub', 0),
                            'tallyLinesSub' => $request->get('tallyLinesSub', 0),
                            'wordPicturesProblemsSub' => $request->get('wordPicturesProblemsSub', 0),
                            'fourDigitSubWithout' => $request->get('fourDigitSubWithout', 0),
                            'fourDigitSubWith' => $request->get('fourDigitSubWith', 0),
                            'montessoriBoardMul' => $request->get('montessoriBoardMul', 0),
                            'mulWithPic' => $request->get('mulWithPic', 0),
                            'montessoriBoardDiv' => $request->get('montessoriBoardDiv', 0),
                            'divWithPic' => $request->get('divWithPic', 0),
                            'money' => $request->get('money', 0)
                        ],
                        'english' => [
                            'shChCk' => $request->get('shChCk', 0),
                            'aeOeUe' => $request->get('aeOeUe', 0),
                            'owOuEaIng' => $request->get('owOuEaIng', 0),
                            'oneAndMany' => $request->get('oneAndMany', 0),
                            'aAndAn' => $request->get('aAndAn', 0),
                            'preposition' => $request->get('preposition', 0),
                            'thisAndThat' => $request->get('thisAndThat', 0),
                            'thoseAndThese' => $request->get('thoseAndThese', 0),
                            'yesAndNo' => $request->get('yesAndNo', 0),
                            'articles' => $request->get('articles', 0),
                            'writeAnsWhat' => $request->get('writeAnsWhat', 0),
                            'nouns' => $request->get('nouns', 0),
                            'verbs' => $request->get('verbs', 0),
                            'adverbs' => $request->get('adverbs', 0),
                            'pronouns' => $request->get('pronouns', 0),
                            'adjectives' => $request->get('adjectives', 0),
                            'masculineFeminine' => $request->get('masculineFeminine', 0),
                            'singularPlurals' => $request->get('singularPlurals', 0)
                        ],
                        'sensorial' => [
                            'family' => $request->get('family', 0),
                            'myAmazingBody' => $request->get('myAmazingBody', 0),
                            'myHome' => $request->get('myHome', 0),
                            'mySchool' => $request->get('mySchool', 0),
                            'myNeighbourhood' => $request->get('myNeighbourhood', 0),
                            'typesOfMetals' => $request->get('typesOfMetals', 0),
                            'livingAndNonLiving' => $request->get('livingAndNonLiving', 0),
                            'communityHelpers' => $request->get('communityHelpers', 0),
                            'typeAnimalsBirds' => $request->get('typeAnimalsBirds', 0),
                            'colours' => $request->get('colours', 0),
                            'transport' => $request->get('transport', 0),
                            'emotions' => $request->get('emotions', 0),
                            'goodBadHabits' => $request->get('goodBadHabits', 0),
                            'electricity' => $request->get('electricity', 0),
                            'seasons' => $request->get('seasons', 0),
                            'peopleOfIndia' => $request->get('peopleOfIndia', 0),
                            'goodHabitsSchools' => $request->get('goodHabitsSchools', 0),
                            'festivals' => $request->get('festivals', 0),
                            'sporst' => $request->get('sporst', 0),
                            'insects' => $request->get('insects', 0),
                            'vegetablesFruits' => $request->get('vegetablesFruits', 0),
                            'birds' => $request->get('birds', 0),
                            'nationalSymbols' => $request->get('nationalSymbols', 0)
                        ],
                        'hindi' => [
                            'aH' => $request->get('aH', 0),
                            'letterLBTN' => $request->get('letterLBTN', 0),
                            'letterWord3' => $request->get('letterWord3', 0),
                            'letterWord4' => $request->get('letterWord4', 0),
                            'letterMixedWord' => $request->get('letterMixedWord', 0)
                        ]
                    );
                    $json = json_encode($json_data);

                    $report = new Report;
                    $report->Rpt_Type = $request->reportType;
                    $report->Rpt_Std_Id = $request->student_id;
                    $report->Rpt_date = date("Y-m-d", strtotime(str_replace("/", "-", $request->date)));
                    $report->Rpt_Data = $json;
                    $report->Rpt_CreatedBy = Auth::user()->Use_Id;
                    $report->Rpt_CreatedAt = date("Y-m-d H:i:s");
                    $report->Rpt_UpdatedBy = Auth::user()->Use_Id;
                    $report->Rpt_UpdatedAt = date("Y-m-d H:i:s");
                    if ($report->save()) {
                        return redirect('Report Mgmt.')->with('save', "Report Successfully Insert.");
                    } else {
                        return back()->with('errors', $validator->errors());
                        ;
                    }
                } else {
                    
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    // Edit Form Get Data
    public function edit($id) {
        try {
            $data['report'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')->leftjoin('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->where('Rpt_Id', $id)->first();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $temp = Report::where('Rpt_Id', $id)->select('Rpt_Data')->first();
            $data['json'] = json_decode($temp, true);
            $data['rData'] = $data['json']['Rpt_Data'];
            $data['CURight'] = UserRights::rights();
            return view('auth.Report.edit', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //update record
    public function update(Request $request) {
        try {
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails()) {
                return Response::json(array('error' => $validator->getMessageBag()->toArray()));
            } else {
                if ($request->reportType === "1") {
                    
                } else if ($request->reportType === "2") {
                    $json_data = array(
                        'student_id' => $request->student_id,
                        'student_gr_no' => $request->student_gr_no,
                        'student_name' => $request->studentName,
                        'student_class' => $request->studentClass,
                        'student_section' => $request->studentSection,
                        'Maths' => [
                            'number1to100' => $request->get('number1to100', 0),
                            'number1to1000' => $request->get('number1to1000', 0),
                            'beforeAfterBeteen' => $request->get('beforeAfterBeteen', 0),
                            'biggestSmallestNumber' => $request->get('biggestSmallestNumber', 0),
                            'expandedForm' => $request->get('expandedForm', 0),
                            'ascDesOrder' => $request->get('ascDesOrder', 0),
                            'greLesThan' => $request->get('greLesThan', 0),
                            'equalNotEqual' => $request->get('equalNotEqual', 0),
                            'montessoriBoardAddition' => $request->get('montessoriBoardAddition', 0),
                            'tallyLinesAddition' => $request->get('tallyLinesAddition', 0),
                            'wordPicturesProblemsAddition' => $request->get('wordPicturesProblemsAddition', 0),
                            'fourDigitAdditionWithout' => $request->get('fourDigitAdditionWithout', 0),
                            'fourDigitAdditionWith' => $request->get('fourDigitAdditionWith', 0),
                            'montessoriBoardSub' => $request->get('montessoriBoardSub', 0),
                            'tallyLinesSub' => $request->get('tallyLinesSub', 0),
                            'wordPicturesProblemsSub' => $request->get('wordPicturesProblemsSub', 0),
                            'fourDigitSubWithout' => $request->get('fourDigitSubWithout', 0),
                            'fourDigitSubWith' => $request->get('fourDigitSubWith', 0),
                            'montessoriBoardMul' => $request->get('montessoriBoardMul', 0),
                            'mulWithPic' => $request->get('mulWithPic', 0),
                            'montessoriBoardDiv' => $request->get('montessoriBoardDiv', 0),
                            'divWithPic' => $request->get('divWithPic', 0),
                            'money' => $request->get('money', 0)
                        ],
                        'english' => [
                            'shChCk' => $request->get('shChCk', 0),
                            'aeOeUe' => $request->get('aeOeUe', 0),
                            'owOuEaIng' => $request->get('owOuEaIng', 0),
                            'oneAndMany' => $request->get('oneAndMany', 0),
                            'aAndAn' => $request->get('aAndAn', 0),
                            'preposition' => $request->get('preposition', 0),
                            'thisAndThat' => $request->get('thisAndThat', 0),
                            'thoseAndThese' => $request->get('thoseAndThese', 0),
                            'yesAndNo' => $request->get('yesAndNo', 0),
                            'articles' => $request->get('articles', 0),
                            'writeAnsWhat' => $request->get('writeAnsWhat', 0),
                            'nouns' => $request->get('nouns', 0),
                            'verbs' => $request->get('verbs', 0),
                            'adverbs' => $request->get('adverbs', 0),
                            'pronouns' => $request->get('pronouns', 0),
                            'adjectives' => $request->get('adjectives', 0),
                            'masculineFeminine' => $request->get('masculineFeminine', 0),
                            'singularPlurals' => $request->get('singularPlurals', 0)
                        ],
                        'sensorial' => [
                            'family' => $request->get('family', 0),
                            'myAmazingBody' => $request->get('myAmazingBody', 0),
                            'myHome' => $request->get('myHome', 0),
                            'mySchool' => $request->get('mySchool', 0),
                            'myNeighbourhood' => $request->get('myNeighbourhood', 0),
                            'typesOfMetals' => $request->get('typesOfMetals', 0),
                            'livingAndNonLiving' => $request->get('livingAndNonLiving', 0),
                            'communityHelpers' => $request->get('communityHelpers', 0),
                            'typeAnimalsBirds' => $request->get('typeAnimalsBirds', 0),
                            'colours' => $request->get('colours', 0),
                            'transport' => $request->get('transport', 0),
                            'emotions' => $request->get('emotions', 0),
                            'goodBadHabits' => $request->get('goodBadHabits', 0),
                            'electricity' => $request->get('electricity', 0),
                            'seasons' => $request->get('seasons', 0),
                            'peopleOfIndia' => $request->get('peopleOfIndia', 0),
                            'goodHabitsSchools' => $request->get('goodHabitsSchools', 0),
                            'festivals' => $request->get('festivals', 0),
                            'sporst' => $request->get('sporst', 0),
                            'insects' => $request->get('insects', 0),
                            'vegetablesFruits' => $request->get('vegetablesFruits', 0),
                            'birds' => $request->get('birds', 0),
                            'nationalSymbols' => $request->get('nationalSymbols', 0)
                        ],
                        'hindi' => [
                            'aH' => $request->get('aH', 0),
                            'letterLBTN' => $request->get('letterLBTN', 0),
                            'letterWord3' => $request->get('letterWord3', 0),
                            'letterWord4' => $request->get('letterWord4', 0),
                            'letterMixedWord' => $request->get('letterMixedWord', 0)
                        ]
                    );
                    $json = json_encode($json_data);

                    $report = Report::find($request->id);
                    $report->Rpt_Type = $request->reportType;
                    $report->Rpt_Std_Id = $request->student_id;
                    $report->Rpt_date = date("Y-m-d", strtotime(str_replace("/", "-", $request->date)));
                    $report->Rpt_Data = $json;
                    $report->Rpt_CreatedBy = Auth::user()->Use_Id;
                    $report->Rpt_CreatedAt = date("Y-m-d H:i:s");
                    $report->Rpt_UpdatedBy = Auth::user()->Use_Id;
                    $report->Rpt_UpdatedAt = date("Y-m-d H:i:s");

                    if ($report->update()) {
                        return redirect('Report Mgmt.');
                    } else {
                        return back();
                    }
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function destroy(Request $request) {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) {
                foreach ($ids as $id) {
                    Report::where('Rpt_Id', $id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function getStudentNameOrGr(Request $request) {
        try {
            $searchParentName = $request->get('term');
            $data = Student::select('Std_Gr_No', 'Std_Name')->where('Std_Name', 'LIKE', '%' . $searchParentName . '%')->orwhere('Std_Gr_No', 'LIKE', '%' . $searchParentName . '%')->get();
            $result = array();
            foreach ($data as $value) {
                $result[] = ['value' => $value->Std_Gr_No . " :- " . $value->Std_Name];
            }
            return response()->json($result);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function getStudentData(Request $request) {
        try {
            $studentGrName = trim($request->Std_Name_Gr);
            if (strpos($studentGrName, ":-") == false) {
                return "false";
            }
            $temp = explode(" :- ", $studentGrName);
            $grNo = $temp[0];
            $name = $temp[1];
            $data = Student::where('Std_Gr_No', $grNo)->where('Std_Name', $name)->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->select('Std_Id', 'Std_Gr_No', 'Std_Name', 'Cla_Class', 'Cla_Section')->get()->first();
            if ($data != "")
                return $data;
            else
                return "Student Not Found";
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function getClass(Request $request) {
        try {
            $branch_id = $request->get("bid");
            $data = ClassTbl::where('Cla_Status', 1)->where('Cla_Bra_Id', $branch_id)->distinct()->get(['Cla_Class', 'Cla_Bra_Id']);
            return $data;
        } catch (\Exception $ex) {
            Exceptions::exception($ex);
        }
    }

    public function getMonthYear(Request $request) {
        try {
            $branch_id = $request->bid;
            $temp = explode(",", $request->cls);
            $class_name = $temp[1];
            $section = $request->section;
            $branch_code = Branch::where("Brn_Id", $branch_id)->first()->Brn_Code;
            $data = Subject::where("Sub_Brn_Code", $branch_code)->where("Sub_Class", $class_name)->where('Sub_Section',$section)->distinct()->get(['Sub_Id', 'Sub_Month', 'Sub_Year']);
            if (count($data) > 0)
                return $data;
            else
                return "false";
        } catch (\Exception $ex) {
            Exceptions::exception($ex);
        }
    }

    public function getStudentList(Request $request) {
        try {
            $branch_id = $request->bid;
            $section = $request->section;
            $class = explode(",", $request->cls);
            $class_name = $class[1];
            $subid = $request->subid;
            if($subid!="none"){
            $class_id = ClassTbl::where("Cla_Bra_Id", $branch_id)->where("Cla_Class", $class_name)->where('Cla_Section',$section)->select(['Cla_Id'])->get()->toArray();

            $student_id = Student::where("Std_Cla_Id", $class_id)->get();
            $report = Report::get();

            $monthYear = Subject::where("Sub_Id", $subid)->select('Sub_Month','Sub_Year')->first();
            $month = $monthYear->Sub_Month;
            $year = $monthYear->Sub_Year;
            $yearMonth = array($year,$month);
            $chkDate = implode("-",$yearMonth);
           // $chkDate = date("Y-m");
            $existsStudent = array();
            foreach ($student_id as $std) {
                foreach ($report as $rpt) {
                    if (date("Y-n", strtotime($rpt->Rpt_Month)) == $chkDate && $std->Std_Id == $rpt->Rpt_Std_Id && $rpt->Rpt_Type == "1") {
                        $existsStudent[] = $std->Std_Id;
                    }
                }
            }

            $data["student_data"] = Student::whereIn("Std_Cla_Id", $class_id)->whereNotIn("Std_Id", $existsStudent)->orderBy("Std_Name")
                            ->join('class_tbl', 'class_tbl.Cla_Id', '=', 'student_tbl.Std_Cla_Id')->get();

            $temp = Subject::where("Sub_Id", $subid)->select("Sub_Json_Data")->first();

            $data["rData"] = $temp;
            $data["subid"] = $subid;
                return view("auth.Report.studentlist")->with($data);
            }else{
                $data['result'] = false;
                return view("auth.Report.studentlist")->with($data);
            }
        } catch (\Exception $ex) {
            Exceptions::exception($ex);
        }
    }

    public function storePorgress(Request $request) {
        try {
            $branch_id = $request->branch_id;
            $temp = explode(",", $request->class);
            $class_name = $temp[1];
            $month_year = $request->month_year;
            $subjectId = $request->subid;
            $stdChecked = $request->std_check;

            $class_id = ClassTbl::where("Cla_Bra_Id", $branch_id)->where("Cla_Class", $class_name)->select(['Cla_Id'])->get()->toArray();
            $student_data = Student::whereIn("Std_Id", $stdChecked)->join("class_tbl", "class_tbl.Cla_Id", "=", "student_tbl.Std_Cla_Id")->get();

            $branch_code = Branch::where("Brn_Id", $branch_id)->first()->Brn_Code;
            $filePath = Subject::where("Sub_Id", $subjectId)->select(["Sub_Json_Data"])->first()->Sub_Json_Data;
            $json_data = json_decode($filePath, true);

            $monthYear = Subject::where("Sub_Id", $subjectId)->select('Sub_Month','Sub_Year')->first();
            $month = $monthYear->Sub_Month;
            $year = $monthYear->Sub_Year;
            $yearMonth = array($year,$month);
            $mon_Year = implode("-",$yearMonth);
            
            foreach ($student_data as $student) {
                $mainData = array();
                $mainData["student_id"] = $student->Std_Id;
                $mainData["student_gr_no"] = $student->Std_Gr_No;
                $mainData["student_name"] = $student->Std_Name;
                $mainData["student_class"] = $student->Cla_Class;
                $mainData["student_section"] = $student->Cla_Section;
                $mainData["sub_id"] = $subjectId;

                $i = 1;
                $tmp = 0;
                $mainData["subject"] = array();
                $abc = "";
                $main = array();
                $xyz = array();
                foreach ($json_data["subject"] as $subject) {
                    $mainData["subject"]["subject_title"] = $subject["title"];
                    foreach ($subject["criteria"] as $key => $value) {
                        $mainData["subject"][$key] = $request->get($key . "-" . $i . "-" . $student->Std_Id, "0");
                    }
                    array_push($main,$mainData["subject"]);
                    $i++;
                    $tmp++;
                }
                $mainData["subject"] = $main;
                $create_json = json_encode($mainData);
                
                $report = new Report;
                $report->Rpt_Type = "1";
                $report->Rpt_Std_Id = $student->Std_Id;
                $report->Rpt_date = date("Y-m-d H:i:s");
                $report->Rpt_Data = $create_json;
                $report->Rpt_Sub_Id = $subjectId;
                $report->Rpt_Month = $mon_Year;
                $report->Rpt_CreatedBy = Auth::user()->Use_Id;
                $report->Rpt_CreatedAt = date("Y-m-d H:i:s");
                $report->Rpt_UpdatedBy = Auth::user()->Use_Id;
                $report->Rpt_UpdatedAt = date("Y-m-d H:i:s");
                if ($report->save()) {
                    
                } else {
                    return back()->with('errors', $validator->errors());
                    ;
                }
            }
            return redirect('Report Mgmt.')->with('save', "Report Successfully Insert.");
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function editPorgress($id) {
        try {
            $data['report'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')->leftjoin('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->where('Rpt_Id', $id)->first();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $temp = Report::where('Rpt_Id', $id)->select('Rpt_Data')->first();
            $data['json'] = json_decode($temp, true);
            $data['rData'] = $data['json']['Rpt_Data'];
            $data['CURight'] = UserRights::rights();
            $data['json_data'] = json_decode($data['rData'], true);
            $data['sub_id'] = $data['json_data']['sub_id'];
            $temp = Subject::where("Sub_Id", $data['sub_id'])->select("Sub_Json_Data")->first();
            $data['sub_data'] = json_decode($temp['Sub_Json_Data'], true);
            $data['reportId'] = $id;
//            dd($data);
            return view('auth.Report.editProgress', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function updateProgress(Request $request) {
        try {
            
            $subjectId = $request->subid;            
            $filePath = Subject::where("Sub_Id", $subjectId)->select(["Sub_Json_Data"])->first()->Sub_Json_Data;
            $json_data = json_decode($filePath, true);

            
                $mainData = array();
                $mainData["student_id"] = $request->studentId;
                $mainData["student_gr_no"] = $request->studentGrNo;
                $mainData["student_name"] = $request->studentName;
                $mainData["student_class"] = $request->studentClass;
                $mainData["student_section"] = $request->studentSection;
                $mainData["sub_id"] = $subjectId;

                $i = 1;
                $tmp = 0;
                $mainData["subject"] = array();
                $abc = "";
                $main = array();
                $xyz = array();
                foreach ($json_data["subject"] as $subject) {
                    $mainData["subject"]["subject_title"] = $subject["title"];
                    foreach ($subject["criteria"] as $key => $value) {
                        $mainData["subject"][$key] = $request->get($key . "-" .$i, "0");
                    }
                    array_push($main,$mainData["subject"]);
                    $i++;
                    $tmp++;
                }
                $mainData["subject"] = $main;
                $create_json = json_encode($mainData);
                $report = Report::find($request->reportId);
                $report->Rpt_Data = $create_json;
                $report->Rpt_UpdatedBy = Auth::user()->Use_Id;
                $report->Rpt_UpdatedAt = date("Y-m-d H:i:s");
                if ($report->update()) {
                    
                } else {
                    return back()->with('errors', $validator->errors());
                    ;
                }
                return redirect('Report Mgmt.')->with('save', "Report Successfully update.");
        } catch (\Exception $ex) {
            Exceptions::exception($ex);
        }
    }

}
