<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Logmodel;

class LogController extends Controller
{
    public static function storeLog($logMessage)
    {
    	$log = new Logmodel;
    	$log->Log_Use_Id = Auth::user()->id;
    	$log->Log_Ip_Address = $this->server->get('REMOTE_ADDR');
    	$log->Log_Message = $logMessage;
    	$log->Log_CreateAt = date("Y-m-d H:i:s");
    	$log->save();
    }
}
