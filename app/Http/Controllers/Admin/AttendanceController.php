<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Helper\Ajax;
use App\Model\Zone;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Student;
use App\Model\Module;
use App\Model\Homework;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use App\Model\Report;
use App\Model\Subject;
use App\Model\Attendence;
use URL;

class AttendanceController extends Controller
{
    protected $rules = ([
            'branch_id' => 'required'
        ]);

    //Branch Information
    public function index()
    {
        try {
             $data['attendance'] = Attendence::join('class_tbl','attendance_tbl.Att_Cla_Id','=','class_tbl.Cla_Bra_Id')
                            ->join('branch_tbl','attendance_tbl.Att_Cla_Id','=','branch_tbl.Brn_Id')
                              ->select('attendance_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section','branch_tbl.Brn_Name as branch_name')->orderBy('Att_CreatedAt')->paginate(10);
            $data['i'] = 1;
            $data['homework'] = Homework::orderBy('Hmw_Id','DESC')->paginate(10);
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.Attendance.index',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Branch Search
    public function search(Request $request)
    {
        try {
            return view('auth.Attendance.search',Ajax::searchBranch($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Branch Creation Form
    public function create()
    {
        try{
            
            $data['zone'] = Zone::where('Zon_Status',1)->orderBy('Zon_Name')->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
             if(Auth::user()->Use_Type == "1"){
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
            }
            // $data['branch'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
            //                 ->select('class_tbl.*','branch_tbl.*')
            //                 ->where('class_tbl.Cla_Status','=',1)
            //                 ->where('branch_tbl.Brn_Status','=',1)
            //                 ->orderBy('class_tbl.Cla_Id')
            //                 ->get();
            $data['student_data'] = array();
            $data['students'] = ClassTbl::join('student_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                            ->select('class_tbl.*','student_tbl.*')
                            ->where('class_tbl.Cla_Status','=',1)
                            ->where('student_tbl.Std_Status','=',1)
                            ->orderBy('class_tbl.Cla_Id')
                            ->get();
            $data['CURight']=UserRights::rights();
            // $data['student_data'] = Student::get();

           //dd($data);
            return view('auth.Attendance.create')->with($data);
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    //Branch Creation
    public function store(Request $request)
    {
        try {
            
                if($request->has('image')){
                        $image = $request->file('image');
                        $imageName = time().$image->getClientOriginalName();
                        $image->move(public_path('images/homework'),$imageName);
                    }else{
                        $imageName = "";
                    }
                    if($request->class_id)
                    {
                       foreach($request->class_id as $classid)
                       {

                        $branch = new Homework;
                        $branch->Hmw_Cla_Id = $classid;
                        $branch->Hmw_Subject = $request->subject;
                        $branch->Hmw_Page_No = $request->pageno;
                        $branch->Hmw_Desscription = $request->description;
                        $branch->Hmw_Chapter = $request->chapter;
                        $branch->Hmw_Date = $request->date;
                        $branch->Hmw_File = $imageName;
                        $branch->Hmw_CreatedBy = Auth::user()->Use_Id;
                        $branch->Hmw_CreatedAt = date('Y-m-d H:i:s');
                        $branch->Hmw_UpdatedBy = Auth::user()->Use_Id;
                        $branch->Hmw_UpdatedAt = date('Y-m-d H:i:s');
                        $branch->save(); 
                    }

                        $class_id = Homework::whereIn('Hmw_Cla_Id',$request->class_id)->select('Hmw_Cla_Id')->get()->toArray();
                        $student = Student::whereIn('Std_Cla_Id',$class_id)->select('Std_Parent_Id')->get()->toArray();
                        $parenttoken = User::whereIn('Use_Id',$student)->select('Use_Token')->get();
                        
                        $homeworkName = $request->description;


                        if(count($parenttoken)){
                            foreach($parenttoken as $key => $tokenid)
                            {
                                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $tokenList[]=$tokenid['Use_Token'];
                        

                        $notification = [
                            'title'=> 'New Homework',
                            'body' => 'Homework :"'.$homeworkName.'"',
                            'sound' => true,
                        ];
                        
                        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

                        $fcmNotification = [
                            'registration_ids' => $tokenList, //multple token array
                            //'to'  => $$tokenList, //single token
                            'notification' => $notification,
                            'data' => $extraNotificationData
                        ];

                        $headers = [
                            'Authorization: key=AAAAbFypH7M:APA91bFRq_uU5F9ANeilCqIMh-UjkIVtEqasvqJnJ-xiJzMDgX52T8uytdSvi0L8uwxF004Kw5w3R9jb0v395rAdikVEj9pAW3MoflcFDhXdvSQbhP5TIHJMErrYLfruqqQnizhbE36c',
                            'Content-Type: application/json'
                        ];


                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                               
                            }
                        }
                    
                if($branch)
                {
                    return redirect('Syllabus Assignment Mgmt');
                }else{
                    return redirect('create_assignment')->with('error','Assignment Creation Fail');
                } 
            }

        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Branch Edit Form
    public function edit($id)
    {
        try{    
            $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('class_tbl.*','branch_tbl.*')
                            ->where('class_tbl.Cla_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('class_tbl.Cla_Id')
                            ->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.Attendance.edit',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Branch Data Update
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),$this->rules,['mobile.numeric' => 'Mobile number must be digit',
                'mobile.regex' => 'Mobile number must be 10 digit']);

            if($validator->fails()){
                return back()->with('errors',$validator->errors())->withInput();
            }                
                $branch['Brn_Zon_Id'] = $request->zone;
                $branch['Brn_Name'] = $request->name;
                $branch['Brn_Code'] = $request->code;
                $branch['Brn_Address'] = $request->address;
                $branch['Brn_Email'] = $request->email;
                if($request->has('image')){
                    $image = $request->file('image');
                    $imageName = time().$image->getClientOriginalName();
                    $image->move(public_path('images/branch'),$imageName);
                    $branch['Brn_Image'] = $imageName;
                }
                $branch['Brn_Mobile_No'] = $request->mobile;
                $branch['Brn_Status'] = $request->status;
                $branch['Brn_UpdatedBy'] = Auth::user()->Use_Id;
                $branch['Brn_UpdatedAt'] = date('Y-m-d H:i:s');

                if(Branch::where('Brn_Id',$request->id)->update($branch)){
                    return redirect('Syllabus Assignment Mgmt');
                }else{
                    return redirect('edit_branch/{id}')->with('Assignment Updation Fail');
                }
        } catch (\Exception $e) {
           Exceptions::exception($e)    ;
        }
    }

    //Branch Deletion
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    Branch::where('Brn_Id',$id)->delete();
                    ClassTbl::where('Cla_Bra_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::branchStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
                              ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
                              ->paginate(10,['*'],'page',$whatIWant);
            else
                $data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
                              ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
                              ->paginate(10);
            return view('Auth.Attendance.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getStudentNameOrGr(Request $request) {
        try {
            $searchParentName = $request->get('term');
            $data = Student::select('Std_Gr_No', 'Std_Name')->where('Std_Name', 'LIKE', '%' . $searchParentName . '%')->orwhere('Std_Gr_No', 'LIKE', '%' . $searchParentName . '%')->get();
            $result = array();
            foreach ($data as $value) {
                $result[] = ['value' => $value->Std_Gr_No . " :- " . $value->Std_Name];
            }
            return response()->json($result);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getStudentData(Request $request) {
        try {
            $studentGrName = trim($request->Std_Name_Gr);
            if (strpos($studentGrName, ":-") == false) {
                return "false";
            }
            $temp = explode(" :- ", $studentGrName);
            $grNo = $temp[0];
            $name = $temp[1];
            $data = Student::where('Std_Gr_No', $grNo)->where('Std_Name', $name)->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')->select('Std_Id', 'Std_Gr_No', 'Std_Name', 'Cla_Class', 'Cla_Section')->get()->first();
            if ($data != "")
                return $data;
            else
                return "Student Not Found";
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
     public function getClass(Request $request) {
        try {
            $branch_id = $request->get("bid");
            $data = ClassTbl::where('Cla_Status', 1)->where('Cla_Bra_Id', $branch_id)->distinct()->get(['Cla_Class', 'Cla_Bra_Id']);
            return $data;
        } catch (\Exception $ex) {
            Exceptions::exception($ex);
        }
    }
     public function getStudentList(Request $request) {
        try {
            $branch_id = $request->bid;
            $section = $request->section;
            $class = explode(",", $request->cls);
            $class_name = $class[1];
            if($class_name!="none"){
            $class_id = ClassTbl::where("Cla_Bra_Id", $branch_id)->where("Cla_Class", $class_name)->where('Cla_Section',$section)->select(['Cla_Id'])->first();
            $class_data = $class_id->Cla_Id;
            $data['student_data'] = Student::where('Std_Cla_Id', $class_data)->get();
                return view("auth.Attendance.studentlist")->with($data);
            }else{
                $data['result'] = false;
                return view("auth.Attendance.studentlist")->with($data);
            }

        } catch (\Exception $ex) {
            log::error($ex);
            // Exceptions::exception($ex);
        }
    }
    
    public function saveAttendance(Request $request)
    {
        try{
            // dd($request->all());
            $Att_Cla_Id = $request->Att_Cla_Id;
            $present_id = $request->present_id;
            $absent_id = $request->absent_id;
            // dd($absent_id);
            if(count($present_id) != 0){
                foreach ($present_id as $present_id) {
                   $attendance = new Attendence;
                   $attendance->Att_Stu_Id = $present_id;
                   $attendance->Att_Cla_Id = $Att_Cla_Id;
                   $attendance->Att_Pre_Abs = "1";
                   $attendance->Att_Date = date("Y-m-d");
                   $attendance->Att_CreatedBy = Auth::user()->Use_Id;
                   $attendance->save();
                }
            }
            if(count($absent_id) != 0) {
                foreach ($absent_id as $absent_id) {
                   $attendance = new Attendence;
                   $attendance->Att_Stu_Id = $absent_id;
                   $attendance->Att_Cla_Id = $Att_Cla_Id;
                   $attendance->Att_Pre_Abs = "0";
                   $attendance->Att_Date = date("Y-m-d");
                   $attendance->Att_CreatedBy = Auth::user()->Use_Id;
                   $attendance->save();
                }
            }
            return redirect('create_Attendance');
        } catch(Exception $e){
            log::error($e);
        }
    }
   /* public function editAttendance($id)
    {
         if(Auth::user()->Use_Type == "1"){
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
            }
        $data['attendance'] = Attendence::join('class_tbl','attendance_tbl.Att_Cla_Id','=','class_tbl.Cla_Bra_Id')
                            ->join('branch_tbl','attendance_tbl.Att_Cla_Id','=','branch_tbl.Brn_Id')
                              ->select('attendance_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section','branch_tbl.Brn_Name as branch_name')->first();
         $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
         $data['CURight'] = UserRights::rights();
        return view('auth.Attendance.edit_Attendance')->with($data);
    }*/
}
