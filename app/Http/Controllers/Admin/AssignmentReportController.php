<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Helper\Ajax;
use App\Model\Zone;
use App\Helper\Notification;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Helper\SMSHelper;
use App\Model\Student;
use App\Model\Module;
use App\Model\Homework;
use App\Model\Homework_view;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use URL;

class AssignmentReportController extends Controller
{
   
    //Branch Information
    public function index()
    {
    	try {
    		$data['i'] = 1;
            if(Auth::user()->Use_Type == "1"){
                $data['homework'] = Homework::orderBy('Hmw_Id','DESC')
			                ->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')
			                ->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')
			                ->leftjoin('branch_tbl','class_tbl.Cla_Section','=','branch_tbl.Brn_Id')
			                ->select(['homework_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section','user_tbl.Use_Name','branch_tbl.Brn_Name','branch_tbl.Brn_Id'])
			                ->get();
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get(); 
            }
            elseif(Auth::user()->Use_Type == "2")
            {
                $teacher = UserRights::teacher();
                $data['homework'] = Homework::whereIn('Hmw_Cla_Id',$teacher['AssignClass'])->orderBy('Hmw_Id','DESC')->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')->select(['homework_tbl.*','class_tbl.Cla_Class','user_tbl.Use_Name'])->paginate(10);
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get(); 
            }
            elseif(Auth::user()->Use_Type == "5")
            {
                $branch = UserRights::branchAdmin();
                $data['homework'] = Homework::whereIn('Hmw_Cla_Id',$branch['branchClass'])->orderBy('Hmw_Id','DESC')->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')->select(['homework_tbl.*','class_tbl.Cla_Class','user_tbl.Use_Name'])->paginate(10);
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get(); 
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.record.assignment_report',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Search
    public function search(Request $request)
    {
        try {
        	$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get(); 
            return view('auth.record.assignment_report',Ajax::searchAssignmentReport($request))->with($data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    
}
