<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use App\User;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Module;
use App\Model\Student;
use App\Helper\Ajax;
use App\Model\Event;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Helper\SMSHelper;
use URL;

class EventReportController extends Controller
{
     public function index()
    {
    	try {
			$data['i'] = 1;
			if(Auth::user()->Use_Type=="1")
            {
                $data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
                        ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
                        ->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')
                        ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.*')
                        ->orderBy('Eve_Id','DESC')
                        ->get();
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();        
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.event_report',$data);
            }
            if(Auth::user()->Use_Type=="5")
            {
            	$branch = Admin_assign_branch::where("Aab_Use_Id",Auth::user()->Use_Id)->select("Aab_Brn_Id")->get()->toArray();
                $data['event'] = Event::whereIn("Eve_Brn_Id",$branch)->join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.*')->orderBy('Eve_Id','DESC')->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.event_report',$data);
            }
            if(Auth::user()->Use_Type=="2"){
                $teacherBrnId = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->select("Cla_Bra_Id")->first()->Cla_Bra_Id;
                // $teacherBrnAllClass = ClassTbl::where("Cla_Bra_Id",$teacherBrnId)->select("Cla_Id")->get()->toArray();

                $teacherClassIds = UserRights::teacherApi(Auth::user()->Use_Id,"AssignClass");

				$tagList =  Event::whereIn('Eve_Cla_Id',$teacherClassIds)->where('Eve_Brn_Id',$teacherBrnId)->select('Eve_Id')->groupBy('Eve_Unique_Id')->get();
                $data['event'] = Event::leftjoin('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
            			->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')
                		->leftjoin('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
                		->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.Brn_Name')
                		// ->where('Eve_CreatedBy',Auth::user()->Use_Id)
                		// ->whereIn("Eve_Cla_Id",$teacherBrnAllClass)
                		->whereIn('event_tbl.Eve_Id',$tagList)
                		->orderBy('Eve_Id','DESC')
                		->paginate(10);

	            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	            $data['CURight']=UserRights::rights();
	    		return view('auth.record.event_report',$data);
	    	 }
		} catch (\Exception $e) {	
			Exceptions::exception($e);
		}
    }

    //Event Search
    public function search(Request $request)
    {
    	try {
            $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
    		return view('auth.record.event_report',Ajax::searchEventReport($request))->with($data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
