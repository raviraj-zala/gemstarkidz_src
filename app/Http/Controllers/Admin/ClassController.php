<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Module;
use App\Model\Event;
use App\Helper\Ajax;
use App\Helper\UserRights;
use App\Helper\Exceptions;
use URL;

class ClassController extends Controller
{
	protected $rules = ([
			'branch' => 'required',
			'class' => 'required',
			'section' => 'required'
		]);

    //Class Information
	public function index()
	{
		try {
			$data['i'] = 1;
			if(Auth::user()->Use_Type == "1"){
				$data['class'] = ClassTbl::join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
							->select('class_tbl.*','branch_tbl.Brn_Name')
							->orderBy('Cla_Id','DESC')
							->paginate(10);
			}elseif(Auth::user()->Use_Type == "2"){
				$teacher = UserRights::teacher();
				$data['class'] = ClassTbl::join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
							->whereIn("Cla_Id",$teacher["branchClass"])
							->select('class_tbl.*','branch_tbl.Brn_Name')
							->orderBy('Cla_Id','DESC')
							->paginate(10);
			}elseif(Auth::user()->Use_Type == "5"){
				$branch = UserRights::branchAdmin();
				$data['class'] = ClassTbl::join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
							->whereIn("Cla_Id",$branch["branchClass"])
							->select('class_tbl.*','branch_tbl.Brn_Name')
							->orderBy('Cla_Id','DESC')
							->paginate(10);
			}
	        $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	        $data['CURight']=UserRights::rights();
			return view('auth.Class.index',$data);
		}catch (\Exception $e) {	
			Exceptions::exception($e);
		}
	}

	//Class Search
	public function search(Request $request)
	{
		try {
			return view('auth.Class.search',Ajax::searchClass($request));
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Class Creation Form
	public function create()
	{
		try {
			if(Auth::user()->Use_Type == "1"){
				$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
			}elseif(Auth::user()->Use_Type == "2"){
				$teacher = UserRights::teacher();
				$data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
			}elseif(Auth::user()->Use_Type == "5"){
				$branch = UserRights::branchAdmin();
				$data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
			}
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
			$data['CURight']=UserRights::rights();
			return view('auth.Class.create',$data);
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Class Creation
	public function store(Request $request)
	{
		try {

			$validator = Validator::make($request->all(),$this->rules);

			if($validator->fails()){
				return Response::json(array('error' => $validator->getMessageBag()->toArray()));
			}else{
				if($request->branch=="none"){
					return back()->with('branch',"Please Select Branch..")->withInput();
				}
				$class = new ClassTbl;
				$class->Cla_Bra_Id = $request->branch;
				$class->Cla_Class = $request->class;
				$class->Cla_Section = $request->section;
				$class->Cla_Status = $request->status;
				$class->Cla_CreatedBy = Auth::user()->Use_Id;
				$class->Cla_CreatedAt = date('Y-m-d H:i:s');
				$class->Cla_UpdatedBy = Auth::user()->Use_Id;
				$class->Cla_UpdatedAt = date('Y-m-d H:i:s');

				if($class->save()){
					return redirect('Class Mgmt.');
				}else{
					return redirect('create_class')->with('error','Class Creation Fail');
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Class Edit Form
	public function edit($id)
	{
		try{	
    		if(Auth::user()->Use_Type == "1"){
				$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
			}elseif(Auth::user()->Use_Type == "2"){
				$teacher = UserRights::teacher();
				$data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
			}elseif(Auth::user()->Use_Type == "5"){
				$branch = UserRights::branchAdmin();
				$data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
			}
	    	$data['class'] = ClassTbl::where('Cla_Id',$id)->first();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
	    	return view('auth.Class.edit',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
	}

	//Class Updation
	public function update(Request $request)
	{
		try {

			$validator = Validator::make($request->all(),$this->rules);

			if($validator->fails()){
				return Response::json(array('error' => $validator->getMessageBag()->toArray()));
			}else{
				$class['Cla_Bra_Id'] = $request->branch;
				$class['Cla_Class'] = $request->class;
				$class['Cla_Section'] = $request->section;
				$class['Cla_Status'] = $request->status;
				$class['Cla_UpdatedBy'] = Auth::user()->Use_Id;
				$class['Cla_UpdatedAt'] = date('Y-m-d H:i:s');

				if(ClassTbl::where('Cla_Id',$request->id)->update($class)){
					return redirect('Class Mgmt.');
				}else{
					return redirect('edit_class/{id}')->with('Class Updation Fail');
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Class Deletion 
	public function destroy(Request $request)
	{
		try {
    		$all_data = $request->except('_token');

    		foreach ($all_data as $ids) 
    		{
    			foreach($ids as $id) 
				{
					ClassTbl::where('Cla_Id',$id)->delete();
					Event::where('Eve_Cla_Id',$id)->delete();
				}
    		}
    		return redirect()->back();
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
	}
	public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::classStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['class'] = ClassTbl::paginate(10,['*'],'page',$whatIWant);
            else
                $data['class'] = ClassTbl::paginate(10);
            return view('Auth.Class.active',$data);
        } catch (\Exception $e) {
        	Exceptions::exception($e);
        }
    }
}
