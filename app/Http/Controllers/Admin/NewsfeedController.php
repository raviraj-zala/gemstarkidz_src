<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Newsfeed;
use App\Model\Newsfeedinfo;
use App\Model\Newsfeedtag;
use App\Model\Branch;
use App\User;
use Validator;
use Auth;
use DB;

class NewsfeedController extends Controller
{
    public function index()
    {
    	try {
    		if(Auth::user()->Use_Type=="1"){
                $data['newsfeed'] = Newsfeed::leftjoin('branch_tbl','branch_tbl.Brn_Id','=','newsfeed.Nfd_Brn_Id')
                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                            ->select('newsfeed.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                            ->orderBy('Nfd_Id','DESC')
                            ->paginate(10);
    		}else if(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $tagList =  Newsfeedtag::whereIn('Nft_Cla_Id',$teacher["AssignClass"])->select('Nft_Nfd_Unique_Id')->groupBy('Nft_Nfd_Unique_Id')->get();
                $data['newsfeed'] = Newsfeed::
                            whereIn('Nfd_Unique_Id',$tagList)
                            ->where('Nfd_Brn_Id',$teacher['branchAccess'])
                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                            ->select('newsfeed.*','user_tbl.Use_Name')
                            ->orderBy('Nfd_Id','DESC')
                            ->paginate(10);
    		}else if(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
    			$data['newsfeed'] = Newsfeed::
                            whereIn('Nfd_Brn_Id',$branch['branchAccess'])
                            ->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','newsfeed.Nfd_Brn_Id')
                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
                            ->select('newsfeed.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                            ->orderBy('Nfd_Id','DESC')
                            ->paginate(10);
    		}
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.newsfeed.index')->with($data);
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function create()
    {
    	try {
    		if(Auth::user()->Use_Type == "1"){
    			$data['branch'] = Branch::leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
    		}elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['students'] = Student::wherein('Std_Cla_Id',$teacher['AssignClass'])->get();
    			$data['branch'] = Branch::whereIn('Cla_Id',$teacher['AssignClass'])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
    		}elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
    			$data['branch'] = Branch::whereIn('Cla_Bra_Id',$branch['branchAccess'])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
    		}
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.newsfeed.create')->with($data);
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function save(Request $request)
    {
    	try {
    		$rules = [
    			'title' => 'required',
    			'description' => 'required',
    			'imageOrVideo' => 'max:25600'
    		];
    		$customeMessage = [
    			'title.required' => 'Please enter title',
    			'description.required' => 'Please enter description.',
    			'imageOrVideo.mimes' => 'Invalid image or video type.',
    			'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                return back()->with('errors',$validator->errors())->withInput();
            }else{
            	if($request->hasFile('imageOrVideo'))
            	{
            		$imagesOrVideo = $request->file('imageOrVideo');
            		$image_count = 0;
            		$error_size = false;
            		foreach ($imagesOrVideo as $image)
                	{
                		if($image->getClientSize() > 26214400)
	                    {
	                        $error_size = true;
	                    }
	                    if($image_count >= 50)
	                    {
	                       return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 50 Image');
	                       break;  
	                    }
                	}
                	if($error_size == true)
	                {
	                   return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');
	                }else{
	                	$i = 0;
	                	foreach($imagesOrVideo as $file)
	                	{
	                		$filename = $file->getClientOriginalName();
                        	$extension = $file->getClientOriginalExtension();
                        	$mime = $file->getMimeType();
	                        if(strstr($mime, "video/")){
	                            $mediaType = "2";
	                        }else if(strstr($mime, "image/")){
	                            $mediaType = "1";
	                        }
	                        $picture[] = ['path' => $i.time().".".$extension,
	                                            'type'=>$mediaType];
	                        $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
							$i++;
                    	}
                    }
            	}else{
            		$picture = array();
            	}
            	$title = trim($request->title);
            	$description = trim($request->description);
                if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5"){
            	   $status = trim($request->status);
                }else if(Auth::user()->Use_Type=="2"){
                    $status = "0";
                }
                $user_type = Auth::user()->Use_Type;
            	$tag_type = $request->tag_type;
            	$created_at = date('Y-m-d H:i:s');
            	$created_by = Auth::user()->Use_Id;
                $unique_id = "NFD".time();

                if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
                {
                    $class_ids = $request->class_id;
                    $branch_ids = ClassTbl::whereIn('Cla_Id',$class_ids)->select('Cla_Bra_Id')->distinct()->get();

                    foreach ($branch_ids as $branch) {
                        $newsfeed = New Newsfeed;
                        $newsfeed->Nfd_Unique_Id = $unique_id;
                        $newsfeed->Nfd_Brn_Id = $branch->Cla_Bra_Id;
                        $newsfeed->Nfd_Title = $title;
                        $newsfeed->Nfd_Description = $description;
                        $newsfeed->Nfd_Status = $status;
                        $newsfeed->Nfd_User_Type = $user_type;
                        $newsfeed->Nfd_Tag_Type = $tag_type;
                        $newsfeed->Nfd_CreatedAt = $created_at;
                        $newsfeed->Nfd_CreatedBy = $created_by;
                        $newsfeed->save();
                    }
                }elseif(Auth::user()->Use_Type == "2" ){
                    $teacher = UserRights::teacher();
                    $newsfeed = New Newsfeed;
                    $newsfeed->Nfd_Unique_Id = $unique_id;
                    $newsfeed->Nfd_Brn_Id = $teacher['branchAccess'];
                    $newsfeed->Nfd_Title = $title;
                    $newsfeed->Nfd_Description = $description;
                    $newsfeed->Nfd_Status = $status;
                    $newsfeed->Nfd_User_Type = $user_type;
                    $newsfeed->Nfd_Tag_Type = $tag_type;
                    $newsfeed->Nfd_CreatedAt = $created_at;
                    $newsfeed->Nfd_CreatedBy = $created_by;
                    $newsfeed->save();
                }

            	if($newsfeed){ //->save()	
            		$nfd_id = $newsfeed->Nfd_Id;
            		if(count($picture)!=0){
	            		foreach ($picture as $pic) {
		            		$newsfeedInfo = New Newsfeedinfo;	
		            		$newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
		            		$newsfeedInfo->Nfi_Path = $pic['path'];
		            		$newsfeedInfo->Nfi_Type = $pic['type'];
		            		$newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
		            		$newsfeedInfo->save();
	            		}
            		}
	    			if(Auth::user()->Use_Type=="1"){
	    				$class_ids = $request->class_id;
	    				$student_list = Student::whereIn('Std_Cla_Id',$class_ids)->get();
	    				foreach ($student_list as $student) {
	    					$branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
	    					$newsfeedTag = New Newsfeedtag;
	    					$newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
	    					$newsfeedTag->Nft_Std_Id = $student->Std_Id;
	    					$newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
	    					$newsfeedTag->Nft_Brn_Id = $branch_id;
	    					$newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
	    					$newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
	    					$newsfeedTag->save();
	    				}
	    				return redirect('Newsfeed Mgmt.');
	    			}elseif(Auth::user()->Use_Type=="2"){
                        $tag_type = $request->tag_type;
                        if($tag_type == "class"){
                            $class_ids = $request->class_id;
                            $student_list = Student::whereIn('Std_Cla_Id',$class_ids)->get();
                            foreach ($student_list as $student) {
                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                            $newsfeedTag = New Newsfeedtag;
                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                            $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                            $newsfeedTag->Nft_Brn_Id = $branch_id;
                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedTag->save();
                            }
                            return redirect('Newsfeed Mgmt.');
                        }elseif($tag_type == "student"){
                            $student_ids = $request->student_id;
                            $student_list = Student::whereIn('Std_Id',$student_ids)->get();
                            foreach ($student_list as $student) {
                                $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                                $newsfeedTag = New Newsfeedtag;
                                $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                                $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                                $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                                $newsfeedTag->Nft_Brn_Id = $branch_id;
                                $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                                $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                                $newsfeedTag->save();
                            }
                            return redirect('Newsfeed Mgmt.');
                        }
	    			}elseif(Auth::user()->Use_Type=="5"){
                        $class_ids = $request->class_id;
                        $student_list = Student::whereIn('Std_Cla_Id',$class_ids)->get();
                        foreach ($student_list as $student) {
                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                            $newsfeedTag = New Newsfeedtag;
                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                            $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                            $newsfeedTag->Nft_Brn_Id = $branch_id;
                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedTag->save();
                        }
                        return redirect('Newsfeed Mgmt.');
	    			}
            	}else{
            		return back()->withInput();
            	}
			}
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function edit($unique_id)
    {
        try {
            $data['newsfeed'] = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
            if($data['newsfeed']->Nfd_User_Type == "2" )
            {
                $data['created_user_type'] = "teacher";
                $teacherAssignClass = UserRights::teacherApi($data['newsfeed']->Nfd_CreatedBy,'AssignClass');
                if($data['newsfeed']->Nfd_Tag_Type == "class"){
                    $data['tag_type'] = "class";
                    $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
                }else if($data['newsfeed']->Nfd_Tag_Type == "student"){
                    $data['tag_type'] = "student";
                    $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Std_Id')->groupBy('Nft_Std_Id')->get();
                }
                $data['branch'] = Branch::whereIn('Cla_Id',$teacherAssignClass)->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['students'] = Student::wherein('Std_Cla_Id',$teacherAssignClass)->get();
            }elseif($data['newsfeed']->Nfd_User_Type == "5"){
                $data['created_user_type'] = "branchAdmin";
                $branchAccess = UserRights::branchAdminApi($data['newsfeed']->Nfd_CreatedBy,'branchAccess');
                $data['branch'] = Branch::whereIn('Cla_Bra_Id',$branchAccess)->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
            }else if($data['newsfeed']->Nfd_User_Type == "1"){
                $data['created_user_type'] = "admin";
                $data['branch'] = Branch::leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['newsfeed_tag'] = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->select('Nft_Cla_Id')->groupBy('Nft_Cla_Id')->get();
            }
            $data['newsfeed_info'] = Newsfeedinfo::where('Nfi_Nfd_Unique_Id',$unique_id)->get();

            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            // dd($data);
            return view('auth.newsfeed.edit')->with($data);
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
    public function update(Request $request)
    {
        try {
            $rules = [
                'title' => 'required',
                'description' => 'required',
                'imageOrVideo' => 'max:25600'
            ];
            $customeMessage = [
                'title.required' => 'Please enter title',
                'description.required' => 'Please enter description.',
                'imageOrVideo.mimes' => 'Invalid image or video type.',
                'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                return back()->with('errors',$validator->errors())->withInput();
            }else{
                $unique_id = trim($request->newsfeed_unique_id);
                $title = trim($request->title);
                $description = trim($request->description);
                $status = $request->status;
                $tag_type = $request->tag_type;
                $updated_at = date('Y-m-d H:i:s');
                $updated_by = Auth::user()->Use_Id;

                $old_newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();

                $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
                if($newsfeed->Nfd_User_Type == "1" || $newsfeed->Nfd_User_Type == "5")
                {
                    $existing_branch_ids = Newsfeed::where('Nfd_Unique_Id',$unique_id)->select('Nfd_Brn_Id')->get()->toArray();
                    $existing_ids = array();
                    foreach ($existing_branch_ids as $key => $value) {
                        array_push($existing_ids, $value['Nfd_Brn_Id']);
                    }

                    $class_ids = $request->class_id;
                    $selected_class = ClassTbl::where('Cla_Bra_Id','1')->whereIn('Cla_Id',$class_ids)->get()->toArray();

                    $branch_ids = ClassTbl::whereIn('Cla_Id',$class_ids)->select('Cla_Bra_Id')->distinct()->get()->toArray();
                    $new_ids = array();
                    foreach ($branch_ids as $key => $value) {
                        array_push($new_ids, $value['Cla_Bra_Id']);
                    }

                    foreach ($existing_ids as $existsKey=>$existsValue) 
                    {
                        if(in_array($existsValue,$new_ids))
                        {
                            // update query
                            $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->where('Nfd_Brn_Id',$existsValue)->first();
                            $newsfeed->Nfd_Title = $title;
                            $newsfeed->Nfd_Description = $description;
                            $newsfeed->Nfd_Status = $status;
                            $newsfeed->Nfd_UpdatedAt = $updated_at;
                            $newsfeed->Nfd_UpdatedBy = $updated_by;
                            $newsfeed->save();

                            $not_exists_class = ClassTbl::where('Cla_Bra_Id',$existsValue)->whereNotIn('Cla_Id',$class_ids)->get()->toArray();
                            $delete_student_tag = Student::whereIn('Std_Cla_Id',$not_exists_class)->get();
                            Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->where('Nft_Brn_Id',$existsValue)->whereIn('Nft_Cla_Id',$not_exists_class)->delete();

                            $selected_class = ClassTbl::where('Cla_Bra_Id',$existsValue)->whereIn('Cla_Id',$class_ids)->select(['Cla_Id'])->get()->toArray();
                            $student_list = Student::whereIn('Std_Cla_Id',$selected_class)->get();
                            
                            foreach ($student_list as $student) {
                                $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                                if(Newsfeedtag::where('Nft_Std_Id',$student->Std_Id)->where('Nft_Cla_Id',$student->Std_Cla_Id)->where('Nft_Brn_Id',$branch_id)->where('Nft_Nfd_Unique_Id',$unique_id)->exists()){
                                }else{
                                    $newsfeedTag = New Newsfeedtag;
                                    $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                                    $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                                    $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                                    $newsfeedTag->Nft_Brn_Id = $branch_id;
                                    $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                                    $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                                    $newsfeedTag->save();
                                }
                            }
                            return redirect('Newsfeed Mgmt.');
                        }
                        else
                        {
                            // delete query
                            Newsfeed::where('Nfd_Unique_Id',$unique_id)->where('Nfd_Brn_Id',$existsValue)->delete();
                            Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->where('Nft_Brn_Id',$existsValue)->delete();
                        }
                    }

                    foreach ($new_ids as $newKey=>$newValue) 
                    {
                        if(in_array($newValue,$existing_ids))
                        {
                            // update query not need
                            // echo "new chhe update not required= ".$newValue;
                        }
                        else
                        {
                            // insert query
                            $newsfeed = New Newsfeed;
                            $newsfeed->Nfd_Unique_Id = $unique_id;
                            $newsfeed->Nfd_Brn_Id = $newValue;
                            $newsfeed->Nfd_Title = $title;
                            $newsfeed->Nfd_Description = $description;
                            $newsfeed->Nfd_Status = $status;
                            $newsfeed->Nfd_User_Type = $old_newsfeed->Nfd_User_Type;
                            $newsfeed->Nfd_Tag_Type = $tag_type;
                            $newsfeed->Nfd_CreatedAt = $old_newsfeed->Nfd_CreatedAt;
                            $newsfeed->Nfd_CreatedBy = $old_newsfeed->Nfd_CreatedBy;
                            $newsfeed->Nfd_UpdatedAt = $updated_at;
                            $newsfeed->Nfd_UpdatedBy = $updated_by;
                            $newsfeed->save();

                            $selected_class = ClassTbl::where('Cla_Bra_Id',$newValue)->whereIn('Cla_Id',$class_ids)->get()->toArray();
                            $student_list = Student::whereIn('Std_Cla_Id',$selected_class)->get();
                            foreach ($student_list as $student) {
                                $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                                $newsfeedTag = New Newsfeedtag;
                                $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                                $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                                $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                                $newsfeedTag->Nft_Brn_Id = $branch_id;
                                $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                                $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                                $newsfeedTag->save();
                            }
                            return redirect('Newsfeed Mgmt.');
                        }
                    }
                }elseif($newsfeed->Nfd_User_Type == "2"){
                    $newsfeed = Newsfeed::where('Nfd_Unique_Id',$unique_id)->first();
                    $newsfeed->Nfd_Title = $title;
                    $newsfeed->Nfd_Description = $description;
                    $newsfeed->Nfd_Status = $status;
                    $newsfeed->Nfd_Tag_Type = $tag_type;
                    $newsfeed->Nfd_UpdatedAt = $updated_at;
                    $newsfeed->Nfd_UpdatedBy = $updated_by;
                    if($newsfeed->save())
                    {
                        if($tag_type == "class")
                        {
                            $class_ids = $request->class_id;
                            $branchAccess = ClassTbl::whereIn('Cla_Id',$class_ids)->select('Cla_Bra_Id')->get()->first();
                            
                            $not_exists_class = ClassTbl::where('Cla_Bra_Id',$branchAccess->Cla_Bra_Id)->whereNotIn('Cla_Id',$class_ids)->get()->toArray();
                            $delete_student_tag = Student::whereIn('Std_Cla_Id',$not_exists_class)->get();
                            $result = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->where('Nft_Brn_Id',$branchAccess->Cla_Bra_Id)->whereIn('Nft_Cla_Id',$not_exists_class)->delete();
                            $selected_class = ClassTbl::where('Cla_Bra_Id',$branchAccess->Cla_Bra_Id)->whereIn('Cla_Id',$class_ids)->select(['Cla_Id'])->get()->toArray();
                            $student_list = Student::whereIn('Std_Cla_Id',$selected_class)->get();
                            foreach ($student_list as $student) 
                            {
                                if(Newsfeedtag::where('Nft_Std_Id',$student->Std_Id)->where('Nft_Cla_Id',$student->Std_Cla_Id)->where('Nft_Nfd_Unique_Id',$unique_id)->exists())
                                {
                                }else{
                                    $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                                    $newsfeedTag = New Newsfeedtag;
                                    $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                                    $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                                    $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                                    $newsfeedTag->Nft_Brn_Id = $branch_id;
                                    $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                                    $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                                    $newsfeedTag->save();
                                }
                            }
                            return redirect('Newsfeed Mgmt.');
                        }elseif($tag_type == "student"){

                            $student_ids = $request->student_id;
                            $delete_student_tag = Newsfeedtag::where('Nft_Nfd_Unique_Id',$unique_id)->whereNotIn('Nft_Std_Id',$student_ids)->delete();
                            $student_list = Student::whereIn('Std_Id',$student_ids)->get();
                            foreach ($student_list as $student) 
                            {
                                if(Newsfeedtag::where('Nft_Std_Id',$student->Std_Id)->where('Nft_Cla_Id',$student->Std_Cla_Id)->where('Nft_Nfd_Unique_Id',$unique_id)->exists())
                                {
                                }else{
                                    $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                                    $newsfeedTag = New Newsfeedtag;
                                    $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                                    $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                                    $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                                    $newsfeedTag->Nft_Brn_Id = $branch_id;
                                    $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                                    $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                                    $newsfeedTag->save();
                                }
                            }
                            return redirect('Newsfeed Mgmt.');
                        }
                    }else{

                    }
                }
                dd("done");
                
                if($request->hasFile('imageOrVideo'))
                {
                    $imagesOrVideo = $request->file('imageOrVideo');
                    $image_count = 0;
                    $error_size = false;
                    foreach ($imagesOrVideo as $image)
                    {
                        if($image->getClientSize() > 26214400)
                        {
                            $error_size = true;
                        }
                        if($image_count >= 50)
                        {
                           return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 50 Image');
                           break;  
                        }
                    }
                    if($error_size == true)
                    {
                       return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');
                    }
                    else
                    {
                        $i = 0;
                        foreach($imagesOrVideo as $file)
                        {
                            $filename = $file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $mime = $file->getMimeType();
                            if(strstr($mime, "video/")){
                                $mediaType = "2";
                            }else if(strstr($mime, "image/")){
                                $mediaType = "1";
                            }
                            $picture[] = ['path' => $i.time().".".$extension,
                                                'type'=>$mediaType];
                            $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                            $i++;
                        }
                    }
                }

            }
            dd($request->all());
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
}
