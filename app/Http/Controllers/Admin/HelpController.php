<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use Auth;
use DB;
use App\User;
use App\Helper\UserRights;
use App\Model\HelpTbl;
use App\Helper\Notification;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;

class HelpController extends Controller
{
    public function index()
    {
    	try {
    		$data["help"] = HelpTbl::leftjoin('user_tbl','user_tbl.Use_Id','=','help_tbl.Help_Use_Id')->select(['user_tbl.Use_Name','help_tbl.*'])->orderBy('Help_Id','DESC')->paginate(10);
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.help.index',$data);
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    HelpTbl::where('Help_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
