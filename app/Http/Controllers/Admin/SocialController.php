<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Collection;
use Cookie;
use Session;
use Kingsley\Mentions\MentionCollection;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\Socialactivitydetails;
use App\Model\Socialactivitytag;
use App\Model\Admin_assign_branch;
use App\Model\Socialactivity;
use App\Model\ClassTbl;
use App\Helper\Exceptions;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Model\Student;
use App\Helper\Ajax;
use App\Model\Branch;
use Illuminate\Http\Response;
use App\User;
use URL;
use DB;
use Log;

class SocialController extends Controller
{
    public $rules =[
                    'title' =>  'required',
                    'description' => 'required',
                    'status' => 'required',
                    'imageOrVideo' => ''
                ];
    public $customMessage = ([
                ]);

    public function index(){
        try {
            $data['i'] = 1;

            //admin user
            if(Auth::user()->Use_Type=="1")
            {
                $data['socialActivitys'] = Socialactivity::join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')->select('socialactivity_tbl.*','branch_tbl.Brn_Name')->orderBy('Soa_Id','DESC')->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.index',$data);
            }

            //branch admin user
            if(Auth::user()->Use_Type=="5")
            {
                $branch = Admin_assign_branch::where("Aab_Use_Id",Auth::user()->Use_Id)->select("Aab_Brn_Id")->get()->toArray();

                $data['socialActivitys'] = Socialactivity::whereIn("Soa_Branch",$branch)->join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')->select('socialactivity_tbl.*','branch_tbl.Brn_Name')->orderBy('Soa_Id','DESC')->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.index',$data);
            }

            //Teacher User
            if(Auth::user()->Use_Type=="2"){
                $teacher = UserRights::teacher();
                $data['socialActivitys'] = Socialactivity::where('Soa_Use_Id',Auth::user()->Use_Id)->whereIn("Soa_Cla_Id",$teacher['AssignClass'])->orderBy('Soa_Id','DESC')->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.index',$data);
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function create(Request $request){
        try {
            if(Auth::user()->Use_Type=="1")
            {
                $data['branch'] = Branch::where('Brn_Status','1')->get();
                // $data['branch'] = Branch::leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->select('class_tbl.*','branch_tbl.*')
                                ->where('class_tbl.Cla_Status','=',1)
                                ->where('branch_tbl.Brn_Status','=',1)
                                ->orderBy('class_tbl.Cla_Id')
                                ->get();

                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.create',$data);    
            }
            if(Auth::user()->Use_Type=="2")
            {
                $teacher = UserRights::teacher();
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->where('Brn_Id',$teacher['branchAccess'])
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('branch_tbl.Brn_Name')
                            ->get();

                // $teacher = UserRights::teacher();
                // $data['branch'] = Branch::whereIn("class_tbl.Cla_Id",$teacher["branchClass"])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.create',$data);                
            }
            if(Auth::user()->Use_Type=="5")
            {
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->whereIn('Brn_Id',$branch["branchAccess"])
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('branch_tbl.Brn_Name')
                            ->get();
                            
                // $branch = UserRights::branchAdmin();
                // $data['branch'] = Branch::whereIn("class_tbl.Cla_Id",$branch["branchClass"])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.create',$data);
            }

        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function store(Request $request){
        try {
            $validator = Validator::make($request->all(),$this->rules);

            if($validator->fails()){
                return back()->with('errors',$validator->errors())->withInput();
            }else{
                $tagType = $request->tag;
                
                $title = $request->title;
                $comment = $request->description;
                $error_size = false;

                $imagesOrVideo = $request->file('imageOrVideo');
                $image_count = count($imagesOrVideo);

                if($imagesOrVideo){
                foreach ($imagesOrVideo as $image)
                {
                    if($image->getClientSize() > 104857600)
                    {
                        $error_size = true;
                    }
                    if($image_count >= 100)
                    {
                       return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25 Image');  
                    }
                }
                }
                if($error_size == true)
                {
                   return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');                   
                }else{
                if($imagesOrVideo){
                    foreach($imagesOrVideo as $file){
                        $filename = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $mime = $file->getMimeType();
                        if(strstr($mime, "video/")){
                            $mediaType = "2";
                        }else if(strstr($mime, "image/")){
                            $mediaType = "1";
                        }
                        $picture[] = ['path' => date('His').$filename,
                                            'type'=>$mediaType];
                        $file->move(public_path('images/social'), date('His').$filename);
                    }
                }else{
                    $imagesOrVideo = "";
                }

// ****************************  Admin Insert Code Start  ***********************************

                if(Auth::user()->Use_Type=="1")
                {
                    $parent_no = array();
                    $unique_id = "SOA".time();
                    foreach ($request->class_id as $value)
                    {
                        $brn_id = ClassTbl::where("Cla_Id",$value)->first()->Cla_Bra_Id;
                        $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_Unique_id'] = $unique_id;
                        $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                        $socialActivity['Soa_Title']  = $title;
                        $socialActivity['Soa_Cla_Id'] = $value;
                        $socialActivity['Soa_Comment'] = $comment;
                        $socialActivity['Soa_Tag'] = $tagType;
                        $socialActivity['Soa_Branch'] = $brn_id;
                        $socialActivity['Soa_Status'] = $request->status;
                        $socialActivity['Soa_CreatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
                        $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
                        $lastid = Socialactivity::insertGetId($socialActivity);
                        $student = Student::where('Std_Cla_Id',$value)->get();
                        $teacherLists = UserRights::classTeacherList($value);
                        if($request->status==1){
                            foreach ($teacherLists as $singleTeacher){
                                if($singleTeacher->Use_Token!=""){
                                    Notification::sendNotification($singleTeacher->Use_Token,Auth::user()->Use_Name." Add New Post",$title);
                                }else{
                                    $singleTeacher_mobile = $singleTeacher->Use_Mobile_No;
                                    array_push($parent_no, $singleTeacher_mobile);
                                }
                            }
                        }
                        $uniques = array();
                        foreach ($student as $c) {
                            $parent = DB::table('user_tbl')
                                        ->select('user_tbl.*')
                                        ->where('Use_Id', $c->Std_Parent_Id)
                                        ->where('Use_Token','=','')
                                        ->where('Use_Mobile_No','!=','')
                                        ->first();  
                            if ($parent!="") {
                                $uniques[$parent->Use_Mobile_No] = $parent;
                            }
                        }
                        foreach ($uniques as $uniques_no) {
                            if($request->status=="1"){
                                if ($uniques_no->Use_Token=="") {
                                    $user_mo = $uniques_no->Use_Mobile_No;
                                    array_push($parent_no, $user_mo);
                                }
                                if($uniques_no->Use_Mother_Token=="" && $uniques_no->Use_Mother_Mobile_No != $uniques_no->Use_Mobile_No){
                                    $user_mother = $uniques_no->Use_Mother_Mobile_No;
                                    array_push($parent_no, $user_mother);
                                }
                            }
                        }
                        foreach ($student as $val)
                        {
                            $tag = new Socialactivitytag;
                            $tag->Tag_Use_Id = Auth::user()->Use_Id;
                            $tag->Tag_Soa_Unique_Id = $unique_id;
                            $tag->Tag_Soa_Id = $lastid;
                            $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                            $tag->Tag_Stu_Id = $val->Std_Id;
                            $tag->Tag_Flag = $tagType;
                            $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                            $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                            $tag->save();
                            if ($request->status=="1") {
                                $parenttoken = User::where('Use_Id',$val->Std_Parent_Id)->first();
                                if($parenttoken->Use_Token!=""){
                                    Notification::sendNotification($parenttoken->Use_Token, Auth::user()->Use_Name." Add New Post",$title);
                                }
                                if($parenttoken->Use_Mother_Token!=""){
                                    Notification::sendNotification($parenttoken->Use_Mother_Token,Auth::user()->Use_Name." Add New Post",$title);
                                }
                            }
                        }
                        if($imagesOrVideo)
                        {
                            foreach ($picture as $value)
                            {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->save();
                            }
                        }  
                    }
                    $all_uniques_no = array_unique($parent_no);
                    foreach ($all_uniques_no as $unique_no) {
                        $socialActivity['Soa_Title']  = $title;
                        SMSHelper::sendSMS($unique_no,Auth::user()->Use_Name." Add New Post".$title);
                    }
                    return redirect("Social Activity.");
                }
                elseif(Auth::user()->Use_Type=="2")
                {
                    $unique_id = "SOA".time();
                    if($request->tag=="1")
                    {
                        foreach ($request->class_id as $value)
                        {
                            $brn_id = ClassTbl::where("Cla_Id",$value)->first()->Cla_Bra_Id;
                            $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                            $socialActivity['Soa_Unique_id'] = $unique_id;
                            $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                            $socialActivity['Soa_Title']  = $title;
                            $socialActivity['Soa_Cla_Id'] = $value;
                            $socialActivity['Soa_Comment'] = $comment;
                            $socialActivity['Soa_Tag'] = $tagType;
                            $socialActivity['Soa_Branch'] = $brn_id;
                            $socialActivity['Soa_Status'] = $request->status;
                            $socialActivity['Soa_CreatedBy'] = Auth::user()->Use_Id;
                            $socialActivity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
                            $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                            $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

                            $lastid = Socialactivity::insertGetId($socialActivity);
                            
                            $student = Student::where('Std_Cla_Id',$value)->get();
                            foreach ($student as $val)
                            {
                                $tag = new Socialactivitytag;
                                $tag->Tag_Use_Id = Auth::user()->Use_Id;
                                $tag->Tag_Soa_Unique_Id = $unique_id;
                                $tag->Tag_Soa_Id = $lastid;
                                $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                                $tag->Tag_Stu_Id = $val->Std_Id;
                                $tag->Tag_Flag = $tagType;
                                $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                                $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                                $tag->save();
                            }
                            if($imagesOrVideo)
                            {
                                foreach ($picture as $value)
                                {
                                    $soa = new Socialactivitydetails;
                                    $soa->Sod_Img_Video = $value['path'];
                                    $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                    $soa->Sod_Soa_Unique_Id = $unique_id;
                                    $soa->Sod_Soa_Id = $lastid;
                                    $soa->Sod_Uploadtype = $value['type'];
                                    $soa->save();
                                }
                            }
                        }
                    }elseif ($request->tag=="2") {

                        $brn_id = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->first()->Cla_Bra_Id;
                        $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_Unique_id'] = $unique_id;
                        $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                        $socialActivity['Soa_Title']  = $title;
                        $socialActivity['Soa_Cla_Id'] = Auth::user()->Use_Cla_Id;
                        $socialActivity['Soa_Comment'] = $comment;
                        $socialActivity['Soa_Tag'] = $tagType;
                        $socialActivity['Soa_Branch'] = $brn_id;
                        $socialActivity['Soa_Status'] = $request->status;
                        $socialActivity['Soa_CreatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
                        $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
                        $lastid = Socialactivity::insertGetId($socialActivity);

                        if($imagesOrVideo)
                        {
                            foreach ($picture as $value)
                            {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->save();
                            }
                        }
                        $student = Student::whereIn('Std_Id',$request->student)->get();
                        foreach ($student as $val)
                        {
                            $tag = new Socialactivitytag;
                            $tag->Tag_Use_Id = Auth::user()->Use_Id;
                            $tag->Tag_Soa_Unique_Id = $unique_id;
                            $tag->Tag_Soa_Id = $lastid;
                            $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                            $tag->Tag_Stu_Id = $val->Std_Id;
                            $tag->Tag_Flag = $tagType;
                            $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                            $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                            $tag->save();
                        }
                    }
                    return redirect("Social Activity.");
                }
                elseif(Auth::user()->Use_Type=="5")
                {
                    $unique_id = "SOA".time();
                    foreach ($request->class_id as $value)
                    {
                        $brn_id = ClassTbl::where("Cla_Id",$value)->first()->Cla_Bra_Id;
                        $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_Unique_id'] = $unique_id;
                        $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                        $socialActivity['Soa_Title']  = $title;
                        $socialActivity['Soa_Cla_Id'] = $value;
                        $socialActivity['Soa_Comment'] = $comment;
                        $socialActivity['Soa_Tag'] = $tagType;
                        $socialActivity['Soa_Branch'] = $brn_id;
                        $socialActivity['Soa_Status'] = $request->status;
                        $socialActivity['Soa_CreatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
                        $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

                        $lastid = Socialactivity::insertGetId($socialActivity);
                        $student = Student::where('Std_Cla_Id',$value)->get();

                        $teacherLists = UserRights::classTeacherList($value);
                        if($request->status==1){
                            foreach ($teacherLists as $singleTeacher)
                            {
                                if($singleTeacher->Use_Token!=""){
                                    Notification::sendNotification($singleTeacher->Use_Token,Auth::user()->Use_Name." Add New Post",$title);
                                }else if($singleTeacher->Use_Mobile_No!=""){
                                    SMSHelper::sendSMS($singleTeacher->Use_Mobile_No,Auth::user()->Use_Name." Add New Post. ".$title); 
                                }
                            }
                        }

                        foreach ($student as $val)
                        {
                            $tag = new Socialactivitytag;
                            $tag->Tag_Use_Id = Auth::user()->Use_Id;
                            $tag->Tag_Soa_Unique_Id = $unique_id;
                            $tag->Tag_Soa_Id = $lastid;
                            $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                            $tag->Tag_Stu_Id = $val->Std_Id;
                            $tag->Tag_Flag = $tagType;
                            $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                            $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                            $tag->save();

                            if($request->status=="1"){
                                $parenttoken = User::where('Use_Id',$val->Std_Parent_Id)->first();
                                if($parenttoken->Use_Token!=""){
                                    Notification::sendNotification($parenttoken->Use_Token, Auth::user()->Use_Name." Add New Post",$title);
                                }else if($parenttoken->Use_Mobile_No!=""){
                                    SMSHelper::sendSMS($parenttoken->Use_Mobile_No,Auth::user()->Use_Name."Add New Post.".$title);
                                }
                                if($parenttoken->Use_Mother_Token!=""){
                                    Notification::sendNotification($parenttoken->Use_Mother_Token,Auth::user()->Use_Name." Add New Post",$title);
                                }else if($parenttoken->Use_Mother_Mobile_No!=""){
                                    SMSHelper::sendSMS($parenttoken->Use_Mother_Mobile_No,Auth::user()->Use_Name."Add New Post.".$title);
                                }
                            }
                        }
                        if($imagesOrVideo)
                        {
                            foreach ($picture as $value)
                            {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->save();
                            }
                        } 
                    }
                    return redirect("Social Activity.");
                }
                else{
                    return back();   
                }
              }              
            }
        }catch(\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getTeacherStudent(Request $request){
        try {
            $teacherClassIds = UserRights::teacherApi(Auth::user()->Use_Id,"branchClass");
            $data['teacherStudents'] = Student::whereIn("Std_Cla_Id",$teacherClassIds)->orderBy("Std_Name","ASC")->get();
            if(isset($request->Soa_Id))
            {
                $data['checkedStudent'] = Socialactivitytag::where('Tag_Use_Id',Auth::user()->Use_Id)->where('Tag_Cla_Id',$teacherClassIds)->where('Tag_Soa_Unique_Id',$request->Soa_Id)->get();
            }
            return view('auth.Social.studentList',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    Socialactivity::where('Soa_Unique_Id',$id)->delete();
                    Socialactivitydetails::where('Sod_Soa_Unique_Id',$id)->delete();
                    Socialactivitytag::where('Tag_Soa_Unique_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function edit($id){
        try {
            $data['social'] = Socialactivity::where('Soa_Unique_Id',$id)->first();
            // $data['branch'] = Branch::get();
            $data['social_images'] = Socialactivitydetails::select('Sod_Img_Video','Sod_Uploadtype','Sod_Id')->where('Sod_Soa_Unique_Id',$id)->groupby('Sod_Img_Video')->get();
            
            $socialBranchId = Socialactivity::where('Soa_Unique_Id',$id)->select('Soa_Branch')->get()->toArray();
            // $data['branchid'] = Branch::get();
            // $data['branch']  =  Branch::whereIn('Brn_Id',$socialBranchId)->get();
            if(Auth::user()->Use_Type == "1"){
                $data['sel_section'] = Socialactivity::where('Soa_Unique_Id',$id)->distinct()->select(['Soa_Branch'])->get();
                $data['sel_class'] = Socialactivity::where('Soa_Unique_Id',$id)->distinct()->select(['Soa_Cla_Id'])->get();
                $data['branch'] = Branch::where('Brn_Status','1')->get();
                // $data['branch'] = Branch::leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();

                $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                    ->select('class_tbl.*','branch_tbl.*')
                                    ->where('class_tbl.Cla_Status','=',1)
                                    ->where('branch_tbl.Brn_Status','=',1)
                                    ->orderBy('class_tbl.Cla_Id')
                                    ->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['sel_section'] = Socialactivity::where('Soa_Unique_Id',$id)->distinct()->select(['Soa_Branch'])->get();
                $data['sel_class'] = Socialactivity::where('Soa_Unique_Id',$id)->distinct()->select(['Soa_Cla_Id'])->get();

                $branch_id = UserRights::teacher();
                $data['branch'] = Branch::where('Brn_Id',$branch_id['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                // $data['branch'] = Branch::whereIn("class_tbl.Cla_Id",$teacher["branchClass"])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();

                $data['CURight']=UserRights::rights();

                $teacherClassIds = UserRights::teacherApi(Auth::user()->Use_Id,"branchClass");
                $data['teacherStudents'] = Student::whereIn("Std_Cla_Id",$teacherClassIds)->orderBy("Std_Name","ASC")->get();
                if(isset($id))
                {
                    $data['checkedStudent'] = Socialactivitytag::where('Tag_Use_Id',Auth::user()->Use_Id)->where('Tag_Cla_Id',$teacherClassIds)->where('Tag_Soa_Unique_Id',$id)->get();
                }
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['sel_section'] = Socialactivity::where('Soa_Unique_Id',$id)->distinct()->select(['Soa_Branch'])->get();
                $data['sel_class'] = Socialactivity::where('Soa_Unique_Id',$id)->distinct()->select(['Soa_Cla_Id'])->get();

                $branch_ids = UserRights::branchAdmin();
                $data['branch'] = Branch::whereIn('Brn_Id',$branch_ids['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                // $data['branch'] = Branch::whereIn("class_tbl.Cla_Id",$branch["branchClass"])->leftjoin("class_tbl","class_tbl.Cla_Bra_Id","=","branch_tbl.Brn_Id")->get();
                // $data['class'] = Branch::whereIn("class_tbl.Cla_Id",$branch["branchClass"])
                //                     ->leftjoin('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                //                     ->select('class_tbl.*','branch_tbl.*')
                //                     ->where('class_tbl.Cla_Status','=',1)
                //                     ->where('branch_tbl.Brn_Status','=',1)
                //                     ->orderBy('class_tbl.Cla_Id')
                //                     ->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
            }
            return view('auth.Social.edit',$data);

        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function update(Request $request){
        try {
            $validator = Validator::make($request->all(),$this->rules);
            if($validator->fails()){
                return back()->with('errors',$validator->errors())->withInput();
            }else{
                $tagType = $request->tag;
                
                $title = $request->title;
                $comment = $request->description;
                $error_size = false;

                $imagesOrVideo = $request->file('imageOrVideo');
                $image_count = count($imagesOrVideo);

                if($imagesOrVideo)
                {
                  foreach ($imagesOrVideo as $image)
                  {
                    if($image->getClientSize() > 104857600)
                    {
                        $error_size = true;
                    }
                    if($image_count >= 100)
                    {
                       return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 20 Image');  
                    }
                   }
                    if($error_size == true)
                    {
                       return back()->with('errors',$validator->errors())->withInput()->with('imageOrVideo','Do Not Upload More Than 25MB File');                   
                    }
                    else
                    {

                        if($imagesOrVideo){
                            foreach($imagesOrVideo as $file){
                                $filename = $file->getClientOriginalName();
                                $extension = $file->getClientOriginalExtension();
                                $mime = $file->getMimeType();
                                if(strstr($mime, "video/")){
                                    $mediaType = "2";
                                }else if(strstr($mime, "image/")){
                                    $mediaType = "1";
                                }
                                $picture[] = ['path' => date('His').$filename,
                                                    'type'=>$mediaType];
                                $file->move(public_path('images/social'), date('His').$filename);
                            }
                        }else{
                            $imagesOrVideo = "";
                        }
                     }
                }

// ****************************  Admin Side Update Code Start  ***********************************
                $unique_Soa_Id = $request->Soa_Id;
                $socialUserId = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->first()->Soa_Use_Id;
                if(Auth::user()->Use_Type=="1" && Auth::user()->Use_Id == $socialUserId )
                {
                    $unique_Soa_Id = $request->Soa_Id;
                    $oldEventData = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->first();
                    $socialactivitys = Socialactivitydetails::where('Sod_Soa_Unique_Id',$unique_Soa_Id)->get();
                    $unique_id = "SOA".time();
                    if (count($socialactivitys)!=0) {
                        foreach ($socialactivitys as $socialactivity) {
                            $social_activitys =  DB::update('update socialactivity_details_tbl set Sod_Soa_Unique_Id = ? where Sod_Id = ?',[$unique_id,$socialactivity->Sod_Id]);
                        }
                    }
                    Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->delete();

                    foreach ($request->class_id as $value) 
                    {
                        $brn_id = ClassTbl::where("Cla_Id",$value)->first()->Cla_Bra_Id;
                        $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_Unique_id'] = $unique_id;
                        $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                        $socialActivity['Soa_Title']  = $title;
                        $socialActivity['Soa_Cla_Id'] = $value;
                        $socialActivity['Soa_Comment'] = $comment;
                        $socialActivity['Soa_Tag'] = $tagType;
                        $socialActivity['Soa_Branch'] = $brn_id;
                        $socialActivity['Soa_Status'] = $request->status;
                        $socialActivity['Soa_CreatedBy'] = $oldEventData['Soa_CreatedBy'];
                        $socialActivity['Soa_CreatedAt'] = $oldEventData['Soa_CreatedAt'];
                        $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
                        $lastid = Socialactivity::insertGetId($socialActivity);

                        // $lastid = 10000;
                        Socialactivitytag::where('Tag_Soa_Unique_Id',$unique_Soa_Id)->delete();
                        $student = Student::where('Std_Cla_Id',$value)->get();
                        foreach ($student as $val)
                        {
                            $tag = new Socialactivitytag;
                            $tag->Tag_Use_Id = Auth::user()->Use_Id;
                            $tag->Tag_Soa_Unique_Id = $unique_id;
                            $tag->Tag_Soa_Id = $lastid;
                            $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                            $tag->Tag_Stu_Id = $val->Std_Id;
                            $tag->Tag_Flag = $tagType;
                            $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                            $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                            $tag->save();
                        }

                        if($imagesOrVideo != "")
                        {
                            $socialactivity = Socialactivitydetails::where('Sod_Soa_Unique_Id',$unique_id)->get();
                            foreach ($picture as $value)
                            {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->save();
                            }
                        }else{
                            $socialDetails = Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->where('Sod_Soa_Id',$oldEventData["Soa_Id"])->get();
                            // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                            foreach ($socialDetails as $socialdetail) {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $socialdetail['Sod_Img_Video'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $socialdetail['Sod_Uploadtype'];
                                $soa->save();
                            }
                        }
                    }
                    return redirect("Social Activity.");
                }else 
                if(Auth::user()->Use_Type=="1"){
                    $unique_Soa_Id = $request->Soa_Id;
                    $oldEventData = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->get();
                    $socialUserId = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->first()->Soa_Use_Id;
                    $updateEventData = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->update([
                        'Soa_Title' => $title,
                        'Soa_Comment' => $comment,
                        'Soa_Status' => $request->status,
                        'Soa_UpdatedBy' => Auth::user()->Use_Id,
                        'Soa_UpdatedAt' => date('Y-m-d H:i:s'),
                    ]);
                    
                    if($imagesOrVideo != "")
                    {
                        Socialactivitydetails::where('Sod_Soa_Unique_Id',$unique_Soa_Id)->delete();
                        
                        foreach ($picture as $value)
                        {
                            foreach ($oldEventData as $eventData) {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = $eventData->Soa_Use_Id;
                                $soa->Sod_Soa_Unique_Id = $eventData->Soa_Unique_Id;
                                $soa->Sod_Soa_Id = $eventData->Soa_Id;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->Sod_CreatedAt = date('Y-m-d H:i:s');
                                $soa->save();
                            }
                        }
                    }
                    return redirect("Social Activity.");
                }
// ****************************  Admin Side Update Code End  ***********************************

// ****************************  Branch Adnin  Side Update Code Start  ****************************
                if(Auth::user()->Use_Type=="5")
                {
                    $unique_Soa_Id = $request->Soa_Id;
                    $oldEventData = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->first();
                    $unique_id = "SOA".time();
                    Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->delete();
                    foreach ($request->class_id as $value)
                    {
                        $brn_id = ClassTbl::where("Cla_Id",$value)->first()->Cla_Bra_Id;
                        $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_Unique_id'] = $unique_id;
                        $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                        $socialActivity['Soa_Title']  = $title;
                        $socialActivity['Soa_Cla_Id'] = $value;
                        $socialActivity['Soa_Comment'] = $comment;
                        $socialActivity['Soa_Tag'] = $tagType;
                        $socialActivity['Soa_Branch'] = $brn_id;
                        $socialActivity['Soa_Status'] = $request->status;
                        $socialActivity['Soa_CreatedBy'] = $oldEventData['Soa_CreatedBy'];
                        $socialActivity['Soa_CreatedAt'] = $oldEventData['Soa_CreatedAt'];
                        $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

                        $lastid = Socialactivity::insertGetId($socialActivity);

                        $student = Student::where('Std_Cla_Id',$value)->get();
                        foreach ($student as $val)
                        {
                            $tag = new Socialactivitytag;
                            $tag->Tag_Use_Id = Auth::user()->Use_Id;
                            $tag->Tag_Soa_Unique_Id = $unique_id;
                            $tag->Tag_Soa_Id = $lastid;
                            $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                            $tag->Tag_Stu_Id = $val->Std_Id;
                            $tag->Tag_Flag = $tagType;
                            $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                            $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                            $tag->save();
                        }

                        if($imagesOrVideo != "")
                        {
                            // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                            foreach ($picture as $value)
                            {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->save();
                            }
                        }else{
                            $socialDetails = Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->where('Sod_Soa_Id',$oldEventData["Soa_Id"])->get();
                            // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                            foreach ($socialDetails as $socialdetail) {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $socialdetail['Sod_Img_Video'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $socialdetail['Sod_Uploadtype'];
                                $soa->save();
                            }
                        }
                    }    
                    return redirect("Social Activity.");
                }
// ****************************  Branch Adnin  Side Update Code Start  ****************************
                if(Auth::user()->Use_Type=="2")
                {
                    $unique_Soa_Id = $request->Soa_Id;
                    $oldEventData = Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->first();
                    $unique_id = "SOA".time();
                    Socialactivity::where('Soa_Unique_Id',$unique_Soa_Id)->delete();

                    if($request->tag == "1")
                    {
                        foreach ($request->class_id as $value)
                        {
                            $brn_id = ClassTbl::where("Cla_Id",$value)->first()->Cla_Bra_Id;
                            $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                            $socialActivity['Soa_Unique_id'] = $unique_id;
                            $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                            $socialActivity['Soa_Title']  = $title;
                            $socialActivity['Soa_Cla_Id'] = $value;
                            $socialActivity['Soa_Comment'] = $comment;
                            $socialActivity['Soa_Tag'] = $tagType;
                            $socialActivity['Soa_Branch'] = $brn_id;
                            $socialActivity['Soa_Status'] = $request->status;
                            $socialActivity['Soa_CreatedBy'] = $oldEventData['Soa_CreatedBy'];
                            $socialActivity['Soa_CreatedAt'] = $oldEventData['Soa_CreatedAt'];
                            $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                            $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

                            $lastid = Socialactivity::insertGetId($socialActivity);

                            $student = Student::where('Std_Cla_Id',$value)->get();
                            foreach ($student as $val)
                            {
                                $tag = new Socialactivitytag;
                                $tag->Tag_Use_Id = Auth::user()->Use_Id;
                                $tag->Tag_Soa_Unique_Id = $unique_id;
                                $tag->Tag_Soa_Id = $lastid;
                                $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                                $tag->Tag_Stu_Id = $val->Std_Id;
                                $tag->Tag_Flag = $tagType;
                                $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                                $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                                $tag->save();
                            }

                            if($imagesOrVideo != "")
                            {
                                // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                                foreach ($picture as $value)
                                {
                                    $soa = new Socialactivitydetails;
                                    $soa->Sod_Img_Video = $value['path'];
                                    $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                    $soa->Sod_Soa_Unique_Id = $unique_id;
                                    $soa->Sod_Soa_Id = $lastid;
                                    $soa->Sod_Uploadtype = $value['type'];
                                    $soa->save();
                                }
                            }else{
                                $socialDetails = Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->where('Sod_Soa_Id',$oldEventData["Soa_Id"])->get();
                                // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                                foreach ($socialDetails as $socialdetail) {
                                    $soa = new Socialactivitydetails;
                                    $soa->Sod_Img_Video = $socialdetail['Sod_Img_Video'];
                                    $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                    $soa->Sod_Soa_Unique_Id = $unique_id;
                                    $soa->Sod_Soa_Id = $lastid;
                                    $soa->Sod_Uploadtype = $socialdetail['Sod_Uploadtype'];
                                    $soa->save();
                                }
                            }
                        }    
                    }elseif($request->tag == "2")
                    {

                        $brn_id = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->first()->Cla_Bra_Id;
                        $socialActivity['Soa_Use_id'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_Unique_id'] = $unique_id;
                        $socialActivity['Soa_User_Type'] = Auth::user()->Use_Type;
                        $socialActivity['Soa_Title']  = $title;
                        $socialActivity['Soa_Cla_Id'] = Auth::user()->Use_Cla_Id;
                        $socialActivity['Soa_Comment'] = $comment;
                        $socialActivity['Soa_Tag'] = $tagType;
                        $socialActivity['Soa_Branch'] = $brn_id;
                        $socialActivity['Soa_Status'] = $request->status;
                        $socialActivity['Soa_CreatedBy'] = $oldEventData['Soa_CreatedBy'];
                        $socialActivity['Soa_CreatedAt'] = $oldEventData['Soa_CreatedAt'];
                        $socialActivity['Soa_UpdatedBy'] = Auth::user()->Use_Id;
                        $socialActivity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
                        $lastid = Socialactivity::insertGetId($socialActivity);

                        $student = Student::whereIn('Std_Id',$request->student)->get();
                        foreach ($student as $val)
                        {
                            $tag = new Socialactivitytag;
                            $tag->Tag_Use_Id = Auth::user()->Use_Id;
                            $tag->Tag_Soa_Unique_Id = $unique_id;
                            $tag->Tag_Soa_Id = $lastid;
                            $tag->Tag_Cla_Id = $val->Std_Cla_Id;
                            $tag->Tag_Stu_Id = $val->Std_Id;
                            $tag->Tag_Flag = $tagType;
                            $tag->Tag_CreatedAt = date('Y-m-d H:i:s');
                            $tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
                            $tag->save();
                        }

                        if($imagesOrVideo != "")
                        {
                            // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                            foreach ($picture as $value)
                            {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $value['path'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $value['type'];
                                $soa->save();
                            }
                        }else{
                            $socialDetails = Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->where('Sod_Soa_Id',$oldEventData["Soa_Id"])->get();
                            // Socialactivitydetails::where('Sod_Soa_Unique_Id',$oldEventData["Soa_Unique_Id"])->delete();
                            foreach ($socialDetails as $socialdetail) {
                                $soa = new Socialactivitydetails;
                                $soa->Sod_Img_Video = $socialdetail['Sod_Img_Video'];
                                $soa->Sod_Use_Id = Auth::user()->Use_Id;
                                $soa->Sod_Soa_Unique_Id = $unique_id;
                                $soa->Sod_Soa_Id = $lastid;
                                $soa->Sod_Uploadtype = $socialdetail['Sod_Uploadtype'];
                                $soa->save();
                            }
                        }
                    }
                    return redirect("Social Activity.");
                }  
// ***************************  Teacher Side Update Code End  ***********************************       
               
        }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function search(Request $request){
        try {
            return view('auth.Social.search',Ajax::searchSocial($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::socialStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['parent'] = Socialactivity::where('Soa_Use_Id',Auth::user()->Use_Id)->orderBy('Soa_Id','DESC')->paginate(10,['*'],'page',$whatIWant);
            else
                $data['parent'] = Socialactivity::where('Soa_Use_Id',Auth::user()->Use_Id)->orderBy('Soa_Id','DESC')->paginate(10);
            return view('auth.parent.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function approve(Request $request)
    {
        try{
            
            $data = $request->except('_token');

            $social['Soa_UpdatedBy'] = Auth::user()->Use_Id;
            $social['Soa_Status'] = 1;
            $social['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
            Socialactivity::where('Soa_Unique_Id',$request->id)->update($social);

            $class_id = Socialactivity::where('Soa_Unique_Id',$request->id)->select('Soa_Cla_Id')->get()->toArray();
            $student = Student::whereIn('Std_Cla_Id',$class_id)->select('Std_Parent_Id')->groupby('Std_Parent_Id')->get()->toArray();
            $parenttoken = User::whereIn('Use_Id',$student)->select('Use_Token','Use_Mother_Token')->get();
            
            $socialData = Socialactivity::where('Soa_Unique_Id',$request->id)->get()->first();
            $createBy = User::where('Use_Id',$socialData["Soa_CreatedBy"])->first()->Use_Name;

            foreach ($parenttoken as $parent) {
                if($parent->Use_Token!=""){
                    Notification::sendNotification($parent->Use_Token,$createBy." Add New Post",$socialData->Soa_Title);
                }else if($parent->Use_Mobile_No!=""){
                    SMSHelper::sendSMS($parent->Use_Mobile_No,$createBy." Add New Post. ".$socialData->Soa_Title);
                }
                if($parent->Use_Mother_Token!=""){
                    Notification::sendNotification($parent->Use_Mother_Token,$createBy." Add New Post",$socialData->Soa_Title);
                }else if($parent->Use_Mother_Mobile_No!=""){
                    SMSHelper::sendSMS($parent->Use_Mother_Mobile_No,$createBy." Add New Post. ".$socialData->Soa_Title);
                }
            }
            if(User::where("Use_Id",$socialData->Soa_CreatedBy)->exists()){
                $teacherData = UserRights::classesTeacherList($class_id);
                // $teacherData = User::where("Use_Id",$socialData->Soa_CreatedBy)->get()->first();
                foreach ($teacherData as $data) {
                    if($data->Use_Token!=""){
                        Notification::sendNotification($data->Use_Token,"Your Social Activity Is Approved.","Post :- ".$socialData->Soa_Title);
                    }else if($data->Use_Mobile_No!=""){
                        SMSHelper::sendSMS($data->Use_Mobile_No,"Your Social Activity Is Approved.","Post :- ".$socialData->Soa_Title);
                    }
                }
            }
            $otherTeacher = User::where("Use_Type","2")->whereIn("Use_Cla_Id",$class_id)->select(["Use_Token"])->get();
            if($otherTeacher != "" ){
                foreach ($otherTeacher as $teacher) {
                    Notification::sendNotification($parent->teacher,$createBy." Add New Post.",$socialData->Soa_Title);
                }
            }

            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['socialActivitys'] = Socialactivity::join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')->select('socialactivity_tbl.*','branch_tbl.Brn_Name')->orderBy('Soa_Id','DESC')
                              ->paginate(10,['*'],'page',$whatIWant);
            else
                $data['socialActivitys'] = Socialactivity::join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')->select('socialactivity_tbl.*','branch_tbl.Brn_Name')->orderBy('Soa_Id','DESC')->paginate(10);
            return view('auth.Social.approve',$data);

        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function send_notification($id)
    {
        try {

            $social['Soa_UpdatedBy'] = Auth::user()->Use_Id;
            $social['Soa_Status'] = 1;
            $social['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
            Socialactivity::where('Soa_Unique_Id',$id)->update($social);

            $class_id = Socialactivity::where('Soa_Unique_Id',$id)->select('Soa_Cla_Id')->get()->toArray();
            $student = Student::whereIn('Std_Cla_Id',$class_id)->select('Std_Parent_Id')->get()->toArray();
            $parenttoken = User::whereIn('Use_Id',$student)->select('Use_Token','Use_Mother_Token')->get();
            
            $socialData = Socialactivity::where('Soa_Unique_Id',$id)->get()->first();
            $createBy = User::where('Use_Id',$socialData["Soa_CreatedBy"])->first()->Use_Name;

            foreach ($parenttoken as $parent) {
                if($parent->Use_Token!=""){
                    Notification::sendNotification($parent->Use_Token,$createBy." Add New Post",$socialData->Soa_Title);
                }else if($parent->Use_Mobile_No!=""){
                    SMSHelper::sendSMS($parent->Use_Mobile_No,$createBy." Add New Post. ".$socialData->Soa_Title);
                }
                if($parent->Use_Mother_Token!=""){
                    Notification::sendNotification($parent->Use_Mother_Token,$createBy." Add New Post",$socialData->Soa_Title);
                }else if($parent->Use_Mother_Mobile_No!=""){
                    SMSHelper::sendSMS($parent->Use_Mother_Mobile_No,$createBy." Add New Post. ".$socialData->Soa_Title);
                }
            }
            if(User::where("Use_Id",$socialData->Soa_CreatedBy)->exists()){
                $teacherData = User::where("Use_Id",$socialData->Soa_CreatedBy)->get()->first();
                if($teacherData->Use_Token!=""){
                    Notification::sendNotification($teacherData->Use_Token,"Your Social Activity Is Approved.","Post :- ".$socialData->Soa_Title);
                }else if($teacherData->Use_Mobile_No!=""){
                    SMSHelper::sendSMS($teacherData->Use_Mobile_No,"Your Social Activity Is Approved.","Post :- ".$socialData->Soa_Title);
                }
            }
            $otherTeacher = User::where("Use_Type","2")->whereIn("Use_Cla_Id",$class_id)->select(["Use_Token"])->get();
            if($otherTeacher != "" ){
                foreach ($otherTeacher as $teacher) {
                    Notification::sendNotification($parent->teacher,$createBy." Add New Post.",$socialData->Soa_Title);
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function tag_list_activity($id)
    {
        try 
        {
           $data['i'] = 1;
            //admin user
                $class_id = Socialactivitytag::where('Tag_Soa_Unique_Id',$id)->select('Tag_Stu_Id')->get()->toArray();
                $data['socialActivitys'] = Student::whereIn('Std_Id',$class_id)->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.Social.student_tag_list',$data);
        } catch (\Exception $e) {
          Exceptions::exception($e);  
        }
    }
    public function imageVideoremove($id)
    {
        try {
            $social_activitys = Socialactivitydetails::where('Sod_Id',$id)->first();
            $social_activitys = Socialactivitydetails::where('Sod_Img_Video',$social_activitys->Sod_Img_Video)->get();
            if (count($social_activitys)!=0) {
                foreach ($social_activitys as $social_activity) {
                    Socialactivitydetails::where('Sod_Id',$social_activity->Sod_Id)->delete();
                }
            } else{
                Socialactivitydetails::where('Sod_Id',$social_activitys->Sod_Id)->delete();
            }
            return back();
        } catch (Exception $e) {
            log::error($e);
        }
    }

}
