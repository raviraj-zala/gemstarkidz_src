<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Ajax;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Module;
use App\Model\Payment;
use App\Model\Student;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use Log;
use URL;

class PaymentController extends Controller
{
    public $rules = [
        'branch' => 'required',
        // 'studentname'   =>  'required|regex:/^[A-Za-z ]+$/',
        // 'parentname'    =>  'required|regex:/^[A-Za-z ]+$/',
    ];

    public function index()
    {
        try {
            if (Auth::user()->Use_Type == "1") {
                $data['student'] = Payment::select('payment_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName', 'u.Use_Name as createdByName', 'branch_tbl.Brn_Name', 'student_tbl.Std_Name')
                    ->join('class_tbl', 'payment_tbl.Pay_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl as u', 'payment_tbl.Pay_CreatedBy', '=', 'u.Use_Id')
                    ->join('branch_tbl', 'class_tbl.Cla_Bra_Id', '=', 'branch_tbl.Brn_Id')
                    ->join('student_tbl', 'student_tbl.Std_Id', '=', 'payment_tbl.Pay_Std_Id')
                    ->join('user_tbl', 'user_tbl.Use_Id', '=', 'student_tbl.Std_Parent_Id')
                    ->groupby('Pay_Std_Id')
                    ->orderBy('Pay_Id', 'DESC')
                    ->paginate(10);
                // $data['student'] = Payment::join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                //                 ->join('user_tbl as u','payment_tbl.Pay_CreatedBy','=','u.Use_Id')
                //                 ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                //                 ->join('student_tbl','student_tbl.Std_Id','=','payment_tbl.Pay_Std_Id')
                //                 ->join('user_tbl','user_tbl.Use_Id','=','student_tbl.Std_Parent_Id')
                //                 ->select('payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name','student_tbl.Std_Name')
                //                 ->orderBy('Pay_Id','DESC')
                //                 ->paginate(10);
            } else if (Auth::user()->Use_Type == "2") {
                $class           = UserRights::teacher();
                $data['student'] = Payment::whereIn('payment_tbl.Pay_Cla_Id', $class['AssignClass'])
                    ->join('class_tbl', 'payment_tbl.Pay_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl as u', 'payment_tbl.Pay_CreatedBy', '=', 'u.Use_Id')
                    ->join('branch_tbl', 'class_tbl.Cla_Bra_Id', '=', 'branch_tbl.Brn_Id')
                    ->join('student_tbl', 'student_tbl.Std_Id', '=', 'payment_tbl.Pay_Std_Id')
                    ->join('user_tbl', 'user_tbl.Use_Id', '=', 'student_tbl.Std_Parent_Id')
                    ->select('payment_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName', 'u.Use_Name as createdByName', 'branch_tbl.Brn_Name', 'student_tbl.Std_Name')
                    ->orderBy('Pay_Id', 'DESC')
                    ->paginate(10);
            } else if (Auth::user()->Use_Type == "5") {
                $class           = UserRights::branchAdmin();
                $data['student'] = Payment::whereIn('payment_tbl.Pay_Cla_Id', $class['branchClass'])
                    ->join('class_tbl', 'payment_tbl.Pay_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl as u', 'payment_tbl.Pay_CreatedBy', '=', 'u.Use_Id')
                    ->join('branch_tbl', 'class_tbl.Cla_Bra_Id', '=', 'branch_tbl.Brn_Id')
                    ->join('student_tbl', 'student_tbl.Std_Id', '=', 'payment_tbl.Pay_Std_Id')
                    ->join('user_tbl', 'user_tbl.Use_Id', '=', 'student_tbl.Std_Parent_Id')
                    ->select('payment_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName', 'u.Use_Name as createdByName', 'branch_tbl.Brn_Name', 'student_tbl.Std_Name')
                    ->orderBy('Pay_Id', 'DESC')
                    ->paginate(10);
            }
            $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $data['CURight'] = UserRights::rights();
            return view('auth.Payment.index', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function create()
    {
        try {
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            if (Auth::user()->Use_Type == "1") {
                $data['branch'] = Branch::where('Brn_Status', 1)->get();
            } else if (Auth::user()->Use_Type == "2") {
                $branch         = UserRights::teacher();
                $data['branch'] = Branch::where('Brn_Id', $branch['branchAccess'])->where('Brn_Status', 1)->get();
            } else if (Auth::user()->Use_Type == "5") {
                $branch         = UserRights::branchAdmin();
                $data['branch'] = Branch::whereIn('Brn_Id', $branch["branchAccess"])->where('Brn_Status', 1)->get();
            }

            $data['CURight'] = UserRights::rights();
            return view('auth.Payment.create', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function search(Request $request)
    {
        try {
            return view('auth.Payment.search', Ajax::searchPayment($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function edit($id)
    {
        try {
            if (Payment::where('Pay_Std_Id', $id)->exists()) {
                $classId             = Payment::where('Pay_Std_Id', $id)->orderBy('Pay_Id', 'DESC')->first();
                $data['branch']      = Branch::where('Brn_Status', 1)->get();
                $branchId            = ClassTbl::where('Cla_Id', $classId->Pay_Cla_Id)->first();
                $data['std_class']   = ClassTbl::where('Cla_Status', 1)->where('Cla_Bra_Id', $branchId->Cla_Bra_Id)->distinct()->get(['Cla_Class', 'Cla_Bra_Id', 'Cla_Id']);
                $data['std_section'] = ClassTbl::where('Cla_Status', 1)->where('Cla_Class', $branchId->Cla_Class)->where('Cla_Bra_Id', $branchId->Cla_Bra_Id)->get();
                $data['student']     = Payment::where('Pay_Std_Id', $id)
                    ->join('student_tbl', 'payment_tbl.Pay_Std_Id', '=', 'student_tbl.Std_Id')
                    ->join('class_tbl', 'payment_tbl.Pay_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'student_tbl.Std_Parent_Id', '=', 'user_tbl.Use_Id')
                    ->select('payment_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName')
                    ->orderBy('Pay_Id', 'DESC')
                    ->first();
                $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                $data['CURight'] = UserRights::rights();
                return view('auth.Payment.create', $data);
            } else {
                return redirect('Payment Master');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getParent(Request $request)
    {
        $searchParentName = $request->get('term');
        $data             = User::select('Use_Name')->where('Use_Type', 4)->where('Use_Name', 'LIKE', '%' . $searchParentName . '%')->get();
        $result           = array();
        foreach ($data as $value) {
            $result[] = ['value' => $value->Use_Name];
        }
        return response()->json($result);
    }
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->rules, [
            ]);
            if ($validator->fails()) {
                return back()->with('errors', $validator->errors())->withInput();
            } else {
                if ($request->branch == "none") {
                    return back()->with('branch', "Please Select Branch")->withInput();
                }
                if ($request->class == "none") {
                    return back()->with('class', "Please Select Class")->withInput();
                }
                if ($request->section == "none") {
                    return back()->with('section', "Please Select Section")->withInput();
                }
                $id         = $request->id;
                $branchId   = $request->branch;
                $classAndId = explode(",", $request->class);
                $class      = end($classAndId);
                $section    = $request->section;
                $classId    = ClassTbl::select(['Cla_Id'])->where('Cla_Bra_Id', $branchId)->where('Cla_Class', $class)->where('Cla_Section', $section)->first();
                $pay_ids    = $request->pay_ids;
                $amounts    = $request->amount;
                $statuses   = $request->status;
                $due_date   = $request->due_date;
                $payment_date = $request->payment_date;
                $pay_type = $request->pay_type;
                $description = $request->description;
                if (count($amounts) != 0) {
                    foreach ($amounts as $amount_key => $amount_value) {
                        /* begin transaction */
                        DB::beginTransaction();

                        $fee_amount   = $amount_value;
                        $fee_status   = $statuses[$amount_key];
                        $fee_due_date = $due_date[$amount_key];
                        $fee_payment_date = $payment_date[$amount_key];
                        $fee_pay_type = $pay_type[$amount_key];
                        $fee_description = $description[$amount_key];

                        if (isset($pay_ids[$amount_key]) && $pay_ids[$amount_key] != "") {
                            $payment = Payment::find($pay_ids[$amount_key]);
                        } else {
                            $payment = new Payment;
                        }
                        $payment->Pay_Bra_Id  = $request->branch;
                        $payment->Pay_Section = $request->section;
                        $payment->Pay_Cla_Id  = $classId->Cla_Id;
                        $payment->Pay_Date    = date('Y-m-d', strtotime(str_replace('/', '-', $fee_due_date)));
                        if (count($fee_payment_date)!=0) {
                            $payment->Payment_date    = date('Y-m-d', strtotime(str_replace('/', '-', $fee_payment_date)));
                        }
                        if ($fee_status == "remaining") {
                            $payment->Pay_Status = "remaining";
                        } elseif ($fee_status == "complate") {
                            $payment->Pay_Status = "complate";
                        }
                        if ($fee_pay_type == "cash") {
                            $payment->pay_type = "cash";
                        } elseif ($fee_pay_type == "cheque") {
                            $payment->pay_type = "cheque";
                        }
                        $payment->Pay_Amount = $fee_amount;
                        $payment->Pay_Std_Id = $request->student_id;
                        $payment->Pay_CreatedBy = Auth::user()->Use_Id;
                        $payment->Pay_CreatedAt = date('Y-m-d H:i:s');
                        $payment->Pay_Description = $fee_description;

                        if (isset($pay_ids[$amount_key]) && $pay_ids[$amount_key] != "") {
                            $payment->Pay_UpdatedBy   = Auth::user()->Use_Id;
                            $payment->Pay_UpdatedAt   = date('Y-m-d H:i:s');
                        }
                        if ($payment->save()) {
                            /* commit transaction */
                            DB::commit();
                        } else {
                            /* roll back transaction */
                            Log::error("ERROR IN FEES MODULE");
                            Log::error($request->all());
                            DB::rollback();
                        }
                    }
                }
                return redirect('Payment Master');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function update(Request $request)
    {
        try {
            $update_rules = [
                'branch'      => 'required',
                'class'       => 'required',
                'section'     => 'required',
                'student_id'  => 'required',
                'status'      => 'required',
                'pay_type'    => 'required',
                'amount'      => 'required',
                'description' => '',
            ];
            $validator = Validator::make($request->all(), $update_rules);
            if ($validator->fails()) {
                return back()->with('errors', $validator->errors())->withInput();
            } else {
                if ($request->branch == "none") {
                    return back()->with('branch', "Please Select Branch")->withInput();
                }
                if ($request->class == "none") {
                    return back()->with('class', "Please Select Class")->withInput();
                }
                if ($request->section == "none") {
                    return back()->with('section', "Please Select Section")->withInput();
                }
                $id         = $request->id;
                $branchId   = $request->branch;
                $classAndId = explode(",", $request->class);
                $class      = end($classAndId);
                $section    = $request->section;
                $classId    = ClassTbl::select(['Cla_Id'])->where('Cla_Bra_Id', $branchId)->where('Cla_Class', $class)->where('Cla_Section', $section)->first();
                // $classId->Cla_Id
                //$parentId->Use_Id
                if (Payment::where('Pay_Id', $id)) {
                    $payment = Payment::find($id);
                } else {
                    $payment = new Payment;
                }
                $payment->Pay_Bra_Id  = $request->branch;
                $payment->Pay_Section = $request->section;
                $payment->Pay_Cla_Id  = $classId->Cla_Id;
                $payment->Pay_Date    = date('Y-m-d', strtotime(str_replace('/', '-', $request->due_date)));
                $payment->Payment_date    = date('Y-m-d', strtotime(str_replace('/', '-', $request->payment_date)));
                if ($request->status == "remaining") {
                    $payment->Pay_Status = "remaining";
                } else {
                    $payment->Pay_Status = "complate";
                }
                if ($request->pay_type == "cash") {
                    $payment->pay_type = "cash";
                } else {
                    $payment->pay_type = "cheque";
                }
                $payment->Pay_Description = $request->description;
                $payment->Pay_Amount      = $request->amount;
                $payment->Pay_Std_Id      = $request->student_id;
                $payment->Pay_CreatedBy   = Auth::user()->Use_Id;
                $payment->Pay_CreatedAt   = date('Y-m-d H:i:s');
                $payment->Pay_UpdatedBy   = Auth::user()->Use_Id;
                $payment->Pay_UpdatedAt   = date('Y-m-d H:i:s');
                if ($payment->save()) {
                    return redirect('Payment Master');
                } else {
                    return back();
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) {
                foreach ($ids as $id) {
                    Payment::where('Pay_Id', $id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getClass(Request $request)
    {
        $data = ClassTbl::where('Cla_Status', 1)->where('Cla_Bra_Id', $request->Cla_Bra_Id)->distinct()->get(['Cla_Class', 'Cla_Bra_Id']);
        return $data;
    }
    public function getSection(Request $request)
    {
        $data = ClassTbl::where('Cla_Status', 1)->where('Cla_Class', $request->Cla_Class)->where('Cla_Bra_Id', $request->Cla_Bra_Id)->get();
        return $data;
    }
    public function active(Request $request)
    {
        try {
            $all_data   = $request->except('_token');
            $chk_update = Ajax::studentStatus($request);
            $whatIWant  = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if (is_numeric($whatIWant)) {
                $data['student'] = Student::join('class_tbl', 'student_tbl.Std_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'student_tbl.Std_Parent_Id', '=', 'user_tbl.Use_Id')
                    ->join('user_tbl as u', 'student_tbl.Std_CreatedBy', '=', 'u.Use_Id')
                    ->select('student_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName', 'u.Use_Name as createdByName')
                    ->paginate(10, ['*'], 'page', $whatIWant);
            } else {
                $data['student'] = Student::join('class_tbl', 'student_tbl.Std_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'student_tbl.Std_Parent_Id', '=', 'user_tbl.Use_Id')
                    ->join('user_tbl as u', 'student_tbl.Std_CreatedBy', '=', 'u.Use_Id')
                    ->select('student_tbl.*', 'class_tbl.*', 'user_tbl.Use_Name as parentName', 'u.Use_Name as createdByName')
                    ->paginate(10);
            }

            return view('Auth.payment.active', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function get_student_list(Request $request)
    {
        try {
            $branchId     = $request->branchId;
            $classId      = $request->className;
            $sectionId    = $request->sectionId;
            $classDetails = ClassTbl::where('Cla_Status', 1)->where('Cla_Class', $classId)->where('Cla_Bra_Id', $branchId)->where('Cla_Section', $sectionId)->get()->first();
            if ($classDetails) {
                $classId     = $classDetails->Cla_Id;
                $studentList = Student::select('Std_Id', 'Std_Name')->where('Std_Cla_Id', $classId)->orderBy("Std_Name")->get();
                return $studentList;
            } else {
                return "";
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function delete(Request $request)
    {
        try {
            if(Payment::find($request->pay_id)->delete()){
                return "true";
            } else{
                return "false";
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
