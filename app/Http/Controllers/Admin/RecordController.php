<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Model\Zone;
use App\Model\ClassTbl;
use App\Model\Student;
use App\Model\Report;
use App\Model\Branch;
use App\Model\Module;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use App\Model\Attendence;
use App\Helper\Ajax;
use App\User;
use Response;
use Auth;
use URL;
use DB;

class RecordController extends Controller
{
    public function index(Request $request){
    	try {
            if(Auth::user()->Use_Type == "1"){
            	$data['reports'] = Attendence::select('class_tbl.Cla_Class','class_tbl.Cla_Section','class_tbl.Cla_Id','branch_tbl.Brn_Name','student_tbl.Std_Name','student_tbl.Std_Id','attendance_tbl.Att_Pre_Abs','attendance_tbl.Att_Date')
            			->leftjoin('class_tbl','class_tbl.Cla_Id', '=', 'Att_Cla_Id')
            			->leftjoin('branch_tbl','branch_tbl.Brn_Id','=','class_tbl.Cla_Bra_Id')
            			->leftjoin('student_tbl','student_tbl.Std_Id','=','Att_Stu_Id')
            			->orderBy('attendance_tbl.Att_CreatedAt', 'DESC')
            			->get();	
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();

            }elseif(Auth::user()->Use_Type == "2"){
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();

                $teacher = UserRights::teacher();
                $data['reports'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')
                	->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')
                	->join('branch_tbl','Brn_Id','=','Cla_Bra_Id')
                	->join('attendance_tbl','Att_Stu_Id','=','Std_Id')
                	->whereIn('Std_Cla_Id',$teacher['AssignClass'])
                	->orderBy('Rpt_CreatedAt', 'DESC')
                	->paginate(10);
                
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                $data['reports'] = Report::leftjoin('student_tbl', 'Std_Id', '=', 'Rpt_Std_Id')
                		->join('class_tbl', 'Cla_Id', '=', 'Std_Cla_Id')
                		->join('attendance_tbl','Att_Stu_Id','=','Std_Id')
                		->join('branch_tbl','Brn_Id','=','Cla_Bra_Id')
                		->whereIn('Std_Cla_Id',$branch['branchClass'])
                		->orderBy('Rpt_CreatedAt', 'DESC')->paginate(10);
            }
			$data['CURight'] = UserRights::rights();
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	    	return view('auth.record.Attendance_report')->with($data);
    	}
    	 catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function search(Request $request) {
        try {
        	$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            return view('auth.record.Attendance_report', Ajax::searchRecord($request))->with($data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    
}
