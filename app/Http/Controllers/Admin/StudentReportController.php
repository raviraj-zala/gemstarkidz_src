<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helper\Exceptions;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\Zone;
use App\Model\ClassTbl;
use App\Model\Branch;
use App\Helper\Ajax;
use App\User;
use Auth;
use App\Model\Student;
use URL;
use DB;

class StudentReportController extends Controller
{
    public function index(Request $request){
    	try{
    		if(Auth::user()->Use_Type == "1"){
                
                $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->leftjoin('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->leftjoin('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                                ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','user_tbl.Use_Mobile_No as parentMobileno','branch_tbl.Brn_Name')
                                ->orderBy('Std_Id','DESC')
                                ->get();
                                
				$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();                                
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();

                return view('auth.record.student_report',$data);
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
               $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                ->whereIn("student_tbl.Std_Cla_Id",$branch["branchClass"])
                                ->leftjoin('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                                ->leftjoin('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                                ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','user_tbl.Use_Mobile_No as parentMobileno','branch_tbl.Brn_Name')
                                ->orderBy('Std_Id','DESC')
                                ->paginate(10);
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();                
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.student_report',$data);
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                            ->whereIn("student_tbl.Std_Cla_Id",$teacher['AssignClass'])
                            ->leftjoin('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                            ->leftjoin('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                            ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name')
                            ->orderBy('Std_Id','DESC')
                            ->paginate(10);
				$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();                            
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.student_report',$data);
            }
    	}
    	catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function search(Request $request) {
        try {
        	$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            return view('auth.record.student_report', Ajax::searchStudentReport($request))->with($data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
