<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Model\Rights;
use App\Helper\UserRights;
use App\Model\Socialactivity;
use DB;
use App\User;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Helper\Ajax;
use App\Model\Event;
use Auth;
use App\Model\Admin_assign_branch;

class IndexController extends Controller
{
    //
	public function index()
	{
		if(Auth::user()){
			if(Auth::user()->Use_Type==1){
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
				$data['CURight'] = array('Admin');
			}else{
				$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
				$user_right = Rights::where('Usr_Use_Id',Auth::user()->Use_Id)->get();
				$right = array();
				foreach ($user_right as $r) {
					array_push($right, $r->Usr_Mod_Id);
				}
				$data['CURight'] = array_unique($right);
			}
		}else{
			$data['CURight'] = array();
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
			return redirect('/');
		}
		return view('home',$data);
	}

	public function home()
	{
		$data['i'] = 1;
		if(Auth::user()->Use_Type=="1"){
			$data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
							->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
						      ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName')
						      ->where('Eve_Date',date('Y-m-d'))
						      ->orderBy('Eve_Id','DESC')
						      ->get();
			$data['social'] = Socialactivity::
				leftjoin('user_tbl','user_tbl.Use_Id','=','socialactivity_tbl.Soa_CreatedBy')
				->join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')
				->whereDate('Soa_CreatedAt',date('Y-m-d '))
				->select(['socialactivity_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name as createdByName'])
				->orderBy('Soa_Id','DESC')
				->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
        }elseif(Auth::user()->Use_Type=="5"){
	        $branch = Admin_assign_branch::where("Aab_Use_Id",Auth::user()->Use_Id)
	        			->select("Aab_Brn_Id")->get()->toArray();
            $data['event'] = Event::whereIn("Eve_Brn_Id",$branch)
            	->join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
            	->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
            	->leftjoin('branch_tbl','event_tbl.Eve_Brn_Id','=','branch_tbl.Brn_Id')
            	->where('Eve_Date',date('Y-m-d'))
			    ->orderBy('Eve_Id','DESC')
            	->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName','branch_tbl.*')
            	->get();
			$data['social'] = Socialactivity::
				whereIn('Soa_Branch',$branch)
				->leftjoin('user_tbl','user_tbl.Use_Id','=','socialactivity_tbl.Soa_CreatedBy')
				->join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')
				->whereDate('Soa_CreatedAt',date('Y-m-d '))
				->select(['socialactivity_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name as createdByName'])
				->orderBy('Soa_Id','DESC')
				->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
        }elseif(Auth::user()->Use_Type=="2"){
            $teacherBrnId = ClassTbl::where("Cla_Id",Auth::user()->Use_Cla_Id)->select("Cla_Bra_Id")->first()->Cla_Bra_Id;
            $teacherBrnAllClass = ClassTbl::where("Cla_Bra_Id",$teacherBrnId)->select("Cla_Id")->get()->toArray();
            $data['event'] = Event::
            		where("Eve_Brn_Id",$teacherBrnId)
            	->join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
            	->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
            	->whereIn("Eve_Cla_Id",$teacherBrnAllClass)
            	->where('Eve_Date',date('Y-m-d'))
            	->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName')
			    ->orderBy('Eve_Id','DESC')
			    ->get();
			$data['social'] = Socialactivity::
					whereIn("Soa_Cla_Id",$teacherBrnAllClass)
					->whereDate('Soa_CreatedAt',date('Y-m-d '))
					->leftjoin('user_tbl','user_tbl.Use_Id','=','socialactivity_tbl.Soa_CreatedBy')
					->select(['socialactivity_tbl.*','user_tbl.Use_Name as createdByName'])
					->orderBy('Soa_Id','DESC')
					->get();

			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
        }
		// if(Auth::user()){
		// 	if(Auth::user()->Use_Type==1){
		// 		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		// 		$data['CURight'] = array('Admin');
		// 	}else{
		// 		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		// 		$user_right = Rights::where('Usr_Use_Id',Auth::user()->Use_Id)->get();
		// 		$right = array();
		// 		foreach ($user_right as $r) {
		// 			array_push($right, $r->Usr_Mod_Id);
		// 		}
		// 		$data['CURight'] = array_unique($right);
		// 	}
		// }else{
		// 	$data['CURight'] = array();
		// 	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
		// 	return redirect('/');
		// }
		return view('home',$data);

	}
}
