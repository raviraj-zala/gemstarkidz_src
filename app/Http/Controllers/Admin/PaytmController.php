<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helper\Exceptions;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\Zone;
use App\Model\ClassTbl;
use App\Model\Branch;
use App\Model\Student;
use App\Model\Payment;
use App\Helper\Ajax;
use App\User;
use Auth;
use URL;

class PaytmController extends Controller
{
    public $rules = [
            'branch' =>  'required',
            'studentname'   =>  'required|regex:/^[A-Za-z ]+$/',
            'parentname'    =>  'required|regex:/^[A-Za-z ]+$/',
        ];

    public function index(){
        try{
           $data['student'] = Payment::join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                            ->join('user_tbl','payment_tbl.Pay_Parent_Id','=','user_tbl.Use_Id')
                            ->join('user_tbl as u','payment_tbl.Pay_CreatedBy','=','u.Use_Id')
                            ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name')
                            ->orderBy('Pay_Id','DESC')
                            ->paginate(10);
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.paytm.index',$data);
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function create(Request $request){
    	try{
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
    		$data['branch'] = Branch::where('Brn_Status',1)->get();
			$data['CURight']=UserRights::rights();
			return view('auth.paytm.TxnTest ',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }
     
    public function search(Request $request)
    {
        try {
            return view('auth.payment.search',Ajax::searchPayment($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function edit($id){
        try{
            $classId = Student::where('Std_Id',$id)->first();
            $data['branch'] = Branch::where('Brn_Status',1)->get();

            $branchId = ClassTbl::where('Cla_Id',$classId->Std_Cla_Id)->first();

            $data['std_class'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$branchId->Cla_Bra_Id)->distinct()->get(['Cla_Class','Cla_Bra_Id']);

            $data['std_section'] = ClassTbl::where('Cla_Status',1)->where('Cla_Class',$branchId->Cla_Class)->where('Cla_Bra_Id',$branchId->Cla_Bra_Id)->get(); 

            $data['student'] = Payment::where('Pay_Id',$id)
                            ->join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                            ->join('user_tbl','payment_tbl.Pay_Parent_Id','=','user_tbl.Use_Id')
                            ->select(['payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName'])
                            ->first();

            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.payment.edit',$data);
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function getParent(Request $request){
        $searchParentName = $request->get('term');
        $data = User::select('Use_Name')->where('Use_Type',4)->where('Use_Name','LIKE','%'.$searchParentName.'%')->get();
        $result=array();
        foreach ($data as $value){
                $result[] = ['value' => $value->Use_Name];
                }
        return response()->json($result);
    }
    public function store(Request $request){
        try {
            
            $formData = $request->all(); 
            return view('auth.paytm.pgRedirect',compact('formData'));
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function update(Request $request){
        try{
        	
        	$std_grno = Student::where('Std_Id',$request->id)->select('Std_Gr_No')->first();
        	if($std_grno['Std_Gr_No']==$request->grno){
        	}else{
        		$this->rules['grno'] =  'required';
        	}
        	
        	$validator = Validator::make($request->all(),$this->rules);
     
                if($validator->fails()){
                    return back()->with('errors',$validator->errors())->withInput();
                }else{
                    if($request->branch=="none"){
                        return back()->with('branch',"Please Select Branch")->withInput();
                    }
                    if($request->class=="none"){
                        return back()->with('class',"Please Select Class")->withInput();
                    }
                    if($request->section=="none"){
                        return back()->with('section',"Please Select Section")->withInput();
                    }
                    $branchId = $request->branch;
	            $classAndId = explode(",", $request->class);
	            $class = end($classAndId);
	            $section = $request->section;
	            $classId = ClassTbl::select(['Cla_Id'])->where('Cla_Bra_Id',$branchId)->where('Cla_Class',$class)->where('Cla_Section',$section)->first();
	            // $classId->Cla_Id
	
	            $parentId = User::select(['Use_Id'])->where('Use_Name',$request->parentname)->first();
	            //$parentId->Use_Id
	
	            $payment = Payment::find($request->id);
	            $payment->Pay_Bra_Id = $request->branch;
                $payment->Pay_Section = $request->section;
                $payment->Pay_Cla_Id = $classId->Cla_Id;
                $payment->Pay_Gr_No = $request->grno;
                $payment->Pay_Stu_Name = $request->studentname;
                $payment->Pay_Parent_Id = $parentId->Use_Id;
                $payment->Pay_Date = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
                $payment->Pay_Status = $request->status;
                $payment->Pay_Type = $request->pay_type;
                $payment->Pay_Description = $request->description;
                $payment->Pay_Amount = $request->amount;
                $payment->Pay_UpdatedBy = Auth::user()->Use_Id;
   		        $payment->Pay_UpdatedAt = date('Y-m-d H:i:s');
               }
            
            if($payment->update()){
                return redirect('Payment Master');
            }else{
                return back();
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    Payment::where('Pay_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function getClass(Request $request){
        $data = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$request->Cla_Bra_Id)->distinct()->get(['Cla_Class','Cla_Bra_Id']);
        return $data;
    }
    public function getSection(Request $request){
        $data = ClassTbl::where('Cla_Status',1)->where('Cla_Class',$request->Cla_Class)->where('Cla_Bra_Id',$request->Cla_Bra_Id)->get();
        return $data;
    }
    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::studentStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                            ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                            ->join('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                            ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName')
                            ->paginate(10,['*'],'page',$whatIWant);
            else
                $data['student'] = Student::join('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                            ->join('user_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')
                            ->join('user_tbl as u','student_tbl.Std_CreatedBy','=','u.Use_Id')
                            ->select('student_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName')
                            ->paginate(10);
            return view('Auth.payment.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

}
