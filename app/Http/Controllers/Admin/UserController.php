<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Model\Zone;
use App\Model\Branch;
use App\Model\Module;
use App\Model\Rights;
use App\Helper\Ajax;
use App\Model\ClassTbl;
use App\Helper\Exceptions;
use App\Model\Student;
use App\Helper\UserRights;
use App\Model\Teacher_assign_class;
use App\Model\RouteTbl;
use App\Model\Admin_assign_branch;
use URL;
use DB;

class UserController extends Controller
{
    protected $rules = ([
            'name' => 'required',
            'userType' => 'required',
            'address' => '',
            'city' => '',
            'state' => '',
            'country' => '',
            'mobile' => 'required|numeric|regex:/[0-9]{10}/',
        ]);

    public $customMessage = ([
        'mobile.numeric' => 'Mobile number must be digit',
        'mobile.regex' => 'Mobile number must be 10 digit',
        'phone.numeric' => 'Phone number must be digit',

    ]);

    //User Information
    public function index()
    {
        try {
            $data['i'] = 1;

            // $data['user'] = User::paginate(10);
            if(Auth::user()->Use_Type == "1"){
                $data['user'] = DB::table('user_tbl as u')
                		->orWhere('cBy.Use_Type','1')
                		->orWhere('cBy.Use_Type','2')
                		->orWhere('cBy.Use_Type','3')
                        ->orWhere('cBy.Use_Type','5')
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $driver = RouteTbl::where("Rou_Brn_Id",$teacher["branchAccess"])->select("Rou_Use_Id")->get()->toArray();
                // $assign_class = Teacher_assign_class::where("Tac_Use_Id",Auth::user()->Use_Id)->select("Tac_Cla_Id")->get()->toArray();
                $data['user'] = DB::table('user_tbl as u')
                        // ->orWhere('cBy.Use_Type','1')
                        // ->where('cBy.Use_Type','2')
                        ->orWhereIn('cBy.Use_Id',$driver)
                        ->OrWhereIn('cBy.Use_Cla_Id',$teacher["branchClass"])
                        // ->orWhere('cBy.Use_Type','5')
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $driver = RouteTbl::where("Rou_Brn_Id",$branch["branchAccess"])->select("Rou_Use_Id")->get()->toArray();
                $data['user'] = DB::table('user_tbl as u')
                        // ->orWhere('cBy.Use_Type','2')
                        ->orWhereIn('cBy.Use_Id',$driver)
                        // ->orWhere('cBy.Use_Type','5')
                        ->OrWhereIn('cBy.Use_Cla_Id',$branch["branchClass"])
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','u.Use_Id')
                        ->select('u.Use_Id','u.Use_Name as createdByName','cBy.*')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.User.index',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //User Search
    public function search(Request $request)
    {
        try {
            return view('auth.User.search',Ajax::searchUser($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //User Creation Form
    public function create()
    {
    	try{
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            if(Auth::user()->Use_Type == "1"){
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('branch_tbl.Brn_Name')
                            ->get();

                $data['classes'] = Branch::where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                //             ->select('class_tbl.*','branch_tbl.*')
                //             ->where('class_tbl.Cla_Status','=',1)
                //             ->where('branch_tbl.Brn_Status','=',1)
                //             ->orderBy('class_tbl.Cla_Bra_Id')
                //             ->get();

                $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->where('Brn_Id',$teacher['branchAccess'])
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('branch_tbl.Brn_Name')
                            ->get();

                $branch_id = UserRights::teacher();
                $data['classes'] = Branch::where('Brn_Id',$branch_id['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                //             ->whereIn('Cla_Id',$teacher['branchClass'])
                //             ->select('class_tbl.*','branch_tbl.*')
                //             ->where('class_tbl.Cla_Status','=',1)
                //             ->where('branch_tbl.Brn_Status','=',1)
                //             ->orderBy('class_tbl.Cla_Id')
                //             ->get();

                $moduleAccess = UserRights::rights();
                $data['module'] = Module::where('Mod_Parent_Id','!=',0)->whereIn("Mod_Id",$moduleAccess)->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->whereIn('Brn_Id',$branch["branchAccess"])
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('branch_tbl.Brn_Name')
                            ->get();

                $branch_ids = UserRights::branchAdmin();
                $data['classes'] = Branch::whereIn('Brn_Id',$branch_ids['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                //             ->whereIn('Cla_Id',$branch["branchClass"])
                //             ->select('class_tbl.*','branch_tbl.*')
                //             ->where('class_tbl.Cla_Status','=',1)
                //             ->where('branch_tbl.Brn_Status','=',1)
                //             ->orderBy('class_tbl.Cla_Id')
                //             ->get();
                $moduleAccess = UserRights::rights();
                $data['module'] = Module::where('Mod_Parent_Id','!=',0)->whereIn("Mod_Id",$moduleAccess)->get();
            }
            $data['CURight']=UserRights::rights();
    		return view('auth.User.create',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    //User Creation 
    public function store(Request $request)
    {   
    	try {
	    	$this->rules['email'] =  'required|email|unique:user_tbl,Use_Email';
	        $this->rules['password'] =  'required|min:8';
	        if($request->phone!="") {
                $this->rules['phone'] = 'numeric';
            }
            $validator = Validator::make($request->all(),$this->rules,$this->customMessage);
 
    		if($validator->fails()){
    			return back()->with('errors',$validator->errors())->withInput();
    		}else{
	    		    if($request->userType=="none"){
	                    return back()->with('userType',"Please Select User Type.")->withInput();;
	                }
	                if($request->userType==2){
	                    if($request->teacherBranch=="none"){
	                        return back()->with('errors',$validator->errors())->with('teacherBranch','Please select branch.')->withInput();
	                    }elseif($request->class=="none"){
	                        return back()->with('errors',$validator->errors())->with('class','Please select class.')->withInput();
	                    }elseif($request->section=="none"){
	                        return back()->with('errors',$validator->errors())->with('section','Please select section')->withInput();
	                    }
	                }

                    $data = ([
                        "Use_Name" => $request->name,
                        "Use_Email" => $request->email,
                        "Use_Type" => $request->userType,
                        "Use_Password" => bcrypt($request->password),
                        "Use_Address" => $request->address,
                        "Use_City" => $request->city,
                        "Use_State" => $request->state,
                        "Use_Country" => $request->country,
                        "Use_Status" => 1,
                        "Use_Phone_No" => $request->phone,
                        "Use_Mobile_No" => $request->mobile,
                        "Use_CreatedBy" => Auth::user()->Use_Id,
                        "Use_CreatedAt" => date('Y-m-d H:i:s'),
                        "Use_UpdatedBy" => Auth::user()->Use_Id,
                        "Use_UpdatedAt" => date('Y-m-d H:i:s')
                    ]);
                    
                    if($request->userType==2){
	                    $temp = explode(",", $request->class);
	                    $cls = end($temp);
	                    $claId = ClassTbl::where('Cla_Bra_Id',$request->teacherBranch)
	                            ->where('Cla_Class',$cls)->where('Cla_Section',$request->section)->first();
                        if($request->permissionMobile!="") 
                        {
	                       $teacherData = ['Use_Cla_Id'=>$claId->Cla_Id,'Use_Show_Mobile' => 1];
                        }else{
                            $teacherData = ['Use_Cla_Id'=>$claId->Cla_Id];
                        }
	                    $data = array_merge($data, $teacherData);
	                }
                    $user = User::insertGetId($data);
                // Date 26-06-2018 upload this code in live project 
                //add multiple class in teacher side

                if($request->class_id)
                {
                    foreach ($request->class_id as $classvalue){
                        $branch_id = ClassTbl::where('Cla_Id',$classvalue)->get()->first()->Cla_Bra_Id;
                        $cla = new Teacher_assign_class;
                        $cla->Tac_Use_Id = $user;
                        $cla->Tac_Brn_Id = $branch_id;
                        $cla->Tac_Cla_Id = $classvalue;
                        $cla->Tac_CreatedBy = Auth::user()->Use_Id;
                        $cla->Tac_CreatedAt = date('Y-m-d H:i:s');
                        $cla->Tac_UpdatedBy = Auth::user()->Use_Id;
                        $cla->Tac_UpdatedAt = date('Y-m-d H:i:s');
                        $cla->save();
                    }
                }

                //add branch admin multiple
                if($request->branch)
                {
                    foreach ($request->branch as $branchvalue)
                    {
                        $brn = new Admin_assign_branch;
                        $brn->Aab_Use_Id = $user;
                        $brn->Aab_Brn_Id = $branchvalue;
                        $brn->Aab_CreatedBy = Auth::user()->Use_Id;
                        $brn->Aab_CreatedAt = date('Y-m-d H:i:s');
                        $brn->Aab_UpdatedBy = Auth::user()->Use_Id;
                        $brn->Aab_UpdatedAt = date('Y-m-d H:i:s');
                        $brn->save();
                    }
                }
               
                //end code
                if($user){
                    if(($request->module != null) || ($request->branch != null)){
                        foreach ($request->module as $mod) {
                            $rights = new Rights;       
                            $rights->Usr_Use_Id = $user;

                            if($request->branch != null){
                                foreach ($request->branch as $branch){
                                    $rights = new Rights;
                                    $rights->Usr_Use_Id = $user;

                                    $zone = Branch::where('Brn_Id',$branch)->first()->Brn_Zon_Id;

                                    $rights->Usr_Mod_Id = $mod;
                                    $rights->Usr_Zon_Id = $zone;
                                    $rights->Usr_Bra_Id = $branch;

                                    $rights->Usr_CreatedAt = date('Y-m-d H:i:s');
                                    $rights->Usr_UpdatedAt = date('Y-m-d H:i:s');

                                    $rights->save();
                                }
                            }
                            else{
                                foreach ($request->module as $mod) {
                                    $rights = new Rights;       
                                    $rights->Usr_Use_Id = $user;
                                    $rights->Usr_Mod_Id = $mod;

                                    $rights->Usr_CreatedAt = date('Y-m-d H:i:s');
                                    $rights->Usr_UpdatedAt = date('Y-m-d H:i:s');

                                    $rights->save();
                                }
                            }
                        }
                        return redirect('User Mgmt.');
                    }else{
                        return redirect('User Mgmt.');
                    }
                }
			}
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //User Edit Form
    public function edit($id)
    {
        try {   
        
            $userData = User::where('Use_Id',$id)->first();
            if(Auth::user()->Use_Type=="2"){
                $teacher = UserRights::teacher();
                $data['branchAccess'] = $teacher['branchAccess'];
                $data['user'] = User::where('Use_Id',$id)->first();
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->where('Brn_Id',$teacher['branchAccess'])
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('zone_tbl.Zon_Id')
                            ->get();

                $branch_id = UserRights::teacher();
                $data['classes'] = Branch::where('Brn_Id',$branch_id['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();


                // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                //             ->whereIn('Cla_Id',$teacher['branchClass'])
                //             ->select('class_tbl.*','branch_tbl.*')
                //             ->where('class_tbl.Cla_Status','=',1)
                //             ->where('branch_tbl.Brn_Status','=',1)
                //             ->orderBy('class_tbl.Cla_Id')
                //             ->get();
                $data['class_select'] = Teacher_assign_class::where('Tac_Use_Id',$id)->select('Tac_Cla_Id')->get();
                $data['teacherData'] = ClassTbl::where('Cla_Id',$userData->Use_Cla_Id)->first();
                $data['tec_class'] = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$data['teacherData']->Cla_Bra_Id)->distinct()->get(['Cla_Class','Cla_Bra_Id']);
                $data['tec_section'] = ClassTbl::where('Cla_Status',1)->where('Cla_Class',$data['teacherData']->Cla_Class)->where('Cla_Bra_Id',$data['teacherData']->Cla_Bra_Id)->get();
                $data['user_right'] = Rights::where('Usr_Use_Id',$id)->get();
                $data['CURight']=UserRights::rights();
                $moduleAccess = UserRights::rights();
                $data['module'] = Module::where('Mod_Parent_Id','!=',0)->whereIn("Mod_Id",$moduleAccess)->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                return view('auth.User.edit',$data);
            }

            if(Auth::user()->Use_Type=="5"){
                $branch = UserRights::branchAdmin();
                $data['branchAccess'] = "";
                $data['user'] = User::where('Use_Id',$id)->first();
                $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->whereIn('Brn_Id',$branch["branchAccess"])
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('zone_tbl.Zon_Id')
                            ->get();

                $branch_ids = UserRights::branchAdmin();
                $data['classes'] = Branch::whereIn('Brn_Id',$branch_ids['branchAccess'])->where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                 // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                 //            ->whereIn('Cla_Id',$branch["branchClass"])
                 //            ->select('class_tbl.*','branch_tbl.*')
                 //            ->where('class_tbl.Cla_Status','=',1)
                 //            ->where('branch_tbl.Brn_Status','=',1)
                 //            ->orderBy('class_tbl.Cla_Id')
                 //            ->get();

                $data['class_select'] = Teacher_assign_class::where('Tac_Use_Id',$id)->select('Tac_Cla_Id')->get();
                $data['user_right'] = Rights::where('Usr_Use_Id',$id)->get();
                $data['teacherData'] = ClassTbl::where('Cla_Id',$userData->Use_Cla_Id)->first();
                $data['tec_class'] = ClassTbl::where('Cla_Status',1)->whereIn('Cla_Id',$branch["branchClass"])->distinct()->get(['Cla_Class','Cla_Bra_Id']);
                $data['tec_section'] = ClassTbl::where('Cla_Status',1)->where("Cla_Id",$data['user']['Use_Cla_Id'])->distinct()->get();
                $data['CURight']=UserRights::rights();
                $moduleAccess = UserRights::rights();
                $data['module'] = Module::where('Mod_Parent_Id','!=',0)->whereIn("Mod_Id",$moduleAccess)->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                return view('auth.User.edit',$data);
            }

            if(Auth::user()->Use_Type=="1"){
                $data['user'] = User::where('Use_Id',$id)->first();
                if($data['user']['Use_Type']=="3"){
                    $data['branchAccess'] = "";
                    $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                ->select('zone_tbl.*','branch_tbl.*')
                                ->where('zone_tbl.Zon_Status','=',1)
                                ->where('branch_tbl.Brn_Status','=',1)
                                ->orderBy('zone_tbl.Zon_Id')
                                ->get();

                    $data['classes'] = Branch::where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                    // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                    //             ->select('class_tbl.*','branch_tbl.*')
                    //             ->where('class_tbl.Cla_Status','=',1)
                    //             ->where('branch_tbl.Brn_Status','=',1)
                    //             ->orderBy('class_tbl.Cla_Id')
                    //             ->get();

                    $data['class_select'] = array();
                    $data['user_right'] = Rights::where('Usr_Use_Id',$id)->get();
                    $data['teacherData'] = array();
                    $data['tec_class'] = array();
                    $data['tec_section'] = array();
                    $data['CURight']=UserRights::rights();
                    $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
                    $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                    return view('auth.User.edit',$data);
                }elseif($data['user']['Use_Type']=="2"){
                    $teacherClass = UserRights::teacherApi($id,"branchClass");
                    $teacherBranch = UserRights::teacherApi($id,"branchAccess");
                    $data['branchAccess'] = $teacherBranch;
                    $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                ->select('zone_tbl.*','branch_tbl.*')
                                ->where('zone_tbl.Zon_Status','=',1)
                                ->where('branch_tbl.Brn_Status','=',1)
                                ->orderBy('zone_tbl.Zon_Id')
                                ->get();
                    $data['classes'] = Branch::where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                    // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                    //             ->select('class_tbl.*','branch_tbl.*')
                    //             ->where('class_tbl.Cla_Status','=',1)
                    //             ->where('branch_tbl.Brn_Status','=',1)
                    //             ->orderBy('class_tbl.Cla_Id')
                    //             ->get();

                    $data['class_select'] = Teacher_assign_class::where('Tac_Use_Id',$id)->select('Tac_Cla_Id')->get();
                    $data['user_right'] = Rights::where('Usr_Use_Id',$id)->get();
                    $data['teacherData'] = ClassTbl::where('Cla_Id',$userData->Use_Cla_Id)->first();
                    $data['tec_class'] = ClassTbl::where('Cla_Status',1)->join('branch_tbl','branch_tbl.Brn_Id','=','class_tbl.Cla_Bra_Id')->where('Cla_Bra_Id',$teacherBranch)->get();
                    $data['tec_section'] = ClassTbl::where('Cla_Status',1)->where("Cla_Id",$data['user']['Use_Cla_Id'])->get();
                    $data['CURight']=UserRights::rights();
                    $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
                    $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                    return view('auth.User.edit',$data);
                }elseif($data['user']['Use_Type']=="5"){
                    $data['branchAccess'] = "";
                    // $teacherClass = UserRights::teacherApi($id,"branchClass");
                    $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                                ->select('zone_tbl.*','branch_tbl.*')
                                ->where('zone_tbl.Zon_Status','=',1)
                                ->where('branch_tbl.Brn_Status','=',1)
                                ->orderBy('zone_tbl.Zon_Id')
                                ->get();

                    $data['classes'] = Branch::where('Brn_Status','=',1)->orderBy('Brn_Id')->get();

                     // $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                     //            ->select('class_tbl.*','branch_tbl.*')
                     //            ->where('class_tbl.Cla_Status','=',1)
                     //            ->where('branch_tbl.Brn_Status','=',1)
                     //            ->orderBy('class_tbl.Cla_Id')
                     //            ->get();
                    $data['class_select'] = Teacher_assign_class::where('Tac_Use_Id',$id)->select('Tac_Cla_Id')->get();
                    $data['user_right'] = Rights::where('Usr_Use_Id',$id)->get();
                    $data['teacherData'] = ClassTbl::where('Cla_Id',$userData->Use_Cla_Id)->first();
                    $data['tec_class'] = ClassTbl::where('Cla_Status',1)->distinct()->get(['Cla_Class','Cla_Bra_Id']);
                    $data['tec_section'] = ClassTbl::where('Cla_Status',1)->get();
                    $data['CURight']=UserRights::rights();
                    $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
                    $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                    return view('auth.User.edit',$data);
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //User Data Update
    public function update(Request $request)
    {
        try {
	        if($request->phone!=""){
                $this->rules['phone'] =  'numeric';
            } 
            $old_mobile = User::where('Use_Id',$request->id)->get()->first()->Use_Mobile_No;
            $new_mobile = trim(strip_tags($request->mobile));
            if($new_mobile!="" && $new_mobile!=$old_mobile){
                $this->rules['mobile'] = 'numeric|regex:/[0-9]{10}/|unique:user_tbl,Use_Mobile_No';
            }
            $user_email = User::where('Use_Id',$request->id)->select('Use_Email')->first();
            if($user_email['Use_Email']==$request->email){
            }else{
                $this->rules['email'] =  'required|email|unique:user_tbl,Use_Email';
            }
            $validator = Validator::make($request->all(),$this->rules,$this->customMessage);
            if($validator->fails()){
               return back()->with('errors',$validator->errors())->withInput();
            }else{
                if($request->userType=="none"){
                    return back()->with('userType',"Please Select User Type.")->withInput();;
                }
                if($request->userType==2){
                    if($request->teacherBranch=="none"){
                        return back()->with('errors',$validator->errors())->with('teacherBranch','Please select branch.')->withInput();
                    }elseif($request->class=="none"){
                        return back()->with('errors',$validator->errors())->with('class','Please select class.')->withInput();
                    }elseif($request->section=="none"){
                        return back()->with('errors',$validator->errors())->with('section','Please select section')->withInput();
                    }
                }
            	
                Rights::where('Usr_Use_Id',$request->id)->delete();
                $data = ([
                    "Use_Name" => $request->name,
                    "Use_Email" => $request->email,
                    "Use_Type" => $request->userType,
                    "Use_Address" => $request->address,
                    "Use_City" => $request->city,
                    "Use_State" => $request->state,
                    "Use_Country" => $request->country,
                    "Use_Status" => 1,
                    "Use_Phone_No" => $request->phone,
                    "Use_Mobile_No" => $request->mobile,
                    "Use_UpdatedBy" => Auth::user()->Use_Id,
                    "Use_UpdatedAt" => date('Y-m-d H:i:s')
                ]);
                    
                 if($request->userType==2){
                    $temp = explode(",", $request->class);
                    $cls = end($temp);
                    $claId = ClassTbl::where('Cla_Bra_Id',$request->teacherBranch)
                            ->where('Cla_Class',$cls)->where('Cla_Section',$request->section)->first();
                    $teacherData = ['Use_Cla_Id'=>$claId->Cla_Id];
                    if($request->permissionMobile!="") 
                    {
                       $teacherData = ['Use_Cla_Id'=>$claId->Cla_Id,'Use_Show_Mobile' => 1];
                    }else{
                        $teacherData = ['Use_Cla_Id'=>$claId->Cla_Id];
                    }
                    $data = array_merge($data, $teacherData);
                }else{
                    $data = array_merge($data, ['Use_Cla_Id'=>0]);
                }
                $user = User::where('Use_Id',$request->id)->update($data);

                if($request->userType==2)
                {
                    Teacher_assign_class::where('Tac_Use_Id',$request->id)->delete();
                    if($request->class_id != ""){
                        foreach ($request->class_id as $classids){
                            $branch = ClassTbl::where('Cla_Id',$classids)->get()->first()->Cla_Bra_Id;
                            $cla = new Teacher_assign_class;
                            $cla->Tac_Use_Id = $request->id;
                            $cla->Tac_Brn_Id = $branch;
                            $cla->Tac_Cla_Id = $classids;
                            $cla->Tac_CreatedBy = Auth::user()->Use_Id;
                            $cla->Tac_CreatedAt = date('Y-m-d H:i:s');
                            $cla->Tac_UpdatedBy = Auth::user()->Use_Id;
                            $cla->Tac_UpdatedAt = date('Y-m-d H:i:s');
                            $cla->save();
                        }
                    }
                }

                 // Date 26-06-2018 upload this code in live project 
                if($request->userType==5){
                    Admin_assign_branch::where('Aab_Use_Id',$request->id)->delete();
                if($request->branch)
                {
                    foreach ($request->branch as $branchvalue){
                    $brn = new Admin_assign_branch;
                    $brn->Aab_Use_Id = $request->id;
                    $brn->Aab_Brn_Id = $branchvalue;
                    $brn->Aab_CreatedBy = Auth::user()->Use_Id;
                    $brn->Aab_CreatedAt = date('Y-m-d H:i:s');
                    $brn->Aab_UpdatedBy = Auth::user()->Use_Id;
                    $brn->Aab_UpdatedAt = date('Y-m-d H:i:s');
                    $brn->save();
                    }
                }
            }
               //end code
		
                if($user){
                    if(($request->module != null) || ($request->branch != null)){
                        foreach ($request->module as $mod) {
                            $rights = new Rights;   

                            $rights->Usr_Use_Id = $request->id;

                            if($request->branch != null){
                                foreach ($request->branch as $branch){
                                    $rights = new Rights;
                                    $rights->Usr_Use_Id = $request->id;

                                    $zone = Branch::where('Brn_Id',$branch)->first()->Brn_Zon_Id;

                                    $rights->Usr_Mod_Id = $mod;
                                    $rights->Usr_Zon_Id = $zone;
                                    $rights->Usr_Bra_Id = $branch;

                                    $rights->Usr_CreatedAt = date('Y-m-d H:i:s');
                                    $rights->Usr_UpdatedAt = date('Y-m-d H:i:s');

                                    $rights->save();
                                }
                            }
                            else{
                                foreach ($request->module as $mod) {
                                    $rights = new Rights;       
                                    $rights->Usr_Use_Id = $request->id;
                                    $rights->Usr_Mod_Id = $mod;

                                    $rights->Usr_CreatedAt = date('Y-m-d H:i:s');
                                    $rights->Usr_UpdatedAt = date('Y-m-d H:i:s');

                                    $rights->save();
                                }
                            }
                        }
                        return redirect('User Mgmt.');
                    }else{
                        return redirect('User Mgmt.');
                    }
                }else{ return redirect('User Mgmt.'); }
                
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //User Deletion 
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    if(User::where('Use_Id',$id)->where('Use_Type','4')->exists()){
                        if(Student::where('Std_Parent_Id',$id)->exists()){
                            return back()->with('error','Parent not detele.');
                        } else{
                            User::where('Use_Id',$id)->delete();
                            Teacher_assign_class::where('Tac_Use_Id',$id)->delete();
                            Admin_assign_branch::where('Aab_Use_Id',$id)->delete();
                        }
                    } else{
                        User::where('Use_Id',$id)->delete();
                        Teacher_assign_class::where('Tac_Use_Id',$id)->delete();
                        Admin_assign_branch::where('Aab_Use_Id',$id)->delete();
                    }

                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::userStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['user'] = User::paginate(10,['*'],'page',$whatIWant);
            else
                $data['user'] = User::paginate(10);
            return view('Auth.User.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function changepassword()
    {
        try{
            $data['user'] = User::where('Use_Id',Auth::user()->Use_Id)->get()->first();
            $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.User.changepassword',$data);
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function password_change_user($id)
    {
        try{
            
            $data['user'] = User::where('Use_Id',$id)->get()->first();
            $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.User.password_change_user',$data);
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }

    public function save_password(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(),$this->rules);
            $pass = User::where('Use_Id',Auth::user()->Use_Id)->get()->first()->Use_Password;
            $password = $request->old_password;
            if(Hash::check($password,$pass))
            {

                $data = ([
                "Use_Password" => bcrypt($request->password),
                "Use_UpdatedBy" => Auth::user()->Use_Id,
                "Use_UpdatedAt" => date('Y-m-d H:i:s')
                ]); 

                $user = User::where('Use_Id',$request->userid)->update($data);

                if($user)
                {
                    return redirect('User Mgmt.');
                }else{
                    return redirect(URL::previous());
                }
                }else{
                     return redirect('changepassword')->with('old_password','old password is not match');
                }
            
        } catch (\Exception $e) {
        Exceptions::exception($e);
        }   
    }

    public function change_password(Request $request)
    {
        try
        {  
            $data = ([
            "Use_Password" => bcrypt($request->password),
            "Use_UpdatedBy" => Auth::user()->Use_Id,
            "Use_UpdatedAt" => date('Y-m-d H:i:s')
            ]); 

            $user = User::where('Use_Id',$request->userid)->update($data);

            if($user)
            {
                return redirect('User Mgmt.');
            }else{
                return redirect(URL::previous());
            }
            
        } catch (\Exception $e) {
        Exceptions::exception($e);
        }
    }

    public function getClassId(Request $request)
    {
        try {
            $branch_id = $request->branch;
            $temp = explode(",", $request->classIdName);
            $className = end($temp) ;
            $section = $request->section;
            $classid = ClassTbl::where('Cla_Bra_Id',$branch_id)->where('Cla_Class',$className)->where('Cla_Section',$section)->first()->Cla_Id;
            return $classid;
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
