<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\UserRights;
use App\Model\Module;
use App\Model\ClassTbl;
use App\User;
use App\Model\Zone;
use App\Model\Branch;
use App\Model\Rights;
use App\Helper\Exceptions;
use Illuminate\Support\Facades\Validator;
use Response;
use Auth;
use App\Helper\Ajax;
use URL;
use App\Model\Student;
use DB;

class ParentController extends Controller
{
	protected $rules=([
			'name' =>	'required|regex:/^[A-Za-z ]+$/',
            'city' => '',
            'state' => '',
            'country' => '',
            'mobile' => 'required|numeric|regex:/[0-9]{10,15}/',
            'image'	=>  'image'
		]);
	
	protected $customValidation=([
            'mobile.numeric' => 'Mobile number must be digit',
            'mobile.unique' => 'Mobile number already exists.',
            'mother_mobile.unique' => 'Mobile number already exists.',
            'mobile.regex' => 'Mobile number must be 10 digit',
            'email.email' => 'Invalid Email',
            'email.unique' => 'Email already exists.',
            'mother_email.email' => 'Invalid Email.',
            'mother_email.unique' => 'Email already exists.',
			'city.regex'=>'The city may contain only latters.',
			'state.regex'=>'The state may contain only latters.',
			'country.regex'=>'The country may contain only latters.',
        ]);
	public function index(){
		try{

            // Admin Side Index View
            if(Auth::user()->Use_Type == "1"){
                $data['parent'] = DB::table('user_tbl as user')
                        ->where('cBy.Use_Type',4)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.parent.index',$data);
            }elseif(Auth::user()->Use_Type == "2")
            {
                $teacher = UserRights::teacher();
                $parent_ids = Student::whereIn("Std_Cla_Id",$teacher["AssignClass"])->select("Std_Parent_Id")->get()->toArray();
                $data['parent'] = DB::table('user_tbl as user')
                        ->where('cBy.Use_Type',4)
                        ->whereIn('cBy.Use_Id',$parent_ids)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.parent.index',$data);
            }elseif(Auth::user()->Use_Type == "5")
            {
                $branch = UserRights::branchAdmin();
                $class_ids = ClassTbl::whereIn("Cla_Bra_Id",$branch)->select("Cla_Id")->get()->toArray();
                $parent_ids = Student::whereIn("Std_Cla_Id",$class_ids)->select("Std_Parent_Id")->get()->toArray();
                $data['parent'] = DB::table('user_tbl as user')
                        ->where('cBy.Use_Type',4)
                        ->whereIn('cBy.Use_Id',$parent_ids)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.parent.index',$data);
            }

		}catch(\Exception $e){
			Exceptions::exception($e);
		}
	}

    public function create(){
    	try{
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
			$data['CURight']=UserRights::rights();
    		return view('auth.parent.create',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    public function search(Request $request)
    {
        try {
            return view('auth.parent.search',Ajax::searchParent($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    
    public function store(Request $request)
    {
    	try{
            $this->rules['email'] =  'required|email|unique:user_tbl,Use_Email';
            $this->rules['mobile'] =  'required|numeric|regex:/[0-9]{10}/|unique:user_tbl,Use_Mobile_No';
            if($request->mother_email!=""){
    		  $this->rules['mother_email'] =  'email|unique:user_tbl,Use_Mother_Email';
            }
            if(abs(ceil($request->mother_mobile))!="null" ){
                $this->rules['mother_mobile'] =  'unique:user_tbl,Use_Mother_Mobile_No|regex:/[0-9]{10}/';
            }
    		$this->rules['password'] =  'required|min:6';
    		$validator = Validator::make($request->all(),$this->rules,$this->customValidation);
    		if($validator->fails()){
    			return back()->with('errors',$validator->errors())->withInput();
    		}else{

    			if($request->has('image')){
    				$image = $request->file('image');
    				$imageName = time().$image->getClientOriginalName();
					$image->move(public_path('images/profile'),$imageName);
    			}else{
    				$imageName = '';
    			}

    			 $parent = new User;
    		 	 $parent->Use_Name = $request->name;
                 $parent->Use_Email	=	$request->email;
                 $parent->Use_Mother_Name = $request->mother_name;
                 $parent->Use_Mother_Email = $request->mother_email;
                 $parent->Use_Address = $request->address;
                 $parent->Use_City = $request->city;
                 $parent->Use_State = $request->state;
                 $parent->Use_Country = $request->country;
                 $parent->Use_Type = 4;
                 $parent->Use_Password = bcrypt($request->password);
                 $parent->Use_Mother_Password = bcrypt($request->mother_password);
                 $parent->Use_Image = $imageName;
                 $parent->Use_Status = $request->status;
                 $parent->Use_Mobile_No = $request->mobile;
                 $parent->Use_Mother_Mobile_No = $request->mother_mobile;
                 $parent->Use_CreatedBy = Auth::user()->Use_Id;
                 $parent->Use_CreatedAt = date('Y-m-d H:i:s');
                 $parent->Use_UpdatedBy = Auth::user()->Use_Id;
                 $parent->Use_UpdatedAt = date('Y-m-d H:i:s');
                 
                 if($parent->save()){
                 	return redirect('Parent Mgmt.');
                 }else{
                 	return back()->with('error','Parent Successfully Insert..');
                 }
    		}
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }
    public function edit($id)
    {
        try {   
            $data['branch'] = Branch::join('zone_tbl','zone_tbl.Zon_Id','=','branch_tbl.Brn_Zon_Id')
                            ->select('zone_tbl.*','branch_tbl.*')
                            ->where('zone_tbl.Zon_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('zone_tbl.Zon_Id')
                            ->get();
            $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['user'] = User::where('Use_Id',$id)->first();
            $data['user_right'] = Rights::where('Usr_Use_Id',$id)->get();
            $data['CURight']= UserRights::rights();

            return view('auth.parent.edit',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function update(Request $request)
    {
        try {
            $parentData = User::where('Use_Id',$request->id)->select('Use_Email','Use_Mother_Email','Use_Mobile_No','Use_Mother_Mobile_No')->first();
            if($parentData['Use_Email']!=$request->email){
                $this->rules['email'] =  'required|email|unique:user_tbl,Use_Email';
            }
            if($request->mother_email!="" && $parentData['Use_Mother_Email']!=$request->mother_email){
                $this->rules['mother_email'] =  'unique:user_tbl,Use_Mother_Email';
            }

            if($parentData['Use_Mobile_No']!=$request->mobile){
                $this->rules['mobile'] =  'unique:user_tbl,Use_Mobile_No';
            }
            if(abs(ceil($request->mother_mobile))!="null" && $parentData['Use_Mother_Mobile_No']!=$request->mother_mobile){
                $this->rules['mother_mobile'] =  'unique:user_tbl,Use_Mother_Mobile_No|unique:user_tbl,Use_Mobile_No';
            }
            $validator = Validator::make($request->all(),$this->rules,$this->customValidation);

            if($validator->fails()){
               return back()->with('errors',$validator->errors())->withInput();
            }else{
                if(User::where('Use_Id',$request->id)->exists()){
                 $parent = User::find($request->id);
    		 	 $parent->Use_Name = $request->name;
                 $parent->Use_Email	=	$request->email;
                 $parent->Use_Address = $request->address;
                 $parent->Use_City = $request->city;
                 $parent->Use_State = $request->state;
                 $parent->Use_Country = $request->country;
                 $parent->Use_Type = 4;
                 if($request->mother_mobile!=""){
                    $parent->Use_Mother_Mobile_No = $request->mother_mobile;
                 }else{
                    $parent->Use_Mother_Mobile_No = null;
                 }
                 if($request->mother_email!=""){
                    $parent->Use_Mother_Email = $request->mother_email;
                 }else{
                    $parent->Use_Mother_Email = "";
                 }
                 if($request->mother_name!=""){
                    $parent->Use_Mother_Name = $request->mother_name;
                 }else{
                    $parent->Use_Mother_Name = "";
                 }
                if($request->has('image')){
    				$image = $request->file('image');
    				$imageName = time().$image->getClientOriginalName();
					$image->move(public_path('images/profile'),$imageName);
                 	$parent->Use_Image = $imageName;
    			}

                 $parent->Use_Status = $request->status;
                 // $parent->Use_Mobile_No = $request->mobile;
                 $parent->Use_UpdatedBy = Auth::user()->Use_Id;
                 $parent->Use_UpdatedAt = date('Y-m-d H:i:s');

                if($parent->update()){
                    return redirect('Parent Mgmt.');
                 }else{
                    return back()->with('error','Something Wrong...');
                 }
                }else{
                    return redirect('Parent Mgmt.');
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    User::where('Use_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::userStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['parent'] = User::paginate(10,['*'],'page',$whatIWant);
            else
                $data['parent'] = User::paginate(10);
            return view('auth.parent.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function change_password_parents(Request $request)
    {
        try
        {  
            if($request->usertype=="parent"){
            $data = ([
            "Use_Password" => bcrypt($request->password),
            "Use_UpdatedBy" => Auth::user()->Use_Id,
            "Use_UpdatedAt" => date('Y-m-d H:i:s')
            ]); 
            }elseif($request->usertype=="mother"){
                $data = ([
                "Use_Mother_Password" => bcrypt($request->password),
                "Use_UpdatedBy" => Auth::user()->Use_Id,
                "Use_UpdatedAt" => date('Y-m-d H:i:s')
                ]); 
            }

            $user = User::where('Use_Id',$request->userid)->update($data);

            if($user)
            {
                return redirect('Parent Mgmt.');
            }else{
                return redirect('Parent Mgmt.');
            }
            
        } catch (\Exception $e) {
        Exceptions::exception($e);
        }
    }

    public function password_change_parents($id)
    {
        try{
            $data['user'] = User::where('Use_Id',$id)->get()->first();
            $data['module'] = Module::where('Mod_Parent_Id','!=',0)->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.parent.password_change_parent',$data);
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
    public function checkemail(Request $request)
    {
        try {
            if($request->femail!=""){
                $femail = trim($request->femail);
                if(User::where('Use_Email',$femail)->exists())
                {
                    return "Exists";
                }else{
                    return "NotExists";
                }
            }else{
                return "Else";
            }
        }catch(\Exception $e){
            Exceptions::exception($e);
        }
    }
}
