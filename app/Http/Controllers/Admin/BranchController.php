<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Helper\Ajax;
use App\Model\Zone;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Module;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use URL;

class BranchController extends Controller
{
    protected $rules = ([
            'zone' => 'required',
            'name' => 'required',
            'code' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|numeric|regex:/[0-9]{10,15}/',
            'status' => 'required'
        ]);

    //Branch Information
    public function index()
    {
    	try {
    		$data['i'] = 1;
            if(Auth::user()->Use_Type == "1"){
                $data['branch'] = Branch::with('zone')->orderBy('Brn_Id','DESC')->paginate(10);
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['branch'] = Branch::where('Brn_Id',$teacher["branchAccess"])->with('zone')->orderBy('Brn_Id','DESC')->paginate(10);
            }else if(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::whereIn('Brn_Id',$branch["branchAccess"])->with('zone')->orderBy('Brn_Id','DESC')->paginate(10);
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.Branch.index',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Search
    public function search(Request $request)
    {
        try {
            return view('auth.Branch.search',Ajax::searchBranch($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Branch Creation Form
    public function create()
    {
    	try{
    		$data['zone'] = Zone::where('Zon_Status',1)->orderBy('Zon_Name')->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.Branch.create',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    //Branch Creation
    public function store(Request $request)
    {
    	try {
    		$validator = Validator::make($request->all(),$this->rules,[
							        'mobile.numeric' => 'Mobile number must be digit',
							        'mobile.regex' => 'Mobile number must be 10 digit'
							    ]);
    		if($validator->fails()){
			return back()->with('errors',$validator->errors())->withInput();;
    		}else{
    			if($request->zone=="none"){
	                    return back()->with('zone',"Please Select Zone.")->withInput();;
	                }
                if($request->has('image')){
                        $image = $request->file('image');
                        $imageName = time().$image->getClientOriginalName();
                        $image->move(public_path('images/branch'),$imageName);
                    }else{
                        $imageName = "";
                    }
        		$branch = new Branch;

        		$branch->Brn_Zon_Id = $request->zone;
        		$branch->Brn_Name = $request->name;
        		$branch->Brn_Code = $request->code;
        		$branch->Brn_Address = $request->address;
        		$branch->Brn_Email = $request->email;
        		$branch->Brn_Mobile_No = $request->mobile;
                $branch->Brn_Image = $imageName;
        		$branch->Brn_Status = $request->status;
        		$branch->Brn_CreatedBy = Auth::user()->Use_Id;
        		$branch->Brn_CreatedAt = date('Y-m-d H:i:s');
        		$branch->Brn_UpdatedBy = Auth::user()->Use_Id;
        		$branch->Brn_UpdatedAt = date('Y-m-d H:i:s');

        		if($branch->save()){
        			return redirect('Branch Master');
        		}else{
        			return redirect('create_branch')->with('error','Branch Creation Fail');
        		}
            }


    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Edit Form
    public function edit($id)
    {
    	try{	
    		$data['zone'] = Zone::where('Zon_Status',1)->orderBy('Zon_Name')->get();
	    	$data['branch'] = Branch::where('Brn_Id',$id)->first();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
	    	return view('auth.Branch.edit',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Data Update
    public function update(Request $request)
    {
    	try {
    		$validator = Validator::make($request->all(),$this->rules,['mobile.numeric' => 'Mobile number must be digit',
    			'mobile.regex' => 'Mobile number must be 10 digit']);

    		if($validator->fails()){
    			return back()->with('errors',$validator->errors())->withInput();
    		}                
        		$branch['Brn_Zon_Id'] = $request->zone;
        		$branch['Brn_Name'] = $request->name;
        		$branch['Brn_Code'] = $request->code;
        		$branch['Brn_Address'] = $request->address;
        		$branch['Brn_Email'] = $request->email;
                if($request->has('image')){
                    $image = $request->file('image');
                    $imageName = time().$image->getClientOriginalName();
                    $image->move(public_path('images/branch'),$imageName);
                    $branch['Brn_Image'] = $imageName;
                }
        		$branch['Brn_Mobile_No'] = $request->mobile;
        		$branch['Brn_Status'] = $request->status;
			    $branch['Brn_UpdatedBy'] = Auth::user()->Use_Id;
        		$branch['Brn_UpdatedAt'] = date('Y-m-d H:i:s');

        		if(Branch::where('Brn_Id',$request->id)->update($branch)){
    				return redirect('Branch Master');
    			}else{
    				return redirect('edit_branch/{id}')->with('Branch Updation Fail');
    			}
    	} catch (\Exception $e) {
    	   Exceptions::exception($e)	;
    	}
    }

    //Branch Deletion
    public function destroy(Request $request)
    {
    	try {
    		$all_data = $request->except('_token');

    		foreach ($all_data as $ids) 
    		{
    			foreach($ids as $id) 
				{
					Branch::where('Brn_Id',$id)->delete();
                    ClassTbl::where('Cla_Bra_Id',$id)->delete();
				}
    		}
    		return redirect()->back();
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }
    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::branchStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
                              ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
                              ->paginate(10,['*'],'page',$whatIWant);
            else
                $data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
                              ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
                              ->paginate(10);
            return view('Auth.Branch.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
