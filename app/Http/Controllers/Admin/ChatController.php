<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Teacher_assign_class;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\Student;
use App\Model\Branch;
use App\Helper\Exceptions;
use App\Model\ClassTbl;
use App\Model\GroupMaster;
use App\Model\GroupDetail;
use App\Model\Chat;
use App\User;
use Validator;
use Exception;
use Auth;
use App\Model\Group;

class ChatController extends Controller
{
    public function index()
    {
        try {
            if(Auth::user()->Use_Type=="1"){
                // Admin
                $data['group'] = GroupMaster::orderby('group_master_tbl.Grp_Mas_Id','DESC')
                                ->select('group_master_tbl.*','user_tbl.Use_Name')
                                ->leftjoin('user_tbl','user_tbl.Use_Id','=','group_master_tbl.Grp_Mas_CreatedBy')
                                ->paginate(10);
            } elseif(Auth::user()->Use_Type=="5"){
            // Branch Admin

            } elseif(Auth::user()->Use_Type=="2"){
            // Teacher

            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.chat.index')->with($data);
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function create()
    {
        try {
            if(Auth::user()->Use_Type=="1"){
            // Admin
                $data['branch'] = Branch::select('Brn_Id','Brn_Name')->orderby('Brn_Name','ASC')->get();
            } elseif(Auth::user()->Use_Type=="5"){
            // Branch Admin

            } elseif(Auth::user()->Use_Type=="2"){
            // Teacher

            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.chat.create_group')->with($data);
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function get_teacher_parent(Request $request)
    {
        try {
            $branch_id = trim(strip_tags($request->id));
            if(Branch::where('Brn_Id',$branch_id)->exists()){    
                $class_ids = ClassTbl::select('Cla_Id')->where('Cla_Bra_Id',$branch_id)->get()->toArray();
                $teacher_ids = Teacher_assign_class::where("Tac_Brn_Id",$branch_id)->select("Tac_Use_Id")->get()->toArray();
                $data['teacher'] = User::select('Use_Id','Use_Name')->where('Use_Type','2')->whereIn('Use_Id',$teacher_ids)->get();
                $parent_ids = Student::select('Std_Parent_Id')->whereIn('Std_Cla_Id',$class_ids)->get()->toArray();
                $data['parent'] = User::select('Use_Id','Use_Name')->whereIn('Use_Id',$parent_ids)->get();
                return view('auth.chat.teacher_parent_list')->with($data);
            } else{
                return "false";
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function save_group(Request $request)
    {
        try {
            $rules = [
                'branch' => 'required',
                'name' => 'required',
            ];
            $customeMessage = [
                'branch.required' => 'Please select branch.',
                'name.required' => 'Please enter group name.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ){
                return back()->withInput()->with($validator->errors());
            } else{
                $branch = trim(strip_tags($request->branch));
                $name = trim(strip_tags($request->name));
                $status = trim(strip_tags($request->status));
                $group_master = New GroupMaster;
                $group_master->Grp_Mas_Name = $name;
                $group_master->Grp_Branch_Id = $branch;
                $group_master->Grp_Mas_Status = $status;
                $group_master->Grp_Mas_CreatedBy = Auth::user()->Use_Id;
                $group_master->Grp_Mas_CreatedAt = date('Y-m-d H:i:s');
                if($group_master->save()){
                    $group_master_id = $group_master->Grp_Mas_Id;
                    if($request->teacher){
                        $teacher_ids = $request->teacher;
                        foreach ($teacher_ids as $teacher) { 
                            $group_detail = New GroupDetail;
                            $group_detail->Grp_Det_Mas_Id = $group_master_id;
                            $group_detail->Grp_Det_Mem_Id = $teacher;
                            $group_detail->Grp_Det_CreatedBy = Auth::user()->Use_Id;
                            $group_detail->Grp_Det_CreatedAt = date('Y-m-d H:i:s');
                            $group_detail->save();
                        }
                    } 
                    if($request->parent){
                        $parent_ids = $request->parent;
                        foreach ($parent_ids as $parent) {
                            $group_detail = New GroupDetail;
                            $group_detail->Grp_Det_Mas_Id = $group_master_id;
                            $group_detail->Grp_Det_Mem_Id = $parent;
                            $group_detail->Grp_Det_CreatedBy = Auth::user()->Use_Id;
                            $group_detail->Grp_Det_CreatedAt = date('Y-m-d H:i:s');
                            $group_detail->save();  
                        }
                    }
                    return redirect()->route('chat.dashboard');
                } else{
                    return back()->withInput();
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);  
        }
    }

    public function groupDetail($group_id)
    {
        try {
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            if($group_id!="" && $group_id!=0){
                if(GroupMaster::where('Grp_Mas_Id',$group_id)->exists()){
                    $data['master'] = GroupMaster::where('Grp_Mas_Id',$group_id)->get()->first();
                    $data['detail'] = GroupDetail::select('group_detail_tbl.Grp_Det_Mem_Id','group_detail_tbl.Grp_Det_Id','Use_Name')
                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','group_detail_tbl.Grp_Det_Mem_Id')
                            ->where('group_detail_tbl.Grp_Det_Mas_Id',$group_id)
                            ->get();
                    return view('auth.chat.group_detail')->with($data);
                } else{
                    return redirect()->route('chat.dashboard');    
                }
            } else{
                return redirect()->route('chat.dashboard');
            }
        } catch (Exception $e) {
            Exceptions::exception($e);     
        }
    }

    public function groupAdd($group_id)
    {
        try {
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            if($group_id!="" && $group_id!=0){
                if(GroupMaster::where('Grp_Mas_Id',$group_id)->exists()){
                    $data['master'] = GroupMaster::where('Grp_Mas_Id',$group_id)->get()->first();
                    $data['details'] = GroupDetail::select('group_detail_tbl.Grp_Det_Mem_Id','group_detail_tbl.Grp_Det_Id as id_member','Use_Name','user_tbl.Use_Id')
                                ->leftjoin('user_tbl','user_tbl.Use_Id','=','group_detail_tbl.Grp_Det_Mem_Id')
                                ->where('group_detail_tbl.Grp_Det_Mas_Id',$group_id)
                                ->get();
                                
                    $branch_id = $data['master']['Grp_Branch_Id'];
                    $class_ids = ClassTbl::select('Cla_Id')->where('Cla_Bra_Id',$branch_id)->get()->toArray();
                    $teacher_ids = Teacher_assign_class::where("Tac_Brn_Id",$branch_id)->select("Tac_Use_Id")->get()->toArray();
                    $data['teacher'] = User::select('Use_Id','Use_Name')->where('Use_Type','2')->whereIn('Use_Id',$teacher_ids)->get();
                    $parent_ids = Student::select('Std_Parent_Id')->whereIn('Std_Cla_Id',$class_ids)->get()->toArray();
                    $data['parent'] = User::select('Use_Id','Use_Name')->whereIn('Use_Id',$parent_ids)->get();
                    dd($data);
                    return view('auth.chat.group_add')->with($data);
                } else{
                    return redirect()->route('chat.dashboard');    
                }
            } else{
                return redirect()->route('chat.dashboard');
            }    
        } catch (Exception $e) {
            Exceptions::exception($e);   
        }
    }

    public function groupChatOpen(Request $request)
    {
        try {
            $id = $request->id;
            if(GroupMaster::where('Grp_Mas_Id',$id)->exists()){
                $data['group_detail'] = GroupMaster::where('Grp_Mas_Id',$id)->get()->first();
            } else{
                return redirect()->route('chat.dashboard');
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.chat.group_open')->with($data);
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
    //Chat Deletion
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    GroupMaster::where('Grp_Mas_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    //Group Member Delete
    public function groupMemberDestroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    GroupDetail::where('Grp_Det_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    // Edit save group
    public function save_group_edit(Request $request)
    {
        try {
            $group_id = $request->group_id;
            if ($request->teacher) {
                $teacher_ids = $request->teacher;
                foreach ($teacher_ids as $teacher) {
                    if (GroupDetail::where('Grp_Det_Mem_Id',$teacher)->where('Grp_Det_Mas_Id',$group_id)->exists()) {
                            $group_detail = GroupDetail::where('Grp_Det_Mem_Id',$teacher)->where('Grp_Det_Mas_Id',$group_id)->first(); 
                    } else{
                        $group_detail = New GroupDetail;    
                    }
                    $group_detail->Grp_Det_Mas_Id = $group_id;
                    $group_detail->Grp_Det_Mem_Id = $teacher;
                    $group_detail->Grp_Det_CreatedBy = Auth::user()->Use_Id;
                    $group_detail->Grp_Det_CreatedAt = date('Y-m-d H:i:s');
                    $group_detail->save();
                }
            }
            if($request->parent){
                $parent_ids = $request->parent;
                foreach ($parent_ids as $parent) {
                    if(GroupDetail::where('Grp_Det_Mem_Id',$parent)->where('Grp_Det_Mas_Id',$group_id)->exists()){
                        $group_detail = GroupDetail::where('Grp_Det_Mem_Id',$parent)->where('Grp_Det_Mas_Id',$group_id)->first();
                    } else{
                        $group_detail = New GroupDetail;    
                    }
                    $group_detail->Grp_Det_Mas_Id = $group_id;
                    $group_detail->Grp_Det_Mem_Id = $parent;
                    $group_detail->Grp_Det_CreatedBy = Auth::user()->Use_Id;
                    $group_detail->Grp_Det_CreatedAt = date('Y-m-d H:i:s');
                    $group_detail->save();  
                }
            }
            return redirect()->route('chat.dashboard');
        }catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function destroyMemberGroup(Request $request)
    {
        try{
            if (GroupDetail::where('Grp_Det_Id',$request->Grp_Det_Mem_Id)->exists()) {
                $group_member_delete =GroupDetail::where('Grp_Det_Id',$request->Grp_Det_Mem_Id)->delete();
            }
        }
         catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function imageGroupUpload(Request $request)
    {
        try{
            $m_chat_type = 2;
            $group_master_id = $request->group_master_id;
            $senderId = $request->sender_id;
            $chat = new Group();
            $chat->Grp_Cha_Sender_Id = $senderId;
            $chat->Grp_Cha_Mas_Id = $group_master_id;
            $chat->Grp_Cha_Type = $m_chat_type;
            $chat->Grp_Cha_CreatedBy = $senderId;
            $chat->Grp_Cha_UpdatedBy = $senderId;
            if ($m_chat_type == 2) {
                if($request->has('image')) {
                    $file = $request->file('image');
                    $time      = md5(time());
                    $extension = $file->getClientOriginalExtension();
                    $image   = $time . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('chat/image/'), $image);
                    
                    $chat->Grp_Cha_Image = $image;
                    return $chat;
                }
            } 
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function videoGroupUpload(Request $request)
    {
        try{
            $m_chat_type = 1;
            $group_master_id = $request->group_master_id;
            $senderId = $request->sender_id;
            if ($m_chat_type == 1) {
                $chat = new Group();
                $chat->Grp_Cha_Sender_Id = $senderId;
                $chat->Grp_Cha_Mas_Id = $group_master_id;
                $chat->Grp_Cha_Type = $m_chat_type;
                $chat->Grp_Cha_CreatedBy = $senderId;
                $chat->Grp_Cha_UpdatedBy = $senderId;
                if($request->has('video')) {
                    $file = $request->file('video');
                    $time      = md5(time());
                    $extension = $file->getClientOriginalExtension();
                    $video   = $time . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('chat/video/'), $video);
                    $chat->Grp_Cha_Video = $video;
                    return $chat;
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function audioGroupUpload(Request $request)
    {
        try{
            $m_chat_type = 3;
            $group_master_id = $request->group_master_id;
            $senderId = $request->sender_id;
            if ($m_chat_type == 3) {
                $chat = new Group();
                $chat->Grp_Cha_Sender_Id = $senderId;
                $chat->Grp_Cha_Mas_Id = $group_master_id;
                $chat->Grp_Cha_Type = $m_chat_type;
                $chat->Grp_Cha_CreatedBy = $senderId;
                $chat->Grp_Cha_UpdatedBy = $senderId;
                if($request->has('audio')) {
                    $file = $request->file('audio');
                    $time      = md5(time());
                    $extension = $file->getClientOriginalExtension();
                    $audio   = $time . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('chat/audio/'), $audio);
                    $chat->Grp_Cha_Audio = $audio;
                    return $chat;
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function fileGroupUpload(Request $request)
    {
        try{
            $m_chat_type = 4;
            $group_master_id = $request->group_master_id;
            $senderId = $request->sender_id;
            if ($m_chat_type == 4) {
                $chat = new Group();
                $chat->Grp_Cha_Sender_Id = $senderId;
                $chat->Grp_Cha_Mas_Id = $group_master_id;
                $chat->Grp_Cha_Type = $m_chat_type;
                $chat->Grp_Cha_CreatedBy = $senderId;
                $chat->Grp_Cha_UpdatedBy = $senderId;
                if($request->has('file')) {
                    $file = $request->file('file');
                    $time      = md5(time());
                    $extension = $file->getClientOriginalExtension();
                    $file_doc   = $time . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('chat/docs/'), $file_doc);
                    $chat->Grp_Cha_Document = $file_doc;
                    return $chat;
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
