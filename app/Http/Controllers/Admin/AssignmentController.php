<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\Helper\Ajax;
use App\Model\Zone;
use App\Helper\Notification;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Helper\SMSHelper;
use App\Model\Student;
use App\Model\Module;
use App\Model\Homework;
use App\Model\Homework_view;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use URL;

class AssignmentController extends Controller
{
    protected $rules = ([
        ]);

    //Branch Information
    public function index()
    {
    	try {
    		$data['i'] = 1;
            if(Auth::user()->Use_Type == "1"){
                $data['homework'] = Homework::orderBy('Hmw_Id','DESC')->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')->select(['homework_tbl.*','class_tbl.Cla_Class','user_tbl.Use_Name'])->paginate(10);
            }
            elseif(Auth::user()->Use_Type == "2")
            {
                $teacher = UserRights::teacher();
                $data['homework'] = Homework::whereIn('Hmw_Cla_Id',$teacher['AssignClass'])->orderBy('Hmw_Id','DESC')->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')->select(['homework_tbl.*','class_tbl.Cla_Class','user_tbl.Use_Name'])->paginate(10);
            }
            elseif(Auth::user()->Use_Type == "5")
            {
                $branch = UserRights::branchAdmin();
                $data['homework'] = Homework::whereIn('Hmw_Cla_Id',$branch['branchClass'])->orderBy('Hmw_Id','DESC')->leftjoin('class_tbl','class_tbl.Cla_Id','=','homework_tbl.Hmw_Cla_Id')->leftjoin('user_tbl','user_tbl.Use_Id','=','homework_tbl.Hmw_CreatedBy')->select(['homework_tbl.*','class_tbl.Cla_Class','user_tbl.Use_Name'])->paginate(10);
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.Assignment.index',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Search
    public function search(Request $request)
    {
        try {
            return view('auth.Assignment.search',Ajax::searchAssignment($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Branch Creation Form
    public function create()
    {
    	try{
    		$data['zone'] = Zone::where('Zon_Status',1)->orderBy('Zon_Name')->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            if(Auth::user()->Use_Type == "1")
            {
            $data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('class_tbl.*','branch_tbl.*')
                            ->where('class_tbl.Cla_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('class_tbl.Cla_Id')
                            ->get();
            }
            if(Auth::user()->Use_Type == "2")
            {
            $teacher = UserRights::teacher();
            $data['class'] = Branch::whereIn("class_tbl.Cla_Id",$teacher["branchClass"])
                            ->join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('class_tbl.*','branch_tbl.*')
                            ->where('class_tbl.Cla_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('class_tbl.Cla_Id')
                            ->get();
            }
            if(Auth::user()->Use_Type == "5")
            {
            $branch = UserRights::branchAdmin();
            $data['class'] = Branch::whereIn("class_tbl.Cla_Id",$branch["branchClass"])
                            ->join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('class_tbl.*','branch_tbl.*')
                            ->where('class_tbl.Cla_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('class_tbl.Cla_Id')
                            ->get();
            }
            $data['CURight']=UserRights::rights();
    		return view('auth.Assignment.create',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    //Branch Creation
    public function store(Request $request)
    {
    	try {
            if($request->has('image')){
                $image = $request->file('image');
                $imageName = time().$image->getClientOriginalName();
                $image->move(public_path('images/homework'),$imageName);
            }else{
                $imageName = "";
            }
            if($request->class_id)
            {
                $date = $request->date;
                $subject = $request->subject;
               foreach($request->class_id as $classid)
               {
                    $branch = new Homework;
                    $branch->Hmw_Cla_Id = $classid;
                    $branch->Hmw_Subject = $request->subject;
                    $branch->Hmw_Page_No = "0";
                    $branch->Hmw_Desscription = $request->description;
                    $branch->Hmw_Class_Work = $request->class_work;
                    $branch->Hmw_Chapter = "0";
                    $branch->Hmw_Date = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
                    $branch->Hmw_File = $imageName;
                    $branch->Hmw_CreatedBy = Auth::user()->Use_Id;
                    $branch->Hmw_CreatedAt = date('Y-m-d H:i:s');
                    $branch->Hmw_UpdatedBy = Auth::user()->Use_Id;
                    $branch->Hmw_UpdatedAt = date('Y-m-d H:i:s');
                    $branch->save(); 
                    $lastid = $branch->Hmw_Id;
                    $class_id = $classid;
                    $teacher_name = User::where('Use_Id',Auth::user()->Use_Id)->first()->Use_Name;
                    $student = Student::where('Std_Cla_Id',$classid)->select('Std_Parent_Id')->get()->toArray();
                    $parent_id = User::whereIn('Use_Id',$student)->get();
                    
                    foreach ($parent_id as $value) {
                        $parent = new Homework_view;
                        $parent->Hwv_Use_Id = $value->Use_Id;
                        $parent->Hwv_Hmw_Id = $lastid;
                        $parent->Hwv_Flag = 0 ;
                        $parent->Hwv_CreatedBy = Auth::user()->Use_Id;
                        $parent->Hwv_UpdatedBy = Auth::user()->Use_Id;;
                        $parent->Hwv_CreatedAt = date('Y-m-d H:i:s');
                        $parent->Hwv_UpdatedAt = date('Y-m-d H:i:s');
                        $parent->save();
                            $student_name = Student::where("Std_Parent_Id",$value->Use_Id)->where("Std_Cla_Id",$class_id)->first()->Std_Name;
                        if($value["Use_Token"]!=""){
                            Notification::sendNotification($value["Use_Token"],$teacher_name." Assign Homework.","Date : ".$date."\nSubject : ".$subject."\nStudent Name : ".$student_name,"HOMEWORK","com.parentsbeep.ReminderActiviy");
                        }else if($value["Use_Mobile_No"]!=""){
                            SMSHelper::sendSMS($value["Use_Mobile_No"],$teacher_name." Assign Homework. Date : ".$date."\nSubject : ".$subject."\nStudent Name : ".$student_name);
                        }
                        if($value["Use_Mother_Token"]!=""){
                            Notification::sendNotification($value["Use_Mother_Token"],$teacher_name." Assign Homework.","Date : ".$date."\nSubject : ".$subject."\nStudent Name : ".$student_name,"HOMEWORK","com.parentsbeep.ReminderActiviy");
                        }else if($value["Use_Mother_Mobile_No"]!=""){
                            SMSHelper::sendSMS($value["Use_Mother_Mobile_No"],$teacher_name." Assign Homework. Date : ".$date."\nSubject : ".$subject."\nStudent Name : ".$student_name);   
                        }
                        }
                }
                if($branch)
                {
                    return redirect('Diary Mgmt.');
                }else{
                    return redirect('create_assignment')->with('error','Assignment Creation Fail');
                } 
            }
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Edit Form
    public function edit($id)
    {
    	try{	
            $data['homework'] = Homework::where('Hmw_Id',$id)->first();
    		$data['class'] = Branch::join('class_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                            ->select('class_tbl.*','branch_tbl.*')
                            ->where('class_tbl.Cla_Status','=',1)
                            ->where('branch_tbl.Brn_Status','=',1)
                            ->orderBy('class_tbl.Cla_Id')
                            ->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['file_ext'] = pathinfo($data['homework']['Hmw_File'], PATHINFO_EXTENSION);
            $data['CURight']=UserRights::rights();
	    	return view('auth.Assignment.edit',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //Branch Data Update
    public function update(Request $request)
    {
    	try {
            $branch = Homework::find($request->Hmw_Id);
            $branch->Hmw_Subject = $request->subject;
            $branch->Hmw_Page_No = "0";
            $branch->Hmw_Desscription = $request->description;
            $branch->Hmw_Class_Work = $request->class_work;
            $branch->Hmw_Chapter = "0";
            $branch->Hmw_Date = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
            if($request->has('image')){
                $image = $request->file('image');
                $imageName = time().$image->getClientOriginalName();
                $image->move(public_path('images/homework'),$imageName);
                $branch->Hmw_File = $imageName;
            }
            $branch->Hmw_UpdatedBy = Auth::user()->Use_Id;
            $branch->Hmw_UpdatedAt = date('Y-m-d H:i:s');
            if($branch->update()){
                return redirect('Diary Mgmt.');
            }else{
                return back();
            }
    	} catch (\Exception $e) {
    	   Exceptions::exception($e)	;
    	}
    }

    //Branch Deletion
    public function destroy(Request $request)
    {
    	try {
    		$all_data = $request->except('_token');

    		foreach ($all_data as $ids) 
    		{
    			foreach($ids as $id) 
				{
					Homework::where('Hmw_Id',$id)->delete();
				}
    		}
    		return redirect()->back();
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }
    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::branchStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
                              ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
                              ->paginate(10,['*'],'page',$whatIWant);
            else
                $data['branch'] = Branch::join('zone_tbl','branch_tbl.Brn_Zon_Id','=','zone_tbl.Zon_Id')
                              ->select('branch_tbl.*','zone_tbl.Zon_Name','branch_tbl.Brn_Name')
                              ->paginate(10);
            return view('Auth.Assignment.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
