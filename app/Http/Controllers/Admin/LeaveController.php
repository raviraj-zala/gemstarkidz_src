<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Leaverequest;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\User;
use Auth;
use DB;

class LeaveController extends Controller
{
    public function index($filter='student')
    {
    	try {
    		if(Auth::user()->Use_Type == "1" )
    		{
                if($filter=='student'){
                    $data['leaves'] = Leaverequest::where('Lea_User_Type','student')
                                    ->select('leave_request.*',
                                        DB::raw("CASE  WHEN leave_request.Lea_User_Type = 'student' THEN  student_tbl.Std_Name END as leave_user_name "),
                                        'branch_tbl.Brn_Name'
                                    )
                                    ->leftjoin('student_tbl','student_tbl.Std_Id','=','leave_request.Lea_User_Id')
                                    ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                    ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                    ->orderBy('Lea_Id','DESC')
                                    ->paginate(10);
                }elseif($filter=='teacher'){
                    $data['leaves'] = Leaverequest::where('Lea_User_Type','teacher')
                                    ->select('leave_request.*','teacher_assign_class_tbl.Tac_Brn_Id','u.Use_Id',
                                        DB::raw("CASE WHEN leave_request.Lea_User_Type = 'teacher' THEN  u.Use_Name END as leave_user_name "),
                                        'branch_tbl.Brn_Name'
                                    )
                                    ->leftjoin('user_tbl as u','u.Use_Id','=','leave_request.Lea_User_Id')
                                    ->leftjoin('teacher_assign_class_tbl','teacher_assign_class_tbl.Tac_Use_Id','=','leave_request.Lea_User_Id')
                                    ->leftjoin('branch_tbl','teacher_assign_class_tbl.Tac_Brn_Id','=','branch_tbl.Brn_Id')
                                    ->distinct()
                                    ->orderBy('Lea_Id','DESC')
                                    ->paginate(10);
                }
			}else if(Auth::user()->Use_Type == "2")
			{
                if($filter=='student'){
                    $classList = UserRights::teacher();
                    $studentList = Student::whereIn('Std_Cla_Id',$classList['AssignClass'])->select('Std_Id')->get()->toArray();
                    $data['leaves'] = Leaverequest::where('Lea_User_Type','student')->whereIn('Lea_User_Id',$studentList)->select('leave_request.*',DB::raw("CASE  WHEN leave_request.Lea_User_Type = 'student' THEN  student_tbl.Std_Name END as leave_user_name "))
                                    ->leftjoin('student_tbl','student_tbl.Std_Id','=','leave_request.Lea_User_Id')
                                    ->orderBy('Lea_Id','DESC')
                                    ->paginate(10);
                }elseif($filter=='teacher'){
                    $data['leaves'] = Leaverequest::where('Lea_User_Type','teacher')->where('Lea_User_Id',Auth::user()->Use_Id)->select('leave_request.*',DB::raw("CASE WHEN leave_request.Lea_User_Type = 'teacher' THEN  user_tbl.Use_Name END as leave_user_name "))
                                    ->leftjoin('user_tbl','user_tbl.Use_Id','=','leave_request.Lea_User_Id')
                                    ->orderBy('Lea_Id','DESC')
                                    ->paginate(10);
                }
			}else if(Auth::user()->Use_Type == "5")
			{
				if($filter=='student'){
                    $classList = UserRights::branchAdmin();
                    $studentList = Student::whereIn('Std_Cla_Id',$classList['branchClass'])->select('Std_Id')->get()->toArray();
                    $data['leaves'] = Leaverequest::where('Lea_User_Type','student')
                                    ->whereIn('Lea_User_Id',$studentList)
                                    ->select('leave_request.*',
                                        DB::raw("CASE  WHEN leave_request.Lea_User_Type = 'student' THEN  student_tbl.Std_Name END as leave_user_name "),
                                        'branch_tbl.Brn_Name'
                                    )
                                    ->leftjoin('student_tbl','student_tbl.Std_Id','=','leave_request.Lea_User_Id')
                                    ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                                    ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                    ->orderBy('Lea_Id','DESC')
                                    ->paginate(10);
                }elseif($filter=='teacher'){
                    $branch_cls = UserRights::branchAdmin();
                    $user_ids = UserRights::classTeacherIdList($branch_cls['branchClass']);
                    $data['leaves'] = Leaverequest::whereIn('Lea_User_Id',$user_ids)
                                    ->where('Lea_User_Type','teacher')
                                    ->select('leave_request.*','teacher_assign_class_tbl.Tac_Brn_Id','u.Use_Id',
                                        DB::raw("CASE WHEN leave_request.Lea_User_Type = 'teacher' THEN  u.Use_Name END as leave_user_name "),
                                        'branch_tbl.Brn_Name'
                                    )
                                    ->leftjoin('user_tbl as u','u.Use_Id','=','leave_request.Lea_User_Id')
                                    ->leftjoin('teacher_assign_class_tbl','teacher_assign_class_tbl.Tac_Use_Id','=','leave_request.Lea_User_Id')
                                    ->leftjoin('branch_tbl','teacher_assign_class_tbl.Tac_Brn_Id','=','branch_tbl.Brn_Id')
                                    ->orderBy('Lea_Id','DESC')
                                    ->distinct('Lea_User_Id')
                                    ->paginate(10);
                }
			}else{
				exit;
			}
			$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
			return view('auth.leave.index')->with($data);
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function create_teacher_leave()
    {
    	try {
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.leave.create_teacher_leave')->with($data);
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function save_teacher_leave(Request $request)
    {
    	try 
        {
    		$leave_type = $request->leave_type;
    		$reason = strip_tags(trim($request->reason));
    		$from_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->from_date)));
    		if($leave_type=="fullday"){
                $to_date = $request->to_date;
                $leaveReauest = new Leaverequest;
                $leaveReauest->Lea_Type = $leave_type;
                $leaveReauest->Lea_User_Type = 'teacher';
                $leaveReauest->Lea_From_Date = $from_date;
                if($to_date==""){
                    $leaveReauest->Lea_To_Date = $from_date;
                }else{
    			     $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->to_date)));
                    $leaveReauest->Lea_To_Date = $to_date;
                }
                $leaveReauest->Lea_User_Id = Auth::user()->Use_Id;
                $leaveReauest->Lea_Reason = $reason;
                $leaveReauest->Lea_Status = '0'; // 0 = pendding, 1 = approve
                $leaveReauest->Lea_CreatedAt = date('Y-m-d H:i:s');
                $leaveReauest->Lea_CreatedBy = Auth::user()->Use_Id;
                if($leaveReauest->save())
                {

                    return redirect('Leave Mgmt./teacher');
                }else{
                    return back()->withInput();
                }
    		}else if($leave_type == "halfday"){
                $to_date = $request->to_date;
                $from_time = trim($request->from_time);
                $to_time = trim($request->to_time);

                $leaveReauest = new Leaverequest;
                $leaveReauest->Lea_Type = $leave_type;
                $leaveReauest->Lea_User_Type = 'teacher';
                $leaveReauest->Lea_From_Date = $from_date;
                if($to_date==""){
                    $leaveReauest->Lea_To_Date = $from_date;
                }else{
                    $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->to_date)));
                    $leaveReauest->Lea_To_Date = $to_date;
                }
                $leaveReauest->Lea_From_Time = $from_time;
                $leaveReauest->Lea_To_Time = $to_time;
                $leaveReauest->Lea_User_Id = Auth::user()->Use_Id;
                $leaveReauest->Lea_Reason = $reason;
                $leaveReauest->Lea_Status = '0'; // 0 = pendding, 1 = approve
                $leaveReauest->Lea_CreatedAt = date('Y-m-d H:i:s');
                $leaveReauest->Lea_CreatedBy = Auth::user()->Use_Id;
                if($leaveReauest->save())
                {
                    return redirect('Leave Mgmt./teacher');
                }else{
                    return back()->withInput();
                }
    		}
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.leave.create_teacher_leave')->with($data);
    	}catch(\Exception $e) {	
			Exceptions::exception($e);
		}
    }
    public function confirm_leave($leave_id)
    {
        try {
            if(Leaverequest::find($leave_id))
            {
                $leaveReauest = Leaverequest::find($leave_id);
                $leaveReauest->Lea_Status = '1';     // 0 = Pendding , 1 = Approve Leave
                $leaveReauest->Lea_UpdatedAt = date('Y-m-d H:i:s');
                $leaveReauest->Lea_UpdatedBy = Auth::user()->Use_Id;
                if($leaveReauest->save())
                {
                    if($leaveReauest->Lea_User_Type == "student")
                    {
                        $student_parent_id = Student::where('Std_Id',$leaveReauest->Lea_User_Id)->get()->first();
                        $parentData = User::where('Use_Id',$student_parent_id->Std_Parent_Id)->get()->first();
                        if($parentData->Use_Token!=""){
                            Notification::sendNotification($parentData->Use_Token, "Leave Request Approve." ,$student_parent_id->Std_Name);
                        }else if($parentData->Use_Mobile_No!=""){
                            SMSHelper::sendSMS($parentData->Use_Mobile_No,$student_parent_id->Std_Name." Leave Request Approve.");
                        }
                        if($parentData->Use_Mother_Token!=""){
                            Notification::sendNotification($parentData->Use_Mother_Token,"Leave Request Approve.",$student_parent_id->Std_Name);
                        }else if($parentData->Use_Mother_Mobile_No!=""){
                            SMSHelper::sendSMS($parentData->Use_Mother_Mobile_No,$student_parent_id->Std_Name." Leave Request Approve.");
                        }
                        return back();
                    }else if($leaveReauest->Lea_User_Type == "teacher")
                    {
                        $teacherData = User::where('Use_Id',$leaveReauest->Lea_User_Id)->get()->first();
                        if($teacherData->Use_Token!=""){
                            Notification::sendNotification($teacherData->Use_Token, "Leave Request Approve." ,$teacherData->Use_Name);
                        }else if($teacherData->Use_Mobile_No!=""){
                            SMSHelper::sendSMS($teacherData->Use_Mobile_No,"Your Leave Request Approve.");
                        }

                        return back();
                    }
                }else{
                    return back();
                }
            }
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
    public function edit_teacher_leave($id)
    {
        try {
            $data['leave_data'] = Leaverequest::where('Lea_Id',$id)->get()->first();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.leave.edit_teacher_leave')->with($data);
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
    public function update_teacher_leave(Request $request){
        try {
            $leave_id = $request->leave_id;
            $leave_type = $request->leave_type;
            $reason = strip_tags(trim($request->reason));
            $from_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->from_date)));
            if($leave_type=="fullday"){
                $to_date = $request->to_date;
                $leaveReauest = Leaverequest::find($leave_id);
                $leaveReauest->Lea_Type = $leave_type;
                $leaveReauest->Lea_User_Type = 'teacher';
                $leaveReauest->Lea_From_Date = $from_date;
                if($to_date==""){
                    $leaveReauest->Lea_To_Date = $from_date;
                }else{
                     $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->to_date)));
                    $leaveReauest->Lea_To_Date = $to_date;
                }
                $leaveReauest->Lea_User_Id = Auth::user()->Use_Id;
                $leaveReauest->Lea_Reason = $reason;
                $leaveReauest->Lea_Status = '0'; // 0 = pendding, 1 = approve
                $leaveReauest->Lea_UpdatedAt = date('Y-m-d H:i:s');
                $leaveReauest->Lea_UpdatedBy = Auth::user()->Use_Id;
                if($leaveReauest->save())
                {
                    return redirect('Leave Mgmt./teacher');
                }else{
                    return back()->withInput();
                }
            }else if($leave_type == "halfday"){
                $to_date = $request->to_date;
                $from_time = trim($request->from_time);
                $to_time = trim($request->to_time);

                $leaveReauest = Leaverequest::find($leave_id);
                $leaveReauest->Lea_Type = $leave_type;
                $leaveReauest->Lea_User_Type = 'teacher';
                $leaveReauest->Lea_From_Date = $from_date;
                if($to_date==""){
                    $leaveReauest->Lea_To_Date = $from_date;
                }else{
                    $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->to_date)));
                    $leaveReauest->Lea_To_Date = $to_date;
                }
                $leaveReauest->Lea_From_Time = $from_time;
                $leaveReauest->Lea_To_Time = $to_time;
                $leaveReauest->Lea_User_Id = Auth::user()->Use_Id;
                $leaveReauest->Lea_Reason = $reason;
                $leaveReauest->Lea_Status = '0'; // 0 = pendding, 1 = approve
                $leaveReauest->Lea_UpdatedAt = date('Y-m-d H:i:s');
                $leaveReauest->Lea_UpdatedBy = Auth::user()->Use_Id;
                if($leaveReauest->save())
                {
                    return redirect('Leave Mgmt./teacher');
                }else{
                    return back()->withInput();
                }
            }
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
    public function leave_request_delete(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    Leaverequest::where('Lea_Id',$id)->delete();
                }
            }
            return redirect()->back();
        }catch(\Exception $e) { 
            Exceptions::exception($e);
        }
    }
}
