<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Student;
use App\Helper\UserRights;
use App\Model\ClassTbl;
use App\Model\Branch;
use App\Model\Subject;
use Auth;
use App\Model\Module;

class SubjectController extends Controller
{
    public function index()
    {
    	try {
	    	if(Auth::user()->Use_Type == "1")
	    	{
	    		$data['subject'] = Subject::orderBy('Sub_Id','DESC')
	    				->leftjoin('branch_tbl','branch_tbl.Brn_Code','=','subject_tbl.Sub_Brn_Code')
	        			->orderBy('Sub_Id','DESC')
	        			->paginate(10);
	    	}
	    	elseif(Auth::user()->Use_Type == "2")
	        {
	        	$teacher = UserRights::teacher();
	        	$branchCode = Branch::where('Brn_Id',$teacher["branchAccess"])->first()->Brn_Code;
	        	$data['subject'] = Subject::where('Sub_Brn_Code',$branchCode)
	        				->leftjoin('branch_tbl','branch_tbl.Brn_Code','=','subject_tbl.Sub_Brn_Code')
	        				->orderBy('Sub_Id','DESC')
	        				->paginate(10);
	        }
	        elseif(Auth::user()->Use_Type == "5")
	        {
	        	$branch = UserRights::branchAdmin();
	        	$branchCode = Branch::whereIn('Brn_Id',$branch["branchAccess"])->select(['Brn_Code'])->get()->toArray();
	        	$data['subject'] = Subject::whereIn('Sub_Brn_Code',$branchCode)
	        				->leftjoin('branch_tbl','branch_tbl.Brn_Code','=','subject_tbl.Sub_Brn_Code')
	        				->orderBy('Sub_Id','DESC')
	        				->paginate(10);
	        }
	        $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
	        $data['CURight'] = UserRights::rights();
	    	return view('auth.subject.index')->with($data);
    	}catch(\Exception $e){
			Exceptions::exception($e);
		}
    }
    public function viewSubjectFile($id)
    {
    	try {
    		$data['filedata'] = Subject::where('Sub_Id',$id)->first()->Sub_Json_Data;
    		return view('auth.subject.filedata')->with($data);
    	}catch(\Exception $e){
			Exceptions::exception($e);
		}
    }
    public function destory(Request $request)
    {
    	try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    Subject::where('Sub_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
