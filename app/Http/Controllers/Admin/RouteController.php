<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Module;
use App\User;
use App\Helper\UserRights;
use Illuminate\Support\Facades\Validator;
use App\Helper\Exceptions;
use App\Model\RouteTbl;
use Auth;
use App\Model\ClassTbl;
use App\Model\Student;
use App\Helper\Ajax;
use URL;
use App\Model\RouteAssign;

class RouteController extends Controller
{
	protected $rules = [
					'_token'	=>	'required',
					'branchId'	=>	'required',
					'driverId'	=>	'required',
					'routeName'	=>	'required',
					'addressFrom'	=>	'required',
					'addressTo'	=>	'required',
					'status'	=>	'required'
						];

	protected $customMessage = [];

    public function index(){
    	try {
            if(Auth::user()->Use_Type == "1"){
    		      $data['route'] = RouteTbl::
    				join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
    				->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
    				->orderBy('Rou_Id','DESC')
    				->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
    				->paginate(10);
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['route'] = RouteTbl::
                    where("route_tbl.Rou_Brn_Id",$teacher["branchAccess"])
                    ->join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
                    ->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
                    ->orderBy('Rou_Id','DESC')
                    ->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                    ->paginate(10);
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['route'] = RouteTbl::
                    where("route_tbl.Rou_Brn_Id",$branch["branchAccess"])
                    ->join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
                    ->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
                    ->orderBy('Rou_Id','DESC')
                    ->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                    ->paginate(10);
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.route.index',$data);
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function assignIndex(){
        try {
            if(Auth::user()->Use_Type == "1"){
                $data['route'] = RouteTbl::
                    join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
                    ->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
                    ->orderBy('Rou_Id','DESC')
                    ->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                    ->paginate(10);
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['route'] = RouteTbl::
                        where("route_tbl.Rou_Brn_Id",$teacher["branchAccess"])
                    ->join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
                    ->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
                    ->orderBy('Rou_Id','DESC')
                    ->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                    ->paginate(10);
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['route'] = RouteTbl::
                        where("route_tbl.Rou_Brn_Id",$branch["branchAccess"])
                    ->join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
                    ->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
                    ->orderBy('Rou_Id','DESC')
                    ->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
                    ->paginate(10);
            }
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
            return view('auth.route.assignIndex',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }   
    }

    public function search(Request $request)
    {
        try {
            return view('auth.route.search',Ajax::searchRoute($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function assignSearch(Request $request)
    {
        try {
            return view('auth.route.assignSearch',Ajax::searchRoute($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function create(Request $request){
    	try{
    		$data['driver'] = User::where('Use_Type',3)->orderBy('Use_Name','ASC')->get();
            if(Auth::user()->Use_Type == "1"){
              $data['driver'] = User::where('Use_Type',3)->orderBy('Use_Name','ASC')->get();
              $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['driver'] = User::where('Use_Type',3)->join('route_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')->where('Rou_Brn_Id',$teacher["branchAccess"])->orderBy('Use_Name','ASC')->get();
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['driver'] = User::where('Use_Type',3)->join('route_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')->whereIn('Rou_Brn_Id',$branch["branchAccess"])->orderBy('Use_Name','ASC')->get();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
            }
	    	$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	        $data['CURight']=UserRights::rights();
			return view('auth.route.create',$data);
		}catch(\Exception $e){
			Exceptions::exception($e);
		}
    }
    public function store(Request $request){
    	try{
			
    		$validator = Validator::make($request->all(),$this->rules);

			if($request->branchId == "none"){
				return back()->with('errors',$validator->errors())->with('branchError','Please Select Branch.')->withInput();
			}
			if($request->driverId == "none"){
				return back()->with('errors',$validator->errors())->with('driverError','Please Select Driver.')->withInput();
			}

    		if($validator->fails()){
    			return back()->with('errors',$validator->errors())->withInput();
    		}else{

				$branchId = trim($request->branchId);
				$driverId = trim($request->driverId);
				$routeName = trim($request->routeName);
				$addressFrom = trim($request->addressFrom);
				$addressTo	= trim($request->addressTo);
				$status = $request->status;


                $place = $request->addressFrom;
                $place = urlencode($place);
                $url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
                $ch = curl_init();//initiating curl
                curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_PROXYPORT,3128);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
                $response = curl_exec($ch);

                $response = json_decode($response);
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

                $route_from_lat = $response->results[0]->geometry->location->lat;
                $route_from_lng = $response->results[0]->geometry->location->lng;


                $place = $request->addressTo;
                $place = urlencode($place);
                $url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
                $ch = curl_init();//initiating curl
                curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_PROXYPORT,3128);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
                $response = curl_exec($ch);

                $response = json_decode($response);
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

                $route_to_lat = $response->results[0]->geometry->location->lat;
                $route_to_lng = $response->results[0]->geometry->location->lng;

				$route = new RouteTbl;
				$route->Rou_Brn_Id = $branchId;
				$route->Rou_Use_Id = $driverId;
				$route->Rou_Name = $routeName;
				$route->Rou_From = $addressFrom;
				$route->Rou_To = $addressTo;
                $route->Rou_From_Lat = $route_from_lat;
                $route->Rou_From_Lng = $route_from_lng;
                $route->Rou_To_Lat = $route_to_lat;
                $route->Rou_To_Lng = $route_to_lng;
				$route->Rou_Status = $status;
				$route->Rou_CreatedBy = Auth::user()->Use_Id;
				$route->Rou_CreatedAt = date("Y-m-d H:i:s");
				$route->Rou_UpdatedBy = Auth::user()->Use_Id;
				$route->Rou_UpdatedAt = date("Y-m-d H:i:s");

				if($route->save()){

            $class_id = ClassTbl::where('Cla_Bra_Id',$branchId)->select('Cla_Id')->get()->toArray();
            $student = Student::whereIn('Std_Cla_Id',$class_id)->select('Std_Parent_Id')->get()->toArray();
            $parenttoken = User::whereIn('Use_Id',$student)->select('Use_Token')->get();
            

            // if(count($parenttoken)){
            //     foreach($parenttoken as $key => $tokenid)
            //     {
            // $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
            // $tokenList[]=$tokenid['Use_Token'];
            

            // $notification = [
            //     'title'=> 'New Route',
            //     'body' => 'Route Name :"'.$routeName.'"',
            //     'sound' => true,
            // ];
            
            // $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

            // $fcmNotification = [
            //     'registration_ids' => $tokenList, //multple token array
            //     //'to'  => $$tokenList, //single token
            //     'notification' => $notification,
            //     'data' => $extraNotificationData
            // ];

            // $headers = [
            //     'Authorization: key=AAAAbFypH7M:APA91bFRq_uU5F9ANeilCqIMh-UjkIVtEqasvqJnJ-xiJzMDgX52T8uytdSvi0L8uwxF004Kw5w3R9jb0v395rAdikVEj9pAW3MoflcFDhXdvSQbhP5TIHJMErrYLfruqqQnizhbE36c',
            //     'Content-Type: application/json'
            // ];


            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL,$fcmUrl);
            // curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            // $result = curl_exec($ch);
            // curl_close($ch);
                   
            //     }
            // }
					// redirect Route Mgmt.
					return redirect('Route Mgmt.');
				}else{
					return back();
				}
    		}

    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    public function edit($id){
    	try{
    		
            if(Auth::user()->Use_Type == "1"){
    		  $data['driver'] = User::where('Use_Type',3)->orderBy('Use_Name','ASC')->get();
              $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['driver'] = User::where('Use_Type',3)->join('route_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')->where('Rou_Brn_Id',$teacher["branchAccess"])->orderBy('Use_Name','ASC')->get();
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['driver'] = User::where('Use_Type',3)->join('route_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')->whereIn('Rou_Brn_Id',$branch["branchAccess"])->orderBy('Use_Name','ASC')->get();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
            }
    		$data['route'] = RouteTbl::where('Rou_Id',$id)->first();
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	    	$data['CURight']=UserRights::rights();
	    	return view('auth.route.edit',$data);

    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    public function update(Request $request){
    	try{
    		$validator = Validator::make($request->all(),$this->rules);

    		if($validator->fails()){
				return back()->with('errors',$validator->errors())->withInput();
    		}else{
    			
    			$branchId = trim($request->branchId);
				$driverId = trim($request->driverId);
				$routeName = trim($request->routeName);
				$addressFrom = trim($request->addressFrom);
				$addressTo	= trim($request->addressTo);
				$status = $request->status;

                $place = $request->addressFrom;
                $place = urlencode($place);
                $url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
                $ch = curl_init();//initiating curl
                curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_PROXYPORT,3128);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
                $response = curl_exec($ch);

                $response = json_decode($response);
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

                $route_from_lat = $response->results[0]->geometry->location->lat;
                $route_from_lng = $response->results[0]->geometry->location->lng;


                $place = $request->addressTo;
                $place = urlencode($place);
                $url = "https://maps.google.com/maps/api/geocode/json?address=".$place."&sensor=false&key=AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk";
                $ch = curl_init();//initiating curl
                curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_PROXYPORT,3128);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
                $response = curl_exec($ch);

                $response = json_decode($response);
                $lat = $response->results[0]->geometry->location->lat;
                $lng = $response->results[0]->geometry->location->lng;

                $route_to_lat = $response->results[0]->geometry->location->lat;
                $route_to_lng = $response->results[0]->geometry->location->lng;

				$route = RouteTbl::find($request->Rou_Id);
				$route->Rou_Brn_Id = $branchId;
				$route->Rou_Use_Id = $driverId;
				$route->Rou_Name = $routeName;
				$route->Rou_From = $addressFrom;
				$route->Rou_To = $addressTo;
				$route->Rou_Status = $status;
                $route->Rou_From_Lat = $route_from_lat;
                $route->Rou_From_Lng = $route_from_lng;
                $route->Rou_To_Lat = $route_to_lat;
                $route->Rou_To_Lng = $route_to_lng;
				$route->Rou_UpdatedBy = Auth::user()->Use_Id;
				$route->Rou_UpdatedAt = date("Y-m-d H:i:s");

				if($route->update()){
					// redirect Route Mgmt.
					return redirect('Route Mgmt.');
				}else{
					return back()->with('errors',$validator->errors())->withInput();
				}
    		}
	    }catch(\Exception $e){
			Exceptions::exception($e);
    	}
    }
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) 
            {
                foreach($ids as $id) 
                {
                    RouteTbl::where('Rou_Id',$id)->delete();
                    RouteAssign::where('Ass_Rou_Id',$id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function active(Request $request)
    {
        try {
            $all_data = $request->except('_token');
            $chk_update = Ajax::routeStatus($request);       
            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if(is_numeric($whatIWant))
                $data['route'] = RouteTbl::
    				join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
    				->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
    				->orderBy('Rou_Id','DESC')
    				->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
    				->paginate(10,['*'],'page',$whatIWant);
            else
                $data['route'] = RouteTbl::
    				join('branch_tbl','route_tbl.Rou_Brn_Id','=','branch_tbl.Brn_Id')
    				->join('user_tbl','route_tbl.Rou_Use_Id','=','user_tbl.Use_Id')
    				->orderBy('Rou_Id','DESC')
    				->select('route_tbl.*','branch_tbl.Brn_Name','user_tbl.Use_Name')
    				->paginate(10);
            return view('auth.route.active',$data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function assignRoute(){
    	try{
            if(Auth::user()->Use_Type == "1"){
    		    $data['route'] = RouteTbl::orderBy('Rou_Name')->get();
            }elseif(Auth::user()->Use_Type == "2"){
                $teacher = UserRights::teacher();
                $data['route'] = RouteTbl::where('Rou_Brn_Id',$teacher["branchAccess"])->orderBy('Rou_Name')->get();
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['route'] = RouteTbl::whereIn('Rou_Brn_Id',$branch["branchAccess"])->orderBy('Rou_Name')->get();
            }
    		$data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
	    	$data['CURight']=UserRights::rights();
    		return view('auth.route.assign_route',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }

    public function getClass(Request $request)
    {
    	$routeData = RouteTbl::where('Rou_Id',$request->Rou_Id)->first();
    	$data = ClassTbl::where('Cla_Status',1)->where('Cla_Bra_Id',$routeData->Rou_Brn_Id)->distinct()->get(['Cla_Class','Cla_Bra_Id']);

        return $data;
    }
    public function getSection(Request $request){
        $data = ClassTbl::where('Cla_Status',1)->where('Cla_Class',$request->Cla_Class)->where('Cla_Bra_Id',$request->Cla_Bra_Id)->get();
        return $data;
    }
    public function getStudent(Request $request){
    	try{	
	    	$routeData = RouteTbl::where('Rou_Id',$request->Rou_Id)->first();
            if($request->Cla_Section!="none"){
                // $classID->Cla_Id   Class Id
                $classID = ClassTbl::where('Cla_Bra_Id',$routeData->Rou_Brn_Id)->where('Cla_Class',$request->Cla_Class)->where('Cla_Section',$request->Cla_Section)->first();

                //get this class student list
                $data['studentList'] = Student::where('Std_Cla_Id',$classID->Cla_Id)->get();
                $data['assignedRoute'] = RouteAssign::where('Ass_Rou_Id',$request->Rou_Id)->get();
            }else{
                $data['studentList'] = array();
                $data['assignedRoute'] = array();
            }
    		return view('auth.route.studentList',$data);
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }
    public function assign(Request $request){
    	try{
    		$routeId = $request->routeID;
    		if(count($request->student)>0){
    			$studentIds = RouteAssign::where('Ass_Rou_Id',$routeId)->select('Ass_Std_Id')->get();
				$student_id = array();
				foreach ($request->student as $std) {
					array_push($student_id, $std);
				}
				RouteAssign::where('Ass_Rou_Id',$routeId)->where('Ass_Sec_Name',$request->section)->delete();
				foreach ($request->student as $std) {
					$routeAssign = new routeAssign;
	    			$routeAssign->Ass_Rou_Id = $routeId;
                    $routeAssign->Ass_Sec_Name = $request->section;
	    			$routeAssign->Ass_Std_Id = $std;
	    			$routeAssign->save();
				}
                return redirect('Assign Route.');
    		}else{
                RouteAssign::where('Ass_Rou_Id',$routeId)->where('Ass_Sec_Name',$request->section)->delete();
    			return redirect('Assign Route.');
    		}
    	}catch(\Exception $e){
    		Exceptions::exception($e);
    	}
    }
}
