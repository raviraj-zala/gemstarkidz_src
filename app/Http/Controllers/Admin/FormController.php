<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;
use DB;
use App\User;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Module;
use App\Model\Student;
use App\Model\Subject;
use App\Model\Homework;
use App\Model\Attendence;
use App\Model\Event;
use URL;
use App\Helper\UserRights;
use App\Helper\Exceptions;
use Excel;

class FormController extends Controller {

    public $rules = [
        'exportType' => 'required',
    ];

    public function index() {
        try {
            $data['branch'] = Branch::where('Brn_Status', 1)->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $data['CURight'] = UserRights::rights();
            return view('auth.Excel.index', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function export_excel(Request $request) {
        try {
            // dd($request->all());
            if ($request->exportType == "1") {
                $branchId = $request->branch;
                $classAndId = explode(",", $request->class);
                $className = end($classAndId);
                $sectionName = $request->section;
                if ($sectionName == "none") {
                    $student_class_ids = ClassTbl::where('Cla_Bra_Id', $branchId)->where('Cla_Class', $className)->select('Cla_Id')->get()->toArray();
                } else {
                    $student_class_ids = ClassTbl::where('Cla_Bra_Id', $branchId)->where('Cla_Class', $className)->where('Cla_Section', $sectionName)->select('Cla_Id')->get()->toArray();
                }
                $data['studentList'] = Student::whereIn('Std_Cla_Id', $student_class_ids)
                        ->join('user_tbl', 'user_tbl.Use_Id', '=', 'student_tbl.Std_Parent_Id')
                        ->join('class_tbl', 'class_tbl.Cla_Id', '=', 'student_tbl.Std_Cla_Id')
                        ->get();

                // dd($data);
                return Excel::create('StudentList', function($excel) use ($data) {
                            $excel->sheet('Students', function($sheet) use ($data) {
                                $sheet->loadView('auth.Excel.studentList')->with($data);
                            });
                        })->export('xls');
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function import_form(Request $request) {
        try {
            if ($request->importType == "1") {
                $errorStudentNotExists = array();
                $tempGrNo = array();
                $tempBranch = array();
                $tempClass = array();
                $tempSection = array();
                $headerRequired = ["branch_name", "old_class", "old_section", "new_class", "new_section", "student_gr_no"];

                if ($request->hasFile('excelFile')) {
                    $fileExtension = $request->file('excelFile')->getClientOriginalExtension();
                    if ($fileExtension == "xls" || $fileExtension == "xlsx") {
                        
                    } else {
                        return back()->with('studentClassNotFound', 'Select Only Excel File.');
                        exit;
                    }
                    $path = $request->file('excelFile')->getRealPath();
                    $data = Excel::load($path, function($reader) {
                                
                            })->get();
                    $headerRow = $data->first()->keys()->toArray();

                    foreach ($headerRow as $row) {
                        if (!in_array($row, $headerRequired)) {
                            return back()->with('studentClassNotFound', 'Please Do Not Change In Header Of Sample File.');
                            exit;
                        }
                    }

                    if (!empty($data) && $data->count()) {
                        foreach ($data as $key => $value) {
                            $tempGrNo[] .= $value['student_gr_no'];
                            $tempBranch[] .= $value['branch_name'];
                            $tempClass[] .= $value['old_class'];
                            $tempSection[] .= $value['old_section'];
                        }
                    }

                    $tempBranch = array_unique($tempBranch);
                    $tempBranchId = Branch::where('Brn_Name', $tempBranch)->first();
                    $tempClass = array_unique($tempClass);
                    $tempSection = array_unique($tempSection);

                    if ($tempBranchId == "") {
                        return back()->with('studentClassNotFound', 'Branch Not Found Please Check Branch Name');
                        exit;
                    }

                    $student_class_ids = ClassTbl::where('Cla_Bra_Id', $tempBranchId->Brn_Id)->where('Cla_Class', $tempClass)->where('Cla_Section', $tempSection)->select('Cla_Id')->get()->toArray();
                    $studentList = Student::whereIn('Std_Cla_Id', $student_class_ids)->get();


                    if (count($student_class_ids) <= 0) {
                        return back()->with('studentClassNotFound', 'Student Old Class And Section Not Found Please Check Old Class And Section');
                        exit;
                    }

                    if (count($studentList) <= 0) {
                        return back()->with('studentClassNotFound', 'Student Not Found In Old Class.');
                    }

                    foreach ($studentList as $student) {
                        if (in_array($student->Std_Gr_No, $tempGrNo)) {
                            
                        } else {
                            $errorStudentNotExists[] .= $student->Std_Gr_No;
                        }
                    }

                    if (count($errorStudentNotExists) > 0) {
                        return back()->with('studentNotImport', implode(",", $errorStudentNotExists));
                    } else {
                        foreach ($data as $key => $value) {
                            $studentdNewClassId = ClassTbl::where('Cla_Bra_Id', $tempBranchId->Brn_Id)->where('Cla_Class', $value['new_class'])->where('Cla_Section', $value['new_section'])->first();
                            if ($studentdNewClassId == "") {
                                return back()->with('studentClassNotFound', 'Student Class And Section Not Found Please Check Class And Section.');
                                exit;
                            } else {
                                $students = DB::table('student_tbl')->where('Std_Gr_No', str_replace(".", "", $value['student_gr_no']))->update(['Std_Cla_Id' => $studentdNewClassId->Cla_Id,
                                    'Std_UpdatedAt' => date("Y-m-d H:i:s"),
                                    'Std_UpdatedBy' => Auth::user()->Use_Id
                                ]);
                            }
                        }
                        return back()->with('studentUpdateDone', 'Student successfully update.');
                    }
                } else {
                    return back();
                }
            }
            if ($request->importType == "2") {

                if ($request->hasFile('excelFile')) {
                    if ($request->file('excelFile')->getClientOriginalExtension() != "json" ) {
                        return back()->with("studentClassNotFound","Please select JSON File.");
                    }
                    $fileData = $request->file('excelFile');
                    $filePath = $request->file('excelFile')->getRealPath();
                    $temp = file_get_contents($filePath);
                    $json_decode = json_decode($temp, true);
                    $json_encode = json_encode($temp);
                    if ($json_decode != null) {
                        $month = $json_decode["month"];
                        $year = $json_decode["year"];
                        $branch_code = $json_decode["branch_code"];
                        $class = $json_decode["class"];
                        $section = $json_decode["section"];
                        $subject_title = array();
                        $subject_criteria = array();
                        foreach ($json_decode["subject"] as $subject) {
                            $subject_title[] = $subject["title"];
                            foreach ($subject['criteria'] as $key => $value) {
                                $subject_criteria[$subject["title"]][] = [$value => $key];
                            }
                        }
                        if ($month == "" || $year == "" || $branch_code == "" || $class == "" || $subject_title == "" || $subject_criteria == "") {
                            return back()->with("studentClassNotFound", "Please fill all the field.");
                        }else{
                            $string = trim(preg_replace('/\s+/', ' ', $temp));
                            $subject = new Subject;
                            $subject->Sub_Brn_Code = $branch_code;
                            $subject->Sub_Class = $class;
                            $subject->Sub_Section = $section;
                            $subject->Sub_Year = $year;
                            $subject->Sub_Month = $month;
                            $subject->Sub_Json_Data = $string;
                            $subject->Sub_CreatedAt = date('Y-m-d H:i:s');
                            $subject->Sub_CreatedBy = Auth::user()->Use_Id;
                            $subject->Sub_UpdatedAt = date('Y-m-d H:i:s');
                            $subject->Sub_UpdatedBy = Auth::user()->Use_Id;
                            if($subject->save()){
                                return back()->with("studentUpdateDone", "File Successfully insert.");
                            }else{
                                return back()->with("studentClassNotFound", "File Not Upload.");
                            }
                        }
                    } else {
                        return back()->with("studentClassNotFound", "JSON format is invalid.");
                    }
                } else {
                    return back();
                }
                $data['json'] = json_decode($temp, true);
            } 
            if ($request->importType == "3") {

                $tempBranchCode = array();
                $tempClass = array();
                $tempSection = array();
                $tempKidName = array();
                $tempFatherFirstName = array();
                $tempFatherLastName = array();
                $tempFatherPhone = array();
                $tempFatherEmailID = array();
                $tempMotherFirstName = array();
                $tempMotherLastName = array();
                $tempMotherPhone = array();
                $tempMotherEmailID = array();

                $tempGrNo = array();
                $tempBranch = array();
                $tempClass = array();
                $tempSection = array();

                $headerRequired = ["branch_code","gr_no.","class","section","kid_name", "father_first_name", "father_last_name", "father_phone", "father_email_id","mother_first_name","mother_last_name","mother_phone","mother_email_id"];

                if ($request->hasFile('excelFile')) {
                    $fileExtension = $request->file('excelFile')->getClientOriginalExtension();
                    if ($fileExtension == "xls" || $fileExtension == "xlsx") {
                        
                    } else {
                        return back()->with('studentClassNotFound', 'Select Only Excel File.');
                        exit;
                    }
                    $path = $request->file('excelFile')->getRealPath();
                    $data = Excel::load($path, function($reader) {
                            })->get();
                    $headerRow = $data->first()->keys()->toArray();

                    foreach ($headerRow as $row) {
                        if (!in_array($row, $headerRequired)) {
                            return back()->with('studentClassNotFound', 'Please Do Not Change In Header Of Sample File.');
                            exit;
                        }
                    }

                    if (!empty($data) && $data->count()) {
                        foreach ($data as $key => $value) {
                            $tempBranch[] .= $value['branch_code'];
                            $tempClass[] .= $value['class'];
                            $tempSection[] .= $value['section'];
                        }
                    }

                    $tempBranch = array_unique($tempBranch);
                    $tempBranchId = Branch::where('Brn_Code', $tempBranch)->first();
                    $tempClass = array_unique($tempClass);
                    $tempClass = array_filter($tempClass);
                    $tempSection = array_unique($tempSection);
                    $tempSection = array_filter($tempSection);


                    if ($tempBranchId == "") {
                        return back()->with('studentClassNotFound', 'Branch Not Found Please Check Branch Code');
                        exit;
                    }else{
                        $classNotFound = array();
                        foreach ($tempClass as $tmpClass) {
                            if(!ClassTbl::where("Cla_Bra_Id",$tempBranchId['Brn_Id'])->where('Cla_Class',$tmpClass)->exists()){
                                $classNotFound[] .= $tmpClass;
                            }
                        }
                        if (count($classNotFound) > 0) {
                            return back()->with('importError', implode(",", $classNotFound)." Class Not Found.");
                        }else{
                            $errorForEmailNo = "";
                            foreach ($data as $key => $value) {
                                if($value['branch_code']!=""){
                                $checkNo = "";
                                $checkEmail = "";
                                $fno = $value['father_phone'];
                                $femail = $value['father_email_id'];

                                $motherCheckNo = "";
                                $mothercheckEmail = "";
                                $mno = $value['mother_phone'];
                                $memail = $value['mother_email_id'];
                                
                                if(!empty($fno) || !empty($femail)){
                                    if($fno!=""){
                                        if(User::where('Use_Mobile_No',$fno)->exists()){
                                            $checkNo = "Exists";
                                        }else{
                                            $checkNo = "Notexists";
                                        }
                                    }else{
                                        $checkNo = "Empty";
                                    }
                                    if($femail!=""){
                                        if(User::where('Use_Email',$femail)->exists()){
                                            $checkEmail = "Exists";
                                        }else{
                                            $checkEmail = "Notexists";
                                        }
                                    }else{
                                        $checkEmail = "Empty";
                                    }
                                    if($checkNo=="Exists"){
                                        $errorForEmailNo .= '=>'.$value['father_first_name'].' '.$value['father_last_name'].' = Mobile No Already Exists.<br>';
                                    }elseif($checkEmail == "Exists"){
                                        $errorForEmailNo .= '=>'.$value['father_first_name'].' '.$value['father_last_name'].' = Email Already Exists.<br>';
                                    }
                                }elseif(empty($fno) && empty($femail)){
                                    $errorForEmailNo .= '=>'.$value['father_first_name'].' '.$value['father_last_name'].' = Please Enter Mobile No or Email.<br>';
                                }

                                if(!empty($mno) || !empty($memail)){
                                    if($mno!=""){
                                        if(User::where('Use_Mother_Mobile_No',$mno)->exists()){
                                            $motherCheckNo = "Exists";
                                        }else{
                                            $motherCheckNo = "Notexists";
                                        }
                                    }else{
                                        $motherCheckNo = "Empty";
                                    }
                                    if($memail!=""){
                                        if(User::where('Use_Mother_Email',$memail)->exists()){
                                            $mothercheckEmail = "Exists";
                                        }else{
                                            $mothercheckEmail = "Notexists";
                                        }
                                    }else{
                                        $mothercheckEmail = "Empty";
                                    }
                                    if($motherCheckNo=="Exists"){
                                        $errorForEmailNo .= '=>'.$value['mother_first_name'].' '.$value['mother_last_name'].' = Mobile No Already Exists.<br>';
                                    }elseif($mothercheckEmail == "Exists"){
                                        $errorForEmailNo .= '=>'.$value['mother_first_name'].' '.$value['mother_last_name'].' = Email Already Exists.<br>';
                                    }
                                }elseif(empty($mno) && empty($memail)){
                                    $errorForEmailNo .= '=>'.$value['mother_first_name'].' '.$value['mother_last_name'].' = Please Enter Mobile No or Email.<br>';
                                }

                                $checkNo = "";
                                $checkEmail = "";
                                $fno = "";
                                $femail = "";
                                $motherCheckNo = "";
                                $mothercheckEmail = "";
                                $mno = "";
                                $memail = "";
                                }
                            }
                            if($errorForEmailNo!=""){
                                return back()->with('importError',$errorForEmailNo);
                                exit;
                            }else{
                                foreach ($data as $key => $value) {
                                    $class_id = Branch::where("Brn_Code",$value["branch_code"])->select("Brn_Id")->get()->first();
                                    $classStd = ClassTbl::where("Cla_Bra_Id",$class_id["Brn_Id"])->where("Cla_Class",$value["class"])->where('Cla_Section',$value["section"])->select(["Cla_Id"])->get()->first();
                                    if($classStd != ""){
                                        $user['Use_Name'] = $value['father_first_name'].' '.$value['father_last_name'];
                                        $user['Use_Mobile_No'] = $value['father_phone'];
                                        $user['Use_Email'] = $value['father_email_id'];
                                        $user['Use_Mother_Name'] = $value['mother_first_name'].' '.$value['mother_last_name'];
                                        $user['Use_Mother_Mobile_No'] = $value['mother_phone'];
                                        $user['Use_Mother_Email'] = $value['mother_email_id'];
                                        $user['Use_Password'] = '$2y$12$PVi0MpbAkROlaek/jxk/Q.1oVG4gNh0QnfGCsjHtcXLCzXmD2qKr2';
                                        $user['Use_Mother_Password'] = '$2y$12$PVi0MpbAkROlaek/jxk/Q.1oVG4gNh0QnfGCsjHtcXLCzXmD2qKr2';
                                        $user['Use_Mother_Flag'] = 1;
                                        $user['Use_Register_Type'] = 0;
                                        $user['Use_Type'] = 4;
                                        $user['Use_Status'] = 1;
                                        $user['Use_CreatedBy'] = Auth::user()->Use_Id;
                                        $user['Use_CreatedAt'] = date('Y-m-d H:i:s');
                                        $user['Use_UpdatedBy'] = Auth::user()->Use_Id;
                                        $user['Use_UpdatedAt'] = date('Y-m-d H:i:s');
                                        $lastid = User::insertGetId($user);
                                        // $lastid = 10000;
                                        if($lastid != "" || $classStd != "" ){
                                            $std = new Student;
                                            $std->Std_Cla_Id = $classStd["Cla_Id"];
                                            $std->Std_Name = $value['kid_name'];
                                            $std->Std_Parent_Id = $lastid;
                                            $std->Std_Status = 1;
                                            $std->Std_Gr_No = $value['gr_no.'];
                                            $std->Std_Fee = 0 ;
                                            $std->Std_Transport = 0;
                                            $std->Std_CreatedBy = 1;
                                            $std->Std_CreatedAt = date('Y-m-d H:i:s');
                                            $std->Std_UpdatedBy = 1;
                                            $std->Std_UpdatedAt = date('Y-m-d H:i:s');
                                            $std->save();
                                        }
                                    }
                                }
                                return back()->with('studentUpdateDone', 'File successfully Upload.');
                            }
                        }
                    }
                } else {
                    return back();
                }
            }

// ********************************** Homework Excle Import Code Start **************************

            if ($request->importType == "4") {
                $tempBranchCode = array();
                $tempClass = array();
                $tempSubject = array();
                $tempChapter = array();
                $tempPageNo = array();
                $tempDescription = array();
                $tempDay = array();
                $tempMonth = array();
                $tempYear = array();
                $headerRequired = ["branch_code","class", "subject", "chapter", "page_no", "description", "day","month","year"];

                if ($request->hasFile('excelFile')) {
                    $fileExtension = $request->file('excelFile')->getClientOriginalExtension();
                    if ($fileExtension == "xls" || $fileExtension == "xlsx") {
                        
                    } else {
                        return back()->with('studentClassNotFound', 'Select Only Excel File.');
                        exit;
                    }
                    $path = $request->file('excelFile')->getRealPath();
                    $data = Excel::load($path, function($reader) {
                            })->get();

                        foreach ($data as $key => $value) {
                            $class_id = Branch::where('Brn_Code',$value['branch_code'])->select('Brn_Id')->get()->first();
                            $class = ClassTbl::where('Cla_Class',$value['class'])->select('Cla_Id')->get()->first();
                            $homework = new Homework;
                            $homework->Hmw_Cla_Id = $class->Cla_Id;
                            $homework->Hmw_Subject  = $value['subject'];
                            $homework->Hmw_Chapter = $value['chapter'];
                            $homework->Hmw_Date = $value['year'].'-'.$value['month'].'-'.$value['day'];
                            $homework->Hmw_Page_No = $value['page_no'];
                            $homework->Hmw_Desscription = $value['description'];
                            $homework->Hmw_CreatedBy = 1;
                            $homework->Hmw_CreatedAt = date('Y-m-d H:i:s');
                            $homework->Hmw_UpdatedBy = 1;
                            $homework->Hmw_UpdatedAt = date('Y-m-d H:i:s');
                            $homework->save();
                        }
                        return back()->with('studentUpdateDone', 'Homework File successfully Upload.');
                } else {
                    return back();
                }
            }

// ********************************** Homework Excle Import Code End **************************

// ********************************** Event Excle Import Code Start **************************

            if ($request->importType == "5") {
                $tempBranchCode = array();
                $tempClass = array();
                $tempSection = array();
                $tempEventName = array();
                $tempDescription = array();
                $tempSDay = array();
                $tempSMonth = array();
                $tempSYear = array();
                $tempEDay = array();
                $tempEMonth = array();
                $tempEYear = array();
                $tempRsvp = array();
                $multipleEvent = array();
                $tempNumber=0;
                $errorMessage = "";

                $headerRequired = ["branch_code","class", "section", "event_name", "description", "startday","startmonth","startyear","endday","endmonth","endyear",'rsvp'];

                if ($request->hasFile('excelFile')) {
                    $fileExtension = $request->file('excelFile')->getClientOriginalExtension();
                    if ($fileExtension == "xls" || $fileExtension == "xlsx") {
                        
                    } else {
                        return back()->with('studentClassNotFound', 'Select Only Excel File.');
                        exit;
                    }
                    $path = $request->file('excelFile')->getRealPath();
                    $data = Excel::load($path, function($reader) {
                            })->get();

                    foreach ($data as $eventKey => $eventValue) 
                    {
                        $branchCode = $eventValue['branch_code'];
                        $className = explode(",", $eventValue['class']);
                        $sectionName = explode(",", $eventValue['section']);
                        $eventName = $eventValue['event_name'];
                        $eventDescription = $eventValue['description'];
                        $eventStartday = $eventValue['startday'];
                        $eventStartmonth = $eventValue['startmonth'];
                        $eventStartyear = $eventValue['startyear'];
                        $eventEndday = $eventValue['endday'];
                        $eventEndmonth = $eventValue['endmonth'];
                        $eventEndyear = $eventValue['endyear'];
                        $eventRsvp = $eventValue['rsvp'];

                        $classCount = count($className);
                        $sectionCount = count($sectionName);

                        $unique_id = "EVENT".time().$tempNumber;
                        if($classCount == $sectionCount)
                        {
                            foreach ($className as $key=>$classValue) 
                            {
                                $singleSectionName = $sectionName[$key];
                                $singleClassName = $classValue;
                                $getBranchId = Branch::where("Brn_Code",$branchCode)->first()->Brn_Id;
                                $getClassIdExists = ClassTbl::where("Cla_Bra_Id",$getBranchId)->where("Cla_Class",$singleClassName)->where("Cla_Section",$singleSectionName)->exists();
                                if($getClassIdExists){
                                    $getClassId = ClassTbl::where("Cla_Bra_Id",$getBranchId)->where("Cla_Class",$singleClassName)->where("Cla_Section",$singleSectionName)->first()->Cla_Id;
                                    $temp = [
                                        'Eve_Unique_Id' => $unique_id,
                                        'Eve_Brn_Id' => $getBranchId,
                                        'Eve_Cla_Id' => $getClassId,
                                        'Eve_Cla_Section' => $singleSectionName,
                                        'Eve_Name' => $eventName,
                                        'Eve_Rsvp' => $eventRsvp,
                                        'Eve_Date' => $eventStartyear."-".$eventStartmonth."-".$eventStartday,
                                        'Eve_End_Date' => $eventEndyear."-".$eventEndmonth."-".$eventEndday,
                                        'Eve_Approve' => 0,
                                        'Eve_Description' => $eventDescription,
                                        'Eve_CreatedBy' => Auth::user()->Use_Id,
                                        'Eve_CreatedAt' => date('Y-m-d H:i:s'),
                                        'Eve_UpdatedBy' => Auth::user()->Use_Id,
                                        'Eve_UpdatedAt' => date('Y-m-d H:i:s'),
                                    ];
                                    $multipleEvent[] = array_merge($temp);
                                    $temp ="";
                                }else{
                                    $errorMessage .= "->".$eventName." - Event Class And Section not match please check.<br>";
                                }
                            }
                        }else{
                            $errorMessage .= "->".$eventName." - Event Class And Section Mismatch Please check Class and section.<br>";
                        }
                        $tempNumber++;
                    }
                    if($errorMessage!=""){
                        return back()->with('importError',$errorMessage);
                    }else{
                        foreach ($multipleEvent as $eventKey=>$eventValue) {
                            $event = New Event;
                            $event->Eve_Unique_Id = $eventValue['Eve_Unique_Id'];
                            $event->Eve_Brn_Id = $eventValue['Eve_Brn_Id'];
                            $event->Eve_Cla_Id = $eventValue['Eve_Cla_Id'];
                            $event->Eve_Cla_Section = $eventValue['Eve_Cla_Section'];
                            $event->Eve_Name = $eventValue['Eve_Name'];
                            $event->Eve_Rsvp = $eventValue['Eve_Rsvp'];
                            $event->Eve_Date = $eventValue['Eve_Date'];
                            $event->Eve_End_Date = $eventValue['Eve_End_Date'];
                            $event->Eve_Approve = 0;
                            $event->Eve_Description = $eventValue['Eve_Description'];
                            $event->Eve_CreatedBy = Auth::user()->Use_Id;
                            $event->Eve_CreatedAt = date('Y-m-d H:i:s');
                            $event->Eve_UpdatedBy = Auth::user()->Use_Id;
                            $event->Eve_UpdatedAt = date('Y-m-d H:i:s');
                            $event->save();
                        }
                        return back()->with('studentUpdateDone', 'Event File successfully Upload.');
                    }
                } else {
                    return back();
                }
            }
// ******************************* Event Excle Import Code End *******************       

// ********************************** Attedence Excle Import Code Start **************************

            if ($request->importType == "6") {
                $tempBranchCode = array();
                $tempClass = array();
                $tempStudentName = array();
                $tempAbsentPresent = array();
                $tempDay = array();
                $tempMonth = array();
                $tempYear = array();
                $headerRequired = ["branch_code","class", "student_name", "absent_present", "day","month","year"];

                if ($request->hasFile('excelFile')) {
                    $fileExtension = $request->file('excelFile')->getClientOriginalExtension();
                    if ($fileExtension == "xls" || $fileExtension == "xlsx") {
                        
                    } else {
                        return back()->with('studentClassNotFound', 'Select Only Excel File.');
                        exit;
                    }
                    $path = $request->file('excelFile')->getRealPath();
                    $data = Excel::load($path, function($reader) {
                            })->get();

                        foreach ($data as $key => $value) {

                            $class = ClassTbl::where('Cla_Class',$value['class'])->select('Cla_Id')->get()->first();
                            $student = Student::where('Std_Name',$value['student_name'])->select('Std_Id')->get()->first();
                            $attedence = new Attendence;
                            $attedence->Att_Stu_Id = $student->Std_Id;
                            $attedence->Att_Cla_Id = $class->Cla_Id;
                            $attedence->Att_Pre_Abs = $value['absent_present'];
                            $attedence->Att_Flag = 2;
                            $attedence->Att_Date = $value['year'].'-'.$value['month'].'-'.$value['day'];
                            $attedence->Att_CreatedBy = 1;
                            $attedence->Att_CreatedAt = date('Y-m-d H:i:s');
                            $attedence->Att_UpdatedBy = 1;
                            $attedence->Att_UpdatedAt = date('Y-m-d H:i:s');
                            $attedence->save();
                        }
                        return back()->with('studentUpdateDone', 'Attedence File successfully Upload.');
                } else {
                    return back();
                }
            }
// ********************************** Attedence Excle Import Code End ***********************     
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

}
