<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Ajax;
use App\Helper\Exceptions;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\UserRights;
use App\Http\Controllers\Controller;
use App\Model\Admin_assign_branch;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Event;
use App\Model\Module;
use App\Model\Student;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use URL;

class EventController extends Controller
{
    protected $rules = ([
        'class'   => '',
        'section' => '',
        'event'   => 'required',
        'date'    => 'required',
    ]);

    //Event Information
    public function index()
    {
        try {
            $data['i'] = 1;
            if (Auth::user()->Use_Type == "1") {
                $data['event'] = Event::select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName', 'branch_tbl.*')
                	->join('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                	->join('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                	->leftjoin('branch_tbl', 'event_tbl.Eve_Brn_Id', '=', 'branch_tbl.Brn_Id')
					->whereDate('event_tbl.Eve_Date','>=',date('Y-m-d'))
					->orderBy('event_tbl.Eve_Date', 'ASC')
                	->paginate(10);

                $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                $data['CURight'] = UserRights::rights();
                return view('auth.Event.index', $data);
            }
            if (Auth::user()->Use_Type == "5") {
                $branch          = Admin_assign_branch::where("Aab_Use_Id", Auth::user()->Use_Id)->select("Aab_Brn_Id")->get()->toArray();
                $data['event']   = Event::select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName', 'branch_tbl.*')
                	->whereIn("Eve_Brn_Id", $branch)
                	->join('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                	->join('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                	->leftjoin('branch_tbl', 'event_tbl.Eve_Brn_Id', '=', 'branch_tbl.Brn_Id')
                	->whereDate('event_tbl.Eve_Date','>=',date('Y-m-d'))
					->orderBy('event_tbl.Eve_Date', 'ASC')
                	->paginate(10);
                $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                $data['CURight'] = UserRights::rights();
                return view('auth.Event.index', $data);
            }
            if (Auth::user()->Use_Type == "2") {
                $teacherBrnId = ClassTbl::where("Cla_Id", Auth::user()->Use_Cla_Id)->select("Cla_Bra_Id")->first()->Cla_Bra_Id;
                $teacherClassIds = UserRights::teacherApi(Auth::user()->Use_Id, "AssignClass");
                $tagList       = Event::whereIn('Eve_Cla_Id', $teacherClassIds)->where('Eve_Brn_Id', $teacherBrnId)->select('Eve_Id')->groupBy('Eve_Unique_Id')->get();
                $data['event'] = Event::select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName', 'branch_tbl.Brn_Name')
                	->leftjoin('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->leftjoin('branch_tbl', 'event_tbl.Eve_Brn_Id', '=', 'branch_tbl.Brn_Id')
                    ->leftjoin('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                    ->whereIn('event_tbl.Eve_Id', $tagList)
                    ->whereDate('event_tbl.Eve_Date','>=',date('Y-m-d'))
					->orderBy('event_tbl.Eve_Date', 'ASC')
                    ->paginate(10);
                $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
                $data['CURight'] = UserRights::rights();
                return view('auth.Event.index', $data);
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Event Search
    public function search(Request $request)
    {
        try {
            return view('auth.Event.search', Ajax::searchEvent($request));
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Event Creation Form
    public function create($branchId = 0)
    {
        try {
            if ($branchId != 0) {
                $data['classList'] = ClassTbl::where('Cla_Bra_Id', $branchId)->get();
            }
            if (Auth::user()->Use_Type == "1") {
                $data['branch'] = Branch::where('Brn_Status', 1)->orderBy('Brn_Name')->get();
            } elseif (Auth::user()->Use_Type == "2") {
                $teacher = UserRights::teacher();
                // $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                $data['branch'] = Branch::where("Brn_Id", $teacher["branchAccess"])->where('Brn_Status', 1)->orderBy('Brn_Name')->get();
            } elseif (Auth::user()->Use_Type == "5") {
                $branch         = UserRights::branchAdmin();
                $data['branch'] = Branch::whereIn("Brn_Id", $branch["branchAccess"])->where('Brn_Status', 1)->orderBy('Brn_Name')->get();
            }
            $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $data['CURight'] = UserRights::rights();
            return view('auth.Event.create', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Event Creation
    public function store(Request $request)
    {
        try {
            $this->rules['image'] = 'image';
            $validator            = Validator::make($request->all(), $this->rules);

            if ($validator->fails()) {
                return back()->with('errors', $validator->errors())->withInput()->with('image', 'Please select image.');
            } else {
                if ($request->branch == "none") {
                    return back()->with('errors', $validator->errors())->with('image', 'Please select image.')->with('branch', 'Please select branch.')->withInput();
                }
                $sections = $request->section;
                if (count($request->section) == 0) {
                    return back()->withInput()->with('image', 'Please select image.')->with('notselected', 'Please select atlist one section.')->with('errors', $validator->errors());
                }
                if ($request->file('image')) {
                    $image     = $request->file('image');
                    $imageName = time() . $image->getClientOriginalName();
                    $image->move(public_path('images/event'), $imageName);
                } else {
                    $imageName = '';
                }
                if ($request->rsvp == 1) {
                    $rsvp = 1;
                } else {
                    $rsvp = 0;
                }

                $unique_id = "EVENT" . time();

                foreach ($sections as $section) {
                    $temp = explode(",", $section);
                    $sec  = $temp[1];
                    $cls  = $temp[0];

                    $event = new Event;

                    $event->Eve_Cla_Id      = $cls;
                    $event->Eve_Unique_Id   = $unique_id;
                    $event->Eve_Brn_Id      = $request->branch;
                    $event->Eve_Cla_Section = $sec;
                    $event->Eve_Name        = $request->event;
                    $event->Eve_Description = $request->description;
                    $event->Eve_Rsvp        = $rsvp;
                    if (Auth::user()->Use_Type == 1) {
                        $event->Eve_Approve = 1;
                    }
                    $event->Eve_Image = $imageName;
                    $event->Eve_Date  = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
                    if ($request->enddate == null) {
                        $event->Eve_End_Date = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
                    } else {
                        $event->Eve_End_Date = date('Y-m-d', strtotime(str_replace('/', '-', $request->enddate)));
                    }
                    $event->Eve_CreatedBy = Auth::user()->Use_Id;
                    $event->Eve_CreatedAt = date('Y-m-d H:i:s');
                    $event->Eve_UpdatedBy = Auth::user()->Use_Id;
                    $event->Eve_UpdatedAt = date('Y-m-d H:i:s');
                    $successStore         = $event->save();
                }

                if ($successStore) {
                    if ($event->Eve_Approve == 1) {
                        $this->send_notification($event->Eve_Unique_Id);
                    }
                    return redirect('Event Mgmt.');
                } else {
                    return redirect('create_event')->with('error', 'Event Creation Fail');
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Event Edit Form
    public function edit($id)
    {
        try {
            // $data['class'] = ClassTbl::where('Cla_Status',1)->get();
            $data['event'] = Event::where('Eve_Unique_Id', $id)->first();

            $data['classList']   = ClassTbl::where('Cla_Bra_Id', $data['event']->Eve_Brn_Id)->get();
            $data['sel_section'] = Event::where('Eve_Unique_Id', $id)->distinct()->select(['Eve_Cla_Id'])->get();

            $data['menu']    = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            $data['CURight'] = UserRights::rights();

            // dd($data);
            return view('auth.Event.edit', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Event Updation
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails()) {
                return back()->with('errors', $validator->errors())->withInput();
            } else {
                if ($request->file('image')) {
                    $image     = $request->file('image');
                    $imageName = time() . $image->getClientOriginalName();
                    $image->move(public_path('images/event'), $imageName);
                } else {
                    $imageName = Event::where('Eve_Unique_Id', $request->id)->select(['Eve_Image'])->first();
                    $imageName = $imageName->Eve_Image;
                }
                if ($request->rsvp == 1) {
                    $rsvp = 1;
                } else {
                    $rsvp = 0;
                }

                $sections = $request->section;
                if (count($request->section) == 0) {
                    return back()->withInput()->with('notselected', 'Please select atlist one section.')->with('errors', $validator->errors());
                }

                $unique_id    = $request->id;
                $oldEventData = Event::where('Eve_Unique_Id', $unique_id)->first();

                Event::where('Eve_Unique_Id', $unique_id)->delete();

                foreach ($sections as $section) {
                    $temp = explode(",", $section);
                    $sec  = $temp[1];
                    $cls  = $temp[0];

                    $event = new Event;

                    $event->Eve_Cla_Id      = $cls;
                    $event->Eve_Unique_Id   = $unique_id;
                    $event->Eve_Brn_Id      = $oldEventData['Eve_Brn_Id'];
                    $event->Eve_Cla_Section = $sec;
                    $event->Eve_Name        = $request->event;
                    $event->Eve_Description = $request->description;
                    $event->Eve_Rsvp        = $rsvp;
                    $event->Eve_Image       = $imageName;
                    $event->Eve_Approve     = $oldEventData['Eve_Approve'];
                    $event->Eve_Date        = date('Y-m-d', strtotime(str_replace('/', '-', $request->date)));
                    $event->Eve_End_Date    = date('Y-m-d', strtotime(str_replace('/', '-', $request->enddate)));
                    $event->Eve_CreatedBy   = Auth::user()->Use_Id;
                    $event->Eve_CreatedAt   = date('Y-m-d H:i:s');
                    $event->Eve_UpdatedBy   = Auth::user()->Use_Id;
                    $event->Eve_UpdatedAt   = date('Y-m-d H:i:s');
                    $checkUpdate            = $event->save();
                }

                if ($checkUpdate) {
                    return redirect('Event Mgmt.');
                } else {
                    return redirect('edit_event/{id}')->with('error', 'Event Updation Fail');
                }
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    //Event Deletion
    public function destroy(Request $request)
    {
        try {
            $all_data = $request->except('_token');

            foreach ($all_data as $ids) {
                foreach ($ids as $id) {
                    Event::where('Eve_Unique_Id', $id)->delete();
                }
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function classChange(Request $request)
    {
        Ajax::clsChange($request);
    }

    public function approve(Request $request)
    {
        try {
            $data = $request->except('_token');

            $event['Eve_UpdatedBy'] = Auth::user()->Use_Id;
            $event['Eve_Approve']   = 1;
            $event['Eve_UpdatedAt'] = date('Y-m-d H:i:s');
            Event::where('Eve_Unique_Id', $request->id)->update($event);

            $eventData = Event::where('Eve_Unique_Id', $request->id)->get()->first();

            $class_id    = Event::where('Eve_Unique_Id', $request->id)->select('Eve_Cla_Id')->get()->toArray();
            $student     = Student::whereIn('Std_Cla_Id', $class_id)->select('Std_Parent_Id')->distinct()->get()->toArray();
            $parenttoken = User::whereIn('Use_Id', $student)->select('Use_Token', 'Use_Mother_Token', 'Use_Mobile_No', 'Use_Mother_Mobile_No')->get();

            foreach ($parenttoken as $parent) {
                if ($parent->Use_Token != "") {
                    Notification::sendNotification($parent->Use_Token, "New Event.", "Event Name :- " . $eventData->Eve_Name);
                } else if ($parent->Use_Mobile_No != "") {
                    SMSHelper::sendSMS($parent->Use_Mobile_No, "New Event Added. Event Name :- " . $eventData->Eve_Name);
                }
                if ($parent->Use_Mother_Token != "") {
                    Notification::sendNotification($parent->Use_Mother_Token, "New Event.", "Event Name :- " . $eventData->Eve_Name);
                } else if ($parent->Use_Mother_Mobile_No != "") {
                    SMSHelper::sendSMS($parent->Use_Mother_Mobile_No, "New Event Added. Event Name :- " . $eventData->Eve_Name);
                }
            }
            if (User::where("Use_Id", $eventData->Eve_CreatedBy)->where("Use_Type", "2")->exists()) {
                $teacherData = User::where("Use_Id", $eventData->Eve_CreatedBy)->get()->first();
                if ($teacherData->Use_Token != "") {
                    Notification::sendNotification($teacherData->Use_Token, "Your Event Is Approved.", "Event Name :- " . $eventData->Eve_Name);
                } else if ($teacherData->Use_Mobile_No != "") {
                    SMSHelper::sendSMS($teacherData->Use_Mobile_No, "Your Event Is Approved. Event Name :- " . $eventData->Eve_Name);
                }
            }

            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if (is_numeric($whatIWant)) {
                $data['event'] = Event::join('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                    ->select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName')
                    ->paginate(10, ['*'], 'page', $whatIWant);
            } else {
                $data['event'] = Event::join('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                    ->select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName')
                    ->paginate(10);
            }
            return view('auth.Event.approve', $data);

        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }

    public function send_notification($id = '')
    {
        try {

            $event['Eve_UpdatedBy'] = Auth::user()->Use_Id;
            $event['Eve_Approve']   = 1;
            $event['Eve_UpdatedAt'] = date('Y-m-d H:i:s');
            Event::where('Eve_Unique_Id', $id)->update($event);

            $eventData = Event::where('Eve_Unique_Id', $id)->get()->first();

            $class_id    = Event::where('Eve_Unique_Id', $id)->select('Eve_Cla_Id')->get()->toArray();
            $student     = Student::whereIn('Std_Cla_Id', $class_id)->select('Std_Parent_Id')->distinct()->get()->toArray();
            $parenttoken = User::whereIn('Use_Id', $student)->select('Use_Token', 'Use_Mother_Token', 'Use_Mobile_No', 'Use_Mother_Mobile_No')->get();

            foreach ($parenttoken as $parent) {
                if ($parent->Use_Token != "") {
                    Notification::sendNotification($parent->Use_Token, "New Event.", "Event Name :- " . $eventData->Eve_Name);
                } else if ($parent->Use_Mobile_No != "") {
                    SMSHelper::sendSMS($parent->Use_Mobile_No, "New Event Added. Event Name :- " . $eventData->Eve_Name);
                }
                if ($parent->Use_Mother_Token != "") {
                    Notification::sendNotification($parent->Use_Mother_Token, "New Event.", "Event Name :- " . $eventData->Eve_Name);
                } else if ($parent->Use_Mother_Mobile_No != "") {
                    SMSHelper::sendSMS($parent->Use_Mother_Mobile_No, "New Event Added. Event Name :- " . $eventData->Eve_Name);
                }
            }
            if (User::where("Use_Id", $eventData->Eve_CreatedBy)->exists()) {
                $teacherData = UserRights::classesTeacherList($class_id);
                foreach ($teacherData as $data) {
                    if ($data->Use_Token != "") {
                        Notification::sendNotification($data->Use_Token, "New Event Added", "Event Name :- " . $eventData->Eve_Name);
                    } else if ($data->Use_Mobile_No != "") {
                        SMSHelper::sendSMS($data->Use_Mobile_No, "New Event Added. Event Name :- " . $eventData->Eve_Name);
                    }
                }
            }

            $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
            if (is_numeric($whatIWant)) {
                $data['event'] = Event::join('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                    ->select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName')
                    ->paginate(10, ['*'], 'page', $whatIWant);
            } else {
                $data['event'] = Event::join('class_tbl', 'event_tbl.Eve_Cla_Id', '=', 'class_tbl.Cla_Id')
                    ->join('user_tbl', 'event_tbl.Eve_CreatedBy', '=', 'user_tbl.Use_Id')
                    ->select('event_tbl.*', 'class_tbl.Cla_Class', 'event_tbl.Eve_Name', 'user_tbl.Use_Name as createdByName')
                    ->paginate(10);
            }
        } catch (Exception $e) {

        }
    }

    //    public function approvehome(Request $request)
    // {
    //     try{
    //         $data = $request->except('_token');
    //         $event['Eve_UpdatedBy'] = Auth::user()->Use_Id;
    //         $event['Eve_Approve'] = 1;
    //         $event['Eve_UpdatedAt'] = date('Y-m-d H:i:s');
    //         Event::where('Eve_Unique_Id',$request->id)->update($event);

    //         $whatIWant = substr(URL::previous(), strpos(URL::previous(), "page=") + 5);
    //            if(is_numeric($whatIWant))
    //                $data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
    //                         ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
    //                           ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName')
    //                           ->paginate(10,['*'],'page',$whatIWant);
    //            else
    //                $data['event'] = Event::join('class_tbl','event_tbl.Eve_Cla_Id','=','class_tbl.Cla_Id')
    //                         ->join('user_tbl','event_tbl.Eve_CreatedBy','=','user_tbl.Use_Id')
    //                           ->select('event_tbl.*','class_tbl.Cla_Class','event_tbl.Eve_Name','user_tbl.Use_Name as createdByName')->where('Eve_Date',date('Y-m-d'))
    //                           ->paginate(10);
    //            return view('auth.Event.approve_home',$data);

    //     }catch(\Exception $e){
    //         Exceptions::exception($e);
    //     }
    // }

    public function getClass(Request $request)
    {
        try {

            return view('auth.Event.classList', $data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
