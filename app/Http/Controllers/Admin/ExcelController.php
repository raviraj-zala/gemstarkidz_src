<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;
use DB;
use App\User;
use App\Model\Branch;
use App\Model\ClassTbl;
use App\Model\Module;
use App\Model\Student;
use URL;
use App\Helper\UserRights;
use App\Helper\Exceptions;
use Excel;

class ExcelController extends Controller
{
	public $rules  = [
				'exportType' => 'required',
			];
    public function index()
    {
    	try {
    		$data['branch'] = Branch::where('Brn_Status',1)->get();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
            $data['CURight']=UserRights::rights();
    		return view('auth.Excel.index',$data);
		} catch (\Exception $e) {	
			Exceptions::exception($e);
		}	
    }

    public function export_excel(Request $request){
    	try {
    		// dd($request->all());
    		if($request->exportType == "1" ){
    			$branchId = $request->branch;
    			$classAndId = explode(",", $request->class);
                $className = end($classAndId);
    			$sectionName = $request->section;
    			if($sectionName=="none"){
    				$student_class_ids = ClassTbl::where('Cla_Bra_Id',$branchId)->where('Cla_Class',$className)->select('Cla_Id')->get()->toArray();
    			}
    			else{
    				$student_class_ids = ClassTbl::where('Cla_Bra_Id',$branchId)->where('Cla_Class',$className)->where('Cla_Section',$sectionName)->select('Cla_Id')->get()->toArray();
    			}
    			$data['studentList'] = Student::whereIn('Std_Cla_Id',$student_class_ids)
    							->join('user_tbl','user_tbl.Use_Id','=','student_tbl.Std_Parent_Id')
    							->join('class_tbl','class_tbl.Cla_Id','=','student_tbl.Std_Cla_Id')
    							->get();
    			
    			// dd($data);
    			return Excel::create('StudentList', function($excel) use ($data) {
						$excel->sheet('Students', function($sheet) use ($data)
				        {
							$sheet->loadView('auth.Excel.studentList')->with($data);
				        });
					})->export('xls');
    		}
    	} catch (\Exception $e) {
			Exceptions::exception($e);
    	}
    }

    public function import_excel(Request $request){
    	try 
        {
    		if($request->importType == "1")
            {
    			$errorStudentNotExists = array();
                $tempGrNo = array();
                $tempBranch = array();
                $tempClass = array();
    			$tempSection = array();
                $headerRequired = ["branch_name", "old_class", "old_section", "new_class", "new_section", "student_gr_no"];

				if($request->hasFile('excelFile'))
                {
                    $fileExtension = $request->file('excelFile')->getClientOriginalExtension();
                    if($fileExtension=="xls" || $fileExtension=="xlsx"){}else{
                        return back()->with('studentClassNotFound','Select Only Excel File.');
                        exit;
                    }
					$path = $request->file('excelFile')->getRealPath();
					$data = Excel::load($path, function($reader) {
					})->get();
                    $headerRow = $data->first()->keys()->toArray();
                    
                    foreach ($headerRow as $row) {
                        if(!in_array($row, $headerRequired)){
                        return back()->with('studentClassNotFound','Please Do Not Change In Header Of Sample File.');
                            exit;
                        }
                    }

					if(!empty($data) && $data->count()){
						foreach ($data as $key => $value) {
                            $tempGrNo[] .= $value['student_gr_no'];
                            $tempBranch[] .= $value['branch_name'];
                            $tempClass[] .= $value['old_class'];
							$tempSection[] .= $value['old_section'];
						}
					}

                    $tempBranch = array_unique($tempBranch);
                    $tempBranchId = Branch::where('Brn_Name',$tempBranch)->first();
                    $tempClass = array_unique($tempClass);
                    $tempSection = array_unique($tempSection);

                    if($tempBranchId==""){
                        return back()->with('studentClassNotFound','Branch Not Found Please Check Branch Name');
                        exit;
                    }

                    $student_class_ids = ClassTbl::where('Cla_Bra_Id',$tempBranchId->Brn_Id)->where('Cla_Class',$tempClass)->where('Cla_Section',$tempSection)->select('Cla_Id')->get()->toArray();
                    $studentList = Student::whereIn('Std_Cla_Id',$student_class_ids)->get();


                    if(count($student_class_ids)<=0){
                        return back()->with('studentClassNotFound','Student Old Class And Section Not Found Please Check Old Class And Section');
                        exit;
                    }

                    if(count($studentList)<=0){
                        return back()->with('studentClassNotFound','Student Not Found In Old Class.');
                    }

					foreach ($studentList as $student) {
						if(in_array($student->Std_Gr_No, $tempGrNo)){
						}else{
							$errorStudentNotExists[] .= $student->Std_Gr_No;
						}
					}

					if(count($errorStudentNotExists)>0){
						return back()->with('studentNotImport',implode(",", $errorStudentNotExists) );
					}else{
                        foreach ($data as $key => $value) 
                        {
                            $studentdNewClassId = ClassTbl::where('Cla_Bra_Id',$tempBranchId->Brn_Id)->where('Cla_Class',$value['new_class'])->where('Cla_Section',$value['new_section'])->first();
                            if($studentdNewClassId==""){
                                return back()->with('studentClassNotFound','Student Class And Section Not Found Please Check Class And Section.');
                                exit;
                            }else{
                                $students = DB::table('student_tbl')->where('Std_Gr_No',str_replace(".", "", $value['student_gr_no']))->update(['Std_Cla_Id' => $studentdNewClassId->Cla_Id,
                                                                'Std_UpdatedAt' => date("Y-m-d H:i:s"),
                                                                'Std_UpdatedBy' =>  Auth::user()->Use_Id
                                                            ]);
                            }
                        }
                        return back()->with('studentUpdateDone','Student successfully update.');
                    }
				}else{ return back(); }
    		}else{return back()->with('importType','Please Select Upload Type.');}
    	} catch (\Exception $e) {	
    		Exceptions::exception($e);
    	}
    }
}
