<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\UserRights;
use App\Model\Module;
use App\Model\ClassTbl;
use App\User;
use App\Model\Zone;
use App\Model\Branch;
use App\Model\Rights;
use App\Helper\Exceptions;
use Illuminate\Support\Facades\Validator;
use Response;
use Auth;
use App\Helper\Ajax;
use URL;
use App\Model\Student;
use DB;

class UserReportController extends Controller
{
   public function index(){
		try{

            // Admin Side Index View
            if(Auth::user()->Use_Type == "1"){
                $data['user_report'] = DB::table('user_tbl as user')
                        ->where('cBy.Use_Type',4)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->get();
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();                         
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.user_report',$data);
            }elseif(Auth::user()->Use_Type == "2")
            {
                $teacher = UserRights::teacher();
                $parent_ids = Student::whereIn("Std_Cla_Id",$teacher["AssignClass"])->select("Std_Parent_Id")->get()->toArray();
                $data['user_report'] = DB::table('user_tbl as user')
                        ->where('cBy.Use_Type',4)
                        ->whereIn('cBy.Use_Id',$parent_ids)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->paginate(10);
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.user_report',$data);
            }elseif(Auth::user()->Use_Type == "5")
            {
                $branch = UserRights::branchAdmin();
                $class_ids = ClassTbl::whereIn("Cla_Bra_Id",$branch)->select("Cla_Id")->get()->toArray();
                $parent_ids = Student::whereIn("Std_Cla_Id",$class_ids)->select("Std_Parent_Id")->get()->toArray();
                $data['user_report'] = DB::table('user_tbl as user')
                        ->where('cBy.Use_Type',4)
                        ->whereIn('cBy.Use_Id',$parent_ids)
                        ->rightjoin('user_tbl as cBy','cBy.Use_CreatedBy','=','user.Use_Id')
                        ->leftjoin('student_tbl','cBy.Use_Id','=','student_tbl.Std_Parent_Id')
                        ->leftjoin('class_tbl','student_tbl.Std_Cla_Id','=','class_tbl.Cla_Id')
                        ->leftjoin('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                        ->select('user.Use_Id', 'user.Use_Name as createdByName','cBy.*','student_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class')
                        ->orderBy('cBy.Use_Id','DESC')
                        ->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.user_report',$data);
            }

		}catch(\Exception $e){
			Exceptions::exception($e);
		}
	}
	public function search(Request $request){
		try{
			$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
             return view('auth.record.user_report', Ajax::searchUserreport($request))->with($data);
	    }
	    catch(\Exception $e){
			Exceptions::exception($e);
		}
	}
}
