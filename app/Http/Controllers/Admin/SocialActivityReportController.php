<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Model\Module;
use App\Helper\UserRights;
use App\Model\Socialactivitydetails;
use App\Model\Socialactivitytag;
use App\Model\Admin_assign_branch;
use App\Model\Socialactivity;
use App\Model\ClassTbl;
use App\Helper\Exceptions;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Model\Student;
use App\Helper\Ajax;
use App\Model\Branch;
use App\User;
use URL;
use DB;


class SocialActivityReportController extends Controller
{
    public function index(){
        try {
            $data['i'] = 1;

            //admin user
            if(Auth::user()->Use_Type=="1")
            {
                $data['socialActivitys'] = Socialactivity::join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')
                            ->leftjoin('class_tbl','socialactivity_tbl.Soa_Cla_Id','=','class_tbl.Cla_Id')
                            ->select('socialactivity_tbl.*','branch_tbl.Brn_Name','class_tbl.Cla_Class','class_tbl.Cla_Section')
                            ->orderBy('Soa_Id','DESC')
                            ->get();
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.social_activity_report',$data);
            }

            //branch admin user
            if(Auth::user()->Use_Type=="5")
            {
                $branch = Admin_assign_branch::where("Aab_Use_Id",Auth::user()->Use_Id)->select("Aab_Brn_Id")->get()->toArray();

                $data['socialActivitys'] = Socialactivity::whereIn("Soa_Branch",$branch)->join('branch_tbl','socialactivity_tbl.Soa_Branch','=','branch_tbl.Brn_Id')->select('socialactivity_tbl.*','branch_tbl.Brn_Name')->orderBy('Soa_Id','DESC')->paginate(10);
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.social_activity_report',$data);
            }

            //Teacher User
            if(Auth::user()->Use_Type=="2"){
                $teacher = UserRights::teacher();
                $data['socialActivitys'] = Socialactivity::where('Soa_Use_Id',Auth::user()->Use_Id)->whereIn("Soa_Cla_Id",$teacher['AssignClass'])->orderBy('Soa_Id','DESC')->paginate(10);
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
                $data['menu'] = Module::with('children')->where('Mod_Parent_Id',0)->get();
                $data['CURight']=UserRights::rights();
                return view('auth.record.social_activity_report',$data);
            }
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function search(Request $request){
    	try {
        	$data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
            return view('auth.record.social_activity_report', Ajax::searchSocialActivity($request))->with($data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
