<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Model\Zone;
use App\Model\ClassTbl;
use App\Model\Student;
use App\Model\Report;
use App\Model\Branch;
use App\Model\Module;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use App\Model\Attendence;
use App\Model\Payment;
use App\Helper\Ajax;
use App\User;
use Response;
use Auth;
use URL;
use DB;


class FeesController extends Controller
{
    public function index(Request $request){
        try {
            if(Auth::user()->Use_Type == "1"){
                $data['fees_report'] = Payment::join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl as u','payment_tbl.Pay_CreatedBy','=','u.Use_Id')
                                ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->join('student_tbl','student_tbl.Std_Id','=','payment_tbl.Pay_Std_Id')
                                ->join('user_tbl','user_tbl.Use_Id','=','student_tbl.Std_Parent_Id')
                                ->select('payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name','student_tbl.Std_Name','student_tbl.Std_Gr_No as Grno','student_tbl.Std_Gr_No')
                                ->orderBy('Pay_Id','DESC')
                                ->paginate(10);
                                
                //dd($data);
                $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();
              

            }elseif(Auth::user()->Use_Type == "2"){
                $data['branch'] = Branch::where('Brn_Status',1)->where('Brn_Id',$teacher["branchAccess"])->orderBy('Brn_Name')->get();

                $class = UserRights::teacher();
                $data['fees_report'] = Payment::whereIn('payment_tbl.Pay_Cla_Id',$class['AssignClass'])
                                ->join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl as u','payment_tbl.Pay_CreatedBy','=','u.Use_Id')
                                ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->join('student_tbl','student_tbl.Std_Id','=','payment_tbl.Pay_Std_Id')
                                ->join('user_tbl','user_tbl.Use_Id','=','student_tbl.Std_Parent_Id')
                                ->select('payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name','student_tbl.Std_Name')
                                ->orderBy('Pay_Id','DESC')
                                ->paginate(10);
                
            }elseif(Auth::user()->Use_Type == "5"){
                $branch = UserRights::branchAdmin();
                $data['branch'] = Branch::where('Brn_Status',1)->whereIn('Brn_Id',$branch["branchAccess"])->orderBy('Brn_Name')->get();
                $data['fees_report'] = Payment::whereIn('payment_tbl.Pay_Cla_Id',$class['branchClass'])
                                ->join('class_tbl','payment_tbl.Pay_Cla_Id','=','class_tbl.Cla_Id')
                                ->join('user_tbl as u','payment_tbl.Pay_CreatedBy','=','u.Use_Id')
                                ->join('branch_tbl','class_tbl.Cla_Bra_Id','=','branch_tbl.Brn_Id')
                                ->join('student_tbl','student_tbl.Std_Id','=','payment_tbl.Pay_Std_Id')
                                ->join('user_tbl','user_tbl.Use_Id','=','student_tbl.Std_Parent_Id')
                                ->select('payment_tbl.*','class_tbl.*','user_tbl.Use_Name as parentName','u.Use_Name as createdByName','branch_tbl.Brn_Name','student_tbl.Std_Name')
                                ->orderBy('Pay_Id','DESC')
                                ->paginate(10);
            }
            $data['CURight'] = UserRights::rights();
            $data['menu'] = Module::with('children')->where('Mod_Parent_Id', 0)->get();
            return view('auth.record.fees_report')->with($data);
        }
         catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
    public function search(Request $request) {
        try {
            $data['branch'] = Branch::where('Brn_Status',1)->orderBy('Brn_Name')->get();

            return view('auth.record.fees_report', Ajax::searchFeesRecord($request))->with($data);
        } catch (\Exception $e) {
            Exceptions::exception($e);
        }
    }
}
