<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use App\Model\Users;
use App\Helper\Exception;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Use_Name' => 'required|string|max:255',
            'Use_Type' => 'required|string|max:255',
            'Use_Password' => 'required|string|confirmed',
            'Use_Address' => 'required|string|max:500',
            'Use_City' => 'required|string|max:255',
            'Use_State' => 'required|string|max:255',
            'Use_Country' => 'required|string|max:255'
        ]);
    }

    protected function index()
    {
        return view('Auth.register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        try {
            $data = ([
                'Use_Name' => $request->name,
                'Use_Type' => $request->userType,
                'Use_Password' => bcrypt($request->password),
                'Use_Address' => $request->address,
                'Use_City' => $request->city,
                'Use_State' => $request->state,
                'Use_Country' => $request->country,
                'Use_Status' => 1,
                'Use_CreatedBy' => Auth::user(),
                'Use_CreatedAt' => date('Y-m-d H:i:s'),
                'Use_UpdatedBy' => Auth::user(),
                'Use_UpdatedAt' => date('Y-m-d H:i:s')
            ]);

            $insert = Users::insert($data);

            if($insert){
                return redirect('login')->with('success','You have Registered Successfully');
            }else{
                return redirect('register')->with('error','Registration Fail');
            }
        } catch (Exception $e) {
            Exception::exception($e);
        }
        
    }
}
