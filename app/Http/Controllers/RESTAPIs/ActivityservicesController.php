<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Helper\UserRights;
use App\User;
use App\Model\Users;
use App\Model\Socialactivity;
use App\Model\Socialactivitydetails;
use App\Model\Socialactivitytag;
use App\Model\Socialactivity_like;
use App\Model\Socialactivity_comment;
use App\Model\Socialactivity_like_image;
use App\Model\Socialactivity_img_cmt;
use App\Model\Student;
use App\Model\ClassTbl;
use Auth;
use Input;
use Validator;
use DB;
use Hash;

class ActivityservicesController extends Controller
{
  //Create Social Activity
	public function create_Activity(Request $request)
	{
		try {
			$rules = [
				'user_id' => 'required',
				'user_type' => 'required',
				'class_id' => 'required',
				'branch_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
					exit;
				}
			}

			$class = Input::get('class_id');
			$std = Input::get('student_id');
			$error_size = false;
			if($std!=""){
				$student = explode(',', $std);
				$studentDetails = Student::whereIn('Std_Id',$student)->get();
				$classIds = Student::whereIn('Std_Id',$student)->select('Std_Cla_Id')->distinct()->get()->toArray();
			}
		    if(Input::get('flag') == 1){
		    	$classIds = UserRights::teacherApi(Input::get('user_id'),"AssignClass");
		    	$classIds = Student::whereIn('Std_Cla_Id',$classIds)->select('Std_Cla_Id')->distinct()->get()->toArray();
		    	$studentDetails = Student::whereIn('Std_Cla_Id',$classIds)->get();

		    	$images = $request->file('image');

	    		if($images)
				{
                    foreach ($images as $image)
					{
						if($image->getClientSize() > 26214400)
						{
							$error_size = true;
						}
					}
                }if($error_size == true)
                {
                   ResponseMessage::error("Do Not Upload More Than 25MB File");                   
                }else{

					if($images){
			            foreach($images as $file){
					        $filename = $file->getClientOriginalName();
					        $extension = $file->getClientOriginalExtension();
					        $picture[] = date('His').$filename;
					        $file->move(public_path('images/social'), date('His').$filename);
					    }
			        }else{
			            $images = "";
			        }

			        $unique_id = "SOA".time();
			        foreach ($classIds as $clsId) {
				    $activity['Soa_Use_id'] = Input::get('user_id');
					$activity['Soa_User_Type'] = Input::get('user_type');
					$activity['Soa_Unique_id'] = $unique_id;
					$activity['Soa_Branch'] = Input::get('branch_id');
					$activity['Soa_Title'] = Input::get('title');
					$activity['Soa_Cla_Id'] = $clsId["Std_Cla_Id"];
					$activity['Soa_Comment'] = Input::get('comment');
					$activity['Soa_Tag'] = Input::get('flag');
					$activity['Soa_Status'] = "0";
					$activity['Soa_CreatedBy'] = Input::get('user_id');
					$activity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
					$activity['Soa_UpdatedBy'] = Input::get('user_id');
					$activity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
					$lastid = Socialactivity::insertGetId($activity);

						if($images){
							foreach ($picture as $value){
								// $unique_id = "SOA".time();
								$soa = new Socialactivitydetails;
								$soa->Sod_Img_Video = $value;
								$soa->Sod_Soa_Unique_Id = $unique_id;
								$soa->Sod_Use_Id = Input::get('user_id');
								$soa->Sod_Soa_Id = $lastid;
								$soa->Sod_Uploadtype = Input::get('file_type');
								$soa->save();
							}
						}
					
						foreach($studentDetails as $val){
							// $unique_id = "SOA".time();
							$tag = new Socialactivitytag;
							$tag->Tag_Use_Id = Input::get('user_id');
							$tag->Tag_Soa_Unique_Id = $unique_id;
							$tag->Tag_Soa_Id = $lastid;
							$tag->Tag_Cla_Id = $val->Std_Cla_Id;
							$tag->Tag_Stu_Id = $val->Std_Id;
							$tag->Tag_Flag = Input::get('flag');
							$tag->Tag_CreatedAt = date('Y-m-d H:i:s');
							$tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
							$tag->save();
						}
					}
					if($images){
						foreach ($soa as $value) {
							$s = Socialactivitydetails::where('Sod_Use_Id',Input::get('user_id'))->get();
						}
						$s1[] = $s;
						$activitydata[] = $activity;
						$soadata = array_merge($activitydata,$s1);

						if($soadata){
							ResponseMessage::success("Activity Created Successfully",$soadata);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}else{
						if($activity){
							ResponseMessage::success("Activity Created Successfully",$activity);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}
				}
		    }else if(Input::get('flag') == 2){
	    		$images = $request->file('image');

	    		if($images)
				{
                    foreach ($images as $image)
					{
						if($image->getClientSize() > 26214400)
						{
							$error_size = true;
						}
					}
                }if($error_size == true)
                {
                   ResponseMessage::error("Do Not Upload More Than 25MB File");                   
                }else{

					if($images){
			            foreach($images as $file){
					        $filename = $file->getClientOriginalName();
					        $extension = $file->getClientOriginalExtension();
					        $picture[] = date('His').$filename;
					        $file->move(public_path('images/social'), date('His').$filename);
					    }
			        }else{
			            $images = "";
			        }

			        $unique_id = "SOA".time();
			        foreach ($classIds as $clsId) {
				    $activity['Soa_Use_id'] = Input::get('user_id');
					$activity['Soa_User_Type'] = Input::get('user_type');
					$activity['Soa_Unique_id'] = $unique_id;
					$activity['Soa_Branch'] = Input::get('branch_id');
					$activity['Soa_Title'] = Input::get('title');
					$activity['Soa_Cla_Id'] = $clsId["Std_Cla_Id"];
					$activity['Soa_Comment'] = Input::get('comment');
					$activity['Soa_Tag'] = Input::get('flag');
					$activity['Soa_Status'] = "0";
					$activity['Soa_CreatedBy'] = Input::get('user_id');
					$activity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
					$activity['Soa_UpdatedBy'] = Input::get('user_id');
					$activity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');
					$lastid = Socialactivity::insertGetId($activity);

						if($images){
							foreach ($picture as $value){
								// $unique_id = "SOA".time();
								$soa = new Socialactivitydetails;
								$soa->Sod_Img_Video = $value;
								$soa->Sod_Soa_Unique_Id = $unique_id;
								$soa->Sod_Use_Id = Input::get('user_id');
								$soa->Sod_Soa_Id = $lastid;
								$soa->Sod_Uploadtype = Input::get('file_type');
								$soa->save();
							}
						}
					
						foreach($studentDetails as $val){
							// $unique_id = "SOA".time();
							$tag = new Socialactivitytag;
							$tag->Tag_Use_Id = Input::get('user_id');
							$tag->Tag_Soa_Unique_Id = $unique_id;
							$tag->Tag_Soa_Id = $lastid;
							$tag->Tag_Cla_Id = $val->Std_Cla_Id;
							$tag->Tag_Stu_Id = $val->Std_Id;
							$tag->Tag_Flag = Input::get('flag');
							$tag->Tag_CreatedAt = date('Y-m-d H:i:s');
							$tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
							$tag->save();
						}
					}
					if($images){
						foreach ($soa as $value) {
							$s = Socialactivitydetails::where('Sod_Use_Id',Input::get('user_id'))->get();
						}
						$s1[] = $s;
						$activitydata[] = $activity;
						$soadata = array_merge($activitydata,$s1);

						if($soadata){
							ResponseMessage::success("Activity Created Successfully",$soadata);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}else{
						if($activity){
							ResponseMessage::success("Activity Created Successfully",$activity);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}
				}
			}else if(Input::get('flag') == 3){
				$teacherBranchClass = explode(",", Input::get('class_id'));
				$images = $request->file('image');
				if($images)
				{
                    foreach ($images as $image)
					{
						if($image->getClientSize() > 26214400)
						{
							$error_size = true;
						}
					}
                }if($error_size == true)
                {
                   ResponseMessage::error("Do Not Upload More Than 25MB File");                   
                }else{

				if($images){
		            foreach($images as $file){
				        $filename = $file->getClientOriginalName();
				        $extension = $file->getClientOriginalExtension();
				        $picture[] = date('His').$filename;
				        $file->move(public_path('images/social'), date('His').$filename);
				    }
		        }else{
		            $images = "";
		        }
		        $unique_id = "SOA".time();
		        foreach ($teacherBranchClass as $key => $value) {
		        	
		    	$activity['Soa_Use_id'] = Input::get('user_id');
				$activity['Soa_User_Type'] = Input::get('user_type');
				$activity['Soa_Unique_id'] = $unique_id;
				$activity['Soa_Branch'] = Input::get('branch_id');
				$activity['Soa_Title'] = Input::get('title');
				$activity['Soa_Cla_Id'] = $value;
				$activity['Soa_Comment'] = Input::get('comment');
				$activity['Soa_Tag'] = Input::get('flag');
				$activity['Soa_Status'] = "0";
				$activity['Soa_CreatedBy'] = Input::get('user_id');
				$activity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
				$activity['Soa_UpdatedBy'] = Input::get('user_id');
				$activity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

				$lastid = Socialactivity::insertGetId($activity);
				
				if($images){
					foreach ($picture as $value){
						// $unique_id = "SOA".time();
						$soa = new Socialactivitydetails;
						$soa->Sod_Img_Video = $value;
						$soa->Sod_Soa_Unique_Id = $unique_id;
						$soa->Sod_Use_Id = Input::get('user_id');
						$soa->Sod_Soa_Id = $lastid;
						$soa->Sod_Uploadtype = Input::get('file_type');
						$soa->save();
					}
				}
				
				$studentDetails = Student::where("Std_Cla_Id",$value)->get();
				// $teacherDetails = User::where('Use_Type',2)->whereIn("Use_Cla_Id",$teacherBranchClass)->orwhere('Use_Type',1)->get();
				// $parentsDetails = User::where('Use_Type',4)->join("student_tbl","student_tbl.Std_Parent_Id","=","user_tbl.Use_Id")->whereIn("Std_Cla_Id",$teacherBranchClass)->get();
				foreach ($studentDetails as $students)
				{ 	
					// $unique_id = "SOA".time();
					$tag = new Socialactivitytag;
					$tag->Tag_Use_Id = Input::get('user_id');
					$tag->Tag_Soa_Unique_Id = $unique_id;
					$tag->Tag_Soa_Id = $lastid;
					$tag->Tag_Cla_Id = $students->Std_Cla_Id;
					$tag->Tag_Stu_Id = $students->Std_Id;
					$tag->Tag_Flag = Input::get('flag');
					$tag->Tag_CreatedAt = date('Y-m-d H:i:s');
					$tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
					$tag->save();
				}
				}

				// foreach ($teacherDetails as $teachers)
				// {   
				// 	// $unique_id = "SOA".time();
				// 	$tag = new Socialactivitytag;
				// 	$tag->Tag_Use_Id = Input::get('user_id');
				// 	$tag->Tag_Soa_Unique_Id = $unique_id;
				// 	$tag->Tag_Soa_Id = $lastid;
				// 	$tag->Tag_Cla_Id = Input::get('class_id');
				// 	$tag->Tag_Tea_Id = $teachers->Use_Id;
				// 	$tag->Tag_Flag = Input::get('flag');
				// 	$tag->Tag_CreatedAt = date('Y-m-d H:i:s');
				// 	$tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
				// 	$tag->save();
				// }

	    //             foreach ($parentsDetails as $parents)
					// {
					// 	// $unique_id = "SOA".time();
					// 	$tag = new Socialactivitytag;
					// 	$tag->Tag_Use_Id = Input::get('user_id');
					// 	$tag->Tag_Soa_Unique_Id = $unique_id;
					// 	$tag->Tag_Soa_Id = $lastid;
					// 	$tag->Tag_Cla_Id = Input::get('class_id');
					// 	$tag->Tag_Par_Id = $parents->Use_Id;
					// 	$tag->Tag_Flag = Input::get('flag');
					// 	$tag->Tag_CreatedAt = date('Y-m-d H:i:s');
					// 	$tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
					// 	$tag->save();
					// }

					if($images){
						foreach ($soa as $value){
							$s = Socialactivitydetails::where('Sod_Use_Id',Input::get('user_id'))->get();
						}

						$s1[] = $s;
						$activitydata[] = $activity;
						$soadata = array_merge($activitydata,$s1);

						if($soadata){
							ResponseMessage::success("Activity Created Successfully",$soadata);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}else{
						if($activity){
							ResponseMessage::success("Activity Created Successfully",$activity);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}
				}
			}else if(Input::get('flag') == 4){

					$images = $request->file('image');

					$user_class_id = Input::get('bra_class_id');
					$student_class_id = explode(',', $user_class_id);

					if($images)
					{
	                    foreach ($images as $image)
						{
							if($image->getClientSize() > 26214400)
							{
								$error_size = true;
							}
						}
	                }if($error_size == true)
	                {
	                   ResponseMessage::error("Do Not Upload More Than 25MB File");                   
	                }else{

					if($images){
			            foreach($images as $file){
					        $filename = $file->getClientOriginalName();
					        $extension = $file->getClientOriginalExtension();
					        $picture[] = date('His').$filename;
					        $file->move(public_path('images/social'), date('His').$filename);
					    }
			        }else{
			            $images = "";
			        }

			        $unique_id = "SOA".time();
			    	$activity['Soa_Use_id'] = Input::get('user_id');
					$activity['Soa_User_Type'] = Input::get('user_type');
					$activity['Soa_Unique_id'] = $unique_id;
					$activity['Soa_Branch'] = Input::get('branch_id');
					$activity['Soa_Title'] = Input::get('title');
					$activity['Soa_Cla_Id'] = $class;
					$activity['Soa_Comment'] = Input::get('comment');
					$activity['Soa_Tag'] = Input::get('flag');
					$activity['Soa_Status'] = "0";
					$activity['Soa_CreatedBy'] = Input::get('user_id');
					$activity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
					$activity['Soa_UpdatedBy'] = Input::get('user_id');
					$activity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

					$lastid = Socialactivity::insertGetId($activity);
					
					if($images){
						foreach ($picture as $value){
							$unique_id = "SOA".time();
							$soa = new Socialactivitydetails;
							$soa->Sod_Img_Video = $value;
							$soa->Sod_Soa_Unique_Id = $unique_id;
							$soa->Sod_Use_Id = Input::get('user_id');
							$soa->Sod_Soa_Id = $lastid;
							$soa->Sod_Uploadtype = Input::get('file_type');
							$soa->save();
						}
					}
					
					$studentDetails = Student::whereIn('Std_Cla_Id',$student_class_id)->get();

					foreach ($studentDetails as $students)
					{   
						$unique_id = "SOA".time();
						$tag = new Socialactivitytag;
						$tag->Tag_Use_Id = Input::get('user_id');
						$tag->Tag_Soa_Unique_Id = $unique_id;
						$tag->Tag_Soa_Id = $lastid;
						$tag->Tag_Cla_Id = Input::get('class_id');
						$tag->Tag_Stu_Id = $students->Std_Id;
						$tag->Tag_Flag = Input::get('flag');
						$tag->Tag_CreatedAt = date('Y-m-d H:i:s');
						$tag->Tag_UpdatedAt = date('Y-m-d H:i:s');
						$tag->save();
					}

					if($images){
						foreach ($soa as $value){
							$s = Socialactivitydetails::where('Sod_Use_Id',Input::get('user_id'))->get();
						}

						$s1[] = $s;
						$activitydata[] = $activity;
						$soadata = array_merge($activitydata,$s1);

						if($soadata){
							ResponseMessage::success("Activity Created Successfully",$soadata);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}else{
						if($activity){
							ResponseMessage::success("Activity Created Successfully",$activity);
						}else{
							ResponseMessage::error("Activity Creation Fail");
						}
					}
			}
			}else if(Input::get('flag') == 0){
					$images = $request->file('image');

					if($images)
					{
	                    foreach ($images as $image)
						{
							if($image->getClientSize() > 26214400)
							{
								$error_size = true;
							}
						}
	                }if($error_size == true)
	                {
	                   ResponseMessage::error("Do Not Upload More Than 25MB File");                   
	                }else{

						if($images)
						{
				            foreach($images as $file){
						        $filename = $file->getClientOriginalName();
						        $extension = $file->getClientOriginalExtension();
						        $picture[] = date('His').$filename;
						        $file->move(public_path('images/social'), date('His').$filename);
						    }
				        }else{
				            $images = "";
				        }

				        $unique_id = "SOA".time();
					   	$activity['Soa_Use_id'] = Input::get('user_id');
						$activity['Soa_User_Type'] = Input::get('user_type');
						$activity['Soa_Unique_id'] = $unique_id;
						$activity['Soa_Branch'] = Input::get('branch_id');
						$activity['Soa_Title'] = Input::get('title');
						$activity['Soa_Cla_Id'] = $class;
						$activity['Soa_Comment'] = Input::get('comment');
						$activity['Soa_Tag'] = Input::get('flag');
						$activity['Soa_Status'] = "0";
						$activity['Soa_CreatedBy'] = Input::get('user_id');
						$activity['Soa_CreatedAt'] = date('Y-m-d H:i:s');
						$activity['Soa_UpdatedBy'] = Input::get('user_id');
						$activity['Soa_UpdatedAt'] = date('Y-m-d H:i:s');

						$lastid = Socialactivity::insertGetId($activity);

						if($images){
							foreach ($picture as $value){
								$unique_id = "SOA".time();
								$soa = new Socialactivitydetails;
								$soa->Sod_Img_Video = $value;
								$soa->Sod_Soa_Unique_Id = $unique_id;
								$soa->Sod_Use_Id = Input::get('user_id');
								$soa->Sod_Soa_Id = $lastid;
								$soa->Sod_Uploadtype = Input::get('file_type');
								$soa->save();
							}
						}

						if($images){
							foreach ($soa as $value) {
								$s = Socialactivitydetails::where('Sod_Use_Id',Input::get('user_id'))->get();
							}
							$s1[] = $s;
							$activitydata[] = $activity;
							$soadata = array_merge($activitydata,$s1);

							if($soadata){
								ResponseMessage::success("Activity Created Successfully",$soadata);
							}else{
								ResponseMessage::error("Activity Creation Fail");
							}
						}else{
							if($activity){
								ResponseMessage::success("Activity Created Successfully",$activity);
							}else{
								ResponseMessage::error("Activity Creation Fail");
							}
						}
					}
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Social Activity List
	public function activity_List(Request $request)
	{
		try {
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required',
				'branch_id' => 'required',
				'debug' => ''
				];

			$validator = Validator::make($request->all(), $rules);
			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}
			$user_id = Input::get('user_id');
			$branch_id = Input::get('branch_id');
			$parent_class_id = Input::get('class_id');
			$teacherClassIds = UserRights::teacherApi($user_id,"AssignClass");
			$class_id = ClassTbl::where('Cla_Bra_Id',$branch_id)->select('Cla_Id')->get()->toArray();

			if($request->debug=="true"){
				$ids = Socialactivity::select('Soa_Unique_Id')->whereIn('Soa_Cla_Id',$teacherClassIds)->where('Soa_Status','1')->groupBy('Soa_Unique_Id')->get()->toArray();
				$social = Socialactivity::with(['SoaImages','SoaLike' => function($query){
		 				$query->where('Sal_Use_Id',Input::get('user_id'));
		 			}])
					->whereIn('Soa_Cla_Id',$teacherClassIds)
		 			->groupBy('Soa_Unique_Id')
		 			->where('Soa_Status','1')
		 			->get();
				ResponseMessage::success("Debug list",$social);
				exit;
			}

		 	$social = Socialactivity::with(['SoaImages','SoaLike' => function($query){
		 				$query->where('Sal_Use_Id',Input::get('user_id'));
		 			}])
		 			// ->where('Soa_Use_Id',Input::get('user_id'))
		 			// ->where('Soa_Branch',$branch_id)
					->whereIn('Soa_Cla_Id',$teacherClassIds)
		 			// ->wherein('Soa_Cla_Id',$class_id)
		 			// ->orwhere('Soa_User_Type',1)
		 			->where('Soa_Status','1')
		 			->groupBy('Soa_Unique_Id')
		 			->orderBy('Soa_Id', 'DESC')
		 			->get();

			if($social){
				ResponseMessage::success("Social Activity List",$social);
			}else{
				ResponseMessage::error("Activity Record Not Found");
			}		
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}	
	}

	//Activity List For Parents 
	public function parents_activity_List(Request $request)
	{
		try {
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required',
				'branch_id' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$branch_id = Input::get('branch_id');
			$parent_class_id = Input::get('class_id');

			$cld_id = ClassTbl::where('Cla_Bra_Id',$branch_id)->select('Cla_Id')->get()->toArray();

			if(Student::where('Std_Parent_Id',Input::get('user_id'))->exists()){	
				$class_id = Student::where('Std_Parent_Id',Input::get('user_id'))->first()->Std_Cla_Id;
				$student_id = Student::where('Std_Parent_Id',Input::get('user_id'))->first()->Std_Id;
				$parent_soa = Socialactivitytag::where("Tag_Cla_Id",$class_id)->where("Tag_Stu_Id",$student_id)->select(["Tag_Soa_Id"])->get()->toArray();

				$social = Socialactivity::whereIn('Soa_Id',$parent_soa)->with(array('SoaImages','SoaLike' => function($query){
						$query->where('Sal_Use_Id',Input::get('user_id'));
					}))
					->where('Soa_Branch',$branch_id)
					->where('Soa_Cla_Id',$parent_class_id)
					// ->wherein('Soa_Cla_Id',$cld_id)
					// ->Orwhere('Soa_User_Type',1)
					->where('Soa_Status','1')
					->orderBy('Soa_Id', 'DESC')
					->get(); 

				if($social){
					ResponseMessage::success("Parent Social Activity List",$social);
				}else{
					ResponseMessage::error("Activity Not Found");
				}	
			}else{
					ResponseMessage::error("Activity Not Found");
				}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
  
     /**
	   * 
	   * User Like and Comment single image to display user wise like and comment list
	   *
	   * @author Jayesh Sukhadiya Date: 09-05-2018
	   *
	   * @param integer user_id =  User Id
	   * @param integer soa_id =  Social Activity Id / Post Id
	   * @param integer image_id = Image Id 
	   * @return JSON User wise like and comment in image to display list
	   */

         //Social Activity Image wise Like
	public function image_Like_List(Request $request)
	{
		try 
		{
			$rules = [
			'user_id' => 'required',
			'soa_id' => 'required',
			'image_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(user::where('Use_Id',Input::get('user_id'))->exists())
			{

				$image = Socialactivity_like_image::where('Sil_Use_Id',Input::get('user_id'))->where('Sil_Img_Id',Input::get('image_id'))->get();

				if($image)
				{	
					ResponseMessage::success("Activity Image Like List",$image);
				}else{
				ResponseMessage::error("Activity Like not Found");
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
		}catch (\Exception $e) {
		Exception::exception($e);	
		}
	}

        /**
	   *
	   * @author Jayesh Sukhadiya Date: 08-05-2018
	   *
	   * @param integer user_id =  User Id Teacher/Parent Id
	   * @param integer soa_id =  Social Activity Id
	   * @param integer image_id =  Image Id Whese Upload in Social Activity
	   * @param integer flag =  flag 1 => Like
	   * @param integer user_type =  User Type 2 => Teachers And 4 => Parents
	   * @return JSON Image like data to Show
	   */

	public function activity_Like_Image(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'soa_id' => 'required',
				'image_id' => 'required',
				'flag' => 'required',
				'user_type' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$user_id = Socialactivity_like_image::where('Sil_Use_Id',Input::get('user_id'))->get();
			$soa_id = Socialactivity_like_image::where('Sil_Soa_Id',Input::get('soa_id'))->get();
			$image_id = Socialactivity_like_image::where('Sil_Img_Id',Input::get('image_id'))->get();
			
			if(Input::get('user_type') == 2){
				if($user_id->isEmpty() || $soa_id->isEmpty() || $image_id->isEmpty() && Input::get('flag') == 1){
					
					$social_like = new Socialactivity_like_image;
			        $social_like->Sil_Use_Id = Input::get('user_id');
					$social_like->Sil_Soa_Id = Input::get('soa_id');
					$social_like->Sil_Img_Id = Input::get('image_id');
					$social_like->Sil_Like = Input::get('flag');
					$social_like->Sil_CreatedAt = date('Y-m-d H:i:s');
					$social_like->Sil_UpdatedAt = date('Y-m-d H:i:s');

					if($social_like->save()){
						ResponseMessage::success("Like",$social_like);
					}else{
						ResponseMessage::error("Fail");
					}
				}else if($user_id->isEmpty() && $soa_id->isEmpty() && $image_id->isEmpty() && Input::get('flag') == 1){
					$social_like = new Socialactivity_like_image;
			        $social_like->Sil_Use_Id = Input::get('user_id');
					$social_like->Sil_Soa_Id = Input::get('soa_id');
					$social_like->Sil_Img_Id = Input::get('image_id');
					$social_like->Sil_Like = Input::get('flag');
					$social_like->Sil_CreatedAt = date('Y-m-d H:i:s');
					$social_like->Sil_UpdatedAt = date('Y-m-d H:i:s');

					if($social_like->save()){
						ResponseMessage::success("Like",$social_like);
					}else{
						ResponseMessage::error("Fail");
					}
				}else{

					$social_delete = Socialactivity_like_image::where('Sil_Use_Id',Input::get('user_id'))->where('Sil_Soa_Id',Input::get('soa_id'))->where('Sil_Img_Id',Input::get('image_id'))->delete();

					if($social_delete){
						ResponseMessage::success("Dislike",$social_delete);
					}else{
						ResponseMessage::error("Fail");
					}
				}
			}

			else if(Input::get('user_type') == 4){
				if($user_id->isEmpty() || $soa_id->isEmpty() || $image_id->isEmpty() && Input::get('flag') == 1){
					
					$social_like = new Socialactivity_like_image;
			        $social_like->Sil_Use_Id = Input::get('user_id');
					$social_like->Sil_Soa_Id = Input::get('soa_id');
					$social_like->Sil_Img_Id = Input::get('image_id');
					$social_like->Sil_Like = Input::get('flag');
					$social_like->Sil_CreatedAt = date('Y-m-d H:i:s');
					$social_like->Sil_UpdatedAt = date('Y-m-d H:i:s');

					if($social_like->save()){
						ResponseMessage::success("Like",$social_like);
					}else{
						ResponseMessage::error("Fail");
					}
				}else if($user_id->isEmpty() && $soa_id->isEmpty() && $image_id->isEmpty() && Input::get('flag') == 1){
					$social_like = new Socialactivity_like_image;
			        $social_like->Sil_Use_Id = Input::get('user_id');
					$social_like->Sil_Soa_Id = Input::get('soa_id');
					$social_like->Sil_Img_Id = Input::get('image_id');
					$social_like->Sil_Like = Input::get('flag');
					$social_like->Sil_CreatedAt = date('Y-m-d H:i:s');
					$social_like->Sil_UpdatedAt = date('Y-m-d H:i:s');

					if($social_like->save()){
						ResponseMessage::success("Like",$social_like);
					}else{
						ResponseMessage::error("Fail");
					}
				}else{
					$social_delete = Socialactivity_like_image::where('Sil_Use_Id',Input::get('user_id'))->where('Sil_Soa_Id',Input::get('soa_id'))->where('Sil_Img_Id',Input::get('image_id'))->delete();

					if($social_delete){
						ResponseMessage::success("Dislike",$social_delete);
					}else{
						ResponseMessage::error("Fail");
					}
				}
			}else{
				ResponseMessage::error("Fail");
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Activity Like Post Wise
	public function activity_Like(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'soa_id' => 'required',
				'flag' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$social = Socialactivity_like::get();
			$user_id = Socialactivity_like::where('Sal_Use_Id',Input::get('user_id'))->where('Sal_Soa_Id',"=",Input::get('soa_id'))->get();
			$soa_id = Socialactivity_like::where('Sal_Soa_Id',Input::get('soa_id'))->where('Sal_Use_Id',"=",Input::get('user_id'))->get();
			//dd($soa_id);
			if(Input::get('user_type') == 2){
				if($user_id->isEmpty() || $soa_id->isEmpty() && Input::get('flag') == 1){
					
					$social_like = new Socialactivity_like;
			        $social_like->Sal_Use_Id = Input::get('user_id');
					$social_like->Sal_Soa_Id = Input::get('soa_id');
					$social_like->Sal_Use_Type = Input::get('user_type');
					$social_like->Sal_Like = Input::get('flag');
					$social_like->Sal_CreatedAt = date('Y-m-d H:i:s');
					$social_like->Sal_UpdatedAt = date('Y-m-d H:i:s');

					if($social_like->save()){
						ResponseMessage::success("Like",$social_like);
					}else{
						ResponseMessage::error("Fail");
					}
				}else if($user_id->isEmpty() && $soa_id->isEmpty() && Input::get('flag') == 1){
					$social_like = new Socialactivity_like;
			        $social_like->Sal_Use_Id = Input::get('user_id');
					$social_like->Sal_Soa_Id = Input::get('soa_id');
					$social_like->Sal_Use_Type = Input::get('user_type');
					$social_like->Sal_Like = Input::get('flag');
					$social_like->Sal_CreatedAt = date('Y-m-d H:i:s');
					$social_like->Sal_UpdatedAt = date('Y-m-d H:i:s');

					if($social_like->save()){
						ResponseMessage::success("Like",$social_like);
					}else{
						ResponseMessage::error("Fail");
					}
				}else{

					$social_delete = Socialactivity_like::where('Sal_Use_Id',Input::get('user_id'))->where('Sal_Soa_Id',Input::get('soa_id'))->delete();

					if($social_delete){
						ResponseMessage::success("Dislike",$social_delete);
					}else{
						ResponseMessage::error("Fail");
					}
				}
			}

			else if(Input::get('user_type') == 4){
				// if($user_id->isEmpty() || $soa_id->isEmpty() && Input::get('flag') == 1){
					
				// 	$social_like = new Socialactivity_like;
			 //        $social_like->Sal_Use_Id = Input::get('user_id');
				// 	$social_like->Sal_Soa_Id = Input::get('soa_id');
				// 	$social_like->Sal_Use_Type = Input::get('user_type');
				// 	$social_like->Sal_Like = Input::get('flag');
				// 	$social_like->Sal_CreatedAt = date('Y-m-d H:i:s');
				// 	$social_like->Sal_UpdatedAt = date('Y-m-d H:i:s');

				// 	if($social_like->save()){
				// 		ResponseMessage::success("Like",$social_like);
				// 	}else{
				// 		ResponseMessage::error("Fail");
				// 	}
				// }else if($user_id->isEmpty() && $soa_id->isEmpty() && Input::get('flag') == 1){
				// 	$social_like = new Socialactivity_like;
			 //        $social_like->Sal_Use_Id = Input::get('user_id');
				// 	$social_like->Sal_Soa_Id = Input::get('soa_id');
				// 	$social_like->Sal_Use_Type = Input::get('user_type');
				// 	$social_like->Sal_Like = Input::get('flag');
				// 	$social_like->Sal_CreatedAt = date('Y-m-d H:i:s');
				// 	$social_like->Sal_UpdatedAt = date('Y-m-d H:i:s');

				// 	if($social_like->save()){
				// 		ResponseMessage::success("Like",$social_like);
				// 	}else{
				// 		ResponseMessage::error("Fail");
				// 	}
				// }else{
				// 	$social_delete = Socialactivity_like::where('Sal_Use_Id',Input::get('user_id'))->where('Sal_Soa_Id',Input::get('soa_id'))->delete();
				// 	if($social_delete){
				// 		ResponseMessage::success("Dislike",$social_delete);
				// 	}else{
				// 		ResponseMessage::error("Fail");
				// 	}
				// }
				if(Input::get('flag') == 1){
					if($user_id->isEmpty() && $soa_id->isEmpty())
					{
						$social_like = new Socialactivity_like;
				        $social_like->Sal_Use_Id = Input::get('user_id');
						$social_like->Sal_Soa_Id = Input::get('soa_id');
						$social_like->Sal_Use_Type = Input::get('user_type');
						$social_like->Sal_Like = Input::get('flag');
						$social_like->Sal_CreatedAt = date('Y-m-d H:i:s');
						$social_like->Sal_UpdatedAt = date('Y-m-d H:i:s');

						if($social_like->save()){
							ResponseMessage::success("Like",$social_like);
						}else{
							ResponseMessage::error("Fail");
						}
					}else{
							ResponseMessage::error("already liked");
						}
				}else{
					$social_delete = Socialactivity_like::where('Sal_Use_Id',Input::get('user_id'))->where('Sal_Soa_Id',Input::get('soa_id'))->delete();
					if($social_delete){
						ResponseMessage::success("Dislike",$social_delete);
					}else{
						ResponseMessage::error("Fail");
					}
				}
			}else{
				ResponseMessage::error("Fail");
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}	
	}

	//Activity Comment in Post
	public function activity_Comment(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'soa_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$comment = new Socialactivity_comment;
			$comment->Sac_Use_Id = Input::get('user_id');
			$comment->Sac_Soa_Id = Input::get('soa_id');
			$comment->Sac_Comment = Input::get('comment');
			$comment->Sac_Use_Type = Input::get('user_type');
			$comment->Sac_CreatedAt = date('Y-m-d H:i:s');
			$comment->Sac_UpdatedAt = date('Y-m-d H:i:s');
			$comment->save();
			$comment->Sac_Id;

			if($comment){
				ResponseMessage::success("Add your comment Successfully",$comment);
			}else{
				ResponseMessage::error("Fail to add Comment");
			}

		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

        /**
	   * 
	   * User add comment for single image
	   *
	   * @author Jayesh Sukhadiya Date: 09-05-2018
	   *
	   * @param integer user_id =  User Id
	   * @param integer soa_id =  Social Activity Id / Post Id
	   * @param integer image_id = Image Id 
	   * @return JSON User create comment single record
	   */

	//In Activity to pass comment image wise
	public function create_Comment_Image(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'soa_id' => 'required',
				'image_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(user::where('Use_Id',Input::get('user_id'))->exists())
			{
				$comment = new Socialactivity_img_cmt;
				$comment->Sic_Use_Id = Input::get('user_id');
				$comment->Sic_Soa_Id = Input::get('soa_id');
				$comment->Sic_Img_Id = Input::get('image_id');
				$comment->Sic_Comment = Input::get('comment');
				$comment->Sic_Use_Type = Input::get('user_type');
				$comment->Sic_CreatedAt = date('Y-m-d H:i:s');
				$comment->Sic_UpdatedAt = date('Y-m-d H:i:s');
				$comment->save();
				$comment->Sac_Id;

				if($comment){
					ResponseMessage::success("Add your comment Successfully",$comment);
				}else{
					ResponseMessage::error("Fail to add comment");
				}
			}else{
				ResponseMessage::error('User Not Found');
			}


		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Comment List Activity wise
	public function comment_List(Request $request)
	{
		try {	
		 	$rules = [
				'soa_id' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

		 	$like = Socialactivity_comment::with('SoaComment')
			->where(function($query){
				$query->where('Sac_Soa_Id',Input::get('soa_id'));
			})->get();

			if($like){
				ResponseMessage::success("Activity Comment Record",$like);
			}else{
				ResponseMessage::error("No Found Activity Comment Record");
			}		
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}	
	} 

        /**
	   * 
	   * User Like and Comment single image to display user comment list
	   *
	   * @author Jayesh Sukhadiya Date: 09-05-2018
	   *
	   * @param integer soa_id =  Social Activity Id / Post Id
	   * @return JSON User wise  comment in image to display list
	   */

	public function comment_Image_List(Request $request)
	{
		try {	
		 	$rules = [
				'soa_id' => 'required',
				'image_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

		 	$like = Socialactivity_img_cmt::select('Sic_Id','Sic_Use_Id','Sic_Soa_Id','Sic_Comment','Sic_Use_Type','Sic_CreatedAt')->with(['SicComment' => function($query){
		 		$query->select('Use_Id','Use_Name','Use_Image');
		 	}])->where('Sic_Soa_Id',Input::get('soa_id'))->where('Sic_Img_Id',Input::get('image_id'))->get();

			if($like){
				ResponseMessage::success("Activity Image Comment Record",$like);
			}else{
				ResponseMessage::error("No Found Activity Image Comment Record");
			}		
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}	
	} 

      /**
	   * 
	   * User Like  single post to display user post list
	   *
	   * @author Jayesh Sukhadiya Date: 11-05-2018
	   * @param integer user_id = User_Id 
	   * @param integer soa_id =  Social Activity Id / Post Id
	   * @return JSON User wise  comment in image to display list
	   */

	public function activity_Like_List(Request $request)
	{
		try {	
		 	$rules = [
		 		'user_id' => 'required',
				'soa_id' => 'required'
				
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

		 	$like = Socialactivity_like::where('Sal_Use_Id',Input::get('user_id'))->where('Sal_Soa_Id',Input::get('soa_id'))->get();

			if($like){
				ResponseMessage::success("Activity Post Like Record",$like);
			}else{
				ResponseMessage::error("No Found Activity Post Like Record");
			}		
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}	
	} 

    //Activity Tag List Student display selected
    public function tag_List(Request $request)
	{
		try
		{
			$rules = [
			'soa_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			$tag = Socialactivitytag::with([
                'TagTeacher' => function($query){
                    $query->select('user_tbl.Use_Id','user_tbl.Use_Name','user_tbl.Use_Image','user_tbl.Use_Type'); 
                },
                'TagParent' => function($parent){
                    $parent->select('user_tbl.Use_Id','user_tbl.Use_Name','user_tbl.Use_Image','user_tbl.Use_Type'); 
                },
                'TagStudent'=>function($students){
                    $students->select('student_tbl.Std_Id','student_tbl.Std_Name','student_tbl.Std_Image'); 
                }
        ])->where('Tag_Soa_Id',Input::get('soa_id'))->get();

			if($tag){
				ResponseMessage::success("Activity Tag Record",$tag);
			}else{
				ResponseMessage::error("Activity Tag Not Found");
			}
		}catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}

	public function get_student_Class_list(Request $request){
		try{
			$rules = [
				'user_id' => 'required',
				'class_id' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(Users::where('Use_Id',Input::get('user_id'))->exists())
			{
				$student = Student::where('Std_Cla_Id',Input::get('class_id'))->get();
				if($student){
					ResponseMessage::success("Student List Successfully",$student);
				}else{
				 	ResponseMessage::error("Record Not Found");
				}
			}else{
				ResponseMessage::error("User Not Found");
			}

		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function branch_wise_Class_List(Request $request)
	{
		try 
		{
			$rules = [
				'user_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				if(Users::where('Use_Id',Input::get('user_id'))->where('Use_Type','2')->exists())
				{
					$user_id = Input::get('user_id');
					$teacherClassIds = UserRights::teacherApi($user_id,"AssignClass");
					$class_list = ClassTbl::whereIn('Cla_Id',$teacherClassIds)->get();
					if($class_list)
					{
						ResponseMessage::success('Branch Class List',$class_list);
					}else{
						ResponseMessage::error('Record Not Found');
					}
				}else{
					ResponseMessage::error("Teacher Not Found");
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function get_teacher_all_student(Request $request){
		try{
			$rules = [
				'user_id' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				if(Users::where('Use_Id',Input::get('user_id'))->where('Use_Type','2')->exists())
				{
					$teacherId = Input::get('user_id');
					$teacherClassIds = UserRights::teacherApi($teacherId,"AssignClass");
					$student = Student::whereIn('Std_Cla_Id',$teacherClassIds)->select(['student_tbl.Std_Id','student_tbl.Std_Name','student_tbl.Std_Image'])->get();
					if($student){
						ResponseMessage::success("Student List Successfully",$student);
					}else{
					 	ResponseMessage::error("Record Not Found");
					}
				}else{
					ResponseMessage::error("Teacher Not Found");
				}
			}
		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
	}
}