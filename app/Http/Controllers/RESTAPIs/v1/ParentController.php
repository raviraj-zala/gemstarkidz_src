<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\User;
use App\Model\Student;
use App\Model\Branch;
use App\Helper\Exceptions;
use Validator;
use App\Model\ClassTbl;
use Exception;

class ParentController extends Controller
{
    public function get_all_student(Request $request)
    {
    	try {
    		$rules = [
                'parent_id' => 'required',
            ];
            $customeMessage = [
                'parent_id.required' => 'Please sent parent id.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ){
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else{
	        	$parent_id = trim(strip_tags($request->parent_id));
	        	if(User::where('Use_Id',$parent_id)->where('Use_Type','4')->where('Use_Status','1')->exists()){
	        		$student = Student::select('student_tbl.*','class_tbl.Cla_Class','class_tbl.Cla_Section','branch_tbl.Brn_Id')
                                            ->where('Std_Parent_Id',$parent_id)
                                            ->join('class_tbl','class_tbl.Cla_Id','=','student_tbl.Std_Cla_Id')
                                            ->join('branch_tbl','branch_tbl.Brn_Id','=','class_tbl.Cla_Bra_Id')
                                            ->get();
	        		ResponseMessage::success("Student List.",$student);
	        	} else{
	        		ResponseMessage::error("Parent not found, please try again.");	
	        	}
	        }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
