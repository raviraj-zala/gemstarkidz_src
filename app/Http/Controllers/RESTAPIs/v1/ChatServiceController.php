<?php

namespace App\Http\Controllers\RESTAPIs\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\User;
use App\Model\Student;
use App\Model\Branch;
use App\Helper\Exceptions;
use App\Model\Payment;
use Validator;
use App\Model\ClassTbl;
use App\Model\GroupMaster;
use App\Model\GroupDetail;
use App\Model\Chat;
use Exception;
use DB;

class ChatServiceController extends Controller
{
    public function restore_chat(Request $request)
    {
        try {
            $rules = [
                'user_id' => 'required',
            ];
            $customeMessage = [
                'user_id.required' => 'Please sent user id.',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);
            if( $validator->fails() ){
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else{
                $user_id = trim(strip_tags($request->user_id));
                if(User::where('Use_Id',$user_id)->exists()){

                    // if(isset($request->debug) && $request->debug=="true"){
                    //     $chat_history = Chat::select('chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage',DB::raw('MAX(chat_tbl.Cha_CreatedAt) AS max_Cha_CreatedAt'))
                    //             ->where('chat_tbl.Cha_Sender_Id',$user_id)->orwhere('chat_tbl.Cha_Receiver_Id',$user_id)
                    //             ->join('user_tbl','user_tbl.Use_Id','=','chat_tbl.Cha_Receiver_Id')
                    //             ->orderBy('max_Cha_CreatedAt','DESC')
                    //             ->groupBy('chat_tbl.Cha_Receiver_Id')
                    //             ->get();

                    //     ResponseMessage::success('Debug',$chat_history);
                    //     exit();
                    // }


                    $chat_history = Chat::select('chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage',DB::raw('MAX(chat_tbl.Cha_CreatedAt) AS Cha_CreatedAt'))
                                ->where('chat_tbl.Cha_Sender_Id',$user_id)->orwhere('chat_tbl.Cha_Receiver_Id',$user_id)
                                ->join('user_tbl','user_tbl.Use_Id','=','chat_tbl.Cha_Receiver_Id')
                                ->orderBy('Cha_CreatedAt','DESC')
                                ->groupBy('chat_tbl.Cha_Receiver_Id')
                                ->get();

                        ResponseMessage::success('Chat History',$chat_history);
                        exit();


                    // $chat_history = Chat::select('chat_tbl.*','user_tbl.Use_Name as receiverName','user_tbl.Use_Image as receiverImage')
                    //                     ->where('chat_tbl.Cha_Sender_Id',$user_id)->orwhere('chat_tbl.Cha_Receiver_Id',$user_id)
                    //                     // ->where('Cha_Receiver_Id','!=',$user_id)
                    //                     ->join('user_tbl','user_tbl.Use_Id','=','chat_tbl.Cha_Receiver_Id')
                    //                     ->where('user_tbl.Use_Id','!=',null)
                    //                     ->groupBy('chat_tbl.Cha_Receiver_Id')
                    //                     ->get();
                    // ResponseMessage::success('Chat History',$chat_history);
                } else{
                    ResponseMessage::error("User not found, please try again.");     
                }
            }
        } catch (Exception $e) {
            Exceptions::exception($e);
        }
    }
}
