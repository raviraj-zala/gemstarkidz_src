<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Branch;
use App\Model\Student;
use Auth;
use Input;
use Validator;
use DB;
use Hash;

class WebservicesController extends Controller 
{

	/**
	   * 
	   * User Sign In Using his username/password
	   *
	   * @author Jayesh Sukhadiya
	   *
	   * @param username  = User Name / Mobile No
	   * @param password = User Password
	   * @return JSON User Details
	   * Note : Use_Type = 2 (Teacher), Use_Type = 3(Driver), Use_Type = 4(Parent)
	   * When user login then his token is update every time.when he/she sign in
	   */

	//User Login Api
	public function sign_In(Request $request)
	{
		try{	
			$username = Input::get('username');
			$password = Input::get('password');

			if(is_numeric($username)){
				$user = User::where('Use_Mobile_No','=',$username)->first();		
				$mother = User::where('Use_Mother_Mobile_No','=',$username)->first();
				if($user){
					$userStatus = User::where('Use_Mobile_No','=',$username)->first()->Use_Status;
					if($userStatus == "1"){
						$pass = $user->Use_Password;
						if(Hash::check($password,$pass)){
							if($user->Use_Type == 2){
								$user = User::where('Use_Mobile_No','=',$username)->join('class_tbl as cls','user_tbl.Use_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_id','=','cls.Cla_Bra_id')->select(['user_tbl.*','cls.*','branch_tbl.Brn_Image'])->first();
								$update_req['Use_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Mobile_No',$username)->update($update_req);
							}else if($user->Use_Type == 3){
								$user = User::where('Use_Mobile_No','=',$username)->first();
								$update_req['Use_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Mobile_No',$username)->update($update_req);

							}else if($user->Use_Type == 4){
								$user = User::where('Use_Mobile_No','=',$username)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_id','=','cls.Cla_Bra_id')->select(['user_tbl.*','stu.*','cls.*','branch_tbl.Brn_Image'])->first();

								$update_req['Use_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Mobile_No',$username)->update($update_req);
							}
							ResponseMessage::success("Login Successfully",$user);
						}else{
							ResponseMessage::error("Please Enter Valid Password");
						}
					}else{
						ResponseMessage::error("User In-Active");
					}
				}else if($mother){
					$motherStatus = User::where('Use_Mother_Mobile_No','=',$username)->first()->Use_Status;
					if($motherStatus=="1"){
						$mother_pass = $mother->Use_Mother_Password;
						if(Hash::check($password,$mother_pass)){
							if($mother->Use_Type == 4){
								$mother = User::where('Use_Mother_Mobile_No','=',$username)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_id','=','cls.Cla_Bra_id')->select(['user_tbl.*','stu.*','cls.*','user_tbl.Use_Mother_Name as Use_Name','user_tbl.Use_Mother_Mobile_No as Use_Mobile_No','user_tbl.Use_Mother_Email as Use_Email','user_tbl.Use_Mother_Image as Use_Image','branch_tbl.Brn_Image'])->first();

								$update_req['Use_Mother_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Mother_Mobile_No',$username)->update($update_req);
							}
							ResponseMessage::success("Login Successfully",$mother);
						}else{
							ResponseMessage::error("Please Enter Valid Password");
						}
					}else{
						ResponseMessage::error("User In-Active");	
					}
				}

				else{
					ResponseMessage::error("Please Enter Valid Mobile No.");
				}
			}
			if(!is_numeric($username)){
				$user = User::where('Use_Email','=',$username)->first();
				$mother = User::where('Use_Mother_Email','=',$username)->first();		
				if($user){
					$userStatus = User::where('Use_Email','=',$username)->first()->Use_Status;
					if($userStatus=="1"){
						$pass = $user->Use_Password;
						if(Hash::check($password,$pass)){
							if($user->Use_Type == 2){
								$user = User::where('Use_Email','=',$username)->join('class_tbl as cls','user_tbl.Use_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_Id','=','cls.Cla_Bra_Id')->select(['user_tbl.*','cls.*','branch_tbl.Brn_Image'])->first();	
								$update_req['Use_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Email',$username)->update($update_req);	
							}else if($user->Use_Type == 3){
								$user = User::where('Use_Email',$username)->first();
								$update_req['Use_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Email',$username)->update($update_req);	
							}else if($user->Use_Type == 4){
								$user = User::where('Use_Email','=',$username)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_id','=','cls.Cla_Bra_id')->select(['user_tbl.*','stu.*','cls.*','branch_tbl.Brn_Image'])->first();
								$update_req['Use_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Email',$username)->update($update_req);	
							}
							ResponseMessage::success("Login Successfully",$user);
						}else{
							ResponseMessage::error("Please Enter Valid Password");
						}
					}else{
						ResponseMessage::error("User In-Active");
					}
				}else if($mother){
					$motherStatus = User::where('Use_Mother_Email','=',$username)->first()->Use_Status;
					if($motherStatus){
						$mother_pass = $mother->Use_Mother_Password;
						if(Hash::check($password,$mother_pass)){
							if($mother->Use_Type == 4){
								$mother = User::where('Use_Mother_Email','=',$username)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_id','=','cls.Cla_Bra_id')->select(['user_tbl.*','stu.*','cls.*','user_tbl.Use_Mother_Name as Use_Name','user_tbl.Use_Mother_Mobile_No as Use_Mobile_No','user_tbl.Use_Mother_Email as Use_Email','user_tbl.Use_Mother_Image as Use_Image','branch_tbl.Brn_Image'])->first();

								$update_req['Use_Mother_Token'] = Input::get('token');
	        					$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        					$update = User::where('Use_Mother_Email',$username)->update($update_req);
							}
							ResponseMessage::success("Login Successfully",$mother);
						}else{
							ResponseMessage::error("Please Enter Valid Password");
						}
					}else{
						ResponseMessage::error("User In-Active");
					}
				}
				else{
					ResponseMessage::error("Please Enter Valid Email Id");
				}	
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function user_profile_Record(Request $request)
	{
		try 
		{
			$rules = [
				'user_id' => 'required',
				'mobile_no' => '',
				'email' => '',
				'student_id' => '',
				];

			$validator = Validator::make($request->all(), $rules);
			$user_id = Input::get("user_id");
			$mobile_no = Input::get("mobile_no");
			$email = Input::get("email");
			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				$user_id = Input::get('user_id');
				$type = User::where('Use_Id','=',$user_id)->first()->Use_Type;
				if($type=="4"){
					if($mobile_no != "" ){
						if(User::where('Use_Id','=',$user_id)->where("Use_Mobile_No",$mobile_no)->exists()){
						$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*'])->get();
						}elseif(User::where('Use_Id','=',$user_id)->where("Use_Mother_Mobile_No",$mobile_no)->exists()){
							$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*','user_tbl.Use_Mother_Name as Use_Name','user_tbl.Use_Mother_Mobile_No as Use_Mobile_No','user_tbl.Use_Mother_Email as Use_Email','user_tbl.Use_Mother_Image as Use_Image'])->get();
						}else{
							$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*'])->get();
						}
					}elseif($email !="" ){
						if(User::where('Use_Id','=',$user_id)->where("Use_Email",$email)->exists()){
							$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*'])->get();
						}elseif(User::where('Use_Id','=',$user_id)->where("Use_Mother_Email",$email)->exists()){
							$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*','user_tbl.Use_Mother_Name as Use_Name','user_tbl.Use_Mother_Mobile_No as Use_Mobile_No','user_tbl.Use_Mother_Email as Use_Email','user_tbl.Use_Mother_Image as Use_Image'])->get();
						}else{
							$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*'])->get();
						}
					}else{
						$userRecord = User::where('Use_Id','=',$user_id)->leftjoin('student_tbl as stu','user_tbl.Use_Id','=','stu.Std_Parent_Id')->leftjoin('class_tbl as cls','stu.Std_Cla_Id','=','cls.Cla_Id')->select(['user_tbl.*','stu.*','cls.*'])->get();
					}
				}elseif($type=="2"){
					$userRecord = User::where('Use_Id',$user_id)->join('class_tbl as cls','user_tbl.Use_Cla_Id','=','cls.Cla_Id')->join('branch_tbl','branch_tbl.Brn_id','=','cls.Cla_Bra_id')->select(['user_tbl.*','cls.*','branch_tbl.Brn_Image'])->first();
				}
				if($userRecord)
				{
					ResponseMessage::success('User Profile List',$userRecord);
				}else{
					ResponseMessage::error('Record Not Found');
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function user_Mobile_No_Valid(Request $request)
	{
		try 
		{
			$rules = [
				'mobile_no' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}
			if(User::where('Use_Mobile_No',Input::get('mobile_no'))->exists()){
				$mobile_no = Input::get('mobile_no');
				if($mobile_no){
					ResponseMessage::success("Mobile Number Valid",$mobile_no);
				}else{
					ResponseMessage::error("Mobile No Not Register");
				}
			}else if(User::where('Use_Mother_Mobile_No',Input::get('mobile_no'))->exists()){
				$mobile_no = Input::get('mobile_no');
				if($mobile_no){
					ResponseMessage::success("Mobile Number Valid",$mobile_no);
				}else{
					ResponseMessage::error("Mobile No Not Register");
				}
			}else{
					ResponseMessage::error("Mobile No Not Register");
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function user_Password_Change(Request $request)
	{
		try 
		{
		    $rules = [
				'user_id' => 'required',
				'old_password' => 'required',
				'password' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$user = User::where('Use_Id','=',Input::get('user_id'))->first();
			$password = Input::get('old_password');

			if(User::where('Use_Id',Input::get('user_id'))->exists()){

					$pass = $user->Use_Password;
					if(Hash::check($password,$pass)){

					$update_req['Use_Password'] = bcrypt(Input::get('password'));
	        		$update_req['Use_UpdatedBy'] = Input::get('user_id');
	        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        		$update = User::where('Use_Id',Input::get('user_id'))->update($update_req);

	        		if($update){
						ResponseMessage::success("Password Change Successfully",$update_req);
					}else{
						ResponseMessage::error("Password Change Fail");
					}
				}else{
				ResponseMessage::error("Old Password is Worng");
				}
			}else{
				ResponseMessage::error("User is Not Exits");
			}

		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function user_Forget_Password(Request $request)
	{
		try 
		{
		    $rules = [
				'mobile_no' => 'required',
				'password' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Mobile_No',Input::get('mobile_no'))->exists()){
					$update_req['Use_Password'] = bcrypt(Input::get('password'));
	        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        		$update = User::where('Use_Mobile_No',Input::get('mobile_no'))->update($update_req);

	        		if($update){
						ResponseMessage::success("Password Change Successfully",$update_req);
					}else{
						ResponseMessage::error("Password Change Fail");
					}
			}else if(User::where('Use_Mother_Mobile_No',Input::get('mobile_no'))->exists()){
					$update_req['Use_Mother_Password'] = bcrypt(Input::get('password'));
	        		$update_req['Use_UpdatedAt']  = date('Y-m-d H:i:s');
	        		$update = User::where('Use_Mother_Mobile_No',Input::get('mobile_no'))->update($update_req);

	        		if($update){
						ResponseMessage::success("Password Change Successfully",$update_req);
					}else{
						ResponseMessage::error("Password Change Fail");
					}
			}else{
				ResponseMessage::error("Mobile Number is Not Exists");
			}

		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
	
	public function logout(Request $request){
		try {
			$rules = [
				'user_id' => 'required',
				'mobile_no' => '',
				'email' => ''
				];
			$validator = Validator::make($request->all(), $rules);
			$user_id = Input::get('user_id');
			$mobile_no = Input::get('mobile_no');
			$email = Input::get('email');
			$data = "";

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				if(User::where('Use_Id',$user_id)->exists()){
					if($mobile_no!=""){
						if(User::where("Use_Id",$user_id)->where("Use_Mobile_No",$mobile_no)->exists()){
							$user = User::find($user_id);
							$user->Use_Token = "";
							$user->update();
							ResponseMessage::success("Logout Successfully",$data);	
						}elseif(User::where("Use_Id",$user_id)->where("Use_Mother_Mobile_No",$mobile_no)->exists()){
							$user = User::find($user_id);
							$user->Use_Mother_Token = "";
							$user->update();
							ResponseMessage::success("Logout Successfully",$data);	
						}
					}elseif($email!=""){
						if(User::where("Use_Id",$user_id)->where("Use_Email",$email)->exists()){
							$user = User::find($user_id);
							$user->Use_Token = "";
							$user->update();	
							ResponseMessage::success("Logout Successfully",$data);	
						}elseif(User::where("Use_Id",$user_id)->where("Use_Mother_Email",$email)->exists()){
							$user = User::find($user_id);
							$user->Use_Mother_Token = "";
							$user->update();
							ResponseMessage::success("Logout Successfully",$data);	
						}
					}else{
						$logout['Use_Token'] = "";
						$update = User::where('Use_Id',Input::get('user_id'))->update($logout);
						ResponseMessage::success("Logout Successfully",$data);	
					}
				}else{
					ResponseMessage::error("User Not Exist");
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function userStatus(Request $request)
	{
		try {
			$rules = [
				'user_id' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);
			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				$user_id = Input::get('user_id');
				if(User::where('Use_Id',$user_id)->exists())
				{
					$data = User::where('Use_Id',$user_id)->first()->Use_Status;
					if($data=="1")
					{
						ResponseMessage::success("Status Active",$data);
					}
					else
					{
						ResponseMessage::error("Status In-Active");
					}
				}else{
					ResponseMessage::error("User Not Exist");	
				}
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
}