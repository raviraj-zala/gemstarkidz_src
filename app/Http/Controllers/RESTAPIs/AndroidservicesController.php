<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Notification;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Student;
use App\Model\Attendence;
use App\Model\ClassTbl;
use App\Model\Branch;
use Auth;
use Input;
use Validator;
use DB;

class AndroidservicesController extends Controller
{
	public function android_version(Request $request)
	{
		try {
			$rules = [
				'version' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);
			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				$version = Input::get('version');
				$data = array();
				if($version=="1.3.3"){
					ResponseMessage::success("Your App Is Up To Date.",$data);
				}else{
					ResponseMessage::error("New Version Available");
				}
			}
		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function suraj_android_version(Request $request)
	{
		try {
			$rules = [
				'version' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);
			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				$version = Input::get('version');
				$data = array();
				if($version=="1.1"){
					ResponseMessage::success("Your App Is Up To Date.",$data);
				}else{
					ResponseMessage::error("New Version Available");
				}
			}
		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	public function get_branch_image(Request $request)
	{
		try {
			$rules = [
				'branch_id' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);
			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				$branch_id = Input::get('branch_id');
				if(Branch::where('Brn_Id',$branch_id)->exists()){
					$branch = Branch::where('Brn_Id',$branch_id)->get()->first();
					ResponseMessage::success("Brnch image",["Brn_Image"=>$branch->Brn_Image]);
				} else{
					ResponseMessage::error("Branch not found.");	
				}
			}
		} catch (Exception $e) {
			Exceptions::exception($e);
		}
	}
}