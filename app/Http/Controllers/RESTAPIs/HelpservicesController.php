<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Student;
use App\Model\Event;
use App\Model\Count;
use App\Model\HelpTbl;
use Auth;
use Input;
use Validator;
use DB;

class HelpservicesController extends Controller
{
	public function add_help(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'description' => 'required',
				'user_type' => 'required',
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}else{
				$userId = Input::get('user_id');
				$description = Input::get('description');
				$user_type = Input::get('user_type');		
				if(User::where('Use_Id',$userId)->exists()){
					$help = new HelpTbl;
					$help->Help_Desctiprion = $description;
					$help->Help_Use_Id = $userId;
					$help->Help_Use_Type = $user_type;
					$help->Help_CreatedAt = date("Y-m-d H:i:s");
					if($help->save()){
						$data = array();
						ResponseMessage::success("Successfully Submitted.",$data);
					}else{
						ResponseMessage::error("Not Submitted.");
					}
				}else{
					ResponseMessage::error("User Not Found");
				}
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
}