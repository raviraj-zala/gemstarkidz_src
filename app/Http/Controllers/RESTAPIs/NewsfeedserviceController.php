<?php

namespace App\Http\Controllers\RESTAPIs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Module;
use App\Helper\UserRights;
use App\Helper\Notification;
use App\Helper\SMSHelper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\Model\Admin_assign_branch;
use App\Model\Student;
use App\Model\ClassTbl;
use App\Model\Newsfeed;
use App\Model\Newsfeedinfo;
use App\Model\Newsfeedtag;
use App\Model\Branch;
use App\User;
use Validator;
use DB;

class NewsfeedserviceController extends Controller
{
    public function create_newsfeed(Request $request)
    {
    	try {
    		$rules = [
    			'title' => 'required',
    			'description' => 'required',
    			'imageOrVideo' => 'max:25600',
    			'tag_type' => 'required',
    			'user_id' => 'required',
    			'classes' => '',
    			'students' => '',
    		];
    		$customeMessage = [
    			'title.required' => 'Please enter title',
    			'description.required' => 'Please enter description.',
    			'imageOrVideo.mimes' => 'Invalid image or video type.',
    			'imageOrVideo.max' => 'Image or Video Per max size is 25MB',
    			'tag_type.required' => 'Please send tag type',
    			'user_id.required' => 'Please send user id',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
					exit;
				}
            }else{
            	$picture = array();
            	$images = $request->file('imageOrVideo');
            	$error_size = false;
	    		if($images)
				{
                    foreach ($images as $image)
					{
						if($image->getClientSize() > 26214400)
						{
							$error_size = true;
						}
					}
                }
                if($error_size == true)
                {
                   ResponseMessage::error("Do Not Upload More Than 25MB File"); 
                   exit;                  
                }
                else
                {
					if($images)
					{
						$i = 0;
			            foreach($images as $file)
			            {
					        $filename = $file->getClientOriginalName();
					        $extension = $file->getClientOriginalExtension();
					        $mime = $file->getMimeType();
	                        if(strstr($mime, "video/")){
	                            $mediaType = "2";
	                        }else if(strstr($mime, "image/")){
	                            $mediaType = "1";
	                        }
	                        $picture[] = ['path' => $i.time().".".$extension,'type'=>$mediaType];
					        $file->move(public_path('attechment/newsfeed'), $i.time().".".$extension);
                          	$i++;
						}
			        }else{
			            $picture = "";
			        }
		        }
        		
            	$title = trim($request->title);
            	$description = trim($request->description);
            	$status = "0";
            	$user_type = "2";
            	$user_id = $request->user_id;
            	$tag_type = $request->tag_type;
            	$created_at = date('Y-m-d H:i:s');
            	$created_by = $user_id;
                $unique_id = "NFD".time();
                $classes = trim($request->classes);
                $students = trim($request->students);

            	$teacherBranch = UserRights::teacherApi($user_id,"branchAccess");
                $newsfeed = New Newsfeed;
                $newsfeed->Nfd_Unique_Id = $unique_id;
                $newsfeed->Nfd_Brn_Id = $teacherBranch;
                $newsfeed->Nfd_Title = $title;
                $newsfeed->Nfd_Description = $description;
                $newsfeed->Nfd_Status = $status;
                $newsfeed->Nfd_User_Type = $user_type;
                $newsfeed->Nfd_Tag_Type = $tag_type;
                $newsfeed->Nfd_CreatedAt = $created_at;
                $newsfeed->Nfd_CreatedBy = $created_by;
                if($newsfeed->save()){
                	if(count($picture)!=0){
	            		foreach ($picture as $pic) {
		            		$newsfeedInfo = New Newsfeedinfo;	
		            		$newsfeedInfo->Nfi_Nfd_Unique_Id = $unique_id;
		            		$newsfeedInfo->Nfi_Path = $pic['path'];
		            		$newsfeedInfo->Nfi_Type = $pic['type'];
		            		$newsfeedInfo->Nfi_CreatedAt = date('Y-m-d H:i:s');
		            		$newsfeedInfo->save();
	            		}
            		}
                    if($tag_type == "class"){
                        $class_ids = explode(",", $classes);
                        $student_list = Student::whereIn('Std_Cla_Id',$class_ids)->get();
                        foreach ($student_list as $student) 
                        {
	                        $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
	                        $newsfeedTag = New Newsfeedtag;
	                        $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
	                        $newsfeedTag->Nft_Std_Id = $student->Std_Id;
	                        $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
	                        $newsfeedTag->Nft_Brn_Id = $branch_id;
	                        $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
	                        $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
	                        $newsfeedTag->save();
                        }
                        ResponseMessage::success("Newsfeed Created Successfully",$newsfeed);
                    }elseif($tag_type == "student"){
                        $student_ids = explode(",", $students);
                        $student_list = Student::whereIn('Std_Id',$student_ids)->get();
                        foreach ($student_list as $student) 
                        {
                            $branch_id = ClassTbl::where('Cla_Id',$student->Std_Cla_Id)->get()->first()->Cla_Bra_Id;
                            $newsfeedTag = New Newsfeedtag;
                            $newsfeedTag->Nft_Nfd_Unique_Id = $unique_id;
                            $newsfeedTag->Nft_Std_Id = $student->Std_Id;
                            $newsfeedTag->Nft_Cla_Id = $student->Std_Cla_Id;
                            $newsfeedTag->Nft_Brn_Id = $branch_id;
                            $newsfeedTag->Nft_Parent_Id = $student->Std_Parent_Id;
                            $newsfeedTag->Nft_CreatedAt = date('Y-m-d H:i:s');
                            $newsfeedTag->save();
                        }
                        ResponseMessage::success("Newsfeed Created Successfully",$newsfeed);
                    }
                }
            	else{
        			ResponseMessage::error("Newsfeed not create please try again."); 
            	}
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function teacher_newsfeed_list(Request $request)
    {
    	try 
    	{
    		$rules = [
    			'user_id' => 'required'
    		];
    		$customeMessage = [
    			'user_id.required' => 'Please send user id',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
					exit;
				}
            }else{
            	$user_id = $request->user_id;
            	if(User::where('Use_Id',$user_id)->where('Use_Type','2')->exists())
            	{
	            	$teacherAssignClass = UserRights::teacherApi($user_id,'AssignClass');
	            	$teacherAssignBranch = UserRights::teacherApi($user_id,'branchAccess');
	            	$tagList =  Newsfeedtag::whereIn('Nft_Cla_Id',$teacherAssignClass)->select('Nft_Nfd_Unique_Id')->groupBy('Nft_Nfd_Unique_Id')->get();
	                $newsfeed = Newsfeed::with('newsfeed_details')
	                            ->whereIn('Nfd_Unique_Id',$tagList)
	                            ->where('Nfd_Brn_Id',$teacherAssignBranch)
	                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
	                            ->select('newsfeed.*','user_tbl.Use_Name')
	                            ->orderBy('Nfd_Id','DESC')
	                            ->get();

	                return ResponseMessage::success("Newsfeed list",$newsfeed);
	            }else{
	            	ResponseMessage::error("Teacher Not Found");
	            }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function parent_newsfeed_list(Request $request)
    {
    	try 
    	{
    		$rules = [
    			'parent_id' => 'required',
    			'student_id' => 'required'
    		];
    		$customeMessage = [
    			'parent_id.required' => 'Please send parent id',
    			'student_id.required' => 'Please send student id'
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()){
                $errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
					exit;
				}
            }else{
            	$parent_id = $request->parent_id;
            	$student_id = $request->student_id;
            	if(User::where('Use_Id',$parent_id)->where('Use_Type','4')->exists())
            	{
            		if(Student::where('Std_Id',$student_id)->where('Std_Parent_Id',$parent_id)->exists())
            		{
            			$studentInfo = Student::where('Std_Id',$student_id)->get()->first();
		            	$studentAssignBranch = ClassTbl::where('Cla_Id',$studentInfo->Std_Cla_Id)->first()->Cla_Bra_Id;
		            	$tagList =  Newsfeedtag::where('Nft_Cla_Id',$studentInfo->Std_Cla_Id)
		            				->where('Nft_Std_Id',$student_id)
		            				->where('Nft_Parent_Id',$studentInfo->Std_Parent_Id)
		            				->select('Nft_Nfd_Unique_Id')
		            				->groupBy('Nft_Nfd_Unique_Id')
		            				->get();
		                $newsfeed = Newsfeed::with('newsfeed_details')
		                            ->whereIn('Nfd_Unique_Id',$tagList)
		                            ->where('Nfd_Brn_Id',$studentAssignBranch)
		                            ->leftjoin('user_tbl','user_tbl.Use_Id','=','newsfeed.Nfd_CreatedBy')
		                            ->select('newsfeed.*','user_tbl.Use_Name')
		                            ->orderBy('Nfd_Id','DESC')
		                            ->get();

		                ResponseMessage::success("Newsfeed list",$newsfeed);
            		}else{
            			ResponseMessage::error("Student Not Found");
            		}
	            }else{
	            	ResponseMessage::error("Parent Not Found");
	            }
            }
    	} catch (Exception $e) {
    		Exceptions::exception($e);
    	}
    }
}
