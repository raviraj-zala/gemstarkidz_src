<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admin_assign_branch extends Model
{
    //
    const id = 'Aab_Id';
    const CREATED_AT = 'Aab_CreatedAt';
	const UPDATED_AT = 'Aab_UpdatedAt';
	
    protected $table = "admin_assign_branch_tbl";
    
}
