<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ClassTbl;

class Socialactivity_like extends Model
{
    const CREATED_AT = 'Sal_CreatedAt';
	const UPDATED_AT = 'Sal_UpdatedAt';

	protected $table = "socialactivity_like_tbl";
}
