<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Homework extends Model
{
	const CREATED_AT = 'Hmw_CreatedAt';
	const UPDATED_AT = 'Hmw_UpdatedAt';

	protected $table = "homework_tbl";
	protected $primaryKey = "Hmw_Id";

	public function Section()
	{
		return $this->hasMany('App\Model\ClassTbl','Cla_Id','Hmw_Cla_Id');
	}
}
