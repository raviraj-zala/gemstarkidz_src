<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    protected $table = "chat_tbl";

    protected $primarykey = 'Cha_Id';

    const CREATED_AT = 'Cha_CreatedAt';
	const UPDATED_AT = 'Cha_UpdatedAt';
}
