<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //
    protected $table = 	"module_tbl";

    public function parent(){
    	return $this->hasOne('App\Model\Module','Mod_Id','Mod_Parent_Id');
    }

    public function children(){
    	return $this->hasMany('App\Model\Module','Mod_Parent_Id','Mod_Id');
    }
}
//https://stackoverflow.com/questions/21305111/how-to-create-a-database-driven-multi-level-navigation-menu-using-laravel