<?php



namespace App\Model;


use Illuminate\Database\Eloquent\Model;



class Socialactivity extends Model

{

	const CREATED_AT = 'Soa_CreatedAt';

	const UPDATED_AT = 'Soa_UpdatedAt';

	protected $table = "socialactivity_tbl";
	
	protected $primaryKey = "Soa_Id";

	public function SoaImages()
	{
		 return $this->hasMany('App\Model\Socialactivitydetails','Sod_Soa_Id','Soa_Id');
	}

	public function SoaLike()
	{
		 return $this->hasMany('App\Model\Socialactivity_like','Sal_Soa_Id','Soa_Id');
	}

}

