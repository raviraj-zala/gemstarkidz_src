<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    const CREATED_AT = 'Std_CreatedAt';
	const UPDATED_AT = 'Std_UpdatedAt';

    protected $table = "student_tbl";
    protected $primaryKey = "Std_Id";

    public function StuAttedence()
	{
		 return $this->hasMany('App\Model\Attendence','Att_Stu_Id','Std_Id');
	}

	public function Use_Student()
	{
		 return $this->hasmany('App\Model\ClassTbl','Cla_Id','Std_Cla_Id');
	}
    public function StdAttedence()
	{
		 return $this->hasMany('App\Model\Attendence','Att_Stu_Id','Std_Id');
	}
}
