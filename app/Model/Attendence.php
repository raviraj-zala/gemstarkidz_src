<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attendence extends Model
{
	const CREATED_AT = 'Att_CreatedAt';
	const UPDATED_AT = 'Att_UpdatedAt';
	protected $table = "attendance_tbl";

	public function AttStudent()
	{
		 return $this->hasMany('App\Model\Student','Std_Id','Att_Stu_Id');
	}
}
