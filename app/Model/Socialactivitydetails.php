<?php



namespace App\Model;



use Illuminate\Database\Eloquent\Model;



class Socialactivitydetails extends Model

{

	const CREATED_AT = 'Sod_CreatedAt';

	const UPDATED_AT = 'Sod_UpdatedAt';

	const Id = 'Sod_Id';
	protected $table = "socialactivity_details_tbl";

}

