<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Newsfeedtag extends Model
{
	const CREATED_AT = 'Nft_CreatedAt';
	const UPDATED_AT = Null;
	protected $primaryKey = "Nft_Id"; 
	protected $table = "newsfeed_tag";

}
