<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Socialactivity_like_image extends Model
{
    const CREATED_AT = 'Sil_CreatedAt';
	const UPDATED_AT = 'Sil_UpdatedAt';

    protected $table = "socialactivity_img_like_tbl";
    protected $primaryKey = "Sil_Id";
   
    public function SilComment()
    {
    	return $this->hasMany('App\Model\Socialactivity_img_cmt','Sic_Img_Id','Sil_Img_Id');
    }

}
