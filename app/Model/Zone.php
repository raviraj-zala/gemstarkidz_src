<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //
    const id = 'Zon_Id';
    const CREATED_AT = 'Zon_CreatedAt';
	const UPDATED_AT = 'Zon_UpdatedAt';

    protected $table = "zone_tbl";
}
