<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    const CREATED_AT = 'Grp_Cha_CreatedAt';
	const UPDATED_AT = 'Grp_Cha_UpdatedAt';

	protected $table = "group_chat_tbl";
}
