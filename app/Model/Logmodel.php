<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Logmodel extends Model
{
   	protected $table = "log_tbl";
   	protected $primaryKey = "Log_Id";
   	public $timestamps = false;
}
