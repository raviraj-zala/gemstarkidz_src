<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Newsfeed extends Model
{
    const CREATED_AT = 'Nfd_CreatedAt';
	const UPDATED_AT = 'Nfd_UpdatedAt';
	public $primaryKey = 'Nfd_Id'; 
	protected $table = "newsfeed";

	public function newsfeedtag(){
		return $this->hasMany('App\Model\Newsfeedtag','Nft_Nfd_Unique_Id','Nfd_Unique_Id');
	}

	public function newsfeedinfo(){
		return $this->hasMany('App\Model\Newsfeedinfo','Nfi_Nfd_Unique_Id','Nfd_Unique_Id');
	}
}
