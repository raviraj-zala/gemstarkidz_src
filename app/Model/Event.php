<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ClassTbl;

class Event extends Model
{
    //
    const CREATED_AT = 'Eve_CreatedAt';
	const UPDATED_AT = 'Eve_UpdatedAt';

	protected $table = "event_tbl";

}
