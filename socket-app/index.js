var app = require("express")();
var http = require("http").Server(app);
var io = require('socket.io')(http);
var mysql = require("mysql");

// Fcm Notification
var FCM = require('fcm-node');
var serverKey = 'AAAAbFypH7M:APA91bFRq_uU5F9ANeilCqIMh-UjkIVtEqasvqJnJ-xiJzMDgX52T8uytdSvi0L8uwxF004Kw5w3R9jb0v395rAdikVEj9pAW3MoflcFDhXdvSQbhP5TIHJMErrYLfruqqQnizhbE36c'; //put your server key here
var fcm = new FCM(serverKey);
var waitForOther = "0";

var users = [];
var groupUsers = [];

var onlineuser = [];
var con = mysql.createConnection({
	host : "localhost",
	user : "root",
	password : "",
	database : "gemstarkidz"
});

con.connect(function(error){
	if(error) console.log(error);
	// console.log("DB Connected");
});


app.get("/",function(req,res){
	res.sendFile(__dirname+"/index.html");
});

io.on("connected",function(socket){
	// console.log(socket);
});


io.on("connection",function(socket){
	
	socket.on("msg",function(data){
		var androidMessage = data.message; 
		if(data.chatType=="0"){
			var androidMessage = data.message;
			var para = " '"+data.senderId+" ','"+data.receiverId+"','"+data.message+"','"+0+"','"+data.senderId+"',' "+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `chat_tbl`(`Cha_Sender_Id`,`Cha_Receiver_Id`,`Cha_Message`,`Cha_Type`,`Cha_CreatedBy`,`Cha_CreatedAt`,`Cha_UpdatedBy`,`Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType=="1"){
			var androidMessage = "New Video Received.";
			var para = " '"+data.senderId+"','"+data.receiverId+"','"+data.fileName+"','"+1+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `chat_tbl`(`Cha_Sender_Id`,`Cha_Receiver_Id`,`Cha_Video`,`Cha_Type`,`Cha_CreatedBy`,`Cha_CreatedAt`,`Cha_UpdatedBy`,`Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType=="2"){
			var androidMessage = "New Image Received.";
			var para = " '"+data.senderId+"','"+data.receiverId+"','"+data.message+"','"+2+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `chat_tbl`(`Cha_Sender_Id`,`Cha_Receiver_Id`,`Cha_Image`,`Cha_Type`,`Cha_CreatedBy`,`Cha_CreatedAt`,`Cha_UpdatedBy`,`Cha_UpdatedAt`) VALUES ("+para+")";
			console.log(para);
		}
		if(data.chatType=="3"){
			var androidMessage = "New Audio Received.";
			var para = " '"+data.senderId+"','"+data.receiverId+"','"+data.fileName+"','"+3+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `chat_tbl`(`Cha_Sender_Id`,`Cha_Receiver_Id`,`Cha_Audio`,`Cha_Type`,`Cha_CreatedBy`,`Cha_CreatedAt`,`Cha_UpdatedBy`,`Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType=="4"){
			var androidMessage = "New Document Received.";
			var para = " '"+data.senderId+"','"+data.receiverId+"','"+data.fileName+"','"+4+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `chat_tbl`(`Cha_Sender_Id`,`Cha_Receiver_Id`,`Cha_Document`,`Cha_Type`,`Cha_CreatedBy`,`Cha_CreatedAt`,`Cha_UpdatedBy`,`Cha_UpdatedAt`) VALUES ("+para+")";
		}
		
		var is_saved = con.query(newMsgQuery,function(err,res){
			console.log("ERROR IN newMsgQuery ::--"+err);
		});			
		var getLastRecord = "select chat_tbl.*,user_tbl.Use_Name as receiverName,user_tbl.Use_Image as receiverImage  from `chat_tbl` join `user_tbl` on `Cha_Receiver_Id`=`Use_Id` and `Cha_Sender_Id`='"+data.senderId+"' and `Cha_Receiver_Id`='"+data.receiverId+"'  order by Cha_Id DESC limit 1";
		var row = con.query(getLastRecord,function(err,rows){
	      if(err) throw console.log(err);	      	    
	      	var senderName = "";
	      	var senderType = "";

			var getSenderName = "select `Use_Name`,`Use_Type` from `user_tbl` where `Use_Id`= '"+data.senderId+"' ";	      	
			var sName = con.query(getSenderName,function(err,name){
		      if(err) throw console.log(err);
		      senderName = name[0]["Use_Name"];
		      senderType = name[0]["Use_Type"];
	    	});

			var getUserToken = "select `Use_Token` from `user_tbl` where `Use_Id`= '"+data.receiverId+"' ";
			var tokenRow = con.query(getUserToken,function(err,token){
		      if(err) throw console.log(err);
	    	       var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
				        to: token[0]["Use_Token"], 
				        collapse_key: 'AIzaSyCOg2gjrVIZBacTLjO9vNnzjWrXE7PHMqA',
				        notification: {
				            title: senderName,
				            type: senderType,
				            body: androidMessage 
				        },
				        data: {  //you can send only notification or only data(or include both)
				            my_key: 'my value',
				            my_another_key: 'my another value'
				        }
				    };
				    console.log("Message := "+JSON.stringify(message));
				    fcm.send(message, function(err, response){
				        if (err) {
				            console.log("Something has gone wrong!");
				        } else {
				            console.log("Successfully sent with response: ", response);
				        }
				    });
		    });
			io.sockets.emit('rmsg', rows);
	    });
	});
	socket.on('send group msg',function(data){
		var androidMessage = data.message;
		if(data.chatType==0){
			var para = " '"+data.senderId+"','"+data.groupMasterId+"','"+data.message+"','"+data.Cha_Type+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"'";
			var newMsgQuery = "INSERT INTO `group_chat_tbl`(`Grp_Cha_Sender_Id`,`Grp_Cha_Mas_Id`,`Grp_Cha_Message`,`Grp_Cha_Type`,`Grp_Cha_CreatedBy`,`Grp_Cha_CreatedAt`,`Grp_Cha_UpdatedBy`,`Grp_Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType==1){
			var androidMessage = "New Video Received.";
			var para = " '"+data.senderId+"','"+data.groupMasterId+"','"+data.fileName+"','"+1+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `group_chat_tbl`(`Grp_Cha_Sender_Id`,`Grp_Cha_Mas_Id`,`Grp_Cha_Video`,`Grp_Cha_Type`,`Grp_Cha_CreatedBy`,`Grp_Cha_CreatedAt`,`Grp_Cha_UpdatedBy`,`Grp_Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType==2){
			var androidMessage = "New Image Received.";
			var para = " '"+data.senderId+"','"+data.groupMasterId+"','"+data.message+"','"+2+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `group_chat_tbl`(`Grp_Cha_Sender_Id`,`Grp_Cha_Mas_Id`,`Grp_Cha_Image`,`Grp_Cha_Type`,`Grp_Cha_CreatedBy`,`Grp_Cha_CreatedAt`,`Grp_Cha_UpdatedBy`,`Grp_Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType==3){
			var androidMessage = "New Audio Received.";
			var para = " '"+data.senderId+"','"+data.groupMasterId+"','"+data.fileName+"','"+3+"','"+data.senderId+"','"+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `group_chat_tbl`(`Grp_Cha_Sender_Id`,`Grp_Cha_Mas_Id`,`Grp_Cha_Audio`,`Grp_Cha_Type`,`Grp_Cha_CreatedBy`,`Grp_Cha_CreatedAt`,`Grp_Cha_UpdatedBy`,`Grp_Cha_UpdatedAt`) VALUES ("+para+")";
		}
		if(data.chatType==4){
			var androidMessage = "New Document Received.";
			var para = " '"+data.senderId+"','"+data.groupMasterId+"','"+data.fileName+"','"+4+"','"+data.senderId+"',' "+data.datetime+"','"+data.senderId+"','"+data.datetime+"' ";
			var newMsgQuery = "INSERT INTO `group_chat_tbl`(`Grp_Cha_Sender_Id`,`Grp_Cha_Mas_Id`,`Grp_Cha_Document`,`Grp_Cha_Type`,`Grp_Cha_CreatedBy`,`Grp_Cha_CreatedAt`,`Grp_Cha_UpdatedBy`,`Grp_Cha_UpdatedAt`) VALUES ("+para+")";
		}

		var is_saved = con.query(newMsgQuery,function(err,res){
			if(err) throw console.log(err);
		});

		var getLastRecord = "select group_chat_tbl.Grp_Cha_Id,group_chat_tbl.Grp_Cha_Sender_Id,group_chat_tbl.Grp_Cha_Mas_Id,group_chat_tbl.Grp_Cha_Message,group_chat_tbl.Grp_Cha_Video,group_chat_tbl.Grp_Cha_Image,group_chat_tbl.Grp_Cha_Audio,group_chat_tbl.Grp_Cha_Document,group_chat_tbl.Grp_Cha_Type,group_chat_tbl.Grp_Cha_CreatedBy,group_chat_tbl.Grp_Cha_CreatedAt,user_tbl.Use_Id,user_tbl.Use_Name,user_tbl.Use_Email,user_tbl.Use_Mobile_No,user_tbl.Use_Image,group_master_tbl.Grp_Mas_Name from `group_chat_tbl` LEFT JOIN `group_master_tbl` ON `Grp_Mas_Id`='"+data.groupMasterId+"' LEFT JOIN `user_tbl` ON  `Grp_Cha_Sender_Id`=`Use_Id` and `Grp_Cha_Mas_Id`='"+data.groupMasterId+"'  order by `Grp_Cha_Id` DESC limit 1";
		var row = con.query(getLastRecord,function(err,lastRows){
	      if(err) throw console.log(err);

	      		var getSenderName = "select `Use_Name`,`Use_Type` from `user_tbl` where `Use_Id`= '"+data.senderId+"' ";	      	
				var sName = con.query(getSenderName,function(err,name){
			      if(err) throw console.log(err);
			      senderName = name[0]["Use_Name"];
			      senderType = name[0]["Use_Type"];
		    	});
	      		// var  query_member_id= "select Grp_Det_Mem_Id from `group_detail_tbl` where `Grp_Det_Mas_Id` = '"+data.groupMasterId+"' ";
	      		
	      		var  query_member_id= "SELECT user_tbl.Use_Id,user_tbl.Use_Token FROM `user_tbl` WHERE user_tbl.Use_Id IN(select group_detail_tbl.Grp_Det_Mem_Id from group_detail_tbl where `Grp_Det_Mas_Id` = '"+data.groupMasterId+"') ";
	      		var group_member_ids = con.query(query_member_id,function(err,rows){
	      				// console.log("llloooggg = "+JSON.stringify(rows));
	      				if(err) throw console.log(err);

	      				for(i=1;i<=rows.length;i++){
	      					if (typeof rows[i] !== 'undefined' && data.senderId!=rows[i].Use_Id){
		      					var user_token = rows[i].Use_Token;
		      					var group_message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
							        to: user_token, 
							        collapse_key: 'AIzaSyCOg2gjrVIZBacTLjO9vNnzjWrXE7PHMqA',
							        notification: {
							            title: lastRows[0].Grp_Mas_Name,
							            body: senderName+" :- "+androidMessage
							        },
							        data: {  //you can send only notification or only data(or include both)
							            my_key: 'my value',
							            my_another_key: 'my another value'
							        }
							    };
							    fcm.send(group_message, function(err, response){
							        if (err) {
							            console.log("Something has gone wrong!");
							        } else {
							            console.log("Successfully sent with response: ", response);
							        }
							    });
						    }
	      				}
  				});
  				io.sockets.emit('read_group_msg', lastRows);
	    });
	});
	socket.on('oldMessages',function(data){
		var sid = data.senderId;
		var rid = data.receiverId;
		var query = "select chat_tbl.*,user_tbl.Use_Name as receiverName,user_tbl.Use_Image as receiverImage from `chat_tbl`,`user_tbl` WHERE `Cha_Sender_Id` in ("+data.senderId+","+data.receiverId+") and `Cha_Receiver_Id` in ("+data.senderId+","+data.receiverId+") and `Cha_Receiver_Id`= `Use_Id` ";
		var chatData = con.query(query,function(err,messages){
			if(err) throw console.log(err);
			io.sockets.emit('getOldMessage', messages);
		});
	});
	socket.on('groupOldMessages',function(data){
		var gmid = data.groupMasterId;
		var query = "select group_chat_tbl.Grp_Cha_Id,group_chat_tbl.Grp_Cha_Sender_Id,group_chat_tbl.Grp_Cha_Mas_Id,group_chat_tbl.Grp_Cha_Message,group_chat_tbl.Grp_Cha_Video,group_chat_tbl.Grp_Cha_Image,group_chat_tbl.Grp_Cha_Audio,group_chat_tbl.Grp_Cha_Document,group_chat_tbl.Grp_Cha_Type,group_chat_tbl.Grp_Cha_CreatedBy,group_chat_tbl.Grp_Cha_CreatedAt,user_tbl.Use_Id,user_tbl.Use_Name,user_tbl.Use_Email,user_tbl.Use_Mobile_No,user_tbl.Use_Image from `group_chat_tbl` LEFT JOIN `user_tbl` ON `Grp_Cha_Sender_Id`=`Use_Id` and `Grp_Cha_Mas_Id`='"+gmid+"'";
		var chatData = con.query(query,function(err,messages){
			if(err) throw console.log(err);
			io.sockets.emit('getGroupOldMessage', messages);
		});
	});	
	socket.on('groupDetail',function(data){
		var grpMasId = data.groupMasterId;
		var query = " select * from `group_master_tbl` JOIN `group_detail_tbl` ON `Grp_Det_Mas_Id`=`Grp_Mas_Id` and  `Grp_Mas_Id`='"+grpMasId+"' JOIN `user_tbl` ON  `Use_Id`=`Grp_Det_Mem_Id`  ";
		var grpData = con.query(query,function(err,grpInfo){
			if(err) throw console.log(err);
			io.sockets.emit('getGroupDetail',grpInfo);
		});
	});

	// ************ Group Create Notification Start *********************

	socket.on('groupDetailSend',function(data){
		var grpMasId = data.groupMasterId;
		var grpCreName = data.groupCreateName;
		var query = " select * from `group_master_tbl` JOIN `group_detail_tbl` ON `Grp_Det_Mas_Id`=`Grp_Mas_Id` and  `Grp_Mas_Id`='"+grpMasId+"' JOIN `user_tbl` ON  `Use_Id`=`Grp_Det_Mem_Id`  ";
				
		var grpData = con.query(query,function(err,grpInfo){
			
			if(err) throw console.log(err);

			var member_count = grpInfo.length;

			var member_id = grpInfo[0]['Grp_Det_Mem_Id'];
			var getUserToken = grpInfo[0]["Use_Token"];
			var groupNamme = grpInfo[0]['Grp_Mas_Name'];
			var groupDetail = grpCreName + " Added you in this group " + groupNamme +".";
			var i;
			if(member_id != "")
			{
				for (i = 0; i <= member_count; i++)
				{
			      	if(err) throw console.log(err);
	    	       	var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
				        to: getUserToken, 
				        collapse_key: 'AIzaSyCOg2gjrVIZBacTLjO9vNnzjWrXE7PHMqA',
				        notification: {
				            title: '',
				            body: groupDetail 
				        },
				        data: {  //you can send only notification or only data(or include both)
				            my_key: 'my value',
				            my_another_key: 'my another value'
				        }
				    };
				    fcm.send(message, function(err, response){
				        if (err) {
				            console.log("Something has gone wrong!");
				        } else {
				            console.log("Successfully sent with response: ", response);
				        }
				    });
				}
		 	}
	  	});
 	});

	// ************ Group Create Notification End *********************

	  //*************** Driver Route Insert Longtitude and Latitude Start ************//
	  
	socket.on('driverRoute',function(data){
		var dt = new Date();
		var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();
		var output = d.getFullYear() + '-' +(month<10 ? '0' : '') + month + '-' +(day<10 ? '0' : '') + day;
		var datetime = output+" "+time;

		var parameter = " '"+data.driverId+" ',' "+data.latitude+"',' "+data.longtitude+"',' "+datetime+"' ";
			var newRouteQuery = "INSERT INTO `driver_route_tbl`(`Dir_Use_Id`,`Dir_Latitude`,`Dir_Longtitude`,`Dir_CreatedAt`) VALUES ("+parameter+")";

		var is_saved = con.query(newRouteQuery,function(err,res){
			console.log(err);
		});
	});
	//*************** Driver Route Insert Longtitude and Latitude End ************//

	 //*************** Driver Route Get Last Record Start ************//

	 socket.on('getRoute',function(data){
		var query = "select * from `driver_route_tbl` where `Dir_Use_Id`='"+data.driverId+"' order by `Dir_Id` DESC limit 1";
		var routeData = con.query(query,function(err,messages){
			if(err) throw console.log(err);
			io.sockets.emit('getLastRoute', messages);
		});
	});

	 //*************** Driver Route Get Last Record End************//

});

http.listen(8000,function(){
	console.log("Server Created Successfully... With 8000");
});