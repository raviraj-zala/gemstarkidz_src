//Create Zone
$("form[name='create-zone']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        name:{
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        name:{
          required: "<span class='text-danger'>Please Enter Name</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

//Edit Zone
$("form[name='edit-zone']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        name:{
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        name:{
          required: "<span class='text-danger'> Please Enter Name</span>",
        },
        status:{
           required: "<span class='text-danger'> Please Select Status</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

//Create Branch
$("form[name='create-branch']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        zone:{
            required: true,
        },
        name:{
            required: true,
        },
        code:{
            required: true,
        },
        email:{
            required: true,
        },
        mobile: {
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        zone:{
          required: "<span class='text-danger'>Please Select Area</span>",
        },
        name:{
            required: "<span class='text-danger'>Please Enter Branch Name</span>",
        },
        code:{
            required: "<span class='text-danger'>Please Enter Branch Code</span>",
        },
        email:{
            required: "<span class='text-danger'>Please Enter Email Address</span>",
        },
        mobile:{
            required: "<span class='text-danger'>Please Enter Phone Number</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

//Edit Branch
$("form[name='edit-branch']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        zone:{
            required: true,
        },
        name:{
            required: true,
        },
        code:{
            required: true,
        },
        email:{
            required: true,
        },
        mobile: {
            required: true,
            maxlength: 12,
        },
        status:{
           required: true,
        }
    },
    messages: {
        zone:{
          required: "<span class='text-danger'>Please Select Area</span>",
        },
        name:{
            required: "<span class='text-danger'>Please Enter Branch Name</span>",
        },
        code:{
            required: "<span class='text-danger'>Please Enter Branch Code</span>",
        },
        email:{
            required: "<span class='text-danger'>Enter Valid Email Id</span>",
        },
        mobile:{
            required: "<span class='text-danger'>Please Enter Phone Number</span>",
            minlength: "<span class='text-danger'>Please Enter Minimum 10 Numbers</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

//Create User
$("form[name='create-user']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        name:{
            required: true,
        },
        email:{
            required: true,
        },
        password:{
            required: true,
        },
        userType:{
            required: true,
        },
        mobile:{
           required: true,
        }
    },
    messages: {
        name:{
          required: "<span class='text-danger'>Please Enter Name</span>",
        },
        email:{
            required: "<span class='text-danger'>Please enter an email address.</span>",
        },
        password:{
            required: "<span class='text-danger'>Please enter password.</span>",
        },
        userType:{
            required: "<span class='text-danger'>Please select user type.</span>",
        },
        mobile:{
            required: "<span class='text-danger'>Please enter mobile number.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});
//Create User
$("form[name='edit-user']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        name:{
            required: true
        },
        email:{
            required: true,
        },
        password:{
            required: true,
        },
        userType:{
            required: true,
        },
        mobile:{
           required: true
        }
    },
    messages: {
        name:{
          required: "<span class='text-danger'>Please Enter Name</span>",
        },
        email:{
            required: "<span class='text-danger'>Please enter an email address.</span>",
        },
        password:{
            required: "<span class='text-danger'>Please enter password.</span>",
        },
        userType:{
            required: "<span class='text-danger'>Please select user type.</span>",
        },
        mobile:{
            required: "<span class='text-danger'>Please enter mobile number.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

// Add Parent
$("form[name='add-parent']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        name:{
            required: true
        },
        email:{
            required: true,
        },
        password:{
            required: true,
        },
        userType:{
            required: true,
        },
        mobile:{
           required: true
        }
    },
    messages: {
        name:{
          required: "<span class='text-danger'>Please Enter Name</span>",
        },
        email:{
            required: "<span class='text-danger'>Please enter an email address.</span>",
        },
        
        password:{
            required: "<span class='text-danger'>Please enter password.</span>",
        },
        userType:{
            required: "<span class='text-danger'>Please select user type.</span>",
        },
        mobile:{
            required: "<span class='text-danger'>Please enter mobile number.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

$("form[name='edit-parent']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        name:{
            required: true
        },
        email:{
            required: true,
        },
        password:{
            required: true,
        },
        userType:{
            required: true,
        },
        mobile:{
           required: true
        }
    },
    messages: {
        name:{
          required: "<span class='text-danger'>Please Enter Name</span>",
        },
        email:{
            required: "<span class='text-danger'>Please enter an email address.</span>",
        },
        
        password:{
            required: "<span class='text-danger'>Please enter password.</span>",
        },
        userType:{
            required: "<span class='text-danger'>Please select user type.</span>",
        },
        mobile:{
            required: "<span class='text-danger'>Please enter mobile number.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

//Create Class
$("form[name='create-class']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        branch:{
            required: true,
        },
        name:{
            required: true
        },
        gr_no:{
            required: true,
        },
        class:{
            required: true,
        },
        section:{
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        branch:{
            required: "<span class='text-danger'>Please Select Branch</span>",
        },
        name:{
          required: "<span class='text-danger'>Please Enter Student Name</span>",
        },
        gr_no:{
           required: "<span class='text-danger'>Please Enter Gr No.</span>"
        },
        class:{
            required: "<span class='text-danger'>Please Enter Class</span>",
        },
        section: {
            required: "<span class='text-danger'>Please Enter Section</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

//Edit Class
$("form[name='edit-class']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        branch:{
            required: true,
        },
        name:{
            required: true
        },
        gr_no:{
            required: true,
        },
        class:{
            required: true,
        },
        section:{
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        branch:{
            required: "<span class='text-danger'>Please Select Branch</span>",
        },
        name:{
          required: "<span class='text-danger'>Please Enter Student Name</span>",
        },
        gr_no:{
           required: "<span class='text-danger'>Please Enter Gr No.</span>"
        },
        class:{
            required: "<span class='text-danger'>Please Enter Class</span>",
        },
        section: {
            required: "<span class='text-danger'>Please Enter Section</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});
//Create Event
//Create Class
$("form[name='create-event']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        class:{
            required: true,
        },
        section:{
            required: true
        },
        event:{
            required: true,
        },
        description:{
            required: true,
        },
        date:{
            required: true,
        },
        image:{
            required: true,
        }
    },
    messages: {
        class:{
            required: "<span class='text-danger'>Please Select Class</span>",
        },
        section:{
            required: "<span class='text-danger'>Please Select Section</span>",
        },
        event:{
           required: "<span class='text-danger'>Please Enter Event Name</span>",
        },
        description:{
            required: "<span class='text-danger'>Please Enter Event Description</span>",
        },
        date:{
            required: "<span class='text-danger'>Please Select Date.</span>",
        },
        image:{
            required: "<span class='text-danger'>Please Select Image.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});


// Edit Event
$("form[name='edit-event']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        class:{
            required: true,
        },
        section:{
            required: true
        },
        event:{
            required: true,
        },
        description:{
            required: true,
        },
        date:{
            required: true,
        }
    },
    messages: {
        class:{
            required: "<span class='text-danger'>Please Select Class</span>",
        },
        section:{
            required: "<span class='text-danger'>Please Select Section</span>",
        },
        event:{
           required: "<span class='text-danger'>Please Enter Event Name</span>",
        },
        description:{
            required: "<span class='text-danger'>Please Enter Event Description</span>",
        },
        date:{
            required: "<span class='text-danger'>Please Select Date.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});


$("form[name='add-student']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        branch:{
            required: true,
        },
        class:{
            required: true
        },
        section:{
            required: true,
        },
        status:{
            required: true,
        },
        grno:{
            required: true,
        },
        studentname:{
           required: true
        },
        parentname:{
           required: true
        },
        image:{
            required: true
        },
        studentfee:{
            required: true,
            maxlength: 7
        }
    },
    messages: {
        branch:{
            required: "<span class='text-danger'>Please Select Branch.</span>",
        },
        class:{
          required: "<span class='text-danger'>Please Select Class.</span>",
        },
        section:{
           required: "<span class='text-danger'>Please Select Section.</span>"
        },
        status:{
            required: "<span class='text-danger'>Please Select Status.</span>",
        },
        grno: {
            required: "<span class='text-danger'>Please Enter G.R. No.</span>",
        },
        studentname:{
           required: "<span class='text-danger'>Please Enter Student Name.</span>",
        },
        parentname:{
            required: "<span class='text-danger'>Please Enter Parent Name.</span>"
        },
        image:{
            required: "<span class='text-danger'>Please Select Image.</span>",
        },
        studentfee:{
            required: "<span class='text-danger'>Please Insert Student Fee.</span>",
            maxlength: "<span class='text-danger'>Maximum 7 Digit allow.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

$("form[name='edit-student']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        branch:{
            required: true,
        },
        class:{
            required: true
        },
        section:{
            required: true,
        },
        status:{
            required: true,
        },
        grno:{
            required: true,
        },
        studentname:{
           required: true
        },
        parentname:{
           required: true
        },
        studentfee:{
            required: true,
            maxlength: 7
        }
    },
    messages: {
        branch:{
            required: "<span class='text-danger'>Please Select Branch.</span>",
        },
        class:{
          required: "<span class='text-danger'>Please Select Class.</span>",
        },
        section:{
           required: "<span class='text-danger'>Please Select Section.</span>"
        },
        status:{
            required: "<span class='text-danger'>Please Select Status.</span>",
        },
        grno: {
            required: "<span class='text-danger'>Please Enter G.R. No.</span>",
        },
        studentname:{
           required: "<span class='text-danger'>Please Enter Student Name.</span>",
        },
        parentname:{
            required: "<span class='text-danger'>Please Enter Parent Name.</span>"
        },
        studentfee:{
            required: "<span class='text-danger'>Please Insert Student Fee.</span>",
            maxlength: "<span class='text-danger'>Maximum 7 Digit allow.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});


$("form[name='create-route']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        branchId:{
            required: true,
        },
        driverId:{
            required: true
        },
        routeName:{
            required: true,
        },
        addressFrom:{
            required: true,
        },
        addressTo:{
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        branchId:{
            required: "<span class='text-danger'>Please Select Branch.</span>",
        },
        driverId:{
          required: "<span class='text-danger'>Please Select Driver.</span>",
        },
        routeName:{
           required: "<span class='text-danger'>Please Enter Route Name.</span>"
        },
        addressFrom:{
            required: "<span class='text-danger'>Please Enter From Address.</span>",
        },
        addressTo: {
            required: "<span class='text-danger'>Please Enter To Address.</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});



$("form[name='edit-route']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        branchId:{
            required: true,
        },
        driverId:{
            required: true
        },
        routeName:{
            required: true,
        },
        addressFrom:{
            required: true,
        },
        addressTo:{
            required: true,
        },
        status:{
           required: true
        }
    },
    messages: {
        branchId:{
            required: "<span class='text-danger'>Please Select Branch.</span>",
        },
        driverId:{
          required: "<span class='text-danger'>Please Select Driver.</span>",
        },
        routeName:{
           required: "<span class='text-danger'>Please Enter Route Name.</span>"
        },
        addressFrom:{
            required: "<span class='text-danger'>Please Enter From Address.</span>",
        },
        addressTo: {
            required: "<span class='text-danger'>Please Enter To Address.</span>",
        },
        status:{
           required: "<span class='text-danger'>Please Select Status.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});


// Add Report
$("form[name='insert-report']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        reportType:{
           required: true
        },
        jsonFile:{
           required: true
        }
    },
    messages: {
        reportType:{
            required: "<span class='text-danger'>Please Select Type.</span>",
        },
        jsonFile:{
          required: "<span class='text-danger'>Please Select File.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});

// Import Export Excel

// Add Report
$("form[id='excel-import']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        excelFile:{
           required: true,
        },
        importType:{
           required: true,
        }
    },
    messages: {
        excelFile:{
            required: "<span class='text-danger'>Please Select File.</span>",
        },
        importType:{
          required: "<span class='text-danger'>Please Select Import Type.</span>",
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});


$("form[name='insert-report']").validate({
    ignore: '',
    onfocusout: function(element, event) {
        if($(event.relatedTarget).hasClass("btn-danger") || $(event.relatedTarget).hasClass("btn-info")) {
            return false;
        }else{
            this.element(element);
        }
    },
    onkeyup: false,
    rules: {
        nameOrGr:{
           required: true
        }
    },
    messages: {
        nameOrGr:{
            required: "<span class='text-danger'>Please Enter Student Name or Gr No.</span>"
        }
    },
    submitHandler: function (form) {
    form.submit();
    return false;  
    }
});
