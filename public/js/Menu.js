﻿
$j = jQuery.noConflict();


$j(function () {
    $j('nav#menu').mmenu({
        slidingSubmenus: false
    });
});

$j(document).ready(function () {
    var checkCookie = $j.cookie("nav-item");

    if (checkCookie != "") {
        $j('#nav > li > .link').eq(checkCookie).addClass('active').next().show();
    }
});
$j(document).ready(function () {
  
    $j('#nav > li > .link').click(function () {
        var navIndex = $j('#nav > li > .link').index(this);
        $j.cookie("nav-item", navIndex);
        $j('#nav li ul').slideUp();
        if ($j(this).next().is(":visible")) {
            $j(this).next().slideUp();
        } else {
            $j(this).next().slideToggle();
        }
        $j('#nav li .link').removeClass('active');
        $j(this).addClass('active');
    });
});

$j(document).ready(function () {

    //GET BROWSER WINDOW HEIGHT
    var currHeight = $j(window).height();
    var cHeight = $j('.right_part').height();
    $j('#left').css('height', cHeight);

    //ON RESIZE OF WINDOW
    $j(window).resize(function () {
        //GET NEW HEIGHT

        var currHeight = $j(window).height();
        //RESIZE BOTH ELEMENTS TO NEW HEIGHT
        var cHeight = $j('.right_part').height();
        $j('#left').css('height', cHeight);

    });


});
