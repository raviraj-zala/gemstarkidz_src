<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_tbl', function (Blueprint $table) {
            $table->increments('Cla_Id',11);
            $table->integer('Cla_Bra_Id')->unsigned();
            $table->string('Cla_Student_Name');
            $table->integer('Cla_Gr_No')->unsigned();
            $table->string('Cla_Class');
            $table->text('Cla_Section');
            $table->tinyInteger('Cla_Status')->default('1')->comment = "1=Active 0=In-Active";
            $table->integer('Cla_CreatedBy')->unsigned()->nullable();
            $table->datetime('Cla_CreatedAt');
            $table->integer('Cla_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Cla_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_tbl');
    }
}
