<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tbl', function (Blueprint $table) {
            $table->increments('Eve_Id',11);
            $table->integer('Eve_Cla_Id')->unsigned();
            $table->text('Eve_Cla_Section');
            $table->string('Eve_Name');
            $table->date('Eve_Date');
            $table->text('Eve_Description')->nullable();
            $table->integer('Eve_CreatedBy')->unsigned()->nullable();
            $table->datetime('Eve_CreatedAt');
            $table->integer('Eve_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Eve_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tbl');
    }
}
