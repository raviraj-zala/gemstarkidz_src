<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_tbl', function (Blueprint $table) {
            $table->increments('Mod_Id');
            $table->text('Mod_Name');
            $table->integer('Mod_Parent_Id')->nullable();
            $table->integer('Mod_Status')->default('1')->comment = "1=Active 0=In-Active";
            $table->integer('Mod_CreatedBy')->unsigned()->nullable();
            $table->datetime('Mod_CreatedAt');
            $table->integer('Mod_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Mod_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_tbl');
    }
}
