<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRightsTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rights_tbl', function (Blueprint $table) {
            $table->increments('Usr_id');
            $table->integer('Usr_Use_Id')->unsigned();
            $table->integer('Usr_Mod_Id')->unsigned()->nullable();
            $table->integer('Usr_Zon_Id')->unsigned()->nullable();
            $table->integer('Usr_Bra_Id')->unsigned()->nullable();
            $table->tinyInteger('Usr_Status')->default('1')->comment = "1=Active 0=In-Active";
            $table->integer('Usr_CreatedBy')->unsigned()->nullable();
            $table->datetime('Usr_CreatedAt');
            $table->integer('Usr_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Usr_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rights_tbl');
    }
}
