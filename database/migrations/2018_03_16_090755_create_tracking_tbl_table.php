<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_tbl', function (Blueprint $table) {
            $table->increments('Tra_Id',11);
            $table->string('Tra_Driver_Name');
            $table->text('Tra_Pickup_Location');
            $table->text('Tra_Dropup_Location');
            $table->integer('Tra_CreatedBy')->unsigned()->nullable();
            $table->datetime('Tra_CreatedAt');
            $table->integer('Tra_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Tra_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_tbl');
    }
}
