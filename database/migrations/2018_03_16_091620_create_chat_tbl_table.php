<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_tbl', function (Blueprint $table) {
            $table->increments('Cha_Id',11);
            $table->integer('Cha_Sender_Id')->unsigned();
            $table->integer('Cha_Receiver_Id')->unsigned();
            $table->string('Cha_Name');
            $table->integer('Cha_Mobile_No')->unsigned();
            $table->integer('Cha_CreatedBy')->unsigned()->nullable();
            $table->datetime('Cha_CreatedAt');
            $table->integer('Cha_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Cha_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_tbl');
    }
}
