<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZoneTblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_tbl', function (Blueprint $table) {
            $table->increments('Zon_Id',11);
            $table->string('Zon_Name');
            $table->text('Zon_Description');
            $table->tinyInteger('Zon_Status')->default('1')->comment = "1=Active 0=In-Active";
            $table->integer('Zon_CreatedBy')->unsigned()->nullable();
            $table->datetime('Zon_CreatedAt');
            $table->integer('Zon_UpdatedBy')->unsigned()->nullable();
            $table->datetime('Zon_UpdatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zone_tbl');
    }
}
