<?php

Route::get('/phpinfo',function(){
	echo phpinfo();exit;
});

Route::get('permission','PermissionController@index');
Route::get('driver/permission','PermissionController@driver_index');

Auth::routes();

Route::get('/','Auth\LoginController@index');
Route::get('home','Admin\IndexController@home');
Route::post('postLogin','Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function(){

//Zone Management
Route::get('Zone Master','Admin\ZoneController@index');
Route::get('create_zone','Admin\ZoneController@create');
Route::post('save_zone','Admin\ZoneController@store');
Route::get('edit_zone/{id}','Admin\ZoneController@edit');
Route::post('update_zone','Admin\ZoneController@update');
Route::post('delete_zone','Admin\ZoneController@destroy');
Route::post('search_zone','Admin\ZoneController@search');
Route::post('zoneStatus','Admin\ZoneController@active');

//Branch Management
Route::get('Branch Master','Admin\BranchController@index');
Route::get('create_branch','Admin\BranchController@create');
Route::post('save_branch','Admin\BranchController@store');
Route::get('edit_branch/{id}','Admin\BranchController@edit');
Route::post('update_branch','Admin\BranchController@update');
Route::post('delete_branch','Admin\BranchController@destroy');
Route::post('search_branch','Admin\BranchController@search');
Route::post('branchStatus','Admin\BranchController@active');

//User Management
Route::get('User Mgmt.','Admin\UserController@index');
Route::get('create_user','Admin\UserController@create');
Route::post('save_user','Admin\UserController@store');
Route::get('edit_user/{id}','Admin\UserController@edit');
Route::post('update_user','Admin\UserController@update');
Route::post('delete_user','Admin\UserController@destroy');
Route::post('search_user','Admin\UserController@search');
Route::post('userStatus','Admin\UserController@active');
Route::post('teacher/getClassId','Admin\UserController@getClassId');
Route::get('changepassword','Admin\UserController@changepassword');
Route::get('password_change_user/{id}','Admin\UserController@password_change_user');
Route::post('save_password','Admin\UserController@save_password');
Route::post('change_password','Admin\UserController@change_password');

//Class Management
Route::get('Class Mgmt.','Admin\ClassController@index');
Route::get('create_class','Admin\ClassController@create');
Route::post('save_class','Admin\ClassController@store');
Route::get('edit_class/{id}','Admin\ClassController@edit');
Route::post('update_class','Admin\ClassController@update');
Route::post('delete_class','Admin\ClassController@destroy');
Route::post('search_class','Admin\ClassController@search');
Route::post('classStatus','Admin\ClassController@active');

//Event Management
Route::get('Event Mgmt.','Admin\EventController@index');
Route::get('create_event','Admin\EventController@create');
Route::get('create_event/{branchId?}','Admin\EventController@create');
Route::post('save_event','Admin\EventController@store');
Route::get('edit_event/{id}','Admin\EventController@edit');
Route::post('update_event','Admin\EventController@update');
Route::post('delete_event','Admin\EventController@destroy');
Route::post('search_event','Admin\EventController@search');
Route::post('approve_event','Admin\EventController@approve');
Route::post('event/getClass','Admin\EventController@getClass');
Route::post('approve_event_home','Admin\EventController@approvehome');

// Parent Management

Route::get('create_parent','Admin\ParentController@create');
Route::post('save_parent','Admin\ParentController@store');
Route::get('Parent Mgmt.','Admin\ParentController@index');
Route::get('edit_parent/{id}','Admin\ParentController@edit');
Route::post('update_parent','Admin\ParentController@update');
Route::post('parentStatus','Admin\ParentController@active');
Route::post('delete_parent','Admin\ParentController@destroy');
Route::post('search_parent','Admin\ParentController@search');
Route::get('password_change_parents/{id}','Admin\ParentController@password_change_parents');
Route::post('change_password_parents','Admin\ParentController@change_password_parents');
Route::post('change_password_mother','Admin\ParentController@change_password_mother');
Route::post('parent/checkemail','Admin\ParentController@checkemail');

//Student Management

Route::get('Student Mgmt.','Admin\StudentController@index');
Route::get('create_student','Admin\StudentController@create');
Route::get('student/getParent','Admin\StudentController@getParent');
Route::post('student/getClass','Admin\StudentController@getClass');
Route::post('student/getSection','Admin\StudentController@getSection');
Route::post('save_student','Admin\StudentController@store');
Route::get('edit_student/{id}','Admin\StudentController@edit');
Route::post('update_student','Admin\StudentController@update');
Route::post('search_student','Admin\StudentController@search');
Route::post('studentStatus','Admin\StudentController@active');
Route::post('delete_student','Admin\StudentController@destroy');

//Route Management

Route::get('Route Mgmt.','Admin\RouteController@index');
Route::get('create_route','Admin\RouteController@create');
Route::post('save_route','Admin\RouteController@store');
Route::get('edit_route/{id}','Admin\RouteController@edit');
Route::post('update_route','Admin\RouteController@update');
Route::post('delete_route','Admin\RouteController@destroy');
Route::post('routeStatus','Admin\RouteController@active');
Route::post('search_route','Admin\RouteController@search');
Route::post('search_assign_route','Admin\RouteController@assignSearch');

Route::get('Assign Route.','Admin\RouteController@assignIndex');
Route::get('assignRoute','Admin\RouteController@assignRoute');
Route::post('route/getClass','Admin\RouteController@getClass');
Route::post('route/getSection','Admin\RouteController@getSection');
Route::post('route/getStudent','Admin\RouteController@getStudent');
Route::post('assign_route','Admin\RouteController@assign');

// Report Management

Route::get('Report Mgmt.','Admin\ReportController@index');
Route::get('create_report','Admin\ReportController@create');
Route::post('save_report','Admin\ReportController@store');
Route::get('report/getStudentNameOrGr','Admin\ReportController@getStudentNameOrGr');
Route::post('report/getStudentData','Admin\ReportController@getStudentData');
Route::post('search_report','Admin\ReportController@search');
Route::post('delete_report','Admin\ReportController@destroy');
Route::get('edit_report/{id}','Admin\ReportController@edit');
Route::post('update_report','Admin\ReportController@update');
Route::post('raport/getclass','Admin\ReportController@getClass');
Route::post('report/getmonthyear','Admin\ReportController@getMonthYear');
Route::post('report/getstudentlist','Admin\ReportController@getStudentList');
Route::post('save_progress_report','Admin\ReportController@storePorgress');
Route::get('edit_progress_report/{id}','Admin\ReportController@editPorgress');
Route::post('update_progress_report','Admin\ReportController@updateProgress');


// Social Activity.
Route::get('Social Activity.','Admin\SocialController@index');
Route::get('create_social','Admin\SocialController@create');
Route::post('save_social','Admin\SocialController@store');
Route::post('social/getTeacherStudent','Admin\SocialController@getTeacherStudent');
Route::post('delete_social','Admin\SocialController@destroy');
Route::get('edit_social_activity/{id}','Admin\SocialController@edit');
Route::post('update_activity','Admin\SocialController@update');
Route::post('search_social_activity','Admin\SocialController@search');
Route::post('socialStatus','Admin\SocialController@active');
Route::post('approve_social','Admin\SocialController@approve');
Route::get('tag_list_activity/{id}','Admin\SocialController@tag_list_activity');
Route::get('social/image_video/{id}','Admin\SocialController@imageVideoremove');

//Fee Payment
Route::get('Payment Master','Admin\PaymentController@index');
Route::get('create_payment','Admin\PaymentController@create');
Route::get('payment/getParent','Admin\PaymentController@getParent');
Route::post('payment/getClass','Admin\PaymentController@getClass');
Route::post('payment/getSection','Admin\PaymentController@getSection');
Route::post('save_payment','Admin\PaymentController@store');
Route::get('edit_payment/{id}','Admin\PaymentController@edit');
Route::post('update_payment','Admin\PaymentController@update');
Route::post('search_payment','Admin\PaymentController@search');
Route::post('paymentStatus','Admin\PaymentController@active');
Route::post('delete_payment','Admin\PaymentController@destroy');
Route::post('get_student_list','Admin\PaymentController@get_student_list');
Route::post('payment/delete','Admin\PaymentController@delete');

//Attendence
Route::get('list_Attendance','Admin\AttendanceController@index');
Route::get('create_Attendance','Admin\AttendanceController@create');
// Route::post('store_attendance','Admin\AttendanceController@storePorgress');
Route::get('save_attendance','Admin\AttendanceController@saveAttendance');
// Route::get('edit_attendance/{id}','Admin\AttendanceController@editAttendance');
/*Route::post('delete_attendence','Admin\AttendanceController@destroy');
Route::post('search_attendence','Admin\AttendanceController@search');
Route::post('update_branch','Admin\AttendanceController@update');
Route::post('active_attendence','Admin\AttendanceController@active');*/
Route::get('attendance/getStudentNameOrGr','Admin\AttendanceController@getStudentNameOrGr');
Route::post('attendance/getstudentdata','Admin\AttendanceController@getStudentData');
Route::post('attendance/getclass','Admin\AttendanceController@getClass');
Route::post('attendance/getstudentlist','Admin\AttendanceController@getStudentList');

//Paytm
Route::get('Paytm Master','Admin\PaytmController@index');
Route::post('save_paytm','Admin\PaytmController@store');

Route::get('create_paytm','Admin\PaytmController@create');
Route::post('payment_order','Admin\PaytmController@order');
//Route::get('conform','Admin\PaytmController@conform');

Route::post('ajax','Admin\EventController@classChange');
Route::get('logout','Auth\LoginController@logout');
//Route::get('/home', 'HomeController@index')->name('home');

// Excel
Route::get('Import Excel','Admin\ExcelController@index');
Route::post('import_excel','Admin\ExcelController@import_excel');

Route::get('Form Mgmt.','Admin\FormController@index');
Route::post('import_form','Admin\FormController@import_form');

//Assignment
Route::get('Diary Mgmt.','Admin\AssignmentController@index');
Route::get('create_assignment','Admin\AssignmentController@create');
Route::post('save_assignment','Admin\AssignmentController@store');
Route::post('search_homework','Admin\AssignmentController@search');
Route::get('edit_assignment/{id}','Admin\AssignmentController@edit');
Route::post('update_assignment','Admin\AssignmentController@update');
Route::post('delete_assignment','Admin\AssignmentController@destroy');

// Help
Route::get('Help Mgmt.','Admin\HelpController@index');
Route::post('delete_help','Admin\HelpController@destroy');

// Get Class Id In user mgmt.
Route::post('getClassId','Admin\UserController@getClassId');

// Subject Module
Route::get('Subject Mgmt.','Admin\SubjectController@index');
Route::get('view/subject/{id}','Admin\SubjectController@viewSubjectFile');
Route::post('delete_subject','Admin\SubjectController@destory');

// Leave Module
Route::get('Leave Mgmt./{filter?}','Admin\LeaveController@index');
Route::get('create_teacher_leave','Admin\LeaveController@create_teacher_leave');
Route::post('save_teacher_leave','Admin\LeaveController@save_teacher_leave');
Route::get('confirm_leave/{leave_id}','Admin\LeaveController@confirm_leave');
Route::post('leave_request_delete','Admin\LeaveController@leave_request_delete');
Route::get('edit_teacher_leave/{id}','Admin\LeaveController@edit_teacher_leave');
Route::post('update_teacher_leave','Admin\LeaveController@update_teacher_leave');

// NewsFeed
Route::get('Newsfeed Mgmt.','Admin\NewsfeedController@index');
Route::get('create_newsfeed','Admin\NewsfeedController@create');
Route::post('save_newsfeed','Admin\NewsfeedController@save');
Route::get('edit_newsfeed/{id}','Admin\NewsfeedController@edit');
Route::post('update_newsfeed','Admin\NewsfeedController@update');

//Chat Management
Route::get('Chat Mgmt.','Admin\ChatController@index')->name('chat.dashboard');
Route::get('create_group','Admin\ChatController@create')->name('chat.create.group');
Route::post('get_teacher_parent','Admin\ChatController@get_teacher_parent')->name('chat.create.get_teacher_parent');
Route::post('save_group','Admin\ChatController@save_group')->name('chat.create.save_group');
Route::get('save_group_edit','Admin\ChatController@save_group_edit')->name('chat.edit.save_group');
Route::get('group_detail/{id}','Admin\ChatController@groupDetail')->name('chat.group.detail');
Route::get('group/add/{id}','Admin\ChatController@groupAdd')->name('chat.group.add');
Route::get('/group/chat','Admin\ChatController@groupChatOpen')->name('chat.group.open');
Route::post('delete_group','Admin\ChatController@destroy');

Route::post('delete_grp_member','Admin\ChatController@destroyMemberGroup');
// Group member delete
Route::post('delete_group_member','Admin\ChatController@groupMemberDestroy');
// Image upload group
Route::post('img_group_upload','Admin\ChatController@imageGroupUpload')->name('group_img_upload');
// Video upload group
Route::post('video_group_upload','Admin\ChatController@videoGroupUpload')->name('group_video_upload');
// Music Upload group
Route::post('audio_group_upload','Admin\ChatController@audioGroupUpload')->name('group_audio_upload');
// Document upload group
Route::post('file_group_upload','Admin\ChatController@fileGroupUpload')->name('group_file_upload');




//Report
Route::get('Report','Admin\RecordController@index')->name('report.dashboard');
Route::get('search_record','Admin\RecordController@search')->name('record.search');
Route::get('record/getClass','Admin\RecordController@getClass')->name('record.getclass');
Route::get('record/getSection','Admin\RecordController@getSection')->name('record.getsection');
});

//Fees Report
Route::get('fees_report','Admin\FeesController@index')->name('fees_report.index');
Route::get('search_fees_record','Admin\FeesController@search')->name('fees_report.search');

//User Report
Route::get('user_report','Admin\UserReportController@index')->name('report.user');
Route::get('user_report_search','Admin\UserReportController@search')->name('user_report.search');

//Student Report
Route::get('student_report','Admin\StudentReportController@index')->name('report.student');
Route::get('student_report_search','Admin\StudentReportController@search')->name('student_report.search');

//Social Activity Report
Route::get('social_activity_report','Admin\SocialActivityReportController@index')->name('report.social_activity');
Route::get('social_activity_search','Admin\SocialActivityReportController@search')->name('social_activity_report.search');

//Event Managment Report
Route::get('event_report','Admin\EventReportController@index')->name('report.event');
Route::get('event_report_search','Admin\EventReportController@search')->name('event_report.search');

//Assignment Report
Route::get('assignment_report','Admin\AssignmentReportController@index')->name('report.assignment');
Route::get('assignment_report_search','Admin\AssignmentReportController@search')->name('assignmentreport.search');



