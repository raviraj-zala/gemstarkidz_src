<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//User Login
Route::post('sign_In','RESTAPIs\WebservicesController@sign_In');
Route::post('logout','RESTAPIs\WebservicesController@logout');
Route::post('user_profile_Record','RESTAPIs\WebservicesController@user_profile_Record');
Route::post('user_Password_Change','RESTAPIs\WebservicesController@user_Password_Change');
Route::post('user_Mobile_No_Valid','RESTAPIs\WebservicesController@user_Mobile_No_Valid');
Route::post('user_Forget_Password','RESTAPIs\WebservicesController@user_Forget_Password');
Route::post('userStatus','RESTAPIs\WebservicesController@userStatus');

//User Module
Route::post('sign_Up','RESTAPIs\ProfileservicesController@sign_Up');
Route::post('profile_Update','RESTAPIs\ProfileservicesController@profile_Update');
Route::post('email_Mobile_Profile','RESTAPIs\ProfileservicesController@email_Mobile_Profile');
Route::post('teacher_Assign_Class','RESTAPIs\ProfileservicesController@teacher_Assign_Class');

//Social Activity Module
Route::post('create_Activity','RESTAPIs\ActivityservicesController@create_Activity');
Route::post('activity_List','RESTAPIs\ActivityservicesController@activity_List');
Route::post('activity_Like','RESTAPIs\ActivityservicesController@activity_Like');
Route::post('activity_Comment','RESTAPIs\ActivityservicesController@activity_Comment');
Route::post('comment_List','RESTAPIs\ActivityservicesController@comment_List');
Route::post('tag_List','RESTAPIs\ActivityservicesController@tag_List');
Route::post('parents_activity_List','RESTAPIs\ActivityservicesController@parents_activity_List');
Route::post('activity_Like_Image','RESTAPIs\ActivityservicesController@activity_Like_Image');
Route::post('create_Comment_Image','RESTAPIs\ActivityservicesController@create_Comment_Image');
Route::post('image_Like_List','RESTAPIs\ActivityservicesController@image_Like_List');
Route::post('comment_Image_List','RESTAPIs\ActivityservicesController@comment_Image_List');
Route::post('activity_Like_List','RESTAPIs\ActivityservicesController@activity_Like_List');
Route::post('get_student_Class_list','RESTAPIs\ActivityservicesController@get_student_Class_list');
Route::post('branch_wise_Class_List','RESTAPIs\ActivityservicesController@branch_wise_Class_List');
Route::post('get_teacher_all_student','RESTAPIs\ActivityservicesController@get_teacher_all_student');

//Event Module
Route::post('event_List','RESTAPIs\EventservicesController@event_List');
Route::post('event_Accept','RESTAPIs\EventservicesController@event_Accept');
Route::post('event_Decline','RESTAPIs\EventservicesController@event_Decline');
Route::post('create_Event','RESTAPIs\EventservicesController@create_Event');
Route::post('event_accept_profile','RESTAPIs\EventservicesController@event_accept_profile');
Route::post('update_Event','RESTAPIs\EventservicesController@update_Event');
Route::post('accept_decline_event','RESTAPIs\EventservicesController@accept_decline_event');
Route::post('event_Monthwise_List','RESTAPIs\EventservicesController@event_Monthwise_List');


//Attendence Module
Route::post('attendence_Update','RESTAPIs\AttendenceservicesController@attendence_Update');
Route::post('attendence_List','RESTAPIs\AttendenceservicesController@attendence_List');
Route::post('create_Attendence','RESTAPIs\AttendenceservicesController@create_Attendence');
Route::post('absent_present_List','RESTAPIs\AttendenceservicesController@absent_present_List');
Route::post('get_student_List','RESTAPIs\AttendenceservicesController@get_student_List');
Route::post('absent_present_Student','RESTAPIs\AttendenceservicesController@absent_present_Student');
Route::post('student_Attendence_List','RESTAPIs\AttendenceservicesController@student_Attendence_List');
Route::post('attendence_Datewise_List','RESTAPIs\AttendenceservicesController@attendence_Datewise_List');
Route::post('attendence_Monthwise_List','RESTAPIs\AttendenceservicesController@attendence_Monthwise_List');
Route::post('absent_present_Check','RESTAPIs\AttendenceservicesController@absent_present_Check');

//HomeWork Module
Route::post('class_Section_List','RESTAPIs\HomeworkservicesController@class_Section_List');
Route::post('create_Homework','RESTAPIs\HomeworkservicesController@create_Homework');
Route::post('homework_List','RESTAPIs\HomeworkservicesController@homework_List');
Route::post('view_Homework','RESTAPIs\HomeworkservicesController@view_Homework');
Route::post('parent_Homework_List','RESTAPIs\HomeworkservicesController@parent_Homework_List');
Route::post('teacher_Homework_List','RESTAPIs\HomeworkservicesController@teacher_Homework_List');
Route::post('homework_Monthwise_List','RESTAPIs\HomeworkservicesController@homework_Monthwise_List');
Route::post('teacher_Homework_Monthwise_List','RESTAPIs\HomeworkservicesController@teacher_Homework_Monthwise_List');
//Report Module
Route::post('report_List','RESTAPIs\ReportservicesController@report_List');
Route::post('report_Update','RESTAPIs\ReportservicesController@report_Update');
Route::post('parent_report_List','RESTAPIs\ReportservicesController@parent_report_List');
Route::post('parent_report_Mothwise_List','RESTAPIs\ReportservicesController@parent_report_Mothwise_List');
//Driver Module
Route::post('driver_Routes','RESTAPIs\RouteservicesController@driver_Routes');
Route::post('parent_Route','RESTAPIs\RouteservicesController@parent_Route');
Route::post('teachers_Routes_List','RESTAPIs\RouteservicesController@teachers_Routes_List');
Route::get('drivers_List','RESTAPIs\RouteservicesController@drivers_List');
//Chat Module
Route::get('chat_user_List','RESTAPIs\ChatServicesController@chat_user_List');
Route::post('create_group_Chat','RESTAPIs\ChatServicesController@create_group_Chat');
Route::post('chat_parent_List','RESTAPIs\ChatServicesController@chat_parent_List');
Route::post('chat_teacher_List','RESTAPIs\ChatServicesController@chat_teacher_List');
Route::post('uploadFile','RESTAPIs\ChatServicesController@uploadFile');
Route::post('staff_Chat_List','RESTAPIs\ChatServicesController@staff_Chat_List');
Route::post('chat_Group_List','RESTAPIs\ChatServicesController@chat_Group_List');
Route::post('chat_user_group_List','RESTAPIs\ChatServicesController@chat_user_group_List');
Route::post('group_Member_List','RESTAPIs\ChatServicesController@group_Member_List');
Route::post('group_Member_Remove','RESTAPIs\ChatServicesController@group_Member_Remove');
Route::post('add_Group_Member','RESTAPIs\ChatServicesController@add_Group_Member');

//Fee Payment
Route::post('create_Paymment','RESTAPIs\PaymentservicesController@create_Paymment');
Route::post('payment_List','RESTAPIs\PaymentservicesController@payment_List');


// Testing Api
Route::post('test','RESTAPIs\TestServiceController@test');
Route::get('sendSMS/{number}/{message}','RESTAPIs\TestServiceController@sendSMS');

//Android Version Update
Route::post('android_version','RESTAPIs\AndroidservicesController@android_version');
Route::post('suraj_android_version','RESTAPIs\AndroidservicesController@suraj_android_version');

// Help 
Route::post('add_help','RESTAPIs\HelpservicesController@add_help');

// Leave 
Route::post('send_leave','RESTAPIs\LeaveservicesController@send_leave');
Route::post('teacher_leave_report','RESTAPIs\LeaveservicesController@teacher_leave_report');
Route::post('student_leave_report','RESTAPIs\LeaveservicesController@student_leave_report');

// Newsfeed
Route::post('create_newsfeed','RESTAPIs\NewsfeedserviceController@create_newsfeed');
Route::post('teacher_newsfeed_list','RESTAPIs\NewsfeedserviceController@teacher_newsfeed_list');
Route::post('parent_newsfeed_list','RESTAPIs\NewsfeedserviceController@parent_newsfeed_list');


Route::group(['prefix' => 'v1'], function () {
// get_all_student
Route::post('get_all_student','RESTAPIs\v1\ParentController@get_all_student');
//Fee Payment
Route::post('get_payment_details','RESTAPIs\v1\PaymentController@get_payment_details');
// Chat
Route::post('restore_chat','RESTAPIs\v1\ChatServiceController@restore_chat');

// Brnach Image
Route::post('branch_image','RESTAPIs\AndroidservicesController@get_branch_image');

});