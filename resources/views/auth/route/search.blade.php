<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th>Route Name</th>
      <th>Branch Name</th>
      <th>Driver Name</th>
      <th>From</th>
      <th>To</th>
      <th>Status</th>
      <th>Edit</th>
    </tr>
		<?php $i=1; ?>
		@forelse($route as $data)
	    <tr>
	      	<td>
	      		<input id="route{{ $i }}" type="checkbox" name="id[]" value="{{ $data->Rou_Id }}" class="css-checkbox check-all">
				<label for="route{{ $i }}" class="css-label table-ckeckbox"></label>
	      	</td>
	      	<td class="td-left">{{ $data->Rou_Name }}</td>
	      	<td class="td-left">{{ $data->Brn_Name }}</td>
	      	<td class="td-left">{{ $data->Use_Name }}</td>
	      	<td class="td-left">{{ $data->Rou_From }}</td>
	      	<td class="td-left">{{ $data->Rou_To }}</td>
	      	<td id="sts{{ $i }}">
	      		@if($data->Rou_Status == 1)
					Active
				@elseif($data->Rou_Status == 0)
					In-Active
				@endif
	      	</td>
	      	<td><a href="{{ url('edit_route',$data->Rou_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
	    </tr>
	    <?php $i++; ?>
	    @empty
	    <tr><td colspan="8" align="center"> No Data Found</td></tr>
	    @endforelse
  </tbody>
</table>