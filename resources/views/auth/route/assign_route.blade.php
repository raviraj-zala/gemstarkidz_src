@extends('auth.Master.master')

@section('title','Assign Student')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/') }}" alt="">Assign Route To Student.</h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<form name="assign-student" role="form" method="POST" action="{{ url('assign_route') }}" enctype="multipart/form-data" id="create-event">

<div class="form-section">


    {{ csrf_field() }}
 <div class="branch-form">

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Route Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                 <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="routeID" id="routeId" required>
                        <option value="none">-- Select --</option>
                        @foreach($route as $data)
                            <option value="{{ $data->Rou_Id }}" >{{ $data->Rou_Name }}</option>
                        @endforeach
                    </select>
                </div>
                @if (Session::has('routeId'))
                    <span class="text-danger">
                         {{ Session::get('routeId') }}
                    </span>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box zone-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" tabindex="2">
                    <option value="none" >-- Select --</option>
                  </select>
                </div>
                </div>
                @if (Session::has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        {{ Session::get('class') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box zone-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="class-sec" name="section" id="section" tabindex="3">
                    <option value="none">-- Select--</option>
                  </select>
                </div>
                </div>
                @if (Session::has('section'))
                <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                </label>
                @endif
                </div>
            </div>
            
    </div>


</div>

    <div id="studentList">
    </div>
</form>

@endsection

@section('footer')

@push('footer_script')
<script type="text/javascript">
// function selectAll(){
//    alert("select all");
//     // $("input[type=checkbox] .std").each(function(i){
//     //     $(this).prop('checked', true);
//     // });
//     // var checkedValue = document.querySelector('.std:checked').value;
//     var checkedValue = null; 
//     var inputElements = document.getElementsByName('student');
//     for(var i=0; inputElements[i]; ++i){
//           if(inputElements[i].checked){
//                checkedValue = inputElements[i].value;
//                break;
//           }
//     }
//     alert(checkedValue);
// }
// function unselectAll(){
//     alert("un select all");
// }
$(document).ready(function(){

    $("#routeId").on('change',function(){
        var routeId = $("#routeId").val();
        $.ajax({
            type : "POST",
            url : "{{ url('route/getClass') }}",
            data : {
                _token : "{{ csrf_token() }}",
                Rou_Id : routeId
            },
            dataType : "JSON",
            success : function(data){
                $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                $(".class-sec option").each(function() {
                        $(this).remove();
                    });
                var items = [];
                 $.each( data, function( key, val ) {
                    items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                 });
                $("#class").append('<option value="none">-- Select--</option>');
                $("#section").append('<option value="none">-- Select --</option>');
                $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });

    $("#class").on('change',function(){
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            $.ajax({
                type : "POST",
                url : "{{ url('route/getSection') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
        });

    });

    $("#section").on('change',function(){
        var routeId = $("#routeId").val();
        var idAndClass = $("#class").val();
        var classIdName = idAndClass.split(",");
        var className = classIdName[1];
        
        var sectionName = $("#section").val();
        
        $.ajax({
            type : "POST",
            url : "{{ url('route/getStudent') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Rou_Id : routeId,
                Cla_Class : className,
                Cla_Section : sectionName
            },
            success : function(data){
                $("#studentList").html("");
                $("#studentList").append(data);
            },
            error : function(error){
                
            }
        });
    })

});

</script>
@endpush


@section('footer_link_and_scripts')
