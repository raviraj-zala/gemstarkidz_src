
@extends('auth.Master.master')

@section('title','Edit Route')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/zone-img.png') }}" alt="zone-img">Edit Route</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>

<div class="form-section">

<form name="edit-route" role="form" method="POST" action="{{ url('update_route') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
    <div class="branch-form">
            <input type="hidden" name="Rou_Id" value="{{ $route->Rou_Id }}">
           <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Branch <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                 <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="branchId" id="branch" required tabindex="1">
                        @foreach($branch as $data)
                            <option value="{{ $data->Brn_Id }}" @if(old('branch')== $data->Brn_Id || $route->Rou_Brn_Id== $data->Brn_Id ) selected @endif>{{ $data->Brn_Name }}</option>
                        @endforeach
                    </select>
                </div>
                @if(Session::has('branchError'))
                <label class="error" for="branch">
                    <span class="text-danger">
                   {{ Session::get('branchError') }}
                    </span>
                </label>
                @endif
                </div>
            </div>
        
            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Driver <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                 <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="driverId" id="driver" required tabindex="2">
                        @foreach($driver as $data)
                            <option value="{{ $data->Use_Id }}" @if( old('driverId') == $data->Use_Id || $route->Rou_Use_Id == $data->Use_Id ) selected @endif>{{ $data->Use_Name }}</option>
                        @endforeach
                    </select>
                </div>
                @if (Session::has('driverError'))
                <label class="error" for="driverId">
                    <span class="text-danger">
                   {{ Session::get('driverError') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Route Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <input type="text" name="routeName" class="branch-control {{ $errors->has('routeName') ? ' is-invalid' : '' }}" tabindex="3" maxlength="20" value="{{ $route->Rou_Name }}">
                </div>
                @if ($errors->has('routeName'))
                <label class="error" for="routeName">
                    <span class="text-danger">
                        {{ $errors->first('routeName') }}
                    </span>
                </label>
                @endif
            </div>


            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>From :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <div id="locationField">
			                <input id="autocomplete" placeholder="From" name="addressFrom" class="branch-control {{ $errors->has('addressFrom') ? ' is-invalid' : '' }}" tabindex="3"
			             onFocus="geolocate()" type="text" value="{{ $route->Rou_From }}">
			          </div>
                @if ($errors->has('addressFrom'))
                <label class="error" for="addressFrom">
                    <span class="text-danger">
                        {{ $errors->first('addressFrom') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>To :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <div id="locationField">
			                <input id="autocomplete2" placeholder="To" name="addressTo" class="branch-control {{ $errors->has('addressTo') ? ' is-invalid' : '' }}" tabindex="4"
			             onFocus="geolocate()" type="text" value="{{ $route->Rou_To }}">
			          </div>
                @if ($errors->has('addressTo'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('addressTo') }}</strong>
                    </span>
                @endif
                </div>
            </div>


        	<div class="form-box">
                <div class="form-text form-box-width">
                  <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box zone-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="5"  name="status" required>
                    <option value="1" @if($route->Rou_Status==1) selected="true" @endif >Active</option>
                    <option value="0" @if($route->Rou_Status==0) selected="false" @endif >In-Active</option>
                </select>
                @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif

                </div>
                </div>    
        </div>

        <div class="form-btn branch-form-btn">
            <input type="submit" value="save" tabindex="6"></input><a href="{{ URL::previous() }}"><input value="Cancel" type="button"  tabindex="7" ></a>
        </div>
    </div>
</form>

</div>
@endsection


@push('footer_script')

 <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);

        autocomplete2 = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete2')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete2.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=
AIzaSyBIW2OkUKhfaay6X1HEgF9c0GylEbAbWgk
&libraries=places&callback=initAutocomplete"
        async defer></script>

@endpush

@section('footer')

@section('footer_link_and_scripts')