<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Event</th>
      <th class="td-left">Class</th>
      <th class="td-left">Section</th>
      <th>Event Date</th>
      <th>Created By</th>
      <th>Approve</th>
      <th>Edit</th>
    </tr>
@if($event)
  <?php $i=1; $eve1 = ""; $eve2 = ""; $eve3 = ""; $eve4 = ""; $eve5 = ""; $eve6 = "";  ?>
  @foreach($event as $data)
    <tr>
        <td>
          @if($eve1 != $data->Eve_Unique_Id)
          <?php $eve1 = $data->Eve_Unique_Id; ?>
          <input id="event{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Eve_Unique_Id }}" class="css-checkbox check-all"><label for="event{{ $i }}" class="css-label table-ckeckbox"></label>
          @endif
        </td>
        
        <td class="td-left">
          @if($eve2 != $data->Eve_Unique_Id)
            <?php $eve2 = $data->Eve_Unique_Id; ?>
            {{ $data->Eve_Name }}
          @endif
      </td>
        
        <td class="td-left">
            {{ $data->Cla_Class }}
        </td>
        
        <td class="td-left">
            {{ $data->Eve_Cla_Section }}
      </td>
        
        <td>
          @if($eve3 != $data->Eve_Unique_Id)
            <?php $eve3 = $data->Eve_Unique_Id; ?>
            {{ date("d-m-Y", strtotime($data->Eve_Date)) }}
          @endif
    </td>
        
        <td>
          @if($eve4 != $data->Eve_Unique_Id)
            <?php $eve4 = $data->Eve_Unique_Id; ?>
            {{ $data->createdByName}}
        @endif
      </td>
        
        <td>
          @if($eve5 != $data->Eve_Unique_Id)
            <?php $eve5 = $data->Eve_Unique_Id; ?>
            @if($data->Eve_Approve==1) 
              <label style="color:  #ff975b;">Approve</label>
        @else
            @if(Auth::user()->Use_Type==1)
            <label style="color:  #ff975b;"><input type="checkbox" class="eve_sts" value="{{ $data->Eve_Unique_Id }}"></label>
            @else(Auth::user()->Use_Type==2 || Auth::user()->Use_Type==3)
            <label style="color:  #ff975b;">Pendding...</label>
            @endif
        @endif
      @endif
        </td>
        
        <td>
          @if($eve6 != $data->Eve_Unique_Id)
            <?php $eve6 = $data->Eve_Unique_Id; ?>
            <a href="{{ url('edit_event',$data->Eve_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>
          @endif
      </td>

    </tr>
  <?php $i++; ?>
    @endforeach
@else
  No Data Found
@endif
</tbody></table>