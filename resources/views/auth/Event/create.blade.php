@extends('auth.Master.master')

@section('title','Create Event')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/event-management.png') }}" alt="">Create Event</h2>      
     </div>
</div>


@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">

<form name="create-event" role="form" method="POST" action="{{ url('save_event') }}" enctype="multipart/form-data" id="create-event">

    {{ csrf_field() }}
 <div class="branch-form">

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Branch <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                 <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="branch" id="branch" required>
                        <option value="none">-- Select --</option>
                        @foreach($branch as $data)
                            <option value="{{ $data->Brn_Id }}" @if($data->Brn_Id==Request::segment(2) || old('branch')== $data->Brn_Id ) selected @endif>{{ $data->Brn_Name }}</option>
                        @endforeach
                    </select>
                </div>
                @if (Session::has('branch'))
                    <span class="text-danger">
			             {{ Session::get('branch') }}
                    </span>
                @endif
                </div>
            </div>


             <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Event Name<span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <input type="text" name="event" class="branch-control {{ $errors->has('event') ? ' is-invalid' : '' }}" required value="{{ old('event') }}">
                </div>
                @if ($errors->has('event'))
                    <span class="text-danger">
                        {{ $errors->first('event') }}
                    </span>
                @endif
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Event Start Date* :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                <!-- <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div> -->
                        <!-- <input data-provide="datepicker" type="text" class="form-control pull-right datepicker" data-date-format="dd/mm/yyyy" name="date" data-bind="autoclose : true" placeholder="dd/mm/yyyy" required value="{{ old('date') }}"> -->
                        <input type="text" name="date" id="datepicker" value="{{ old('date') }}" required placeholder="dd/mm/yyyy">
                <!-- </div> -->
                @if($errors->has('date'))
                    <span class="text-danger">
                        {{ $errors->first('date') }}
                    </span>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Event End Date* :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                <!-- <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div> -->
                        <!-- <input data-provide="datepicker" type="text" class="form-control pull-right" id="enddatepicker" name="enddate" data-date-format="dd/mm/yyyy" data-bind="autoclose : true" placeholder="dd/mm/yyyy" value="{{ old('enddate') }}"> -->

                        <input type="text" name="enddate" id="enddatepicker" value="{{ old('enddate') }}" placeholder="dd/mm/yyyy">
                <!-- </div> -->
                @if($errors->has('enddate'))
                    <span class="text-danger">
                        {{ $errors->first('enddate') }}
                    </span>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Image :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box"> 
                    <input name="image" class="browser-btn {{ $errors->has('image') ? ' is-invalid' : '' }}" type="file" accept="image/*">
                </div>
                @if (Session::has('image'))
                    <span class="text-danger">
                        {{ Session::get('image') }}
                    </span>
                @endif
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Description <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <textarea name="description" class="branch-control {{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5">{{ old('description') }}</textarea>
                </div>
                @if ($errors->has('description'))
                    <span class="text-danger">
                        {{ $errors->first('description') }}
                    </span>
                @endif
            </div>
            <div class="branch-box col-md-3">
                           <input type="checkbox" name="rsvp" value="1"><label class="css-label">:  RSVP</label>
                        </div>
            
    </div>
</div>


<div id="classList">

@if(isset($classList))

<div class="row">
        <div class="col-lg-12">
        @if(Session::has('notselected'))
	    <br><span class="text-danger">
	        {{ Session::get('notselected') }}
	    </span>
	@endif
        <?php $i=1; $cls=""; $sid=""; ?>   
        @foreach($classList as $data)
                <div class="box box-primary">
                @if($data->Cla_Class!=$cls)
                    <?php $cls=$data->Cla_Class; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="class[]" value="{{ $data->Cla_Class }}">{{ $data->Cla_Class }}</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll('{{ $data->Cla_Class }}')">Check All</a>|
                            <a href="javascript:void(0);" onclick="unselectAll('{{ $data->Cla_Class }}')">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="section[]" id="chckbox{{ $i }}" class=" class_id css-checkbox {{ 'sec'.$data->Cla_Class }}" value="{{ $data->Cla_Id.','.$data->Cla_Section }}"><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Section }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
</div>

@endif

</div>

<div class="row">
<div class="col-lg-12">
    <div class="form-btn">
        <input value="save" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button"   ></a>
    </div>
</div>
</div>
</form>

@endsection

@section('footer')

@push('footer_script')
<script type="text/javascript">
$(document).ready(function(){
    $("#submitBtn").on("click",function(e){
        var clsRight =$('.class_id:checkbox:checked').length;
        if( clsRight == 0){
            alert("Please select atleast one class.");
            e.preventDefault();
        }
    });
});
function myFunction() {
    alert("The form was submitted");
    return false;
}

$(function() {
$( "#datepicker" ).datepicker({
    dateFormat: "dd/mm/yy",
    minDate: 0,
    onSelect: function(selectedDate) {
        $('#enddatepicker').datepicker('option', 'minDate', selectedDate || '2013-09-10');
    }
});
$( "#enddatepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        // minDate: parsed_date
    });
});

    $("#branch").on('change',function(){
        var branchId = $("#branch").val();
          if (branchId) { // require a URL
              window.location = "{{ url('create_event') }}"+"/"+branchId; // redirect
          }
        // $.ajax({
        //     type : "POST",
        //     url : "{{ url('event/getClass') }}",
        //     data : {
        //         _token : "{{ csrf_token() }}",
        //         Cla_Brn_Id : branchId
        //     },
        //     success : function(data){
        //         $("#classList").html(data);
        //     },error : function(error){
        //         console.log(error);
        //     }
        // });
    });

function selectAll(cls)
{
    $("input.sec"+cls).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAll(cls)
{
    $("input.sec"+cls).each(function(i){
        $(this).prop('checked', false);
    });   
}
</script>
@endpush


@section('footer_link_and_scripts')
