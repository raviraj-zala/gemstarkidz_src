@extends('auth.Master.master')

@section('title','Edit Event')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/event-management.png') }}" alt="">Edit Event</h2>
     </div>
</div>


@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">

<form name="create-event" role="form" method="POST" action="{{ url('update_event') }}" enctype="multipart/form-data">
    {{ csrf_field() }}

 <div class="branch-form">
        <input type="hidden" name="id" value="{{ $event->Eve_Unique_Id }}">
            
            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Event Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <input type="text" name="event" class="branch-control{{ $errors->has('event') ? ' is-invalid' : '' }}" value="{{ $event->Eve_Name }}" required >
                </div>
                @if ($errors->has('event'))
                    <label class="error" for="event">
                    <span class="text-danger">
                        {{ $errors->first('event') }}
                    </span>
                    </label>
                @endif
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Event Start Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                <!-- <div class="input-group date"> -->
                    <!-- <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                        <input data-provide="datepicker" type="text" class="form-control pull-right datepicker" data-date-format="dd/mm/yyyy" name="date" required data-bind="autoclose : true" value="{{ date('d/m/Y',strtotime($event->Eve_Date)) }}"> -->

                        <input type="text" name="date" id="datepicker" value="{{ date('d/m/Y',strtotime($event->Eve_Date)) }}" placeholder="dd/mm/yyyy">
                <!-- </div> -->
                @if($errors->has('date'))
                <label class="error" for="date">
                    <span class="text-danger">
                        {{ $errors->first('date') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Event End Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                <!-- <div class="input-group date"> -->
                    <!-- <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                        <input data-provide="datepicker" type="text" class="form-control pull-right" id="enddatepicker" name="enddate" data-date-format="dd/mm/yyyy" data-bind="autoclose : true" value="{{ date('d/m/Y',strtotime($event->Eve_End_Date)) }}"> -->

                        <input type="text" name="enddate" id="enddatepicker" value="{{ date('d/m/Y',strtotime($event->Eve_End_Date)) }}" placeholder="dd/mm/yyyy">
                <!-- </div> -->
                @if($errors->has('enddate'))
                <label class="error" for="enddate">
                    <span class="text-danger">
                        {{ $errors->first('enddate') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Image :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box sl-img"> 
                    <img src="{{ url('public/images/event/'.$event->Eve_Image) }}">
                    <input type="file" name="image" class="browser-btn" >
                    @if ($errors->has('image'))
                    <label for="image" class="error">
                        <span class="text-danger" >
                            {{ $errors->first('image') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Description <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <textarea name="description" class="branch-control {{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5">{{ $event->Eve_Description }}</textarea>
                    @if ($errors->has('description'))
                    <label class="error" for="description">
                        <span class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>
           <div class="branch-box col-md-3">
               <input type="checkbox" name="rsvp" value="1" @if($event->Eve_Rsvp == 1) checked="true" @endif><label style="color:white;padding-left:10px;">  RSVP</label>
            </div>

    </div>
</div>

<div class="row">
        <div class="col-lg-12">
        @if(Session::has('notselected'))
	    <br><span class="text-danger">
	        {{ Session::get('notselected') }}
	    </span>
	@endif
        <?php $i=1; $cls=""; $sid=""; ?>   
        @foreach($classList as $data)
                <div class="box box-primary">
                @if($data->Cla_Class!=$cls)
                    <?php $cls=$data->Cla_Class; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="class[]" value="{{ $data->Cla_Class }}">{{ $data->Cla_Class }}</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll('{{ $data->Cla_Class }}')">Check All</a>|
                            <a href="javascript:void(0);" onclick="unselectAll('{{ $data->Cla_Class }}')">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="section[]" id="chckbox{{ $i }}" class=" class_id css-checkbox {{ 'sec'.$data->Cla_Class }}" value="{{ $data->Cla_Id.','.$data->Cla_Section }}" 

                            @foreach($sel_section as $sel_sec)  
                                @if($data->Cla_Id == $sel_sec->Eve_Cla_Id)
                                    checked="true"
                                @endif
                            @endforeach

                           ><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Section }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
</div>

<div class="row">
<div class="col-lg-12">
    <div class="form-btn">
        <input value="save" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button"   ></a>
    </div>
</div>
</div>
</form>




@endsection

@section('footer')

@push('footer_script')

<script type="text/javascript">
    $(document).ready(function(){
        $("#submitBtn").on("click",function(e){
            var clsRight =$('.class_id:checkbox:checked').length;
            if( clsRight == 0){
                alert("Please select atleast one class.");
                e.preventDefault();
            }
        });

        $('#cls').on('change',function(){
            var Cla_Id = $(this).val();
            if(Cla_Id){
                $.ajax({
                    type : 'POST',
                    url : '{{ url('ajax') }}',
                    data : 'Cla_Id='+Cla_Id,
                    success : function(data){
                        $('#section').html(data);
                        //$('#city').html('<option value="">Select State First</option>');
                    }
                });
            }
            else{
                $('#section').html('<option value="">Select Class First</option>');
            }
        });
        
    });
    $(function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "dd/mm/yy",
            minDate: 0,
            onSelect: function(selectedDate) {
                $('#enddatepicker').datepicker('option', 'minDate', selectedDate || '2013-09-10');
            }
        });
        $( "#enddatepicker" ).datepicker({
                dateFormat: "dd/mm/yy",
                minDate: 0,
                onSelect: function(selectedDate) {
                $('#enddatepicker').datepicker('option', 'minDate', selectedDate || '2013-09-10');
            }
                // minDate: parsed_date
            });
      });
function selectAll(cls)
{
    $("input.sec"+cls).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAll(cls)
{
    $("input.sec"+cls).each(function(i){
        $(this).prop('checked', false);
    });   
}
</script>

@endpush

@section('footer_link_and_scripts')
