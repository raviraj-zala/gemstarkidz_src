@extends('auth.Master.master')

@section('title','Event Management')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/event-management.png') }}" alt="branch-img"> Event Management</h2>      
	</div>
</div>

<div class="clearfix"></div>
	<div class="row">

	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	    <div class="col-md-4 col-sm-4 col-xs-12">
		<div class="search-box">
			<input id="btnSearch" name="Eve_Name" placeholder="Search" type="search" />
	      </div>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
	      	<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_event') }}'">
	      		</li>
	      		<li>
			 		<input type="submit" value="Delete" class="delete" id="btnDelete" >
	      		</li>
	      	</ul>
	      </div>
	    </div>
	  </div>
<div class="table-form">   
<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Event</th>
      <th class="td-left">Branch</th>
      <th class="td-left">Class</th>
      <th class="td-left">Section</th>
      <th>Event Date</th>
      <th>Created By</th>
      <th>Approve</th>
      <th>Edit</th>
    </tr>
@if($event)
	<?php $i=1; $eve1 = ""; $eve2 = ""; $eve3 = ""; $eve4 = ""; $eve5 = ""; $eve6 = ""; $eve7 = "";  ?>
	@foreach($event as $data)
    <tr>
      	<td>
      		@if($eve1 != $data->Eve_Unique_Id)
      		<?php $eve1 = $data->Eve_Unique_Id; ?>
      		<input id="event{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Eve_Unique_Id }}" class="css-checkbox check-all"><label for="event{{ $i }}" class="css-label table-ckeckbox"></label>
      		@endif
      	</td>
      	
      	<td class="td-left">
      		@if($eve2 != $data->Eve_Unique_Id)
      			<?php $eve2 = $data->Eve_Unique_Id; ?>
      			{{ $data->Eve_Name }}
      		@endif
  		</td>

  		<td class="td-left">
      		@if($eve7 != $data->Eve_Unique_Id)
      			<?php $eve7 = $data->Eve_Unique_Id; ?>
      			{{ $data->Brn_Name }}
      		@endif
  		</td>
      	
      	<td class="td-left">
      			{{ $data->Cla_Class }}
      	</td>
      	
      	<td class="td-left">
      			{{ $data->Eve_Cla_Section }}
  		</td>
      	
      	<td>
      		@if($eve3 != $data->Eve_Unique_Id)
      			<?php $eve3 = $data->Eve_Unique_Id; ?>
      			{{ date("d-m-Y", strtotime($data->Eve_Date)) }}
      		@endif
		</td>
      	
      	<td>
      		@if($eve4 != $data->Eve_Unique_Id)
      			<?php $eve4 = $data->Eve_Unique_Id; ?>
      			{{ $data->createdByName}}
  			@endif
  		</td>
      	
      	<td>
      		@if($eve5 != $data->Eve_Unique_Id)
      			<?php $eve5 = $data->Eve_Unique_Id; ?>
	      		@if($data->Eve_Approve==1 ) 
	      			<label style="color:  #ff975b;">Approve</label>
				@else
			    	@if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5 )
			    	<label style="color:  #ff975b;"><input type="checkbox" class="eve_sts" value="{{ $data->Eve_Unique_Id }}"></label>
			    	@else(Auth::user()->Use_Type==2 || Auth::user()->Use_Type==3)
			    	<label style="color:  #ff975b;">Pendding...</label>
			    	@endif
				@endif
			@endif
      	</td>
      	
      	<td>
      		@if($eve6 != $data->Eve_Unique_Id)
      			<?php $eve6 = $data->Eve_Unique_Id; ?>
	      		<a href="{{ url('edit_event',$data->Eve_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>
      		@endif
  		</td>

    </tr>
	<?php $i++; ?>
    @endforeach
@else
	<tr><td colspan="8">No Data Found</td></tr>
@endif

  </tbody></table>
</div>
<div class="paggination-section">
@if ($event->lastPage() > 1)
    <ul>
        @if ($event->currentPage() != 1 && $event->lastPage() >= 5)
            <li><a href="{{ $event->url($event->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($event->currentPage() != 1)
            <li>
                <a  href="{{ $event->url($event->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($event->currentPage()-2, 1); $i <= min(max($event->currentPage()-2, 1)+4,$event->lastPage()); $i++)
                <li>
                    <a class="{{ ($event->currentPage() == $i) ? 'active' : '' }}" href="{{ $event->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($event->currentPage() != $event->lastPage())
            <li>
                <a href="{{ $event->url($event->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($event->currentPage() != $event->lastPage() && $event->lastPage() >= 5)
            <li>
                <a href="{{ $event->url($event->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});
$('#btnSearch').on('change',function(){
	var Eve_Name = $(this).val();
	if(Eve_Name!=""){
		$.ajax({
	        type : "POST",
	        url : "{{ url('search_event') }}",
	        data : { 
		            _token:     '{{ csrf_token() }}',
		            Eve_Name : Eve_Name
		        },
	        success : function(data){
	        	$(".paggination-section").html('');
	        	$('table#example2').html(data);
	        },
	        error: function(error){
	        }
	    });
	}else{
		window.location = "{{ url('Event Mgmt.') }}";
	}
});

$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		
			$.ajax({
                type : "POST",
                url : "{{ url('delete_event') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Eve_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
					$(".eve_sts").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});

$(".eve_sts").click(function(){
	var id = $(this).val();
	var ch_sts=confirm('Aprrove Event ???');
		if(ch_sts){
		$.ajax({
			type : "POST",
			url : "{{ url('approve_event') }}",
			data : {
				_token : '{{ csrf_token() }}',
				id : id
			},
			success : function(data){
				$('table#example2').html(data);
				$(".eve_sts").each(function(i){
					$(this).prop('checked', false);
				});
	            window.location.reload();
			}
		});
	}
});


</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')