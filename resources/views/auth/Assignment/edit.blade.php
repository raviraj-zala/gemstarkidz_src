@extends('auth.Master.master')

@section('title','Create Assignment')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
   <h2><img src="{{ asset('public/images/') }}" alt="">Edit Assignment</h2>   
 </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
        <form name="edit-branch" role="form" method="POST" action="{{ url('update_assignment') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
       <div class="left-form">
                 <input type="hidden" name="Hmw_Id" value="{{ $homework->Hmw_Id }}">
                <div class="form-box">
                    <div class="form-text ">
                      <h5>Subject <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                    <input type="text" name="subject" class="{{ $errors->has('subject') ? ' is-invalid' : '' }}" value="{{ $homework->Hmw_Subject }}" maxlength="25" id="subject" >
                    </div>
                    <div id="subjectError" style="display: none;">
                      <label class="error" for="chapter">
                        <span class="text-danger">
                            Please enter subject.
                        </span>
                        </label>
                    </div>
                    @if ($errors->has('subject'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('subject') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-box">
                    <div class="form-text ">
                      <h5>Homework <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                    <!-- <input type="text" name="description" class="{{ $errors->has('description') ? ' is-invalid' : '' }}" value="{{ $homework->Hmw_Chapter }}" maxlength="500">
                     -->
                    <textarea name="description" class="{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" tabindex="4" maxlength="500">{{ $homework->Hmw_Desscription }}</textarea>
                    </div>
                    @if ($errors->has('description'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>

               <div class="form-box">
                    <div class="form-text">
                      <h5>Classwork :</h5>
                    </div>
                    <div class="form-typ-box">
                        <textarea name="class_work" class="{{ $errors->has('class_work') ? ' is-invalid' : '' }}" rows="5" maxlength="500">{{ $homework->Hmw_Class_Work }}</textarea>
                    </div>
                    @if ($errors->has('class_work'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('class_work') }}</strong>
                    </span>
                    @endif
                </div>
         </div>


         <div class="right-form">
                 <!-- <div class="form-box">
                    <div class="form-text">
                      <h5>Chapter <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                        <input type="text" name="chapter" class="{{ $errors->has('chapter') ? 'is-invalid' : '' }}" value="{{ $homework->Hmw_Chapter }}" maxlength="60" id="chapter">
                        <div id="chapterError" style="display: none;">
                            <label class="error" for="chapter">
                              <span class="text-danger">
                                  Please enter chapter.
                              </span>
                              </label>
                        </div>
                    @if($errors->has('chapter'))
                        <label class="error" for="chapter">
                        <span class="text-danger">
                            {{ $errors->first('chapter') }}
                        </span>
                        </label>
                    @endif
                    </div>
                </div> -->

                <div class="form-box">
                    <div class="form-text ">
                      <h5>Date <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                          <input type="text" name="date" class="{{ $errors->has('date') ? 'is-invalid' : '' }}" tabindex="5" value="{{ date('d/m/Y',strtotime($homework->Hmw_Date)) }}" id="datepicker" placeholder="DD/MM/YYYY">
                        @if($errors->has('date'))
                        <label class="error">
                            <span class="text-danger">
                                {{ $errors->first('date') }}
                            </span>
                        </label>
                        @endif
                    </div>
                </div>

              <div class="form-box">
                <div class="form-text">
                    <h5>Image :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%">
                    <input type="file" name="image" class="browser-btn">
                    @if ($errors->has('image'))
                      <label class="error" for="image">
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        </label>
                    @endif
                </div>
            </div>
           <div class="form-text">
                @if($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "png")
                  <img src="{{ url('public/images/homework/'.$homework->Hmw_File) }}" width="50px" height="50px">
                @else
                  @if($homework->Hmw_File!="")
                  {{ $file_ext }} <a href="{{ url('public/images/homework/'.$homework->Hmw_File) }}" style="color: blue;" download>Download</a>
                  @else
                  No File.
                  @endif
                @endif
            </div>

    </div>
             <div class="form-btn branch-form-btn">
                <input type="submit" value="save"></input><a href="{{ URL::previous() }}"><input value="Cancel" type="button"   ></a>
            </div>
    </form>

</div>

@endsection

@push('footer_script')
<script type="text/javascript">
  function checkMobileNo()
  {
    var mobileNo = $("#mobile").val();
    if(mobileNo.length<10){
      if(mobileNo.indexOf(' ')>=0){
        alert("Space Not Allow in mobile number.");
      }else{
        alert("Please enter valid mobile number.");
      }
      $("#mobile").focus();
      return false;
    }else{
      if(isNaN(mobileNo)){
        alert("Allow Only Digits.");
        $("#mobile").focus();
        return false;
      }
    }
  }
  $(function() {
    $( "#datepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        minDate: 0,
    });
    $("#submitBtn").on("click",function(e){
        var home_date = $("#datepicker").val();
        var subject = $("#subject").val();
        var chapter = $("#chapter").val();
        $("#subjectError").css("display","none");
        $("#dateError").css("display","none");
        $("#chapterError").css("display","none");
        if(subject == ""){
          $("#subjectError").css("display","block");
          $("#subject").focus();
          e.preventDefault();
        }else if(chapter == ""){
          $("#chapterError").css("display","block");
          $("#chapter").focus();
          e.preventDefault();
        }
        else if(home_date == ""){
          $("#dateError").css("display","block");
          $("#datepicker").focus();
          e.preventDefault();
        }
    });
  });
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
