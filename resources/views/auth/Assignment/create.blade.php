@extends('auth.Master.master')

@section('title','Create Assignment')

@section('site_header')

@section('sidebar')

@section('content')
 <div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/branch-list.png') }}" alt="">Create Assignment</h2>   
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <form name="create-branch" role="form" method="POST" action="{{ url('save_assignment') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
       <div class="left-form">

        <div class="form-box">
        <div class="form-text">
          <h5>Subject <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
          <input type="text" name="subject" class="{{ $errors->has('subject') ? ' is-invalid' : '' }}" tabindex="2" value="{{ old('subject') }}" maxlength="25" id="subject">
            <div id="subjectError" style="display: none;">
              <label class="error" for="chapter">
                <span class="text-danger">
                    Please enter subject.
                </span>
                </label>
            </div>
            </div>
            @if ($errors->has('subject'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif
        </div>    

        <div class="form-box">
        <div class="form-text">
          <h5>Homework :</h5>
        </div>   
        <div class="form-typ-box">
          <textarea name="description" class="{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" tabindex="4" maxlength="500">{{ old('description') }}</textarea>
        </div>    
              @if ($errors->has('description'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                  </span>
              @endif
       </div>

       <div class="form-box">
        <div class="form-text">
          <h5>Classwork :</h5>
        </div>   
        <div class="form-typ-box">
          <textarea name="class_work" class="{{ $errors->has('class_work') ? ' is-invalid' : '' }}" rows="5" tabindex="4" maxlength="500">{{ old('class_work') }}</textarea>
        </div>    
              @if ($errors->has('class_work'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('class_work') }}</strong>
                  </span>
              @endif
       </div>
</div>

<div class="right-form">

        <!-- <div class="form-box">
        <div class="form-text">
          <h5> Chapter<span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
          <input type="text" name="chapter" id="chapter" class="{{ $errors->has('chapter') ? 'is-invalid' : '' }}" tabindex="5" value="{{ old('chapter') }}" maxlength="60">
          <div id="chapterError" style="display: none;">
              <label class="error" for="chapter">
                <span class="text-danger">
                    Please enter chapter.
                </span>
                </label>
          </div>
          @if($errors->has('chapter'))
            <label class="error" for="chapter">
              <span class="text-danger">
                  {{ $errors->first('chapter') }}
              </span>
              </label>
          @endif
        </div>    
        </div> -->

        <div class="form-box">
        <div class="form-text">
          <h5>Date <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
          <input type="text" name="date" class="{{ $errors->has('date') ? 'is-invalid' : '' }}" tabindex="5" value="{{ old('date') }}" id="datepicker" placeholder="DD/MM/YYYY">
          <div id="dateError" style="display: none;">
            <label class="error" for="chapter">
              <span class="text-danger">
                  Please select date.
              </span>
              </label>
          </div>
        </div>    
        </div>

        <div class="form-box">
                <div class="form-text">
                    <h5>File <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                    <input name="image" class=" browser-btn" type="file" tabindex="10" value="">
                </div>
            </div>

       </div> 
     </div>
<!-- Single Branch and It multiple Class Start-->
    <div class="row">
        <div class="col-lg-12">
        <?php $i=1; $cid=""; $cids=""; ?>   
        @foreach($class as $data)
                <div class="box box-primary">
                @if($data->Brn_Id==$data->Brn_Zon_Id && $data->Brn_Id!=$cid)
                    <?php $cid=$data->Brn_Id; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                        </div>
                    </div>
                @endif
                  <div class="branch-box col-md-3">
                     <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class="css-checkbox {{ 'brn'.$data->Cla_Id }}" value="{{ $data->Cla_Id }}"
                     @if(old('branch'))
                          @foreach(old('branch') as $b)
                              @if($b == $data->Cla_Bra_Id )
                                  checked="true"
                              @endif
                          @endforeach
                     @endif
                     ><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Class }}</label>
                  </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
    </div>
    <!-- Single Branch and It multiple Class End-->

  <div class="form-btn branch-form-btn">
    <input type="submit" value="save" tabindex="8" id="submitBtn"></input>
    <a href="{{ URL::previous() }}"><input value="Cancel" type="button"  tabindex="9" ></a>
  </div>

</form>
</div>
@endsection

@push('footer_script')

<script type="text/javascript">
$(function() {
    $( "#datepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        minDate: 0,
    });
    $(".css-checkbox").on("change",function(){
      var $box = $(this);
      if ($box.is(":checked")) {
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });
    $("#submitBtn").on("click",function(e){
        var home_date = $("#datepicker").val();
        var subject = $("#subject").val();
        var chapter = $("#chapter").val();
        $("#subjectError").css("display","none");
        $("#dateError").css("display","none");
        $("#chapterError").css("display","none");
        if(subject == ""){
          $("#subjectError").css("display","block");
          $("#subject").focus();
          e.preventDefault();
        }else if(chapter == ""){
          $("#chapterError").css("display","block");
          $("#chapter").focus();
          e.preventDefault();
        }
        else if(home_date == ""){
          $("#dateError").css("display","block");
          $("#datepicker").focus();
          e.preventDefault();
        }
    });
});
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
