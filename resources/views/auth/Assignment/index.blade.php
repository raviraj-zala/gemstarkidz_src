@extends('auth.Master.master')

@section('title','Assignment')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/branch-list.png') }}" alt="branch-img"> Diary Management.</h2>      
	</div>
</div>


<div class="clearfix"></div>
	<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <div class="search-box">
				<input id="btnSearch" name="Hmw_Subject" placeholder="Search" type="search" />
	      </div>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
	      	<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_assignment') }}'">
	      		</li>

	      		<li>
	      		<li>
				<input type="submit" value="Delete" class="delete" id="btnDelete">
	      		</li>
	      	</ul>
	      </div>
	    </div>
	  </div>

<div class="table-form">   
<table id="example2"> 
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      <label for="selectall" class="css-label table-ckeckbox">
      <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label></th>
      <th class="td-left">Subject</th>
      <th class="td-left">Date</th>
      <th>HomeWork</th>
      <th>Classwork</th>
      <th>Class</th>
      <th>Created By</th>
      <th>Edit</th>
    </tr> 

	<?php $i=1; ?>
	@forelse($homework as $data)
        <tr>
          	<td><input id="branch{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Hmw_Id }}" class="css-checkbox check-all"><label for="branch{{ $i }}" class="css-label table-ckeckbox"></label></td>
          	<td class="td-left">{{ $data->Hmw_Subject  }}</td>
          	<td class="td-left">{{ date("d-m-Y", strtotime($data->Hmw_Date)) }}</td>
          	<td class="td-left">{{ $data->Hmw_Desscription }}</td>
          	<td class="td-left">{{ $data->Hmw_Class_Work }}</td>
          	<td class="td-left">{{ $data->Cla_Class }}</td>
          	<td class="td-left">{{ $data->Use_Name }}</td>
          	<td><a href="{{ url('edit_assignment',$data->Hmw_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
        </tr>
    <?php $i++; ?>
    @empty
    <tr><td colspan="7" align="center"> No Data Found</td></tr>
    @endforelse
  
  </tbody></table>
  <!-- </form> -->
  </div>


<div class="paggination-section">
@if ($homework->lastPage() > 1)
    <ul>
        @if ($homework->currentPage() != 1 && $homework->lastPage() >= 5)
            <li><a href="{{ $homework->url($homework->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($homework->currentPage() != 1)
            <li>
                <a  href="{{ $homework->url($homework->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($homework->currentPage()-2, 1); $i <= min(max($homework->currentPage()-2, 1)+4,$homework->lastPage()); $i++)
                <li>
                    <a class="{{ ($homework->currentPage() == $i) ? 'active' : '' }}" href="{{ $homework->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($homework->currentPage() != $homework->lastPage())
            <li>
                <a href="{{ $homework->url($homework->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($homework->currentPage() != $homework->lastPage() && $homework->lastPage() >= 5)
            <li>
                <a href="{{ $homework->url($homework->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});
$('#btnSearch').on('change',function(){
	var Hmw_Subject = $(this).val();
	if(Hmw_Subject!="")
	{
		$.ajax({
	        type : "POST",
	        url : "{{ url('search_homework') }}",
	        data : { 
		            _token:     '{{ csrf_token() }}',
		            Hmw_Subject : Hmw_Subject
		        },
	        success : function(data){
	        	$(".paggination-section").html('');
	        	$('table#example2').html(data);
	        },
	        error: function(error){
	        }
	    });
    }else{
    	window.location = "{{ url('Diary Mgmt.') }}";
    }
});

$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		
			$.ajax({
                type : "POST",
                url : "{{ url('delete_assignment') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Hmw_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')