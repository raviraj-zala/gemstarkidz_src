<table id="example2"> 
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      <label for="selectall" class="css-label table-ckeckbox">
      <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label></th>
      <th class="td-left">Subject</th>
      <th class="td-left">Date</th>
      <th>HomeWork</th>
      <th>Classwork</th>
      <th>Class</th>
      <th>Created By</th>
      <th>Edit</th>
    </tr> 

  <?php $i=1; ?>
  @forelse($homework as $data)
        <tr>
            <td><input id="branch{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Hmw_Id }}" class="css-checkbox check-all"><label for="branch{{ $i }}" class="css-label table-ckeckbox"></label></td>
            <td class="td-left">{{ $data->Hmw_Subject  }}</td>
            <td class="td-left">{{ date("d-m-Y", strtotime($data->Hmw_Date)) }}</td>
            <td class="td-left">{{ $data->Hmw_Desscription }}</td>
            <td class="td-left">{{ $data->Hmw_Class_Work }}</td>
            <td class="td-left">{{ $data->Cla_Class }}</td>
            <td class="td-left">{{ $data->Use_Name }}</td>
            <td><a href="{{ url('edit_assignment',$data->Hmw_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
        </tr>
    <?php $i++; ?>
    @empty
    <tr><td colspan="7" align="center"> No Data Found</td></tr>
    @endforelse
  
  </tbody></table>