@extends('auth.Master.master')

@section('title','Edit Parent')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="">Edit Parent</h2>
    </div>
</div>
@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <form name="edit-parent" role="form" method="POST" action="{{ url('update_parent') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="left-form">
        <input type="hidden" name="id" value="{{ $user->Use_Id }}">

    <div class="form-box">
        <div class="form-text ">
            <h5>Father Name <span>*</span> :</h5>
        </div>
        <div class="form-typ-box">
            <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $user->Use_Name }}" required tabindex="1">
            @if ($errors->has('name'))
                <label class="error" for="name">
                <span class="text-danger">
                    {{ $errors->first('name') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
            <h5>Father Email <span>*</span> :</h5>
        </div>
        <div class="form-typ-box"> 
            <input type="text" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" required tabindex="2" value="{{ $user->Use_Email }}" >
            @if ($errors->has('email'))
              <label class="error" for="email"> 
                <span class="text-danger">
                    {{ $errors->first('email') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
          <h5>Father Mobile No <span>*</span> :</h5>
        </div> 
        <div class="form-typ-box">
                <input type="text" class="number {{ $errors->has('mobile') ? 'is-invalid' : '' }}" name="mobile" value="{{ $user->Use_Mobile_No }}" tabindex="3"  maxlength="10" readonly="true">
            @if($errors->has('mobile'))
              <label class="error" for="mobile">
                <span class="text-danger">
                    {{ $errors->first('mobile') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text ">
            <h5>Mother Name :</h5>
        </div>
        <div class="form-typ-box"> 
            <input type="text" name="mother_name" class="{{ $errors->has('mother_name') ? ' is-invalid' : '' }}" tabindex="4" value="{{ $user->Use_Mother_Name }}" >
            @if ($errors->has('mother_name'))
            <label class="error" for="mother_name">
                <span class="text-danger">
                    {{ $errors->first('mother_name') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
            <h5>Mother Email : </h5>
        </div>
        <div class="form-typ-box"> 
            <input type="text" name="mother_email" class="{{ $errors->has('mother_email') ? ' is-invalid' : '' }}" tabindex="5" value="{{ $user->Use_Mother_Email }}" >
            @if ($errors->has('mother_email'))
               <label class="error" for="mother_email">
                <span class="text-danger">
                    {{ $errors->first('mother_email') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
            <h5>Mother Mobile :</h5>
        </div>
        <div class="form-typ-box"> 
            <input type="text" name="mother_mobile" class="number {{ $errors->has('mother_mobile') ? ' is-invalid' : '' }}" tabindex="6" value="{{ $user->Use_Mother_Mobile_No }}" maxlength="10">
            @if ($errors->has('mother_mobile'))
              <label class="class" for="mother_mobile">
                <span class="text-danger">
                    {{ $errors->first('mother_mobile') }}
                </span>
              </label>
            @endif
        </div>
    </div> 

    
</div>

  <div class="right-form">


    <div class="form-box">
        <div class="form-text">
          <h5>Status<span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
        <div class="btn-group bootstrap-select show-tick branch-control">
        <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="7">
            <option value="1" @if($user->Use_Status==1) selected @endif>Active</option>
            <option value="0" @if($user->Use_Status==0) selected @endif>In-Active</option>
        </select>
        </div>
        @if ($errors->has('status'))
            <label class="error" for="status">
            <span class="text-danger">
                {{ $errors->first('status') }}
            </span>
            </label>
        @endif
        </div>    
    </div>
    
    <div class="form-box">
        <div class="form-text ">
          <h5>Address :</h5>
        </div>
        <div class="form-typ-box">
        <textarea name="address" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" tabindex="8">{{ $user->Use_Address }}</textarea>
        @if ($errors->has('address'))
            <label class="error" for="address">
            <span class="text-danger">
                {{ $errors->first('address') }}
            </span>
            </label>
        @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text h">
          <h5>City :</h5>
        </div>   
            <div class="form-typ-box">
              <input type="text" name="city" class="{{ $errors->has('city') ? ' is-invalid' : '' }}" value="{{ $user->Use_City }}" tabindex="9" >
            @if ($errors->has('city'))
                <label class="error" for="city">
                <span class="text-danger">
                    {{ $errors->first('city') }}
                </span>
                </label>
            @endif
            </div>    
    </div>

    <div class="form-box">
        <div class="form-text">
          <h5>State :</h5>
        </div>  
        <div class="form-typ-box">
            <input type="text" name="state" class="{{ $errors->has('state') ? ' is-invalid' : '' }}" value="{{ $user->Use_State }}" tabindex="10" >
            @if ($errors->has('state'))
            <label class="error" for="state">
                <span class="text-danger">
                    {{ $errors->first('state') }}
                </span>
            </label>
            @endif
        </div>
    </div>
       
    <div class="form-box">
        <div class="form-text ">
          <h5>Country :</h5>
        </div>
        <div class="form-typ-box">
            <input type="text" name="country" class="{{ $errors->has('country') ? ' is-invalid' : '' }}" value="{{ $user->Use_Country }}" tabindex="11" >
            @if ($errors->has('country'))
            <label class="error" for="country">
            <span class="text-danger">
                {{ $errors->first('country') }}
            </span>
            </label>
            @endif
        </div>
    </div>

    

    <div class="form-box">
        <div class="form-text">
            <h5>Image</h5>
        </div>
        <div class="form-typ-box zone-box browser-box" style="width: 75%; ">
            
            <input type="file" name="image" tabindex="12" accept="image/*" class="browser-btn">
            @if ($errors->has('image'))
                <label class="error" for="image">
                <span class="text-danger">
                    {{ $errors->first('image') }}
                </span>
                </label>
            @endif
           
        </div>
         
    </div>
    
    <div class="form-text">
    <img src="{{ url('public/images/profile/'.$user->Use_Image) }}" width="50px" height="50px">
    </div>
    
    </div>
    <div class="form-btn branch-form-btn">
        <input type="submit" value="save" tabindex="13"></input><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="14"></a>
    </div>
</div>
</form>
</div>
@endsection

@section('footer')
@push('footer_script')
<script type="text/javascript">

$(document).ready(function(){
    $(".number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});


function checkSpace(txtval){
    if(txtval.value.indexOf(' ')>=0){
        txtval.value="";
        txtval.focus();
        alert("Space Not Allow In "+txtval.name);
    }
}

function checkLength(pass) {
  if (pass.value.length < 6) {
    document.getElementById("password").value="";
    document.getElementById("password").focus();
    alert("Password Length Minimum 6 characters..");
  }else{
    if(pass.value.indexOf(' ')>=0){
        document.getElementById("password").value="";
        alert("Space Not Allow In Password.");
    }
  }
}
function checkMobileNo()
  {
    var mobileNo = $("#mobile").val();
    if(mobileNo.length<10){
      if(mobileNo.indexOf(' ')>=0){
        alert("Space Not Allow in mobile number.");
      }else{
        alert("Please enter valid mobile number.");
      }
      $("#mobile").val("");
      $("#mobile").focus();
      return false;
    }else{
        if(mobileNo.indexOf(' ')>=0){
            alert("Space Not Allow in mobile number.");
            $("#mobile").val("");
            $("#mobile").focus();
        }
        if(isNaN(mobileNo)){
            alert("Allow Only Digits.");
            $("#mobile").val("");
            $("#mobile").focus();
            return false;
        }
    }
  }
</script>
@endpush

@section('footer_link_and_scripts')