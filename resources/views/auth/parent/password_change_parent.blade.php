@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/zone-img.png') }}" alt="zone-img">Change Password</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>


<div class="form-section">
<div class="left-form">
<form name="create-zone" role="form" method="POST" action="{{ url('change_password_parents') }}" enctype="multipart/form-data" autocomplete="off">
        {{ csrf_field() }}
    <div class="branch-form">
                
                    <div class="form-text form-box-width">
                      <h5>Parent <span>*</span> :</h5>
                    </div>   
                    <div class="form-text form-box-width">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="usertype" required tabindex="13">
                        <option value="parent" >{{{ $user->Use_Name }}}</option>
                        <option value="mother">{{{ $user->Use_Mother_Name }}}</option>
                    </select>
                    @if ($errors->has('status'))
                        <label class="error" for="status">
                        <span class="text-danger">
                            {{ $errors->first('status') }}
                        </span>
                        </label>
                    @endif
                    </div>

                <div class="form-text form-box-width">
                    <input type="hidden" name="userid" value="{{ $user->Use_Id }}">
                    <h5>Password <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <input type="password" id="password" name="password" class="branch-control {{ $errors->has('password') ? ' is-invalid' : '' }}" tabindex="1" required maxlength="20">
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
             <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Confirm Password <span>*</span> :</h5>
                </div>
                
                <div class="form-typ-box zone-box">
                <input type="password" id="conform_password" name="conform_password" class="branch-control {{ $errors->has('conform_password') ? ' is-invalid' : '' }}" tabindex="1" required maxlength="20">
                </div>
                @if ($errors->has('conform_password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('conform_password') }}</strong>
                    </span>
                @endif
            </div>

        

        <div class="form-btn branch-form-btn">
            <input type="submit" value="save" tabindex="4" onclick="return parentValidate()"></input><a href="{{ URL::to('/home') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
        </div>
    </div>
</form>
</div>
</div>
<script>
    function parentValidate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("conform_password").value;
        if (password != confirmPassword) {
            document.getElementById("conform_password").focus();
            alert("Comform Passwords do not match.");
            return false;
        }
        else if(password.length < 8){
        document.getElementById("password").value="";
        document.getElementById("conform_password").value="";
        document.getElementById("password").focus();
        alert("Password Length Minimum 8 characters..");
        return false;
        }
        else if(password.indexOf(' ') >=0){
        document.getElementById("password").value="";
        document.getElementById("conform_password").value="";
        document.getElementById("password").focus();
        alert("Space Not Allow In Password.");
         }
        return true;
    }
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')