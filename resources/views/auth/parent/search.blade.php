<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Parent Name</th>
      <th class="td-left">Mother Name</th>
      <th clas="td-left">Student Name</th>
      <th clas="td-left">Student GR No</th>
      <th clas="td-left">Student Class</th>
      @if(Auth::user()->Use_Type == 1)
      <th>Branch Name</th>
      @endif
      @if(Auth::user()->Use_Type == 5)
      <th>Branch Name</th>
      @endif
      <!-- <th class="td-left">Address</th> -->
      <th class="td-left">City</th>
      <th>Mobile</th>
      <th>Status</th>
      @if(Auth::user()->Use_Type == 2)
      <th> Created By </th>
      @endif
      <th>Edit</th>
      @if(Auth::user()->Use_Type == 1)
      <th> Password </th>
      @endif
    </tr>
	<?php $i=1; ?>
	@forelse($parent as $data)
    <tr>
      	<td><input id="user{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Use_Id }}" class="css-checkbox check-all"><label for="user{{ $i }}"" class="css-label table-ckeckbox"></label></td>
      	<td class="td-left">{{ $data->Use_Name }}</td>
        <td class="td-left">{{ $data->Use_Mother_Name }}</td>
        <td clas="td-left">{{ $data->Std_Name }}</td>
        <td clas="td-left">{{ $data->Std_Gr_No }}</td>
        <td clas="td-left">{{ $data->Cla_Class }}</td>
        @if(Auth::user()->Use_Type == 1)
        <td>{{ $data->Brn_Name }}</td>
        @endif
        @if(Auth::user()->Use_Type == 5)
        <td>{{ $data->Brn_Name }}</td>
        @endif
        <!-- <td class="td-left">{{ $data->Use_Address }}</td> -->
        <td class="td-left">{{ $data->Use_City }}</td>
        <td>{{ $data->Use_Mobile_No }}</td>
        @if(Auth::user()->Use_Type == 2)
        <td>{{ $data->createdByName}} </td>
        @endif
        <td>@if($data->Use_Status == 1)
        Active
        @elseif($data->Use_Status == 0)
          In-Active
        @endif</td>
        <td><a href="{{ url('edit_parent',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
        @if(Auth::user()->Use_Type == 1)
        <td><a href="{{ url('password_change_parents',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Password"></a></td>
        @endif
    </tr>
	<?php $i++; ?>   
  @empty
    <tr><td colspan="9">No Record Found</td></tr>
    @endforelse
  </tbody>
</table>