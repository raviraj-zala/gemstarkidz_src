@extends('auth.Master.master')

@section('title','Add Parent')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="">Add Parent</h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
<form name="add-parent" role="form" method="POST" action="{{ url('save_parent') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
<div class="left-form">

            <div class="form-box">
                <div class="form-text ">
                    <h5>Father Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" required tabindex="1" value="{{ old('name') }}" >
                    @if ($errors->has('name'))
                    <label class="error" for="name">
                        <span class="text-danger">
                            {{ $errors->first('name') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Father Email <span>*</span> : </h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="email" id="email" class="{{ $errors->has('Emaill') ? ' is-invalid' : '' }}" required tabindex="2" value="{{ old('email') }}" >
                    <div id="emailError"></div>
                    @if ($errors->has('email'))
                       <label class="error" for="email">
                        <span class="text-danger">
                            {{ $errors->first('email') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Father Mobile <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="mobile" id="mobile" class="number {{ $errors->has('mobile') ? ' is-invalid' : '' }}" tabindex="3" value="{{ old('mobile') }}" maxlength="10">
                    <div id="mobileError"></div>
                    @if ($errors->has('mobile'))
                      <label class="class" for="mobile">
                        <span class="text-danger">
                            {{ $errors->first('mobile') }}
                        </span>
                      </label>
                    @endif
                </div>
            </div> 

            <div class="form-box">
                <div class="form-text">
                    <h5>Father Password <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="password" name="password" id="password" class=" {{ $errors->has('password') ? ' is-invalid' : '' }}" required tabindex="4" >
                    @if ($errors->has('password'))
                        <label class="error" for="password">
                        <span class="text-danger">
                         {{ $errors->first('password') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Mother Name :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="mother_name" class="{{ $errors->has('mother_name') ? ' is-invalid' : '' }}" tabindex="5" value="{{ old('mother_name') }}" >
                    @if ($errors->has('mother_name'))
                    <label class="error" for="mother_name">
                        <span class="text-danger">
                            {{ $errors->first('mother_name') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Mother Email : </h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="mother_email" class="{{ $errors->has('mother_email') ? ' is-invalid' : '' }}" tabindex="6" value="{{ old('mother_email') }}" >
                    @if ($errors->has('mother_email'))
                       <label class="error" for="mother_email">
                        <span class="text-danger">
                            {{ $errors->first('mother_email') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Mother Mobile :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="mother_mobile" id="mother_mobile" class="number {{ $errors->has('mother_mobile') ? ' is-invalid' : '' }}" tabindex="7" value="{{ old('mother_mobile') }}" maxlength="10" >
                    
                    @if ($errors->has('mother_mobile'))
                      <label class="class" for="mother_mobile">
                        <span class="text-danger">
                            {{ $errors->first('mother_mobile') }}
                        </span>
                      </label>
                    @endif
                </div>
            </div> 

            <div class="form-box">
                <div class="form-text">
                    <h5>Mother Password :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="password" name="mother_password" id="mother_password" class=" {{ $errors->has('mother_password') ? ' is-invalid' : '' }}" tabindex="8" >
                    @if ($errors->has('mother_password'))
                        <label class="error" for="mother_password">
                        <span class="text-danger">
                         {{ $errors->first('mother_password') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>
</div>

<div class="right-form">

             <div class="form-box">
                <div class="form-text">
                  <h5>City :</h5>
                </div>   
                    <div class="form-typ-box">
                      <input type="text" name="city" class="{{ $errors->has('city') ? ' is-invalid' : '' }}" tabindex="9" value="{{ old('city') }}">
                    @if ($errors->has('city'))
                        <label class="error" for="city">
                        <span class="text-danger">
                            {{ $errors->first('city') }}
                        </span>
                        </label>
                    @endif
                    </div>    
           </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>State :</h5>
                </div>  
                <div class="form-typ-box">
                    <input type="text" name="state" class=" {{ $errors->has('state') ? ' is-invalid' : '' }}" tabindex="10" value="{{ old('state') }}" >
                    @if ($errors->has('state'))
                        <label class="error" for="state">
                        <span class="text-danger">
                            {{ $errors->first('state') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                  <h5>Country :</h5>
                </div>
                <div class="form-typ-box">
                    <input type="text" name="country" class="{{ $errors->has('country') ? ' is-invalid' : '' }}" tabindex="11" value="{{ old('country') }}">
                    @if ($errors->has('country'))
                        <label class="error" for="country">
                        <span class="text-danger">
                            {{ $errors->first('country') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Address :</h5>
                </div>
                <div class="form-typ-box">
                <textarea name="address" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" tabindex="12" >{{ old('address') }}</textarea>
                @if ($errors->has('address'))
                    <label class="error" for="address">
                    <span class="text-danger">
                        {{ $errors->first('address') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Status <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="13">
                    <option value="1" >Active</option>
                    <option value="0">In-Active</option>
                </select>
                </div>
                @if ($errors->has('status'))
                    <label class="error" for="status">
                    <span class="text-danger">
                        {{ $errors->first('status') }}
                    </span>
                    </label>
                @endif
                </div>    
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Profile :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                    <input name="image" class="browser-btn {{ $errors->has('image') ? ' is-invalid' : '' }}" type="file" accept="image/*" tabindex="14" value="{{ old('image') }}">
                    @if ($errors->has('image'))
                        <label class="error" for="image">
                        <span class="text-danger">
                            {{ $errors->first('image') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            
    </div>
            <div class="form-btn branch-form-btn">
            <input value="save" type="submit" tabindex="15" id="btnSave"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="16" ></a>
            </div>
</form>
</div>

@endsection

@push('footer_script')
<script type="text/javascript">
$(document).ready(function(){
    $("#btnSave").on('click',function(e){
        var femail = $("#email").val();
        var fmobile = document.getElementById('mobile');
        if(femail!=""){
            $.ajax({
                url : "{{ url('parent/checkemail') }}",
                type : "POST",
                data : { 
                    _token : "{{ csrf_token() }}",
                    femail : femail
                },success:function(data){
                    if(data == "Exists")
                    {
                        $("#emailError").html(" <label class='error' for='email'><span class='text-danger'>Mobile number already exists.</sapn></label> ");
                        $("#email").focus();
                        e.preventDefault();    
                    }else{
                        $("#emailError").html("");
                    }
                },error:function(error){
                    
                }
            });
        }else if(fmobile==""){
            $("#mobile").focus();
                $("#mobileError").html("<label class='error' for='mobile'><span class='text-danger'>Mobile number required.</sapn></label>");
                e.preventDefault();
        }
    })
    $(".number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
function checkSpace(txtval){
    if(txtval.value.indexOf(' ')>=0){
        txtval.value="";
        txtval.focus();
        alert("Space Not Allow In "+txtval.name);
    }
}

function checkLength(pass) {
  if (pass.value.length < 6) {
    document.getElementById("password").value="";
    document.getElementById("password").focus();
    alert("Password Length Minimum 6 characters..");
  }else{
    if(pass.value.indexOf(' ')>=0){
        document.getElementById("password").value="";
        alert("Space Not Allow In Password.");
    }
  }
}
function checkMobileNo()
  {
    var mobileNo = $("#mobile").val();
    if(mobileNo.length<10){
      if(mobileNo.indexOf(' ')>=0){
        alert("Space Not Allow in mobile number.");
      }else{
        alert("Please enter valid mobile number.");
      }
      $("#mobile").val("");
      $("#mobile").focus();
      return false;
    }else{
        if(mobileNo.indexOf(' ')>=0){
            alert("Space Not Allow in mobile number.");
            $("#mobile").val("");
            $("#mobile").focus();
        }
        if(isNaN(mobileNo)){
            alert("Allow Only Digits.");
            $("#mobile").val("");
            $("#mobile").focus();
            return false;
        }
    }
  }
</script>
<style type="text/css">
.error {
    color: white;
}
</style>
@endpush

@section('footer')

@section('footer_link_and_scripts')
