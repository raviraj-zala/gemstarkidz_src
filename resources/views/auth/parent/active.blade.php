<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Name</th>
      <th class="td-left">Address</th>
      <th class="td-left">City</th>
      <th class="td-left">State</th>
      <th>Mobile</th>
      <th>Status</th>
      <th>Created By</th>
      <th>Edit</th>
    </tr>


@if($parent)
	<?php $i=1; ?>
	@foreach($parent as $data)
    <tr>
      	<td><input id="parent{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Use_Id }}" class="css-checkbox check-all"><label for="parent{{ $i }}"" class="css-label table-ckeckbox"></label></td>
      	<td class="td-left">{{ $data->Use_Name }}</td>
        <td class="td-left">{{ $data->Use_Address }}</td>
        <td class="td-left">{{ $data->Use_City }}</td>
        <td class="td-left">{{ $data->Use_State }}</td>
        <td>{{ $data->Use_Mobile_No }}</td>
        <td>@if($data->Use_Status == 1)
        Active
      @elseif($data->Use_Status == 0)
        In-Active
      @endif</td>
        <td>{{ $data->Use_CreatedBy }}</td>
        <td><a href="{{ url('edit_parent',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
    </tr>
	<?php $i++; ?>   
    @endforeach
@else
	No Data Found
	@endif
  
  </tbody></table>