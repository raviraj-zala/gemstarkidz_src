@extends('auth.Master.master')

@section('title','Help Management')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt=""> Help Management</h2>      
	</div>
</div>


<div class="clearfix"></div>
	<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	<div class="col-md-4 col-sm-4 col-xs-12">
		</div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
			 		<input type="submit" value="Delete" class="delete" id="btnDelete" >
	      		</li>
	      	</ul>
	      </div>
	    </div>
	  </div>

<div class="table-form">   
<table id="example2"> 
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      <label for="selectall" class="css-label table-ckeckbox">
      <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label></th>
      <th class="td-left">Name</th>
      <th class="td-left">Description</th>
      <th class="td-left">Date</th>
      <th class="td-left">Type</th>
    </tr> 

	<?php $i=1; ?>
	@forelse($help as $data)
        <tr>
          	<td><input id="branch{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Help_Id }}" class="css-checkbox check-all"><label for="branch{{ $i }}" class="css-label table-ckeckbox"></label></td>
          	<td class="td-left">{{ $data->Use_Name }}</td>
          	<td class="td-left">{{ $data->Help_Desctiprion }}</td>
          	<td class="td-left">{{ date("d-m-Y", strtotime($data->Help_CreatedAt)) }}</td>
          	<td>
          		@if($data->Help_Use_Type == 2)
					Teacher
				@elseif($data->Help_Use_Type == 4)
					Parent
				@endif
          	</td>
        </tr>
    <?php $i++; ?>
    @empty
    <tr><td colspan="7" align="center"> No Data Found</td></tr>
    @endforelse
  
  </tbody></table>
  <!-- </form> -->
  </div>


<div class="paggination-section">
@if ($help->lastPage() > 1)
    <ul>
        @if ($help->currentPage() != 1 && $help->lastPage() >= 5)
            <li><a href="{{ $help->url($help->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($help->currentPage() != 1)
            <li>
                <a  href="{{ $help->url($help->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($help->currentPage()-2, 1); $i <= min(max($help->currentPage()-2, 1)+4,$help->lastPage()); $i++)
                <li>
                    <a class="{{ ($help->currentPage() == $i) ? 'active' : '' }}" href="{{ $help->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($help->currentPage() != $help->lastPage())
            <li>
                <a href="{{ $help->url($help->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($help->currentPage() != $help->lastPage() && $help->lastPage() >= 5)
            <li>
                <a href="{{ $help->url($help->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});

$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		
			$.ajax({
                type : "POST",
                url : "{{ url('delete_help') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Help_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')