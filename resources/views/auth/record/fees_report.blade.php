@extends('auth.Master.master')
@section('title','Student Fee Payment List')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="branch-img">Fees Report</h2>
    </div>
</div>
<div class="clearfix"></div>
<br>
@php 
    $tempQuery = Request::getQueryString(); 
@endphp
@if(isset($tempQuery) && isset($fees_report))
<div class="row">
    <div class="col-lg-12">  
        <div class="export-data">
            <button class="btn btn-info search-btn pull-right" onclick="exportTableToExcel('fees_report', 'Fees reports')">Excel</button>
        </div>
    </div>
</div>
@endif
<div class="form-section">
    <form action="{{route('fees_report.search')}}" method="get" enctype="multipart/form-data" autocomplete="off">
        <div class="row">
            <div class="col-lg-2">
               <input class="form-control" type="text" placeholder="From date"  id="from_date" tabindex="1" name="from_date" value="@if(Request::input('from_date')!=''){{ Request::input('from_date') }}@endif" readonly>
            </div>
            <div class="col-lg-2">
               <input  class="form-control" type="text" placeholder="To date"  id="to_date" tabindex="2" name="to_date" value="@if(Request::input('to_date')!=''){{ Request::input('to_date') }}@endif" readonly>
            </div>
            <div class="col-lg-4">
                <div class="form-box">
                    <div class="form-typ-box">
                        <div class="btn-group bootstrap-select show-tick form-control">
                            <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                                <option value="none">-- Select Branch --</option>
                                @foreach($branch as $data)
                                <option value="{{ $data->Brn_Id }}"
                                    @if(old('branch')==$data->Brn_Id)
                                    selected="true"
                                    @endif
                                    @if($data->Brn_Id == Request::input('branch'))
                                        selected="true"
                                    @endif
                                    >
                                    {{ $data->Brn_Name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="col-lg-2" style="margin-left: -70px;">
                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box">
                          <select class="branch-cls" name="class" id="class" tabindex="2">
                            <option value="">Select Class</option>
                          </select>
                          <input type="hidden" id="selected_class" name="selected_class" value="{{Request::input('class')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2" style="margin-left: -60px;">
                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                          <select class="class-sec" name="section" id="section" tabindex="3">
                            <option value="">Section</option>
                          </select>
                          <input type="hidden" id="selected_section" name="selected_section" value="{{Request::input('section')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3" style="margin-left: -60px;">
                <div class="form-box">
                    <div class="form-typ-box ">
                        <div class="btn-group bootstrap-select show-tick form-control">
                            <div class="select-box" >
                                <select class="student-class" name="status" tabindex="4">
                                    <option value="">Status</option>
                                    <option value="complate" @if(Request::input('status')=='complate') selected="selected" @endif>Completed</option>
                                    <option value="remaining" @if(Request::input('status')=='remaining') selected="selected" @endif>Remaining</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <input type="submit" class="btn btn-info search-btn pull-right" name="search" value="Search" tabindex="5">
            </div>
        </div>
    </form>
</div>
<br>
<div class="clearfix"></div>
<br>
@if(isset($tempQuery) && isset($fees_report))
<div class="table-form">   
    <table id="fees_report" border="1" style="width: 100%">
        <thead>
            <tr>
                <th>
                    <input type="checkbox" id="selectall" class="checked css-checkbox" />
                    <label for="selectall" class="css-label table-ckeckbox">
                    <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white">
                </th>
                <th class="td-left">Gr.no</th>
                <th class="td-left">Student Name</th>
                <th class="td-left">Branch</th>
                <th class="td-left">Class</th>
                <th class="td-left">Section</th>
                <th class="td-left">Parent Name</th>
                <th class= "td-left">Amount</th>
                <th class= "td-left">Date</th>
                <th>Created By</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @forelse($fees_report as $data)
            <tr>
                <td>
                    <input id="fees_report{{ $i }}" type="checkbox" name="id[]" value="{{ $data->Pay_Id }}" class="css-checkbox check-all"><label for="fees_report{{ $i }}" class="css-label table-ckeckbox"></label>
                </td>
                <td class="td-left">{{ $data->Std_Gr_No }}</td>
                <td class="td-left">{{ $data->Std_Name }}</td>
                <td class="td-left">{{ $data->Brn_Name }}</td>
                <td class="td-left">{{ $data->Cla_Class }}</td>
                <td class="td-left">{{ $data->Cla_Section }}</td>
                <td class="td-left">{{ $data->parentName }}</td>
                <td class="td-left">{{ $data->Pay_Amount }}</td>
                <td class="td-left">{{ date('d-m-Y',strtotime($data->Pay_Date)) }}</td>
                <td>{{ $data->createdByName }}</td>
                <td>{{ $data->Pay_Status }}</td>
            </tr>
            <?php $i++; ?>   
            @empty
            <tr><td colspan="11" align="center">No Record Found</td></tr>
            @endforelse
        </tbody>
    </table>
</div>
@endif
@php 
    $tempQuery = Request::getQueryString(); 
    $tempQuery = explode("&",$tempQuery);
    $newQueryString = "";
    foreach ($tempQuery as $queryString) {
if(explode("=",$queryString)[0]=="page"){
        } else{
            $newQueryString .= "&".$queryString;
        }
    }
@endphp
@endsection
<style type="text/css">
.col-lg-3 {
    margin-left: -50px;

}
.col-lg-2{
    margin-left: -20px;
}
.col-lg-1{
    margin-left: -20px;   
}
.col-lg-4{
    margin-left: -40px;   
}
.col-lg-10{
    margin-left: 90%;   
}
</style>
@section('footer')
@push('footer_script')
  
  <script type="text/javascript">
    $(document).ready(function(){
    var brnErr,clsErr,secErr;
    var branch,selected_class,selected_student=null;

        $('#to_date').datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,
        });

        $('#from_date').datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,

        });
        $("#branch").on('change',function(){
            branchId = $("#branch").val();
            if(branchId!=""){
                getbranch(branchId);
            }
        });
        branchId = $("#branch").val();
        if(branchId!=""){
            getbranch(branchId);
        }

        $("#class").on('change',function(){
            branchId = $("#branch").val();
            idAndClass = $("#class").val();
            if(idAndClass!=""){
                getclass(idAndClass);
            }
        });
        branchId = $("#branch").val();
        idAndClass = $("#selected_class").val();
        if (idAndClass!="") {
            getclass(idAndClass);    
        }
        
        function getbranch(branchId){
            selected_class = $("#selected_class").val();
            if(branchId!=null){
                $.ajax({
                    type : "POST",
                    url : "{{ url('student/getClass') }}",
                    data : {
                        _token:     '{{ csrf_token() }}',
                        Cla_Bra_Id : branchId
                    },
                    dataType : "JSON",
                    success :function(data){
                            $("#class").empty();
                            $("#section").empty();
                            $("#status").empty();
                            $('#class').append('<option value="">Class</option>');
                            $('#section').append('<option value="">Section</option>');
                            $('#status').append('<option value="">Status</option>');
                            $.each( data, function( key, val ) {
                                if(selected_class==this['Cla_Class']){
                                    $("#class").append('<option value="'+this['Cla_Class']+'"selected="true">'+this['Cla_Class'] +'</option>');

                                } else{
                                    $("#class").append('<option value="'+this['Cla_Class']+'">'+this['Cla_Class'] +'</option>');
                                }
                            });
                        },
                    error : function(error){
                        console.log(error);
                    }
                })
            }
        }
        function getclass(idAndClass){
            selected_section = $("#selected_section").val();
            branch = $("#branch").val();  
            if(idAndClass!=null){
                $.ajax({
                    type : "POST",
                    url : "{{ url('student/getSection') }}",
                    data : {
                        _token:     '{{ csrf_token() }}',
                        Cla_Class : idAndClass,
                        Cla_Bra_Id : branch, 
                    },
                    dataType : "JSON",
                    success : function(data){
                            console.log(data);
                            $('#section').empty();
                            $('#status').empty();
                            $('#section').append('<option value="">Section</option>');
                            $('#status').append('<option value="">Status</option>');
                            $.each( data, function( key, val ) {
                                if(selected_section==this['Cla_Section']){
                                    $("#section").append('<option value="'+this['Cla_Section']+'"selected="true">'+this['Cla_Section'] +'</option>');
                                } else{
                                    $("#section").append('<option value="'+this['Cla_Section']+'">'+this['Cla_Section'] +'</option>');
                                }
                            });  
                    },
                    error :function(error){
                        console.log(error);
                    }
                });
            }
        }

        function getsection(sectionId){
            selected_student = $("#selected_student").val();
            selected_class = $("#selected_class").val();
            selected_section = $("#selected_section").val();
            $.ajax({
                type : "POST",
                url : "{{ url('get_student_list') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    branchId : branchId,
                    className : idAndClass,
                    sectionId : sectionId,
                },
                dataType : "JSON",
                success : function(data){
                    console.log(data);
                    $('#status').empty();
                            $('#student').append('<option value="">Status</option>');
                            $.each( data, function( key, val ) {
                                if(selected_student==this['Std_Id']){
                                    $("#student").append('<option value="'+this['Std_Id']+'"selected="true">'+this['Std_Name'] +'</option>');
                                } else{
                                    $("#student").append('<option value="'+this['Std_Id']+'">'+this['Std_Name'] +'</option>');
                                }
                            });  
                    },
                    
                error :function(error){
                    console.log(error);
                }
            });
        } 
});
</script>
<script type="text/javascript">
function exportTableToExcel(tableID, filename = ''){
    console.log(tableID);
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        var css;
        
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");


        css = (
         '<style>' +
         'table-responsive{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
         'div.table-responsive {page: table-responsive;}' +
         'table{border-collapse:collapse; border:5px}td{border:1px gray solid;width:5em;padding:2px;}'+
         '</style>'
        );
        
        document.body.appendChild(downloadLink);
        
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff',css + tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML + ','+ css;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();

        }
    }
</script>
@endpush
@section('footer_link_and_scripts')
