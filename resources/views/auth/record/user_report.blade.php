@extends('auth.Master.master')
@section('title','Parent Managment')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="branch-img">User Report</h2>
	</div>
</div>
<div class="clearfix"></div>
<br>
@php 
    $tempQuery = Request::getQueryString(); 
@endphp
@if((count($tempQuery)!=0) && (count($user_report)!=0))
    <div class="row">
        <div class="col-lg-12 col-md-offset-11">  
            <div class="export-data">
                <button class="btn btn-info search-btn" onclick="exportTableToExcel('example2', 'User reports')">Excel</button>    
            </div>  
        </div>
    </div>
@endif
<div class="form-section">
    <form action="{{route('user_report.search')}}">
        <div class="col-lg-4">
                <div class="form-box">
                    <div class="form-typ-box">
                        <div class="btn-group bootstrap-select show-tick form-control">
                            <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                                <option value="none">-- Select Branch --</option>
                                        @foreach($branch as $data)
                                            <option value="{{ $data->Brn_Id }}"
                                                @if(old('branch')==$data->Brn_Id)
                                                selected="true"
                                                @endif
                                                @if($data->Brn_Id == Request::input('branch'))
                                                    selected="true"
                                                @endif
                                                >
                                                {{ $data->Brn_Name}}
                                            </option>
                                        @endforeach
                                    </select>
                            @if (Session::has('branch'))
                            <label class="error" for="branch">
                                <span class="text-danger">
                                    {{ Session::get('branch') }}
                                </span>
                            </label>
                            @endif
                        </div>
                        <div id="branchError"></div>
                    </div>    
                </div>
        </div>
        <div class="col-lg-3">
                <div class="form-typ-box ">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                          <select class="branch-cls" title="Class" name="class" id="class" tabindex="2">
                            <option value="">Select Class</option>
                          </select>
                          <input type="hidden" id="selected_class" name="selected_class" value="{{Request::input('class')}}">
                        </div>
                    </div>
                    <div id="classError"></div>
                    @if (Session::has('class'))
                    <label class="error" for="class">
                        <span class="text-danger">
                            {{ Session::get('class') }}
                        </span>
                    </label>
                    @endif
                </div>
        </div>
        <div class="col-lg-2">
            <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                    <div class="select-box" >
                      <select class="class-sec" title="Section" name="section" id="section" tabindex="3">
                        <option value="">Select</option>
                      </select>
                      <input type="hidden" id="selected_section" name="selected_section" value="{{Request::input('section')}}">
                    </div>
                </div>
                <div id="sectionError"></div>
                @if (Session::has('section'))
                <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                </label>
                @endif
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                    <div class="select-box" >
                        <select class="class-sec" title="Section" name="status" id="status" tabindex="3">
                            <option value="" @if(Request::input('status')=='is_active') selected="selected" @endif>Is Active?</option>
                            <option value="1" @if(Request::input('status')=='1') selected="selected" @endif>Active</option>
                            <option value="0" @if(Request::input('status')=='0') selected="selected" @endif>In Active</option>
                        </select>
                    </div>
                </div>
            </div>    
        </div>
        <div class="col-lg-1">
            <input type="submit" class="btn btn-info search-btn" name="search" value="Search" tabindex="5">
        </div>
    </form>
</div>
<div class="clearfix"></div>
<br>
@if((count($tempQuery)!=0) && (count($user_report)!=0))
    <div class="table-form">   
        <table id="example2" border="2px">
            <tbody>
                <tr>
                    <th class="td-left">Parent Name</th>
                    <th clas="td-left">Student Name</th>
                    <th clas="td-left">Student GR No</th>
                    <th clas="td-left">Student Class</th>
                    @if(Auth::user()->Use_Type == 1 || Auth::user()->Use_Type == 5)
                    <th>Branch Name</th>
                    @endif
                    <th class="td-left">City</th>
                    <th>Mobile</th>
                    <th>Status</th>
                    <th>Created By </th>
                </tr>
            	<?php $i=1; ?>
            	@forelse($user_report as $data)
                <tr>
                    <td class="td-left">{{ $data->Use_Name }}</td>
                    <td clas="td-left">{{ $data->Std_Name }}</td>
                    <td clas="td-left">{{ $data->Std_Gr_No }}</td>
                    <td clas="td-left">{{ $data->Cla_Class }}</td>
                    @if(Auth::user()->Use_Type == 1 || Auth::user()->Use_Type == 5)
                    <td>{{ $data->Brn_Name }}</td>
                    @endif
                  	<td class="td-left">{{ $data->Use_City }}</td>
                  	<td>{{ $data->Use_Mobile_No }}</td>
                    <td>
                        @if($data->Use_Status == 1)
                            Active
                        @elseif($data->Use_Status == 0)
                          In-Active
                        @endif
                    </td>
                    <td>{{ $data->createdByName}}</td>
                </tr>
            	<?php $i++; ?>   
            	@empty
            	<tr>
                    <td colspan="10" align="center">No Record Found</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@elseif(count($user_report)==0)
    <div class="table-form">   
        <table id="example2" border="2px">
            <tbody>
                <tr>
                    <th class="td-left">Parent Name</th>
                    <th clas="td-left">Student Name</th>
                    <th clas="td-left">Student GR No</th>
                    <th clas="td-left">Student Class</th>
                    @if(Auth::user()->Use_Type == 1)
                    <th>Branch Name</th>
                    @endif
                    @if(Auth::user()->Use_Type == 5)
                    <th>Branch Name</th>
                    @endif
                    <th class="td-left">City</th>
                    <th>Mobile</th>
                    <th>Status</th>
                    <th>Created By </th>
                </tr>
                <tr>
                    <td colspan="10" align="center">No Record Found</td>
                </tr>
            </tbody>
        </table>
    </div>
@endif
<style type="text/css">
    .col-lg-3 {
        margin-left: -34px;
        margin-right: -27px;
    }
    .col-lg-2{
        margin-left: -15px;

    }
    .col-lg-1{
          
    }
    .col-lg-4{
        margin-left: -40px;   
    }
</style>
<script type="text/javascript">
    $(function(){
        $('#to_date').datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,
        });

        $('#from_date').datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,

        });
        $("#selectall").click(function(){
            if ($("#selectall").is(':checked')) {
                $("input[type=checkbox]").each(function(){
                    $('.check-all').prop('checked', true);
                });
                $("#active").show();
            } else {
                $("input[type=checkbox]").each(function(){
                    $('.check-all').prop('checked', false);
                });
                $("#active").show();
            }
        });

        $("#branch").on('change',function(){
            branchId = $("#branch").val();
            if(branchId!=""){
                getbranch(branchId);
            }
        });
        branchId = $("#branch").val();
        if(branchId!=""){
            getbranch(branchId);
        }

        $("#class").on('change',function(){
            branchId = $("#branch").val();
            idAndClass = $("#class").val();
            if(idAndClass!=""){
                getclass(idAndClass);
            }
        });
        branchId = $("#branch").val();
        idAndClass = $("#selected_class").val();
        if (idAndClass!="") {
            getclass(idAndClass);    
        }
        
        function getbranch(branchId){
            selected_class = $("#selected_class").val();
            if(branchId!=null){
                $.ajax({
                    type : "POST",
                    url : "{{ url('student/getClass') }}",
                    data : {
                        _token:     '{{ csrf_token() }}',
                        Cla_Bra_Id : branchId
                    },
                    dataType : "JSON",
                    
                    success :function(data){
                            $("#class").empty();
                            $("#section").empty();
                            $('#class').append('<option value="">Class</option>');
                            $('#section').append('<option value="">Sec..</option>');
                            $.each( data, function( key, val ) {
                                if(selected_class==this['Cla_Class']){
                                    $("#class").append('<option value="'+this['Cla_Class']+'"selected="true">'+this['Cla_Class'] +'</option>');

                                } else{
                                    $("#class").append('<option value="'+this['Cla_Class']+'">'+this['Cla_Class'] +'</option>');
                                }
                            });
                        },
                    error : function(error){
                        console.log(error);
                    }
                })
            }
        }

        function getclass(idAndClass){
            selected_section = $("#selected_section").val();
            branch = $("#branch").val();  
            if(idAndClass!=null){
                $.ajax({
                    type : "POST",
                    url : "{{ url('student/getSection') }}",
                    data : {
                        _token:     '{{ csrf_token() }}',
                        Cla_Class : idAndClass,
                        Cla_Bra_Id : branch, 
                    },
                    dataType : "JSON",
                    success : function(data){
                            $('#section').empty();
                            $('#section').append('<option value="">Sec..</option>');
                            $.each( data, function( key, val ) {
                                if(selected_section==this['Cla_Section']){
                                    $("#section").append('<option value="'+this['Cla_Section']+'"selected="true">'+this['Cla_Section'] +'</option>');
                                } else{
                                    $("#section").append('<option value="'+this['Cla_Section']+'">'+this['Cla_Section'] +'</option>');
                                }
                            });  
                    },
                    error :function(error){
                        console.log(error);
                    }
                });
            }
        }  
    });
    
    function exportTableToExcel(tableID, filename = ''){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        var css;


        
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");

        css = (
         '<style>' +
         'table-responsive{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
         'div.table-responsive {page: table-responsive;}' +
         'table{border-collapse:collapse; border:5px}td{border:1px gray solid;width:5em;padding:2px;}'+
         '</style>'
        );
        
        document.body.appendChild(downloadLink);
        
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff',css + tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML + ','+ css;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();
        }
    }
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')