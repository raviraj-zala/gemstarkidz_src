@extends('auth.Master.master')
@section('title','Assignment')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/branch-list.png') }}" alt="branch-img"> Diary Management.</h2>      
	</div>
</div>
<div class="clearfix"></div>
<br>
@php 
    $tempQuery = Request::getQueryString(); 
@endphp
@if((count($tempQuery)!=0) && (count($homework)!=0))
<div class="row">
    <div class="col-lg-12 col-md-offset-11">  
        <div class="export-data">
            <button class="btn btn-info search-btn" onclick="exportTableToExcel('example2', 'Assignment reports')">Excel</button>
        </div>
    </div>
</div>
@endif
<div class="form-section">
    <form action="{{route('assignmentreport.search')}}" method="get" enctype="multipart/form-data" autocomplete="off">
        <div class="row">
        	<div class="col-lg-2">
               <input class="form-control" type="text" placeholder="From date"  id="from_date" tabindex="1" name="from_date" value="@if(Request::input('from_date')!=''){{ Request::input('from_date') }}@endif" readonly>
            </div>

            <div class="col-lg-2">
               <input  class="form-control" type="text" placeholder="To date"  id="to_date" tabindex="2" name="to_date" value="@if(Request::input('to_date')!=''){{ Request::input('to_date') }}@endif" readonly>
            </div> 
        	<div class="col-lg-4">
        	    <div class="form-box">
        	        <div class="form-typ-box">
        		        <div class="btn-group bootstrap-select show-tick form-control">
        			        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
        			            <option value="none">-- Select Branch --</option>
        			                    @foreach($branch as $data)
        			                        <option value="{{ $data->Brn_Id }}"
                                                @if(old('branch')==$data->Brn_Id)
                                                selected="true"
                                                @endif
                                                @if($data->Brn_Id == Request::input('branch'))
                                                    selected="true"
                                                @endif
                                                >
                                                {{ $data->Brn_Name}}
                                            </option>
        			                    @endforeach
        			                </select>
        			        @if (Session::has('branch'))
        			        <label class="error" for="branch">
        			            <span class="text-danger">
        			                {{ Session::get('branch') }}
        			            </span>
        			        </label>
        			        @endif
        		        </div>
        	        	<div id="branchError"></div>
        	        </div>    
        	    </div>
        	</div>

            <div class="col-lg-3">
                <div class="form-typ-box ">
        	        <div class="btn-group bootstrap-select show-tick form-control">
        		        <div class="select-box" >
        		          <select class="branch-cls" title="Class" name="class" id="class" tabindex="2">
        		            <option value="">Select Class</option>
        		          </select>
                          <input type="hidden" id="selected_class" name="selected_class" value="{{Request::input('class')}}">
        		        </div>
        	        </div>
        	        <div id="classError"></div>
        	        @if (Session::has('class'))
        	        <label class="error" for="class">
        	            <span class="text-danger">
        	                {{ Session::get('class') }}
        	            </span>
        	        </label>
        	        @endif
                </div>
            </div>

            <div class="col-lg-2">
                <div class="form-typ-box">
        	        <div class="btn-group bootstrap-select show-tick form-control">
        		        <div class="select-box" >
        		          <select class="class-sec" title="Section" name="section" id="section" tabindex="3">
        		            <option value="">Select</option>
        		          </select>
                          <input type="hidden" id="selected_section" name="selected_section" value="{{Request::input('section')}}">
        		        </div>
        	        </div>
        	        <div id="sectionError"></div>
        	        @if (Session::has('section'))
        	        <label class="error" for="section">
        	            <span class="text-danger">
        	                {{ Session::get('section') }}
        	            </span>
        	        </label>
        	        @endif
                </div>
            </div>
            <div class="col-lg-1">
                <input type="submit" class="btn btn-info search-btn" name="search" value="Search" tabindex="5">
            </div>
        </div>
    </form>
</div>
<br>
<div class="clearfix"></div>
<br>
@if((count($tempQuery)!=0) && (count($homework)!=0))
<div class="table-form">   
	<table id="example2" border="1"> 
	    <tbody>
	    	<tr>
				<th class="td-left">Subject</th>
				<th class="td-left">Date</th>
				<th>HomeWork</th>
				<th>Classwork</th>
				<th>Branch</th>
				<th>Class</th>
				<th>Section</th>
			</tr> 
			<?php $i=1; ?>
			@forelse($homework as $data)
	        <tr>
	          	<td class="td-left">{{ $data->Hmw_Subject  }}</td>
	          	<td class="td-left">{{ date("d-m-Y", strtotime($data->Hmw_Date)) }}</td>
	          	<td class="td-left">{{ $data->Hmw_Desscription }}</td>
	          	<td class="td-left">{{ $data->Hmw_Class_Work }}</td>
	          	<td class="td-left">{{ $data->Brn_Name }}</td>
	          	<td class="td-left">{{ $data->Cla_Class }}</td>
	          	<td class="td-left">{{ $data->Cla_Section }}</td>
	        </tr>
		    <?php $i++; ?>
		    @empty
		    
		    @endforelse
		</tbody>
	</table>
  <!-- </form> -->
</div>
@elseif(count($homework)==0)
<div class="table-form">   
	<table id="example2"> 
	    <tbody>
	    	<tr>
				<th class="td-left">Subject</th>
				<th class="td-left">Date</th>
				<th>HomeWork</th>
				<th>Classwork</th>
				<th>Branch</th>
				<th>Class</th>
				<th>Section</th>
			</tr> 
			<tr>
				<td colspan="7" align="center"> No Data Found</td>
			</tr>
		</tbody>
	</table>
</div>
@endif
<style type="text/css">
.col-lg-3 {
    margin-left: -60px;
}
.col-lg-2{
    margin-left: -15px;
}
.col-lg-1{
    margin-left: -30px;   
}
.col-lg-4{
    margin-left: -40px;   
}
</style>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});
$(document).ready(function(){
    var brnErr,clsErr,secErr;
    var branch;

    	$('#to_date').datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,
        });

        $('#from_date').datepicker({
            autoclose: true,  
           	dateFormat: "dd/mm/yy",
            todayHighlight: true,

        });
        $("#branch").on('change',function(){
            branchId = $("#branch").val();
            if(branchId!=""){
                getbranch(branchId);
            }
        });
        branchId = $("#branch").val();
        if(branchId!=""){
            getbranch(branchId);
        }

        $("#class").on('change',function(){
            branchId = $("#branch").val();
            idAndClass = $("#class").val();
            if(idAndClass!=""){
                getclass(idAndClass);
            }
        });
        branchId = $("#branch").val();
        idAndClass = $("#selected_class").val();
        if (idAndClass!="") {
            getclass(idAndClass);    
        }
        
        function getbranch(branchId){
            selected_class = $("#selected_class").val();
            if(branchId!=null){
                $.ajax({
                    type : "POST",
                    url : "{{ url('student/getClass') }}",
                    data : {
                        _token:     '{{ csrf_token() }}',
                        Cla_Bra_Id : branchId
                    },
                    dataType : "JSON",
                    
                    success :function(data){
                            $("#class").empty();
                            $("#section").empty();
                            $('#class').append('<option value="">Class</option>');
                            $('#section').append('<option value="">Sec..</option>');
                            $.each( data, function( key, val ) {
                                if(selected_class==this['Cla_Class']){
                                    $("#class").append('<option value="'+this['Cla_Class']+'"selected="true">'+this['Cla_Class'] +'</option>');

                                } else{
                                    $("#class").append('<option value="'+this['Cla_Class']+'">'+this['Cla_Class'] +'</option>');
                                }
                            });
                        },
                    error : function(error){
                        console.log(error);
                    }
                })
            }
        }


        function getclass(idAndClass){
            selected_section = $("#selected_section").val();
            branch = $("#branch").val();  
            if(idAndClass!=null){
                $.ajax({
                    type : "POST",
                    url : "{{ url('student/getSection') }}",
                    data : {
                        _token:     '{{ csrf_token() }}',
                        Cla_Class : idAndClass,
                        Cla_Bra_Id : branch, 
                    },
                    dataType : "JSON",
                    success : function(data){
                            $('#section').empty();
                            $('#section').append('<option value="">Sec..</option>');
                            $.each( data, function( key, val ) {
                                if(selected_section==this['Cla_Section']){
                                    $("#section").append('<option value="'+this['Cla_Section']+'"selected="true">'+this['Cla_Section'] +'</option>');
                                } else{
                                    $("#section").append('<option value="'+this['Cla_Section']+'">'+this['Cla_Section'] +'</option>');
                                }
                            });  
                    },
                    error :function(error){
                        console.log(error);
                    }
                });
            }
        }      
    });
</script>
<script type="text/javascript">
	 function exportTableToExcel(tableID, filename = ''){
        document.getElementById("example2").style.fontFamily  = "Calibri";
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);
        
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff' + tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML ;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();
        }
        document.getElementById("example2").style.fontFamily  = "Raleway", 'sans-serif';
    }
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')