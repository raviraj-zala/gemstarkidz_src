@extends('auth.Master.master')

@section('title','Branch')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/branch-list.png') }}" alt="branch-img"> Branch List</h2>      
	</div>
</div>


<div class="clearfix"></div>
	<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <div class="search-box">
				<input id="btnSearch" name="Brn_Name" placeholder="Search" type="search" />
	      </div>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
	      	<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_branch') }}'">
	      		</li>

	      		<li>
	            <!-- <form method="POST" id="" action="{{ url('status') }}">
									{{ csrf_field() }} -->
				<input type="button" id="active"  value="Active" class="active btnActInact" >
				</li>
	            <!-- <input class="active" value="active" type="button"> -->
	            <li>
	            <input type="button" id="inactive" value="In-Active" class="in-active btnActInact">
				<!-- </form> -->
				</li>

	      		<li>
			        <!-- <form action="{{ url('delete_branch') }}" method="POST" id="getId">
				{{ csrf_field() }} -->
				<input type="submit" value="Delete" class="delete" id="btnDelete">
	      		</li>
	      	</ul>
	      </div>
	    </div>
	  </div>

<div class="table-form">   
<table id="example2"> 
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      <label for="selectall" class="css-label table-ckeckbox">
      <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label></th>
      <th class="td-left">Zone</th>
      <th class="td-left">Branch Name</th>
      <th class="td-left">Branch Code</th>
      <th class="td-left">Address</th>
      <th>Status</th>
      <th>Edit</th>
    </tr> 

	<?php $i=1; ?>
	@forelse($branch as $data)
        <tr>
          	<td><input id="branch{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Brn_Id }}" class="css-checkbox check-all"><label for="branch{{ $i }}" class="css-label table-ckeckbox"></label></td>
			<td class="td-left">{{ $data->zone->Zon_Name }}</td>
          	<td class="td-left">{{ $data->Brn_Name }}</td>
          	<td class="td-left">{{ $data->Brn_Code }}</td>
          	<td class="td-left">{{ $data->Brn_Address }}</td>
          	<td>
          		@if($data->Brn_Status == 1)
					Active
				@elseif($data->Brn_Status == 0)
					In-Active
				@endif
          	</td>
          	<td><a href="{{ url('edit_branch',$data->Brn_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
        </tr>
    <?php $i++; ?>
    @empty
    <tr><td colspan="7" align="center"> No Data Found</td></tr>
    @endforelse
  
  </tbody></table>
  <!-- </form> -->
  </div>


<div class="paggination-section">
@if ($branch->lastPage() > 1)
    <ul>
        @if ($branch->currentPage() != 1 && $branch->lastPage() >= 5)
            <li><a href="{{ $branch->url($branch->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($branch->currentPage() != 1)
            <li>
                <a  href="{{ $branch->url($branch->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($branch->currentPage()-2, 1); $i <= min(max($branch->currentPage()-2, 1)+4,$branch->lastPage()); $i++)
                <li>
                    <a class="{{ ($branch->currentPage() == $i) ? 'active' : '' }}" href="{{ $branch->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($branch->currentPage() != $branch->lastPage())
            <li>
                <a href="{{ $branch->url($branch->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($branch->currentPage() != $branch->lastPage() && $branch->lastPage() >= 5)
            <li>
                <a href="{{ $branch->url($branch->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});
$('#btnSearch').on('change',function(){
	var Brn_Name = $(this).val();
	if(Brn_Name!="")
	{
		$.ajax({
	        type : "POST",
	        url : "{{ url('search_branch') }}",
	        data : { 
		            _token:     '{{ csrf_token() }}',
		            Brn_Name : Brn_Name
		        },
	        success : function(data){
	        	$('table#example2').html(data);
	        },
	        error: function(error){
	        }
	    });
    }else{
    	window.location = "{{ url('Branch Master') }}";
    }
});

$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		
			$.ajax({
                type : "POST",
                url : "{{ url('delete_branch') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Brn_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});

$("input.btnActInact").on('click',function(){
	var n = $("input:checked").length;
	var val = $(this).val();
	var ids = [];
	var ch_sts=false;
	var status=null;
	if( n >0 )
	{
		$('input:checked').each(function(i){ ids[i] = $(this).val(); });
		if(val == 'Active'){ status = 1; }else if(val == 'In-Active'){ status = 0; }
    	if(status==1) { ch_sts=confirm('Activate selected records???'); }
    	else if(status==0) { ch_sts=confirm('In-activate selected records???'); }
    	if(ch_sts){
		    $.ajax({
			        type: "POST",
			        url: "{{ url('branchStatus') }}",
			        data: { 
			            id: ids, 
			            status: status,
			            _token:     '{{ csrf_token() }}'
			        },
			        success: function(data) {
			        	$("input[type='checkbox']").each(function(i){
							$(this).prop('checked', false);
						});
			        	window.location.reload();
			        },
			        error: function(result) {
			            console.log(result.responseText);
			        }
		    });
    	}
    }
    else
    {
    	alert('Please select atleast one record!');
    }
});    
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')