@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
   <h2><img src="{{ asset('public/images/') }}" alt="">Edit Branch</h2>   
 </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif



<div class="form-section">

        <form name="edit-branch" role="form" method="POST" action="{{ url('update_branch') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        
       <div class="left-form">

         <input type="hidden" name="id" value="{{ $branch->Brn_Id }}">
                <div class="form-box">
                    <div class="form-text">
                      <h5>Zone <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                            <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('zone') ? 'is-invalid' : '' }}" name="zone">
                                @foreach($zone as $d)
                                    <option value="{{ $d->Zon_Id }}" {{ ($branch->Brn_Zon_Id == $d->Zon_Id) ? 'selected' : '' }}>{{ $d->Zon_Name }}</option>
                                @endforeach
                            </select>
                        </div> 
                            @if ($errors->has('status'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif  
                    </div>
                </div>

                <div class="form-box">
                    <div class="form-text ">
                      <h5>Branch Name <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                    <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $branch->Brn_Name }}" required maxlength="25">
                    </div>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-box">
                    <div class="form-text ">
                      <h5>Branch Code <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                    <input type="text" name="code" class="{{ $errors->has('code') ? ' is-invalid' : '' }}" value="{{ $branch->Brn_Code }}" maxlength="20">
                    </div>
                    @if ($errors->has('code'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                    @endif
                </div>

               <div class="form-box">
                    <div class="form-text">
                      <h5>Address :</h5>
                    </div>
                    <div class="form-typ-box">
                        <textarea name="address" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" maxlength="80">{{ $branch->Brn_Address }}</textarea>
                    </div>
                    @if ($errors->has('address'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                    @endif
                </div>
         </div>


         <div class="right-form">
                 <div class="form-box">
                    <div class="form-text">
                      <h5>Email <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                        <input type="text" name="email" class="{{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ $branch->Brn_Email }}" maxlength="60">
                    @if($errors->has('email'))
                        <label class="error" for="email">
                        <span class="text-danger">
                            {{ $errors->first('email') }}
                        </span>
                        </label>
                    @endif
                    </div>
                </div>

                <div class="form-box">
                    <div class="form-text ">
                      <h5>Mobile No <span>*</span> :</h5>
                    </div>
                    <div class="form-typ-box">
                            <input type="text" class=" {{ $errors->has('mobile') ? 'is-invalid' : '' }}" name="mobile" value="{{ $branch->Brn_Mobile_No }}" minlength="10" maxlength="15" id="mobile">
                        @if($errors->has('mobile'))
                        <label class="error">
                            <span class="text-danger">
                                {{ $errors->first('mobile') }}
                            </span>
                        </label>
                        @endif
                    </div>
                </div>

                <div class="form-box">
                    <div class="form-text"><h5>Status</h5></div>
                    <div class="form-typ-box select-text">
                    <div class="btn-group bootstrap-select show-tick form-control">
                    <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status">
                        <option value="1" {{ ($branch->Brn_Status == 1) ? 'selected' : '' }}>Active</option>
                        <option value="0" {{ ($branch->Brn_Status == 0) ? 'selected' : '' }}>In-Active</option>
                    </select>
                    @if ($errors->has('status'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
                </div>
                </div>
             </div>

              <div class="form-box">
                <div class="form-text">
                    <h5>Image :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%">
                    <input type="file" name="image" accept="image/*" class="browser-btn">
                    @if ($errors->has('image'))
                      <label class="error" for="image">
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        </label>
                    @endif
                </div>
            </div>
           <div class="form-text">
                <img src="{{ url('public/images/branch/'.$branch->Brn_Image) }}" width="50px" height="50px">
            </div>

    </div>
             <div class="form-btn branch-form-btn">
                <input type="submit" value="save"></input><a href="{{ URL::previous() }}"><input value="Cancel" type="button"   ></a>
            </div>
    </form>

</div>

@endsection

@push('footer_script')
<script type="text/javascript">
  function checkMobileNo()
  {
    var mobileNo = $("#mobile").val();
    if(mobileNo.length<10){
      if(mobileNo.indexOf(' ')>=0){
        alert("Space Not Allow in mobile number.");
      }else{
        alert("Please enter valid mobile number.");
      }
      $("#mobile").focus();
      return false;
    }else{
      if(isNaN(mobileNo)){
        alert("Allow Only Digits.");
        $("#mobile").focus();
        return false;
      }
    }
  }
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
