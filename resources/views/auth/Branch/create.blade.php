@extends('auth.Master.master')

@section('title','Create Branch')

@section('site_header')

@section('sidebar')

@section('content')
 <div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/branch-list.png') }}" alt="">Create Branch</h2>   
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <form name="create-branch" role="form" method="POST" action="{{ url('save_branch') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
       <div class="left-form">

        <div class="form-box">
        <div class="form-text">
          <h5>Zone<span>*</span> :</h5>
        </div>   
        <div class="form-typ-box select-text">
        <div class="btn-group bootstrap-select show-tick form-control">

          <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('zone') ? 'is-invalid' : '' }}" name="zone" required autofocus tabindex="1" id="zone">
            <option value="none">-- Select --</option>
            @foreach($zone as $data)
                <option value="{{ $data->Zon_Id }}" @if(old('zone')==$data->Zon_Id) selected @endif >{{ $data->Zon_Name }}</option>
            @endforeach
            </select>
        </div>    
            @if (Session::has('zone'))
            <label class="error" for="zone">
                <span class="text-danger">
                     {{ Session::get('zone') }}
                </span>
            </label>
            @endif
       </div>
       </div>

        <div class="form-box">
        <div class="form-text">
          <h5>Branch Name <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
          <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" tabindex="2" required value="{{ old('name') }}" maxlength="25">
            </div>
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>    

        <div class="form-box">
        <div class="form-text">
          <h5>Branch Code <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
          <input type="text" name="code" class="{{ $errors->has('code') ? ' is-invalid' : '' }}" tabindex="3" required value="{{ old('code') }}" maxlength="20">
            </div>
            @if ($errors->has('code'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('code') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-box">
        <div class="form-text">
          <h5>Address :</h5>
        </div>   
        <div class="form-typ-box">
          <textarea name="address" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" tabindex="4" maxlength="80">{{ old('address') }}</textarea>
        </div>    
              @if ($errors->has('address'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
              @endif
       </div>
</div>

<div class="right-form">

        <div class="form-box">
        <div class="form-text">
          <h5>Email Id <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
          <input type="text" name="email" class="{{ $errors->has('email') ? 'is-invalid' : '' }}" tabindex="5" value="{{ old('email') }}" maxlength="60">
          @if($errors->has('email'))
            <label class="error" for="email">
              <span class="text-danger">
                  {{ $errors->first('email') }}
              </span>
              </label>
          @endif
        </div>    
        </div>

        <div class="form-box">
        <div class="form-text">
          <h5>Mobile No. <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
            <input type="text" class="{{ $errors->has('mobile') ? 'is-invalid' : '' }}" name="mobile" tabindex="6" id="mobile" value="{{ old('mobile') }}" maxlength="15" >
            @if($errors->has('mobile'))
              <label class="error" for="mobile">
                <span class="text-danger">
                    {{ $errors->first('mobile') }}
                </span>
              </label>
            @endif
        </div>    
        </div>


        <div class="form-box">
        <div class="form-text">
          <h5>Status<span>*</span> :</h5>
        </div>   
        <div class="form-typ-box select-text">
        <div class="btn-group bootstrap-select show-tick form-control">

          <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="7">
                        <option value="1" @if(old('status')==1) {{ 'selected' }} @endif >Active</option>
                        <option value="0">In-Active</option>
                    </select>
        </div>    
       </div>
                    @if ($errors->has('status'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
       </div>

        <div class="form-box">
                <div class="form-text">
                    <h5>Image <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                    <input name="image" class=" browser-btn{{ $errors->has('image') ? ' is-invalid' : '' }}" type="file" tabindex="10" value="{{ old('image') }}" accept="image/*">
                    @if ($errors->has('image'))
                        <label class="error" for="image">
                        <span class="text-danger">
                            {{ $errors->first('image') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>
       </div> 

  <div class="form-btn branch-form-btn">
    <input type="submit" value="save" tabindex="8"></input>
    <a href="{{ URL::previous() }}"><input value="Cancel" type="button"  tabindex="9" ></a>
  </div>

</form>
</div>
@endsection

@push('footer_script')
<script type="text/javascript">
function checkMobileNo(){

    var mobileNo = $("#mobile").val();
    if(mobileNo==""){
    }else if(mobileNo.length<10){
      if(isNaN(mobileNo)){
        alert("Allow Only Digits.");
        $("#mobile").val('');
        return false;
        $("#mobile").focus();
      }else{
        if(mobileNo.indexOf(' ')>=0){
          alert("Space Not Allow in mobile number.");
        }else{
          alert("Please enter valid mobile number.");
        }
        $("#mobile").val('');
        return false;
        $("#mobile").focus();

      }      
    }else if(mobileNo.length>15){
        alert("Please enter valid mobile number.");
        $("#mobile").val('');
        return false;
        $("#mobile").focus();

    }
    else{
      if(isNaN(mobileNo)){
        alert("Allow Only Digits.");
        $("#mobile").val('');
        return false;
        $("#mobile").focus();

      }
    }
}
</script>
@endpush

@section('footer')

@section('footer_link_and_scripts')
