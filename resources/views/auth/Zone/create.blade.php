@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/zone-img.png') }}" alt="zone-img">Create Zone</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>


<div class="form-section">

<form name="create-zone" role="form" method="POST" action="{{ url('save_zone') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
    <div class="branch-form">
           <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Zone Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <input type="text" name="name" class="branch-control {{ $errors->has('name') ? ' is-invalid' : '' }}" tabindex="1" required maxlength="20">
                </div>
                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        
            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Zone Description :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <textarea name="description" class="branch-control {{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" tabindex="2" maxlength="80"></textarea>
                </div>
                @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>


        <div class="form-box">
                <div class="form-text form-box-width">
                  <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box zone-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="3"  name="status" required>
                    <option value="1">Active</option>
                    <option value="0">In-Active</option>
                </select>
                @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif

                </div>
                </div>    
        </div>

        <div class="form-btn branch-form-btn">
            <input type="submit" value="save" tabindex="4"></input><a href="{{ URL::to('/Zone Master') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
        </div>
    </div>
</form>

</div>
@endsection

@section('footer')

@section('footer_link_and_scripts')