@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/zone-img.png') }}" alt="zonr-managment">Edit Zone</h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>

<div class="form-section">
<form name="edit-zone" role="form" method="POST" action="{{ url('update_zone') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="branch-form">
            <input type="hidden" name="id" value="{{ $zone->Zon_Id }}">
           <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Zone Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <input type="text" class="branch-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $zone->Zon_Name }}" required>
                </div>
                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        
            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Zone Description  :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <textarea name="description" value="{{ $zone->Zon_Description }}"
                 class="branch-control {{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5">{{ $zone->Zon_Description }}</textarea>
                </div>
                @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-box">
                <div class="form-text form-box-width"><h5>Status</h5></div>
                <div class="form-typ-box zone-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status">
                    <option value="1" {{ ($zone->Zon_Status == 1) ? 'selected' : '' }}>Active</option>
                    <option value="0" {{ ($zone->Zon_Status == 0) ? 'selected' : '' }}>In-Active</option>
                </select>
                </div>
                @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>
            </div>

            <div class="form-btn branch-form-btn">
            <input type="submit" value="save"></input><a href="{{ URL::previous() }}"><input value="Cancle" type="button"   ></a>
        </div>
</div>
</form>
</div>

@endsection

@section('footer')

@section('footer_link_and_scripts')
