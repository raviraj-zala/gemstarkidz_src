<div class="table-form">   
<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Zone Name</th>
      <th class="td-left">Zone Description</th>
      <th>Status</th>
      <th>Edit</th>
    </tr>
    

    <?php $i=1; ?>
    @forelse($zone as $data)
      <tr>
          <td>
            <input id="zone{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Zon_Id }}" class="css-checkbox check-all">
        <label for="zone{{ $i }}" class="css-label table-ckeckbox"></label>
            </td>
          <td class="td-left">{{ $data->Zon_Name }}</td>
          <td class="td-left">{{ $data->Zon_Description }}</td>
          <td id="sts{{ $i }}">
            @if($data->Zon_Status == 1)
          Active
        @elseif($data->Zon_Status == 0)
          In-Active
        @endif
          </td>
          <td><a href="{{ url('edit_zone',$data->Zon_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
      </tr>
      <?php $i++; ?>
      @empty
      <tr><td colspan="5" align="center" >No Data Found</td></tr>
      @endforelse
  </tbody>
</table>
</div>