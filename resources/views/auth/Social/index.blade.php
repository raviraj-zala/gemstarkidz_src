@extends('auth.Master.master')

@section('title','Social Activity Management')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/event-management.png') }}" alt="branch-img"> Social Activity</h2>      
    </div>
</div>

<div class="clearfix"></div>
    <div class="row">

    @if(Session::has('success'))
    <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ Session::get('success') }}
    </div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ Session::get('error') }}
    </div>
    @endif
        <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="search-box">
            <input id="btnSearch" name="Soa_Title" placeholder="Search" type="search" />
          </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="main-btn">
            <ul>
                <li>
            <input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_social') }}'">
                </li>
                 
                <li>
                <input type="button" id="active"  value="Active" class="active btnActInact" >
                </li>
                <li>
                <input type="button" id="inactive" value="In-Active" class="in-active btnActInact">
                </li>
                <li>
                    <input type="submit" value="Delete" class="delete" id="btnDelete" >
                </li>
            </ul>
          </div>
        </div>
      </div>
<div class="table-form">   
<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Title</th>
      <th class="td-left">Description</th>
      @if(Auth::user()->Use_Type == 1)
      <th class="td-left">Branch Name</th>
      @endif
      @if(Auth::user()->Use_Type == 5)
      <th class="td-left">Branch Name</th>
      @endif
      <th>Date</th>
      <th>Status</th>
      
      <th>Edit</th>
      <th class="td-left">Tag List</th>
     
    </tr>
  @if($socialActivitys)
    <?php $i=1; $soa1 = ""; $soa2 = ""; $soa3 = ""; $soa4 = ""; $soa5 = ""; $soa6 = ""; $soa7 = ""; $soa8 = ""; ?>
    @foreach($socialActivitys as $data)
    <tr>        
        <td>
            @if($soa1 != $data->Soa_Unique_Id)
            <?php $soa1 = $data->Soa_Unique_Id; ?><input id="activity{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Soa_Unique_Id }}" class="css-checkbox check-all"><label for="activity{{ $i }}" class="css-label table-ckeckbox"></label>
        @endif
        </td>

        <td class="td-left">
            @if($soa2 != $data->Soa_Unique_Id)
                <?php $soa2 = $data->Soa_Unique_Id; ?>
                {{ $data->Soa_Title }}
            @endif
        </td>
        <td class="td-left">
            @if($soa3 != $data->Soa_Unique_Id)
                <?php $soa3 = $data->Soa_Unique_Id; ?>
                {{ $data->Soa_Comment }}
            @endif</td>
        @if(Auth::user()->Use_Type == 1)
        <td class="td-left">{{ $data->Brn_Name }}</td>
        @endif
        @if(Auth::user()->Use_Type == 5)
        <td class="td-left">{{ $data->Brn_Name }}</td>
        @endif
        <td class="td-left">
            @if($soa8 != $data->Soa_Unique_Id)
                <?php $soa8 = $data->Soa_Unique_Id; ?>
                {{ date("d-m-Y", strtotime($data->Soa_CreatedAt)) }}
            @endif
        </td>
        <td>
            @if($soa4 != $data->Soa_Unique_Id)
                <?php $soa4 = $data->Soa_Unique_Id; ?>
                @if($data->Soa_Status==1) 
                    <label style="color:  #ff975b;">Approve</label>
                @else
                    @if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5)
                    <label style="color:  #ff975b;"><input type="checkbox" class="social_sts" value="{{ $data->Soa_Unique_Id }}"></label>
                    @else(Auth::user()->Use_Type==2 || Auth::user()->Use_Type==3)
                    <label style="color:  #ff975b;">Pendding...</label>
                    @endif
                @endif
            @endif
        </td>
         
        <td>@if(Auth::user()->Use_Type == 1)
            @if($soa5 != $data->Soa_Unique_Id)
                <?php $soa5 = $data->Soa_Unique_Id; ?>
            <a href="{{ url('edit_social_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif

            @elseif(Auth::user()->Use_Type == 5 )
            @if($soa5 != $data->Soa_Unique_Id)
                <?php $soa5 = $data->Soa_Unique_Id; ?>
            <a href="{{ url('edit_social_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif

            @elseif(Auth::user()->Use_Type == 2)
            @if($soa5 != $data->Soa_Unique_Id)
            <?php $soa5 = $data->Soa_Unique_Id; ?>
            <a href="{{ url('edit_social_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>
            @endif
            @endif</td>
        <td>
            @if($soa7 != $data->Soa_Unique_Id)
                <?php $soa7 = $data->Soa_Unique_Id; ?>
            <a href="{{ url('tag_list_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/tag.png') }}" alt="tag"></a>@endif
        </td>
    </tr>



        <?php $i++; ?>
    @endforeach
@else
    <tr><td colspan="8">No Data Found</td></tr>
@endif


  </tbody></table>
</div>
<div class="paggination-section">
@if ($socialActivitys->lastPage() > 1)
    <ul>
        @if ($socialActivitys->currentPage() != 1 && $socialActivitys->lastPage() >= 5)
            <li><a href="{{ $socialActivitys->url($socialActivitys->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($socialActivitys->currentPage() != 1)
            <li>
                <a  href="{{ $socialActivitys->url($socialActivitys->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($socialActivitys->currentPage()-2, 1); $i <= min(max($socialActivitys->currentPage()-2, 1)+4,$socialActivitys->lastPage()); $i++)
                <li>
                    <a class="{{ ($socialActivitys->currentPage() == $i) ? 'active' : '' }}" href="{{ $socialActivitys->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($socialActivitys->currentPage() != $socialActivitys->lastPage())
            <li>
                <a href="{{ $socialActivitys->url($socialActivitys->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($socialActivitys->currentPage() != $socialActivitys->lastPage() && $socialActivitys->lastPage() >= 5)
            <li>
                <a href="{{ $socialActivitys->url($socialActivitys->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
    $("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
});
$('#btnSearch').on('change',function(){
    var Soa_Title = $(this).val();
    if(Soa_Title!=""){
        $.ajax({
            type : "POST",
            url : "{{ url('search_social_activity') }}",
            data : { 
                    _token:     '{{ csrf_token() }}',
                    Soa_Title : Soa_Title
                },
            success : function(data){
                $(".paggination-section").html('');
                $('table#example2').html(data);
            },
            error: function(error){
            }
        });
    }else{
        window.location = "{{ url('Social Activity.') }}";
    }
});

$('#btnDelete').on('click',function(){
    var n = $("input:checked").length;
    if (n > 0)
    {
        var ch_delete=confirm('Delete selected records???');
        if(ch_delete){
            var ids = [];
            $('input:checked').each(function(i){
                ids[i] = $(this).val();
            });     
            $.ajax({
                type : "POST",
                url : "{{ url('delete_social') }}",
                data : { 
                        _token:     '{{ csrf_token() }}',
                        Soa_Id : ids
                    },
                success : function(data){
                    $("input[type='checkbox']").each(function(i){
                        $(this).prop('checked', false);
                    });
                    window.location.reload();
                }
            });
        }
    }else{
        alert('Please select atleast one record!');
    }
});
$("input.btnActInact").on('click',function(){
    var n = $("input:checked").length;
    var val = $(this).val();
    var ids = [];
    var ch_sts=false;
    var status=null;
    if( n >0 )
    {
        $('input:checked').each(function(i){ ids[i] = $(this).val(); });
        if(val == 'Active'){ status = 1; }else if(val == 'In-Active'){ status = 0; }
        if(status==1) { ch_sts=confirm('Activate selected records???'); }
        else if(status==0) { ch_sts=confirm('In-activate selected records???'); }

        if(ch_sts)
        {
            $.ajax({
                type: "POST",
                url: "{{ url('socialStatus') }}",
                data: { 
                    id: ids, 
                    status: status,
                    _token:     '{{ csrf_token() }}'
                },
                success: function(data) {
                    $("input[type='checkbox']").each(function(i){
                            $(this).prop('checked', false);
                    });
                    window.location.reload();
                },
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        }   
    }
    else
    {
        alert('Please select atleast one record!');
    }
});
$(".social_sts").click(function(){
    var id = $(this).val();
    var ch_sts=confirm('Aprrove Social Activate ???');
        if(ch_sts){
        $.ajax({
            type : "POST",
            url : "{{ url('approve_social') }}",
            data : {
                _token : '{{ csrf_token() }}',
                id : id
            },
            success : function(data){
                $('table#example2').html(data);
                $(".social_sts").each(function(i){
                    $(this).prop('checked', false);
                });
                window.location.reload();
            }
        });
    }
});

</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')