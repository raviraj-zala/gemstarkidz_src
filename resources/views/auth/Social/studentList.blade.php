    <div class="row">
        <div class="col-lg-12">
                <div class="box box-primary">
                @if(count($teacherStudents)>0)
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                        <h2 class="box-title"><b><input type="hidden" name="teacherStudents[]" value="">Student List</b></h2>
                        </div>
                    </div>
                @endif
                    <div class="branch-box">
                    <?php $i=1; ?>
                    @forelse($teacherStudents as $data)
                        <div class="col-md-3">
                            <input type="checkbox" name="student[]" id="ckbox{{ $i }}" class="css-checkbox std_class_id std" value="{{ $data->Std_Id }}" 
                            @if(isset($checkedStudent))
                                @foreach($checkedStudent as $std)
                                    @if($data->Std_Id == $std->Tag_Stu_Id)
                                        checked
                                    @endif
                                @endforeach
                            @endif
                            ><label for="ckbox{{ $i }}" class="css-label"
                            >{{ $data->Std_Name }}</label>
                        </div>
                    <?php $i++; ?>
                    @empty
                        <div style="color: #ff975b;"> No Record Found. </div>
                    @endforelse
                    </div>
                </div>
            </div>
    </div>