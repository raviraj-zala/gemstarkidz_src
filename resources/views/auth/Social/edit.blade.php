@extends('auth.Master.master')

@section('title','Edit Social Activity')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">
    <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/') }}" alt="">Edit Social Activity</h2>   
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif
<form name="edit-social-activity" role="form" method="POST" action="{{ url('update_activity') }}" enctype="multipart/form-data">

<div class="form-section">
        {{ csrf_field() }}
    <div class="left-form">
        <input type="hidden" name="Soa_Id" id="id" value="{{ $social->Soa_Unique_Id }}">
        <div class="form-box">
            <div class="form-text">
                <h5>Title <span>*</span> :</h5>
            </div>
            <div class="form-typ-box"> 
                <input type="text" name="title" class="branch-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ $social->Soa_Title }}" id="title_id">
                <div id="titleError"></div>
                @if ($errors->has('title'))
                    <label class="error" for="title">
                        <span class="text-danger">
                            {{ $errors->first('title') }}
                        </span>
                    </label>
                @endif
            </div>
        </div>
        <div class="form-box">
            <div class="form-text">
                <h5>Description<span>*</span> : </h5>
            </div>
            <div class="form-typ-box "> 
                <textarea name="description">{{ $social->Soa_Comment }}</textarea>
                @if($errors->has('description'))
                    <label class="error" for="description">
                        <span class="text-danger">
                            {{ $errors->first('description') }}
                        </span>
                    </label>
                @endif
            </div>
        </div>
        @if(Auth::user()->Use_Type == 2)
        <div class="form-box">
            <div class="form-text ">
                <h5>Tag  <span>*</span> :</h5>
            </div>
            <div class="form-typ-box "> 
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="tag" id="tag" >
                    <option value="0" @if($social->Soa_Tag == 0) selected="true" @endif >--Select--</option>
                    <option value="1" @if($social->Soa_Tag == 1) selected="true" @endif >Class</option>
                    <option value="2" @if($social->Soa_Tag == 2) selected="true" @endif >Student</option>
                </select>
                </div>
                <div id="tagError"></div>
                @if($errors->has('tag'))
                    <span class="text-danger">
                        {{ $errors->first('tag') }}
                    </span>
                @endif
            </div>
        </div>
        @endif
        @if(Auth::user()->Use_Type == 1)
        <input type="hidden" value="1" id="tag">
        <div class="form-box" style="display: none">
            <div class="form-text ">
                <h5>Tag  <span>*</span> :</h5>
            </div>
            <div class="form-typ-box "> 
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="tag" id="tag" >
                    <option value="0" @if($social->Soa_Tag == 0) selected="true" @endif >--Select--</option>
                </select>
                </div>
                <div id="tagError"></div>
                @if($errors->has('tag'))
                    <span class="text-danger">
                        {{ $errors->first('tag') }}
                    </span>
                @endif
            </div>
        </div>
        @endif
        @if(Auth::user()->Use_Type == 5)
        <input type="hidden" value="1" id="tag">
        <div class="form-box" style="display: none">
            <div class="form-text ">
                <h5>Tag  <span>*</span> :</h5>
            </div>
            <div class="form-typ-box "> 
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="tag" id="tag" >
                    <option value="0" @if($social->Soa_Tag == 0) selected="true" @endif >--Select--</option>
                </select>
                </div>
                <div id="tagError"></div>
                @if($errors->has('tag'))
                    <span class="text-danger">
                        {{ $errors->first('tag') }}
                    </span>
                @endif
            </div>
        </div>
        @endif    
        <div class="form-box" style="@if(Auth::user()->Use_Type=='2') display: none; @endif">
                <div class="form-text">
                  <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="3"  name="status" required>
                    <option value="1" @if($social->Soa_Status==1) selected="true" @endif>Active</option>
                    <option value="0" @if($social->Soa_Status==0) selected="true" @endif>In-Active</option>
                </select>
                @if ($errors->has('status'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
                </div>
                </div>    
        </div> 
    </div>

    <div class="right-form">
        <div class="form-box">
            <div class="form-text">
                <h5>Image/Video :</h5>
            </div>
            <div class="form-typ-box browser-box"> 
                <input name="imageOrVideo[]" class="browser-btn {{ $errors->has('imageOrVideo') ? ' is-invalid' : '' }}" type="file" accept="image/*,video/*" multiple="true" id="imageOrVideo">
            </div>
            @if ($errors->has('imageOrVideo'))
            <label class="error" for="imageOrVideo">
                <span class="text-danger">
                {{ $errors->first('imageOrVideo') }}
                </span>
            </label>
            @endif
        </div>
        <div class="form-box">
            <div class="form-typ-box browser-box"> 
                <div class="row">
                    @forelse($social_images as $imgVid)
                    <div class="col-lg-6 col-md-6 ">
                        
                        @if($imgVid->Sod_Uploadtype==1)
                        <img src="{{ url('public/images/social/'.$imgVid->Sod_Img_Video) }}" width="100%">
                        <a onclick="return confirm('Are you sure you want to delete?')" type="button" class="btn btn-danger btn-sm btn-block" id=" {{$imgVid->Sod_Id}}" href="{{ url('social/image_video',$imgVid->Sod_Id)}}" name="{{$imgVid->Sod_Id}}">Remove?
                        </a>
                        @endif
                        @if($imgVid->Sod_Uploadtype==2)
                        <video src="{{ url('public/images/social/'.$imgVid->Sod_Img_Video) }}" width="100%" controls></video>
                        <a onclick="return confirm('Are you sure you want to delete?')" type="button" class="btn btn-danger btn-sm btn-block" id=" {{$imgVid->Sod_Id}}" href="{{ url('social/image_video',$imgVid->Sod_Id)}}" name="{{$imgVid->Sod_Id}}">Remove?
                        </a>
                        @endif
                    </div>
                    @empty
                    </div>
                      No Media Found.
                    @endforelse
            </div>
        </div>  
    </div> 
</div>
</div>


<div id="teacherStudents">
</div>

@if(Auth::user()->Use_Type=="2" && $social->Soa_Tag == 2)
    <div class="row" id="default_student_list">
        <div class="col-lg-12">
            <div class="box box-primary">
            @if(count($teacherStudents)>0)
                <div class="line-1 branch-line">        
                     <div class="mangement-btn user-text">
                    <h2 class="box-title"><b><input type="hidden" name="teacherStudents[]" value="">Student List</b></h2>
                    </div>
                </div>
            @endif
                <div class="branch-box">
                <?php $i=1; ?>
                @forelse($teacherStudents as $data)
                    <div class="col-md-3">
                        <input type="checkbox" name="student[]" id="ckbox{{ $i }}" class="css-checkbox std_class_id std" value="{{ $data->Std_Id }}" 
                        @if(isset($checkedStudent))
                            @foreach($checkedStudent as $std)
                                @if($data->Std_Id == $std->Tag_Stu_Id)
                                    checked
                                @endif
                            @endforeach
                        @endif
                        ><label for="ckbox{{ $i }}" class="css-label"
                        >{{ $data->Std_Name }}</label>
                    </div>
                <?php $i++; ?>
                @empty
                    <div style="color: #ff975b;"> No Record Found. </div>
                @endforelse
                </div>
            </div>
        </div>
    </div>
@endif

<div id="select_class_id" style="@if(Auth::user()->Use_Type == 2 && $social->Soa_Tag == 2 || Auth::user()->Use_Type == 1 && $social->Soa_Use_Id != Auth::user()->Use_Id ) display: none; @endif">
    <div class="row">
        <div class="col-lg-12">
        @php $i=0; @endphp
        @foreach($branch as $data)
            <div class="box box-primary">
                <div class="line-1 branch-line">        
                     <div class="mangement-btn user-text">
                      <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                    </div>
                    <div class="user-checkall checkAll {{ 'checkAll_'.$data->Brn_Id }}">
                        <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
                        <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
                    </div>
                </div>
            </div>

            @php
                $classes_details = \App\Model\ClassTbl::where('Cla_Bra_Id',$data->Brn_Id)->get();
            @endphp

            @foreach($classes_details as $detail)
                <div class="branch-box col-md-3">
                   <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class="class_id css-checkbox {{ 'branch'.$detail->Cla_Id }} {{ 'brn'.$detail->Cla_Bra_Id }} " value="{{ $detail->Cla_Id }}"
                        @foreach($sel_class as $right)
                            @if($right->Soa_Cla_Id == $detail->Cla_Id)
                                {{ "checked" }}
                            @endif
                        @endforeach
                        {{-- @if($detail->Cla_Bra_Id==$branchAccess)
                        @else
                            disabled="true"
                        @endif --}}
                   ><label class="css-label" for="chckbox{{ $i }}">{{ $detail->Cla_Class }}</label>
                </div>
                @php $i++; @endphp
            @endforeach            

        @endforeach 

        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="form-btn">
            <input type="submit" value="save" tabindex="8" id="submitBtn">
            <a href="{{ URL::to('/Social Activity.') }}"><input value="Cancel" type="button"  tabindex="9" ></a>
        </div>
    </div>
</div>

</form>

@endsection

@section('footer')

@push('footer_script')
<script type="text/javascript">
$(document).ready(function(){

    $("#submitBtn").on("click",function(e){
        var title = $("#title_id").val();
        var tag = $("#tag").val();
        if(title == ""){
            $("#titleError").html("<label><span class='text-danger'>Please enter title.</span></label>");
            $("#title_id").focus();
            e.preventDefault();
        }else if(tag=="1"){
            var clsRight =$('.class_id:checkbox:checked').length;
            if( clsRight == 0){
                alert("Please select atleast one class.");
                e.preventDefault();
            }
        }else if(tag=="2"){
            var stdRight =$('.std_class_id:checkbox:checked').length;
            if( stdRight == 0){
                alert("Please select atleast one student.");
                e.preventDefault();
            }
        }
    });

    var tagType = $("#tag").val();
    var id = $("#id").val();
    if(tagType==2){
      $.ajax({
          type : "POST",
          url : "{{ url('social/getTeacherStudent') }}",
          data : {
              _token : "{{ csrf_token() }}",
              Soa_Id : id
          },
          success : function(data){
              // $("#teacherStudents").html(data);
          },
          error : function(error){
              console.log(error)
          }
      });
    }else{

    }
    $("#tag").on('change',function(){
        if($(this).val()=="2"){
            $.ajax({
                type : "POST",
                url : "{{ url('social/getTeacherStudent') }}",
                data : {
                    _token : "{{ csrf_token() }}",
                    Soa_Id : id
                },
                success : function(data){
                    $("#teacherStudents").html(data);
                    $("#default_student_list").html("");
                    $("#select_class_id").css('display','none');
                },
                error : function(error){
                    console.log(error)
                }
            });
        }else if($(this).val()=="1"){
            $("#select_class_id").css('display','block');
            $("#teacherStudents").html("");
            $("#default_student_list").html("");
        }else{
            $("#select_class_id").css('display','none');
            $("#teacherStudents").html("");
        }
    });

});
</script>
@endpush

@section('footer_link_and_scripts')
