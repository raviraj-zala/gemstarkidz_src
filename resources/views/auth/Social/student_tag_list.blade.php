@extends('auth.Master.master')

@section('title','Social Activity Management')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/event-management.png') }}" alt="branch-img"> Student Tags List</h2>      
	</div>
</div>

<div class="clearfix"></div>
	<div class="row">

	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	  </div>
	</br>
</br>
<div class="table-form">   
<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Student Gr No</th>
      <th class="td-left">Student Name</th>
      
     
    </tr>
  @if($socialActivitys)
	
	@foreach($socialActivitys as $data)
    <tr>
    	
      	<td>
      		<input id="activity{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Soa_Unique_Id }}" class="css-checkbox check-all"><label for="activity{{ $i }}" class="css-label table-ckeckbox"></label>
      	</td>

      	<td class="td-left">
      		
      			{{ $data->Std_Gr_No }}
      	</td>
      	<td class="td-left">
      		
      			{{ $data->Std_Name }}
      		</td>
      	
         
      	
    </tr>



		<?php $i++; ?>
    @endforeach
@else
	<tr><td colspan="8">No Data Found</td></tr>
@endif


  </tbody></table>
</div>
<div class="paggination-section">
@if ($socialActivitys->lastPage() > 1)
    <ul>
        @if ($socialActivitys->currentPage() != 1 && $socialActivitys->lastPage() >= 5)
            <li><a href="{{ $socialActivitys->url($socialActivitys->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($socialActivitys->currentPage() != 1)
            <li>
                <a  href="{{ $socialActivitys->url($socialActivitys->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($socialActivitys->currentPage()-2, 1); $i <= min(max($socialActivitys->currentPage()-2, 1)+4,$socialActivitys->lastPage()); $i++)
                <li>
                    <a class="{{ ($socialActivitys->currentPage() == $i) ? 'active' : '' }}" href="{{ $socialActivitys->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($socialActivitys->currentPage() != $socialActivitys->lastPage())
            <li>
                <a href="{{ $socialActivitys->url($socialActivitys->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($socialActivitys->currentPage() != $socialActivitys->lastPage() && $socialActivitys->lastPage() >= 5)
            <li>
                <a href="{{ $socialActivitys->url($socialActivitys->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});
$('#btnSearch').on('change',function(){
	var Soa_Title = $(this).val();
	if(Soa_Title!=""){
		$.ajax({
	        type : "POST",
	        url : "{{ url('search_social_activity') }}",
	        data : { 
		            _token:     '{{ csrf_token() }}',
		            Soa_Title : Soa_Title
		        },
	        success : function(data){
	        	$(".paggination-section").html('');
	        	$('table#example2').html(data);
	        },
	        error: function(error){
	        }
	    });
	}else{
		window.location = "{{ url('Social Activity.') }}";
	}
});

$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		
			$.ajax({
                type : "POST",
                url : "{{ url('delete_social') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Soa_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});
$("input.btnActInact").on('click',function(){
	var n = $("input:checked").length;
	var val = $(this).val();
	var ids = [];
	var ch_sts=false;
	var status=null;
	if( n >0 )
	{
		$('input:checked').each(function(i){ ids[i] = $(this).val(); });
		if(val == 'Active'){ status = 1; }else if(val == 'In-Active'){ status = 0; }
    	if(status==1) { ch_sts=confirm('Activate selected records???'); }
    	else if(status==0) { ch_sts=confirm('In-activate selected records???'); }

		if(ch_sts)
		{
			$.ajax({
		        type: "POST",
		        url: "{{ url('socialStatus') }}",
		        data: { 
		            id: ids, 
		            status: status,
		            _token:     '{{ csrf_token() }}'
		        },
		        success: function(data) {
		        	$("input[type='checkbox']").each(function(i){
							$(this).prop('checked', false);
					});
		        	window.location.reload();
		        },
		        error: function(result) {
		            console.log(result.responseText);
		        }
		    });
		}	
    }
    else
    {
    	alert('Please select atleast one record!');
    }
});
$(".social_sts").click(function(){
	var id = $(this).val();
	var ch_sts=confirm('Aprrove Social Activate ???');
		if(ch_sts){
		$.ajax({
			type : "POST",
			url : "{{ url('approve_social') }}",
			data : {
				_token : '{{ csrf_token() }}',
				id : id
			},
			success : function(data){
				$('table#example2').html(data);
				$(".social_sts").each(function(i){
					$(this).prop('checked', false);
				});
	            window.location.reload();
			}
		});
	}
});

</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')