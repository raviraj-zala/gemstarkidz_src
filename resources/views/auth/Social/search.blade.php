<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Title</th>
      <th class="td-left">Description</th>
      @if(Auth::user()->Use_Type == 1)
      <th class="td-left">Branch Name</th>
      @endif
      @if(Auth::user()->Use_Type == 5)
      <th class="td-left">Branch Name</th>
      @endif
      <th>Date</th>
      <th>Status</th>
      
      <th>Edit</th>
       @if(Auth::user()->Use_Type == 1)
      <th class="td-left">Tag List</th>
      @endif
     
    </tr>
  @if($socialActivitys)
  <?php $i=1; $soa1 = ""; $soa2 = ""; $soa3 = ""; $soa4 = ""; $soa5 = ""; $soa6 = ""; $soa7 = ""; $soa8 = ""; ?>
  @foreach($socialActivitys as $data)
    <tr>
      
        <td>
          @if($soa1 != $data->Soa_Unique_Id)
          <?php $soa1 = $data->Soa_Unique_Id; ?><input id="activity{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Soa_Unique_Id }}" class="css-checkbox check-all"><label for="activity{{ $i }}" class="css-label table-ckeckbox"></label>
        @endif
        </td>

        <td class="td-left">
          @if($soa2 != $data->Soa_Unique_Id)
            <?php $soa2 = $data->Soa_Unique_Id; ?>
            {{ $data->Soa_Title }}
          @endif
        </td>
        <td class="td-left">
          @if($soa3 != $data->Soa_Unique_Id)
            <?php $soa3 = $data->Soa_Unique_Id; ?>
            {{ $data->Soa_Comment }}
          @endif</td>
        @if(Auth::user()->Use_Type == 1)
        <td class="td-left">{{ $data->Brn_Name }}</td>
        @endif
        @if(Auth::user()->Use_Type == 5)
        <td class="td-left">{{ $data->Brn_Name }}</td>
        @endif
        <td class="td-left">
          @if($soa8 != $data->Soa_Unique_Id)
            <?php $soa8 = $data->Soa_Unique_Id; ?>
            {{ date("d-m-Y", strtotime($data->Soa_CreatedAt)) }}
          @endif
        </td>
      <td>
          @if($soa4 != $data->Soa_Unique_Id)
            <?php $soa4 = $data->Soa_Unique_Id; ?>
            @if($data->Soa_Status==1) 
              <label style="color:  #ff975b;">Approve</label>
        @else
            @if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5)
            <label style="color:  #ff975b;"><input type="checkbox" class="social_sts" value="{{ $data->Soa_Unique_Id }}"></label>
            @else(Auth::user()->Use_Type==2 || Auth::user()->Use_Type==3)
            <label style="color:  #ff975b;">Pendding...</label>
            @endif
        @endif
      @endif
        </td>
         
        <td>@if(Auth::user()->Use_Type == 1)
          @if($soa5 != $data->Soa_Unique_Id)
            <?php $soa5 = $data->Soa_Unique_Id; ?>
          <a href="{{ url('edit_social_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif

          @elseif(Auth::user()->Use_Type == 5)
          @if($soa5 != $data->Soa_Unique_Id)
            <?php $soa5 = $data->Soa_Unique_Id; ?>
          <a href="{{ url('edit_social_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif

        @elseif(Auth::user()->Use_Type == 2 && Auth::user()->Use_Id == $data->Soa_Use_Id)<a href="{{ url('edit_social_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif</td>
        <td>
          @if(Auth::user()->Use_Type == 1)
          @if($soa7 != $data->Soa_Unique_Id)
            <?php $soa7 = $data->Soa_Unique_Id; ?>
          <a href="{{ url('tag_list_activity',$data->Soa_Unique_Id) }}"><img src="{{ asset('public/images/tag.png') }}" alt="tag"></a>@endif
          @endif
        </td>
    </tr>



    <?php $i++; ?>
    @endforeach
@else
  <tr><td colspan="8">No Data Found</td></tr>
@endif