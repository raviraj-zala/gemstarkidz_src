@extends('auth.Master.master')

@section('title','Create Social Activity')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/') }}" alt="">Create Social Activity</h2>
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<form name="create-social-activity" role="form" method="POST" action="{{ url('save_social') }}" enctype="multipart/form-data" id="create-social">
<div class="form-section">

    {{ csrf_field() }}
 <div class="branch-form">

             <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Title <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <input type="text" name="title" class="branch-control {{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}" id="title_id">
                    <div id="titleError"></div>
                    @if ($errors->has('title'))
                        <label class="error" for="title">
                            <span class="text-danger">
                                {{ $errors->first('title') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Description<span>*</span> : </h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <textarea name="description"></textarea>
                    @if($errors->has('description'))
                        <label class="error" for="description">
                            <span class="text-danger">
                                {{ $errors->first('description') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Image/Video :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box"> 
                    <input name="imageOrVideo[]" class="browser-btn {{ $errors->has('imageOrVideo') ? ' is-invalid' : '' }}" type="file" accept="image/*,video/*" multiple="true">
                </div>
                @if ($errors->has('imageOrVideo'))
                    <label class="error" for="imageOrVideo">
                        <span class="text-danger">
                        {{ $errors->first('imageOrVideo') }}
                        </span>
                    </label>
                    @endif
            </div>

            @if(Auth::user()->Use_Type == 2)
            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Tag  <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="tag" id="tag" required>
                        <option value="0">-- Select --</option>
                        <option value="1">Class</option>
                        <option value="2">Student</option>
                    </select>
                    </div>
                    <div id="tagError"></div>
                    @if($errors->has('tag'))
                        <span class="text-danger">
                            {{ $errors->first('tag') }}
                        </span>
                    @endif
                </div>
            </div>
            @endif
            @if(Auth::user()->Use_Type == 1)
            <input type="hidden" value="1" id="tag">
            <div class="form-box" style="display: none">
                <div class="form-text form-box-width">
                    <h5>Tag  <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="tag" id="tag" required>
                        <option value="0">-- Select --</option>
                    </select>
                    </div>
                    <div id="tagError"></div>
                    @if($errors->has('tag'))
                        <span class="text-danger">
                            {{ $errors->first('tag') }}
                        </span>
                    @endif
                </div>
            </div>
            @endif

            @if(Auth::user()->Use_Type == 5)
            <input type="hidden" value="1" id="tag">
            <div class="form-box" style="display: none">
                <div class="form-text form-box-width">
                    <h5>Tag  <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box"> 
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2-hidden-accessible" name="tag" id="tag" required>
                        <option value="0">-- Select --</option>
                    </select>
                    </div>
                    <div id="tagError"></div>
                    @if($errors->has('tag'))
                        <span class="text-danger">
                            {{ $errors->first('tag') }}
                        </span>
                    @endif
                </div>
            </div>
            @endif

            <div class="form-box" style="@if(Auth::user()->Use_Type=='1' || Auth::user()->Use_Type=='5') display: block; @else display: none; @endif">
                    <div class="form-text form-box-width">
                      <h5>Status<span>*</span> :</h5>
                    </div>   
                    <div class="form-typ-box zone-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="3"  name="status" required>
                        <option value="1">Active</option>
                        <option value="0" selected="true">In-Active</option>
                    </select>
                    @if ($errors->has('status'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif

                    </div>
                    </div>    
            </div>
    </div>
</div>
<!-- Select Multiple Class For Social Activity  -->
<div id="select_class_id">
    <div class="row">
        <div class="col-lg-12">

        @php $i=0; @endphp
        @foreach($branch as $data)
            <div class="box box-primary">
                <div class="line-1 branch-line">        
                     <div class="mangement-btn user-text">
                      <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                    </div>
                    <div class="user-checkall checkAll {{ 'checkAll_'.$data->Brn_Id }}">
                        <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
                        <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
                    </div>
                </div>
            </div>

            @php
                $classes_details = \App\Model\ClassTbl::where('Cla_Bra_Id',$data->Brn_Id)->get();
            @endphp

            @foreach($classes_details as $detail)
                <div class="branch-box col-md-3">
                    <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class="class_id css-checkbox {{ 'branch'.$detail->Cla_Id }} {{ 'brn'.$detail->Cla_Bra_Id }} " value="{{ $detail->Cla_Id }}" @if(old('branch')) @foreach(old('branch') as $b) @if($b == $detail->Cla_Bra_Id ) checked="true" @endif @endforeach @endif >
                    <label class="css-label" for="chckbox{{ $i }}">{{ $detail->Cla_Class }}</label>
                </div>
                @php $i++; @endphp
            @endforeach            
        @endforeach     
        
        
        </div>
    </div>
</div>

<div id="teacherStudents">
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-btn">
            <input value="save" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" ></a>
        </div>
    </div>
</div>
</form>

@endsection

@section('footer')

@push('footer_script')
<script type="text/javascript">
$(document).ready(function(){

    $("#create-social").on('submit',function(e){
        if($("#tag").val()=="none"){
            $("#tagError").html("<label><span class='text-danger'>Please Select Tag.</span></label>");
            e.preventDefault();
        }

    });
    $("#submitBtn").on("click",function(e){
        var title = $("#title_id").val();
        var tag = $("#tag").val();
        if(title == ""){
            $("#titleError").html("<label><span class='text-danger'>Please enter title.</span></label>");
            $("#title_id").focus();
            e.preventDefault();
        }else if(tag=="1"){
            var clsRight =$('.class_id:checkbox:checked').length;
            if( clsRight == 0){
                alert("Please select atleast one class.");
                e.preventDefault();
            }
        }else if(tag=="2"){
            var stdRight =$('.std_class_id:checkbox:checked').length;
            if( stdRight == 0){
                alert("Please select atleast one student.");
                e.preventDefault();
            }
        }
    });

    $("#tag").on('change',function(){
        if($(this).val()=="2"){
            $.ajax({
                type : "POST",
                url : "{{ url('social/getTeacherStudent') }}",
                data : {
                    _token : "{{ csrf_token() }}"
                },
                success : function(data){
                    $("#teacherStudents").html(data);
                    $("#select_class_id").css('display','none');
                },
                error : function(error){
                    console.log(error)
                }
            });
        }else if($(this).val()=="1"){
            $("#select_class_id").css('display','block');
            $("#teacherStudents").html("");
        }else{
            $("#teacherStudents").html("");
        }
    });

});
function selectAll(bool,ids)
{
    $("input.brn"+ids).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAll(bool,ids)
{
    $("input.brn"+ids).each(function(i){
        $(this).prop('checked', false);
    });   
}

function setText(){
    var chkArray = [];
    /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
    $(".css-checkbox:checked").each(function() {
        chkArray.push($(this).val());
    });
    
    /* we join the array separated by the comma */
    var selected;
    selected = chkArray.join(',') ;
    
    /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
    if(selected.length > 0){
        return true;
    }else{
        alert("Please at least check one of the Branch"); 
        return false;
    }
    }

</script>
@endpush


@section('footer_link_and_scripts')
