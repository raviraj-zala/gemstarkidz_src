@extends('auth.Master.master')

@section('title','Create Zone')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/class-management.png') }}" alt="">Edit Class</h2>      
    </div>
</div>


@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif


<div class="form-section">
    <form name="edit-class" role="form" method="POST" action="{{ url('update_class') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $class->Cla_Id }}">

<div class="branch-form">


<div class="form-box">


        <div class="form-text form-box-width">
          <h5>Branch * :</h5>
        </div>   
        <div class="form-typ-box zone-box">

        <div class="btn-group bootstrap-select show-tick branch-control">

        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}"" tabindex="-98" name="branch" required>
                    @foreach($branch as $data)
                         <option value="{{ $data->Brn_Id }}" {{ ($class->Cla_Bra_Id == $data->Brn_Id) ? 'selected' : '' }}>{{ $data->Brn_Name }}</option>
                    @endforeach
                </select>
        @if ($errors->has('branch'))
        <label class="error" for="branch">
            <span class="text-danger">
                {{ $errors->first('branch') }}
            </span>
        </label>
        @endif

        </div>
        </div>    
</div>

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Class* :</h5>
                </div>
                <div class="form-typ-box zone-box">
                    <input type="text" name="class" class="branch-control {{ $errors->has('class') ? ' is-invalid' : '' }}" value="{{ $class->Cla_Class }}" required>
                @if ($errors->has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        <strong>{{ $errors->first('class') }}</strong>
                    </span>
                </label>
                @endif
                </div>
            </div>


            <div class="form-box">
                <div class="form-text form-box-width ">
                    <h5>Section* :</h5>
                </div>
                <div class="form-typ-box zone-box">
                <input type="text" name="section" class="branch-control {{ $errors->has('section') ? ' is-invalid' : '' }}" value="{{ $class->Cla_Section }}" required autofocus>
                @if ($errors->has('section'))
                <label class="error" for="section">
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('section') }}</strong>
                    </span>
                </label>
                @endif
                </div>
            </div>



            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Status:</h5>
                </div>
                <div class="form-typ-box zone-box">
                <div class="btn-group bootstrap-select show-tick branch-control">

                <select class="selectpicker show-tick branch-control form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status">
                                <option value="1" {{ ($class->Cla_Status == 1) ? 'selected' : '' }}>Active</option>
                                <option value="0" {{ ($class->Cla_Status == 0) ? 'selected' : '' }}>In-Active</option>
                            </select>
                            @if ($errors->has('status'))
                            <label class="error" for="status">
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            </label>
                            @endif
                </div>
            </div>
            </div>


        <div class="form-btn branch-form-btn">
            <input value="save" type="submit"><a href="{{ URL::previous() }}"><input value="Cancel" type="button"   ></a>
        </div>

</div>
</form>
</div>

@endsection

@section('footer')

@section('footer_link_and_scripts')
