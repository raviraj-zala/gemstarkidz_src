<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Branch</th>
      <th class="td-left">Class</th>
      <th class="td-left">Section</th>
      <th>Status</th>
      <th>Edit</th>
    </tr>


@if($class)
  <?php $i=1; ?>
  @foreach($class as $data)
    <tr>
        <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Cla_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
        <td class="td-left">{{ $data->Brn_Name }}</td>
        <td class="td-left">{{ $data->Cla_Class }}</td>
        <td class="td-left">{{ $data->Cla_Section }}</td>
        <td>
          @if($data->Cla_Status == 1)
        Active
      @elseif($data->Cla_Status == 0)
        In-Active
      @endif
        </td>
        <td><a href="{{ url('edit_class',$data->Cla_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
    </tr>
  <?php $i++; ?>
    @endforeach
@else
  No Data Found
@endif    

  
  </tbody></table>