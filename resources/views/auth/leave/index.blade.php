@extends('auth.Master.master')
@section('title','Leave Management')
@section('site_header')
@section('sidebar')
@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt=""> Leave Management</h2>      
	</div>
</div>
<div class="clearfix"></div>
<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	<div class="col-md-2 col-sm-2 col-xs-12">
		<div class="form-box">
			<div class="form-text form-box-width">
			  <h5>Status<span>*</span> :</h5>
			</div>   
			<div class="form-typ-box zone-box" style="border: 1px solid #ff975b;">
			<div class="btn-group bootstrap-select show-tick branch-control" >
				<select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('leave_filter') ? 'is-invalid' : '' }}" name="leave_filter" id="leave_filter_id">
				    <option value="teacher" @if(Request::segment(2)=="teacher") selected="true" @endif>Teacher</option>
				    <option value="student" @if(Request::segment(2)=="") selected="true" @endif>Student</option>
				</select>
				@if ($errors->has('leave_filter'))
				    <span class="invalid-feedback">
				        <strong>{{ $errors->first('leave_filter') }}</strong>
				    </span>
				@endif

				</div>
			</div>
    	</div>
	</div>
	<div class="col-md-10 col-sm-10 col-xs-12">
	    <div class="main-btn">
	      	<ul>
	      		@if(Auth::user()->Use_Type=="2")
	      		<li>
	      			<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_teacher_leave') }}'">
	      		</li>
	      		@endif
	      		<li>
					<input type="submit" value="Delete" class="delete" id="btnDelete">
	      		</li>			 		
	      	</ul>
	    </div>
	</div>
</div>

<div class="table-form">

	<table id="example2"> 
	    <tbody>
	    	<tr>
	      		<th><input type="checkbox" id="selectall" class="checked css-checkbox"/>
		      		<label for="selectall" class="css-label table-ckeckbox">
		      		<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label>
		      	</th>
		      	@if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
		      	<th class="td-left">Branch Name</th>
		      	@endif
		      	<th class="td-left">Name</th>
		      	<th class="td-left">Reason</th>
		      	<th class="td-left">Leave Type</th>
		      	<th class="td-left">Leave Date/Time</th>
		      	<th>Status</th>
		      	@if(Auth::user()->Use_Type=="2")
		      	<th>Action</th>
		      	@endif
	    	</tr> 
			<?php $i=1; ?>
			@forelse($leaves as $data)
	        <tr>
	          	<td>
	          		<input id="leave{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Lea_Id }}" class="css-checkbox check-all" @if($data->Lea_Status=="1") disabled="true" readonly="true" @endif>
	          		<label for="leave{{ $i }}" class="css-label table-ckeckbox"></label>
	          	</td>
	          	@if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
	          	<td class="td-left">{{ $data->Brn_Name }}</td>
	          	@endif
	          	<td class="td-left">
	          		@if($data->Lea_User_Id == Auth::user()->Use_Id && Auth::user()->Use_Type=="2")
	          			Me
	          		@else
	          			{{ $data->leave_user_name }}
	          		@endif
	          	</td>
	          	<td class="td-left">{{ $data->Lea_Reason }}</td>
	          	<td class="td-left">{{ $data->Lea_Type }}</td>
	          	<td class="td-left">
	          		@if($data->Lea_Type == 'fullday')
		          		@if($data->Lea_From_Date == $data->Lea_To_Date)
		          			{{ date("d-m-Y", strtotime($data->Lea_From_Date)) }}
		          		@else
		          			{{ date("d-m-Y", strtotime($data->Lea_From_Date))." To ".date("d-m-Y", strtotime($data->Lea_To_Date)) }}
		          		@endif
	          		@else
	          			{{ date("d-m-Y", strtotime($data->Lea_From_Date))." Time : ".$data->Lea_From_Time." To ".$data->Lea_To_Time }}
	          		@endif
	          	</td>
	          	<td>
	          		@if(Auth::user()->Use_Type=="1")
	          			@if($data->Lea_User_Type=="teacher")
		          			@if($data->Lea_Status=="0")
		          				<label style="color:  #ff975b;"><input type="checkbox" class="leave_sts" value="{{ $data->Lea_Id }}"></label>
		          			@elseif($data->Lea_Status=="1")
		          				<label style="color:  #ff975b;">Approve</label>
		          			@endif
	          			@elseif($data->Lea_User_Type=="student")
	          				@if($data->Lea_Status=="0")
		          				<label style="color:  #ff975b;"><input type="checkbox" class="leave_sts" value="{{ $data->Lea_Id }}"></label>
		          			@elseif($data->Lea_Status=="1")
		          				<label style="color:  #ff975b;">Approve</label>
		          			@endif
	          			@endif
	          		@elseif(Auth::user()->Use_Type=="2")
		          		@if($data->Lea_User_Type=="teacher")
		          			@if($data->Lea_Status=="0")
		          				Pendding
		          			@elseif($data->Lea_Status=="1")
		          				Approve
		          			@endif
	          			@elseif($data->Lea_User_Type=="student")
	          				@if($data->Lea_Status=="0")
		          				<label style="color:  #ff975b;"><input type="checkbox" class="leave_sts" value="{{ $data->Lea_Id }}"></label>
		          			@elseif($data->Lea_Status=="1")
		          				<label style="color:  #ff975b;">Approve</label>
		          			@endif
	          			@endif
	          		@elseif(Auth::user()->Use_Type=="5")
	          			@if($data->Lea_User_Type=="teacher")
		          			@if($data->Lea_Status=="0")
		          				<label style="color:  #ff975b;"><input type="checkbox" class="leave_sts" value="{{ $data->Lea_Id }}"></label>
		          			@elseif($data->Lea_Status=="1")
		          				<label style="color:  #ff975b;">Approve</label>
		          			@endif
	          			@elseif($data->Lea_User_Type=="student")
	          				@if($data->Lea_Status=="0")
		          				<label style="color:  #ff975b;"><input type="checkbox" class="leave_sts" value="{{ $data->Lea_Id }}"></label>
		          			@elseif($data->Lea_Status=="1")
		          				<label style="color:  #ff975b;">Approve</label>
		          			@endif
	          			@endif
	          		@endif
	          	</td>
	          	@if(Auth::user()->Use_Type=="2")
	          	<td>
	          		@if(Auth::user()->Use_Type=="1")
	          		
	          		@elseif(Auth::user()->Use_Type=="2")
	          			@if($data->Lea_Status=="0" && $data->Lea_User_Id==Auth::user()->Use_Id)
	          				<a href="{{ url('edit_teacher_leave',$data->Lea_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Edit"></a>
	          			@endif
	          		@elseif(Auth::user()->Use_Type=="5")
	          			
	          		@endif
	          	</td>
	          	@endif
	        </tr>
		    <?php $i++; ?>
		    @empty
		    	<tr><td colspan="10" align="center"> No Leave Request Found</td></tr>
		    @endforelse
	  	</tbody>
	</table>
</div>

<div class="paggination-section">
@if ($leaves->lastPage() > 1)
    <ul>
        @if ($leaves->currentPage() != 1 && $leaves->lastPage() >= 5)
            <li><a href="{{ $leaves->url($leaves->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($leaves->currentPage() != 1)
            <li>
                <a  href="{{ $leaves->url($leaves->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($leaves->currentPage()-2, 1); $i <= min(max($leaves->currentPage()-2, 1)+4,$leaves->lastPage()); $i++)
                <li>
                    <a class="{{ ($leaves->currentPage() == $i) ? 'active' : '' }}" href="{{ $leaves->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($leaves->currentPage() != $leaves->lastPage())
            <li>
                <a href="{{ $leaves->url($leaves->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($leaves->currentPage() != $leaves->lastPage() && $leaves->lastPage() >= 5)
            <li>
                <a href="{{ $leaves->url($leaves->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>

<script type="text/javascript">
$(document).ready(function(e){
	$("#leave_filter_id").on('change',function(){
		if($("#leave_filter_id").val()=="teacher"){
			window.location.href = "/Leave Mgmt./"+$("#leave_filter_id").val();
		}else{
			window.location.href = "/Leave Mgmt.";
		}
	});
	$(".leave_sts").on('change',function(){
		if ($(this).is(':checked')) {
            var confirm_leave = confirm("Are you sure?");
            if(confirm_leave == true){
            	window.location.href = "/confirm_leave/"+$(this).val();
            }
        }
	});
	$('#btnDelete').on('click',function(){
	    var n = $("input:checked").length;
	    if (n > 0)
	    {
	        var ch_delete=confirm('Delete selected records???');
	        if(ch_delete==true){
		    	var ids = [];
		    	$('input:checked').each(function(i){
	                ids[i] = $(this).val();
	            });
	            $.ajax({
	            	type : "POST",
	            	url : "{{ url('leave_request_delete') }}",
	            	data : { 
                        _token:     '{{ csrf_token() }}',
                        ids : ids
                    },success : function(data){
                    	$("input[type='checkbox']").each(function(i){
	                        $(this).prop('checked', false);
	                    });
	                    window.location.reload();
                    }
	            });
	        }
	    }else{
	        alert('Please select atleast one record!');
	    }
	});
	$("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
})
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')