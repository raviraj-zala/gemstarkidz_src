@extends('auth.Master.master')

@section('title','Leave Create')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt="">Create Leave</h2>
	</div>
</div>
<div class="clearfix"></div>

<div class="form-section">
	<div class="container">
			<form name="create-leave" role="form" method="POST" action="{{ url('save_teacher_leave') }}" enctype="multipart/form-data">
	    		{{ csrf_field() }}
	    		<input type="hidden" name="leave_type" value="fullday" id="leave_type_id">
				<div class="row">
					<div class="col-lg-12">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#fullday" onclick="leave_type('fullday')">Full Day</a></li>
							<li><a data-toggle="tab" href="#halfday" onclick="leave_type('halfday')">Half Day</a></li>
						</ul>
						<div class="branch-form">
							<div class="tab-content">
								<div class="form-box">
									<div class="form-text form-box-width">
										<h5>Reason<span>*</span> :</h5>
									</div>
									<div class="form-typ-box zone-box"> 
										<textarea class="branch-control" name="reason" id="reason" rows="5">{{ old('reason') }}</textarea>
										<div id="reasonError"></div>
										@if ($errors->has('reason'))
											<span class="text-danger">
												{{ $errors->first('reason') }}
											</span>
										@endif
									</div>
								</div>

								<div class="form-box">
									<div class="form-text form-box-width">
									    <h5>From Date <span>*</span> :</h5>
									</div>
									<div class="form-typ-box zone-box"> 
										<input type="text" name="from_date" id="from_datepicker" value="{{ old('from_date') }}" placeholder="dd/mm/yyyy" readonly="">
										<div id="from_dateError"></div>
									    @if ($errors->has('name'))
									    <label class="error" for="from_date">
									        <span class="text-danger">
									            {{ $errors->first('from_date') }}
									        </span>
									        </label>
									    @endif
									</div>
								</div>

								<!-- FULL DAY -->
								<div id="fullday" class="tab-pane fade in active">
									<div class="form-box">
										<div class="form-text form-box-width">
										    <h5>To Date :</h5>
										</div>
										<div class="form-typ-box zone-box"> 
											<input type="text" name="to_date" id="to_datepicker" value="{{ old('to_date') }}" placeholder="dd/mm/yyyy" readonly="">
											<div id="to_dateError"></div>
										    @if ($errors->has('name'))
										    <label class="error" for="name">
										        <span class="text-danger">
										            {{ $errors->first('to_date') }}
										        </span>
										        </label>
										    @endif
										</div>
									</div>
								</div>

								<!-- HALF DAY  -->
								<div id="halfday" class="tab-pane fade">
									<div class="form-box">
										<div class="form-text form-box-width">
										    <h5>From Time <span>*</span> :</h5>
										</div>
										<div class="form-typ-box zone-box"> 
											<div class="clockpicker">
											    <input type="text" class="form-control" name="from_time" id="from_time" placeholder="hh:mm" readonly="">
											</div>
											<div id="from_timeError"></div>
										    @if ($errors->has('from_time'))
										    <label class="error" for="from_time">
										        <span class="text-danger">
										            {{ $errors->first('from_time') }}
										        </span>
										        </label>
										    @endif
										</div>
									</div>
									
									<div class="form-box">
										<div class="form-text form-box-width">
										    <h5>To Time <span>*</span> :</h5>
										</div>
										<div class="form-typ-box zone-box"> 
											<div class="clockpicker">
											    <input type="text" class="form-control" name="to_time" id="to_time" placeholder="hh:mm" readonly="">
											</div>
											<div id="to_timeError"></div>
										    @if ($errors->has('to_time'))
										    <label class="error" for="to_time">
										        <span class="text-danger">
										            {{ $errors->first('to_time') }}
										        </span>
										        </label>
										    @endif
										</div>
									</div>
								</div>

								<div class="form-btn branch-form-btn">
									<input value="save" type="submit" tabindex="" id="btnSave">
									<a href="{{ URL::previous() }}">
										<input value="Cancel" type="button" tabindex="" >
									</a>
						        </div>
							</div>
						</div>
					</div>
				</div>
			</form>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{{ url('public/css/clockpicker.css') }}">

<script src="{{ url('public/js/clockpicker.js') }}" ></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#from_datepicker").datepicker({
	    dateFormat: "dd/mm/yy",
	    minDate: 0,
	    onSelect: function(selectedDate) {
	        $('#to_datepicker').datepicker('option', 'minDate', selectedDate);
	    }
	});
	$("#to_datepicker").datepicker({
	    dateFormat: "dd/mm/yy",
	    minDate: 0
	});
	$('.clockpicker').clockpicker({
	    placement: 'top',
	    align: 'left',
	    donetext: 'Done'
	});
	// Form Submit
	$("#btnSave").click(function(e){
		var lea_type = $("#leave_type_id").val();
		if(lea_type == "fullday"){
			var reason = $("#reason").val();
			var from_date = $("#from_datepicker").val();
			var to_date = $("#to_datepicker").val();
			$("#reasonError").html('');
			$("#from_dateError").html('');
			$("#to_dateError").html('');
			if(reason==""){
				$("#reasonError").html("<label for='reason' class='error'><span class='text-danger'>Please enter reason.</span></label>");
				$("#reason").focus();
				e.preventDefault();
			}else if(from_date == ""){
				$("#from_dateError").html("<label for='from_datepicker' class='error'><span class='text-danger'>Please enter from date.</span></label>");
				$("#from_datepicker").focus();
				e.preventDefault();
			}
			// else if(to_date == ""){
			// 	$("#to_dateError").html("<label for='from_datepicker' class='error'><span class='text-danger'>Please enter from date.</span></label>");
			// 	$("#to_datepicker").focus();
			// 	e.preventDefault();
			// }
		}else if(lea_type == "halfday"){
			var reason = $("#reason").val();
			var from_date = $("#from_datepicker").val();
			var from_time = $("#from_time").val();
			var to_time = $("#to_time").val();
			$("#reasonError").html('');
			$("#from_dateError").html('');
			$("#from_timeError").html('');
			$("#to_timeError").html('');
			if(reason==""){
				$("#reasonError").html("<label for='reason' class='error'><span class='text-danger'>Please enter reason.</span></label>");
				$("#reason").focus();
				e.preventDefault();
			}else if(from_date == ""){
				$("#from_dateError").html("<label for='from_datepicker' class='error'><span class='text-danger'>Please enter from date.</span></label>");
				$("#from_datepicker").focus();
				e.preventDefault();
			}else if(from_time == ""){
				$("#from_timeError").html("<label for='from_time' class='error'><span class='text-danger'>Please enter from time.</span></label>");
				$("#from_time").focus();
				e.preventDefault();
			}else if(to_time == ""){
				$("#to_timeError").html("<label for='to_time' class='error'><span class='text-danger'>Please enter to time.</span></label>");
				$("#to_time").focus();
				e.preventDefault();
			}
		}
		// e.preventDefault();
	})
});
function leave_type(type){
	$("#leave_type_id").val(type);
}
</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')