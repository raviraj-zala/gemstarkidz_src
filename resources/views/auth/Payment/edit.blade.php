@extends('auth.Master.master')

@section('title','Edit Student Payment')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/event-management.png') }}" alt="">Edit Student Payment</h2>
     </div>
</div>

<div class="form-section">
<form role="form" method="POST" action="{{ url('update_payment') }}" enctype="multipart/form-data" id="studentForm">
    {{ csrf_field() }}

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<form name="add-payment" role="form" method="POST" action="{{ url('update_payment') }}" enctype="multipart/form-data" id="studentForm">
            
        {{ csrf_field() }}

        <div class="left-form">
            <input type="hidden" name="id" value="{{$student->Pay_Id}}">

            <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">

                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch" readonly="true">
                    <option value="none">-- Select --</option>
                            @foreach($branch as $data)
                                <option value="{{ $data->Brn_Id }}" @if($student->Cla_Bra_Id==$data->Brn_Id) selected @endif>{{ $data->Brn_Name }}</option>
                            @endforeach
                        </select>
                    @if (Session::has('branch'))
                      <label class="error" for="branch">
                        <span class="text-danger">
                            {{ Session::get('branch') }}
                        </span>
                        </label>
                    @endif
                </div>
                </div>    
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                            <select readonly="true" class="branch-cls" name="class" id="class" tabindex="2">
                                @foreach($std_class as $data)
                                    <option value="{{ $data->Cla_Id.','.$data->Cla_Class }}" @if($student->Cla_Class==$data->Cla_Class) selected @endif>{{ $data->Cla_Class }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if (Session::has('class'))
                        <label class="error" for="class">
                            <span class="text-danger">
                                {{ Session::get('class') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select readonly="true" class="class-sec" name="section" id="section" tabindex="3">
                        @foreach($std_section as $data)
                            <option value="{{ $data->Cla_Section }}" @if($student->Cla_Section==$data->Cla_Section) selected @endif>{{ $data->Cla_Section }}</option>
                        @endforeach
                  </select>
                </div>
                </div>
                 @if (Session::has('section'))
                    <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Student Name <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <div class="select-box" >
                            <select readonly="true" class="student-class" name="student_id" id="student" tabindex="4">
                                <option value="none">-- Select--</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" id="student_id" value="{{ $student->Pay_Std_Id }}">
                    @if (Session::has('student_id'))
                        <label class="error" for="student_id">
                            <span class="text-danger">
                                {{ Session::get('student_id') }}
                            </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                        <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="4" id="status">
                            <option value="remaining" @if($student->Pay_Status=='remaining') selected @endif >Pending</option>
                            <option value="complate" @if($student->Pay_Status=='complate') selected @endif>Paid</option>
                        </select>
                    </div>
                </div>    
                @if ($errors->has('status'))
                    <label class="error" for="status">
                        <span class="text-danger">
                            {{ $errors->first('status') }}
                        </span>
                    </label>
                @endif
            </div>

            <div class="form-box" id="due_date_id">
                <div class="form-text ">
                    <h5>Due Date <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                   <input type="text" name="due_date" id="datepicker" value="{{ date('d/m/Y',strtotime($student->Pay_Date)) }}" required placeholder="dd/mm/yyyy" tabindex="5">
                    <!-- </div> -->
                    @if($errors->has('date'))
                        <span class="text-danger">
                            {{ $errors->first('date') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="right-form">
            
            <div class="form-box">
                <div class="form-text">
                    <h5>Payment Type <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                   <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('pay_type') ? 'is-invalid' : '' }}" name="pay_type" required tabindex="9">
                    <option value="cash" @if($student->Pay_Type=='cash') selected @endif >Cash</option>
                    <option value="cheque" @if($student->Pay_Type=='cheque') selected @endif>Cheque</option>
                </select>
                    @if ($errors->has('pay_type'))
                        <label class="error" for="pay_type">
                        <span class="text-danger">
                            {{ $errors->first('pay_type') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Amount <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" class="number" name="amount" class="{{ $errors->has('amount') ? ' is-invalid' : '' }}" required tabindex="10" value="{{ $student->Pay_Amount }}">
                    @if ($errors->has('amount'))
                      <label class="error" for="amount">
                        <span class="text-danger">
                            {{ $errors->first('amount') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Description :</h5>
                </div>
                <div class="form-typ-box">
                <textarea name="description" class="{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" tabindex="9" >{{ $student->Pay_Description }}</textarea>
                @if ($errors->has('description'))
                    <label class="error" for="description">
                    <span class="text-danger">
                        {{ $errors->first('description') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>  
        </div>

        <div class="form-btn branch-form-btn">
            <input value="save" type="submit" tabindex="8"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="9" ></a>
        </div>
</form>
</div>

@endsection

@section('footer')

@push('footer_script')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ url('public/js/index.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){

    $("#datepicker").datepicker({
        dateFormat: "dd/mm/yy",
    });
    
    $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault()    
        }
        if(class_val=="none"){
            alert("Please Select  Class...");
            e.preventDefault()    
        }
        if(section=="none"){
            alert("Please Select  Section...");
            e.preventDefault()    
        }
    });

    $(".number").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    })

    var branchId = $("#branch").val();
    var idAndClass = $("#class").val();
    var classIdName = idAndClass.split(",");
    var sectionId = $("#section").val();
    var selectedStd = $("#student_id").val();
    
    var status = $("#status").val();
    if(status!=''){
        if(status=='remaining'){
            $("#due_date_id").css("display","block");
        } else{
            $("#due_date_id").css("display","none");
        }
    }
    $("#status").on("change",function(){
        status = $("#status").val();
        if(status=='remaining'){
            $("#due_date_id").css("display","block");
        } else{
            $("#due_date_id").css("display","none");
        }
    });

    if(branchId!='' && idAndClass!='' && sectionId!=''){
        getSection(branchId,classIdName,sectionId,selectedStd);
    }

    // $("#parent_autocomplete").autocomplete({
    //     source : "{{ url('student/getParent') }}"
    // });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#class").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });

    $("#class").on('change',function(){
        var idAndClass = $(this).val();
        var classIdName = idAndClass.split(",");
        $.ajax({
            type : "POST",
            url : "{{ url('student/getSection') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Class : classIdName[1],
                Cla_Bra_Id : classIdName[0]
            },
            dataType : "JSON",
            success : function(data){
                    $(".class-sec option").each(function() {
                        $(this).remove();
                    });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                     });
                    $("#section").append('<option value="none">-- Select --</option>');
                    $("#section").append(items);
            },
            error :function(error){
                console.log(error);
            }
        });
    });

    $("#section").on('change',function(){
        var branchId = $("#branch").val();
        var idAndClass = $("#class").val();
        var classIdName = idAndClass.split(",");
        var sectionId = $("#section").val();
        var selectedStd = $("#student_id").val();
        getSection(branchId,classIdName,sectionId,selectedStd);
    });

});
function getSection(branchId,classIdName,sectionId,selectedStd=''){
    $.ajax({
        type : "POST",
        url : "{{ url('get_student_list') }}",
        data : {
            _token:     '{{ csrf_token() }}',
            branchId : branchId,
            className : classIdName[1],
            sectionId : sectionId
        },
        dataType : "JSON",
        success : function(data){
            $(".student-class option").each(function() {
                $(this).remove();
            });
            var items = [];
             $.each( data, function( key, val ) {
                if(selectedStd==this['Std_Id']){
                    items.push( "<option selected='true' value='" + this['Std_Id'] + "'>" + this['Std_Name'] + "</option>" );
                } else{
                    items.push( "<option value='" + this['Std_Id'] + "'>" + this['Std_Name'] + "</option>" );
                }
             });
            $("#student").append('<option value="none">-- Select --</option>');
            $("#student").append(items);
        },
        error :function(error){
            console.log(error);
        }
    });
}
</script>
@endpush
@section('footer_link_and_scripts')