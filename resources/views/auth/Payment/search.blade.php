<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">Name</th>
      <th class="td-left">Branch</th>
      <th class="td-left">Class</th>
      <th class="td-left">Section</th>
      <th class="td-left">Parent Name</th>
      <th>Created By</th>
      <th>Status</th>
      <th>Edit</th>
    </tr>
	<?php $i=1; ?>
	@forelse($payment as $data)
    <tr>
      	<td><input id="student{{ $i }}" type="checkbox" name="id[]" value="{{ $data->Pay_Id }}" class="css-checkbox check-all"><label for="student{{ $i }}" class="css-label table-ckeckbox"></label></td>
      	<td class="td-left">{{ $data->Std_Name }}</td>
      	<td class="td-left">{{ $data->Brn_Name }}</td>
        <td class="td-left">{{ $data->Cla_Class }}</td>
      	<td class="td-left">{{ $data->Cla_Section }}</td>
        <td class="td-left">{{ $data->parentName }}</td>
      	<td>{{ $data->createdByName }}</td>
      	<td>@if($data->Pay_Status == 1)
				Pending
			@elseif($data->Pay_Status == 0)
				Completed
			@endif</td>
      	<td><a href="{{ url('edit_payment',$data->Pay_Std_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
    </tr>
	<?php $i++; ?>   
	@empty
	<tr><td colspan="10" align="center">No Record Found</td></tr>
    @endforelse

  </tbody>
</table>