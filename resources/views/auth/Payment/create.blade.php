@extends('auth.Master.master')
    
@if(isset($student))
    @section('title','Edit Payment')
@else
    @section('title','Add Payment')
@endif

@section('site_header')
@section('sidebar')
@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2>
            <img src="{{ asset('public/images/event-management.png') }}" alt="">
            @if(isset($student))
                Edit Payment
            @else
                Add Payment
            @endif
        </h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
    {!! Session::get('error') !!}
@endif

<div class="form-section">
    <form name="add-payment" role="form" method="POST" action="{{ url('save_payment') }}" enctype="multipart/form-data" id="studentForm">

        {{ csrf_field() }}
        @if(isset($student))
            <input type="hidden" name="id" value="{{ $student->Pay_Std_Id }}">
        @endif
    
        <div class="left-form">

            <div class="form-box">
                <div class="form-text">
                  <h5>Branch <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                            <option value="none">-- Select --</option>
                            @foreach($branch as $data)
                                <option value="{{ $data->Brn_Id }}" @if(isset($student) && $student->Pay_Bra_Id == $data->Brn_Id) selected="true" @endif >{{ $data->Brn_Name }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="old_branch" value="@if(isset($student) && !empty($student->Pay_Bra_Id)){{ $student->Pay_Bra_Id }}@endif">
                    </div>
                </div>    
                @if (Session::has('branch'))
                <label class="error" for="branch">
                    <span class="text-danger">
                        {{ Session::get('branch') }}
                    </span>
                </label>
                @endif
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" tabindex="2">
                    <option value="none">-- Select--</option>
                  </select>
                  <input type="hidden" id="old_class" value="@if(isset($student) && !empty($student->Pay_Cla_Id)){{ $student->Pay_Cla_Id }}@endif">
                </div>
                </div>
                @if (Session::has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        {{ Session::get('class') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="class-sec" name="section" id="section" tabindex="3">
                    <option value="none">-- Select--</option>
                  </select>
                  <input type="hidden" id="old_section" value="@if(isset($student) && !empty($student->Pay_Section)){{ $student->Pay_Section }}@endif">
                </div>
                </div>
                @if (Session::has('section'))
                <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Student Name <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="student-class" name="student_id" id="student" tabindex="4">
                    <option value="none">-- Select--</option>
                  </select>
                  <input type="hidden" id="old_student_id" value="@if(isset($student) && !empty($student->Pay_Std_Id)){{ $student->Pay_Std_Id }}@endif">
                </div>
                </div>
                @if (Session::has('section'))
                <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                </label>
                @endif
                </div>
            </div>
        </div>

        <div class="right-form">

            {{-- <div class="form-box">
                <div class="form-text">
                    <h5>Payment Type <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                   <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('pay_type') ? 'is-invalid' : '' }}" name="pay_type" required tabindex="9">
                     <option value="cash" @if(isset($student) && $student->Pay_Type=="cash"){{ $student->Pay_Type }}@endif>Cash</option>
                    <option value="cheque" @if(isset($student) && $student->Pay_Type=="cheque"){{ $student->Pay_Type }}@endif>Cheque</option>
                </select>
                    @if ($errors->has('pay_type'))
                        <label class="error" for="pay_type">
                        <span class="text-danger">
                            {{ $errors->first('pay_type') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div> --}}

            {{-- <div class="form-box">
                <div class="form-text">
                  <h5>Description :</h5>
                </div>
                <div class="form-typ-box">
                <textarea name="description" class="{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5" tabindex="9" >@if(isset($student) && !empty($student->Pay_Description)){{ $student->Pay_Description  }} @else {{ old('description') }} @endif</textarea>
                @if ($errors->has('description'))
                    <label class="error" for="description">
                    <span class="text-danger">
                        {{ $errors->first('description') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>  --}}   

            {{-- <div class="form-box">
                <div class="form-text ">
                    <h5>G.R No <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="grno" class="{{ $errors->has('grno') ? ' is-invalid' : '' }}" required tabindex="6" value="{{ old('grno') }}" maxlength="15">
                @if ($errors->has('grno'))
                    <label class="error" for="grno">
                    <span class="text-danger">
                        {{ $errors->first('grno') }}
                    </span>
                    </label>
                @endif
                </sdiv>
            </div> --}}

            {{-- <div class="form-box">
                <div class="form-text ">
                    <h5>Student Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="studentname" class="{{ $errors->has('studentname') ? ' is-invalid' : '' }}" required tabindex="7" value="{{ old('studentname') }}" maxlength="50" >
                </div>
                @if ($errors->has('studentname'))
                    <label class="error" for="studentname">
                    <span class="text-danger">
                        {{ $errors->first('studentname') }}
                    </span>
                    </label>
                @endif
            </div> --}}

            {{-- <div class="form-box">
                <div class="form-text ">
                    <h5>Parent Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="parentname" class="{{ $errors->has('parentname') ? ' is-invalid' : '' }}" required tabindex="8" value="{{ old('parentname') }}" id="parent_autocomplete" maxlength="50" >
                    @if ($errors->has('parentname'))
                      <label class="error" for="parentname">
                        <span class="text-danger">
                            {{ $errors->first('parentname') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div> --}}
        </div>

        <div class="amount-boxes">
            @if(isset($student))
                @php
                    $fees = \App\Model\Payment::where('Pay_Std_Id',$student->Pay_Std_Id)->orderby('Pay_Id','ASC')->get();
                    $counter = 1;
                @endphp
            @endif
            <input type="hidden" id="fee_count" value="@if(isset($fees)){{ count($fees)+1 }}@else{{ '2' }}@endif">
            <div id="multiple_txt">
                @if(isset($fees) && count($fees)!=0)
                    @forelse($fees as $fee)
                        <div class="row" id="row_{{ $counter }}">
                            <input type="hidden" name="pay_ids[]" value="{{ $fee->Pay_Id }}">
                            <div class="col-lg-12">
                                <div class="form-box amount-box">
                                    <div class="form-text ">
                                        <h5>Amount  <span>*</span> :</h5>
                                    </div>
                                    <div class="form-typ-box"> 
                                        <input type="text" name="amount[]" class="number amount-txt amount_{{ $counter }}" maxlength="6" id="amount" value="{{ $fee->Pay_Amount }}" data-id="{{ $counter }}">
                                    </div>
                                </div>
                                <div class="form-box status-box">
                                    <div class="form-text">
                                      <h5>Status <span>*</span> :</h5>
                                    </div>   
                                    <div class="form-typ-box">
                                    <div class="">
                                    <select class="form-control status-txt status_{{ $counter }}" name="status[]" id="status" required id="status">
                                        <option value="remaining" @if($fee->Pay_Status=="remaining") selected="true" @endif>Pending</option>
                                        <option value="complate" @if($fee->Pay_Status=="complate") selected="true" @endif>Paid</option>
                                    </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-box payment-box">
                                    <div class="form-text">
                                        <h5>Type <span>*</span> :</h5>
                                    </div>
                                    <div class=""> 
                                       <select class="form-control type-txt type_{{ $counter }}" name="pay_type[]" required >
                                        <option value="cash" @if($fee->Pay_Type=="cash") selected="true" @endif>Cash</option>
                                        <option value="cheque" @if($fee->Pay_Type=="cheque") selected="true" @endif>Cheque</option>
                                    </select>
                                        @if ($errors->has('pay_type'))
                                            <label class="error" for="pay_type">
                                            <span class="text-danger">
                                                {{ $errors->first('pay_type') }}
                                            </span>
                                            </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-box due-box" id="due_date_id">
                                    <div class="form-text ">
                                        <h5>Due Date<span>*</span> :</h5>
                                    </div>
                                    <div class="form-typ-box"> 
                                       <input type="text" class="datepicker-cls date-txt date_{{ $counter }}" name="due_date[]" id="datepicker_{{ $counter }}" placeholder="dd/mm/yyyy" value="{{ date('d/m/Y',strtotime($fee->Pay_Date)) }}" readonly="true">
                                    </div>
                                </div>
                                <div class="form-box due-box" id="due_date_id">
                                    <div class="form-text ">
                                        <h5>Payment Date<span style="font-size: 22px;">:</span></h5>
                                    </div>
                                    <div class="form-typ-box"> 
                                       <input type="text" class="datepicker-cls date-txt date_{{ $counter }}" name="payment_date[]" placeholder="dd/mm/yyyy" value="@if(isset($fee->Payment_date)){{ date('d/m/Y',strtotime($fee->Payment_date)) }}@endif" readonly="true">
                                    </div>
                                </div>
                                <div class="form-box description-box">
                                    <div class="form-text">
                                      <h5>Description <span style="font-size: 22px;">:</span> </h5>
                                    </div>
                                    <div class="form-typ-box">
                                    <textarea name="description[]" class="description-txt description_{{ $counter }}" rows="2" cols="40" >@if(isset($fee) && !empty($fee->Pay_Description)){{ $fee->Pay_Description  }}@endif</textarea>
                                    @if ($errors->has('description'))
                                        <label class="error" for="description">
                                        <span class="text-danger">
                                            {{ $errors->first('description') }}
                                        </span>
                                        </label>
                                    @endif
                                    </div>
                                </div>
                                @if($counter==1)
                                    <div class="form-box add-box" >
                                        <div class="form-text ">
                                            <h5></h5>
                                        </div>
                                        <div class="form-typ-box" style="margin-top: 24px;"> 
                                            <button id="add">Add</button>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-box add-box" >
                                        <div class="form-text ">
                                            <h5></h5>
                                        </div>
                                        <div class="form-typ-box" style="margin-top: 24px;">
                                            <a href="JavaScript:void(0);" class="delete-row" data-id="{{ $counter }}" onclick="deleteRow('{{ $fee->Pay_Id }}','{{ $counter }}')">Delete</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @php $counter++ @endphp
                    @empty  
                    @endforelse
                @else
                    <div class="row" id="row_1">
                        <div class="col-lg-12">
                            <div class="form-box amount-box">
                                <div class="form-text">
                                    <h5>Amount <span>*</span> :</h5>
                                </div>
                                <div class="form-typ-box"> 
                                    <input type="text" name="amount[]" class="number amount-txt amount_1" maxlength="6" id="amount" data-id="1">
                                </div>
                            </div>
                            <div class="form-box status-box">
                                <div class="form-text">
                                  <h5>Status <span>*</span> :</h5>
                                </div>   
                                <div class="form-typ-box">
                                <div class="">
                                <select class="form-control status-txt status_1" name="status[]" id="status" required id="status">
                                    <option value="remaining" >Pending</option>
                                    <option value="complate" >Paid</option>
                                </select>
                                </div>
                                </div>
                            </div>
                            <div class="form-box payment-box">
                                <div class="form-text">
                                    <h5>Type <span>*</span> :</h5>
                                </div>
                                <div class=""> 
                                   <select class="form-control type-txt type_1" name="pay_type[]" required>
                                        <option value="cash">Cash</option>
                                        <option value="cheque">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-box due-box" id="due_date_id">
                                <div class="form-text ">
                                    <h5>Due Date/Date <span>*</span> :</h5>
                                </div>
                                <div class="form-typ-box"> 
                                   <input type="text" class="datepicker-cls date-txt date_1" name="due_date[]" id="datepicker" placeholder="dd/mm/yyyy" readonly="true">
                                </div>
                            </div>
                            <div class="form-box description-box" id="payment_date_id">
                                <div class="form-text ">
                                    <h5>Payment Date<span style="font-size: 22px;">:</span></h5>
                                </div>
                                <div class="form-typ-box"> 
                                   <input type="text" class="datepicker-cls date-txt date_1" name="payment_date[]" id="" placeholder="dd/mm/yyyy" readonly="true">
                                </div>
                            </div>
                            <div class="form-box description-box">
                                <div class="form-text">
                                  <h5>Description <span style="font-size: 22px;">:</span> </h5>
                                </div>
                                <div class="form-typ-box">
                                <textarea name="description[]" class="description-txt description_1" rows="2" cols="40" ></textarea>
                                @if ($errors->has('description'))
                                    <label class="error" for="description">
                                    <span class="text-danger">
                                        {{ $errors->first('description') }}
                                    </span>
                                    </label>
                                @endif
                                </div>
                            </div>
                            <div class="form-box add-box">
                                <div class="form-text ">
                                    <h5></h5>
                                </div>
                                <div class="form-typ-box" style="margin-top: 24px;"> 
                                    <button id="add">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="form-btn branch-form-btn">
            <input value="save" type="submit" id="submitId" tabindex="8"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="9" ></a>
        </div>
    </form>
</div>

<style type="text/css">
    .amount-boxes{
        width: 100%;
        display: inline-block;
        margin-top: 20px;
    }
    #ui-datepicker-div {
        z-index: 99999999 !important;
    }
    .delete-row:hover{
        color: white;
    }
    .delete-row{
        color: red;
    }
    #add {
        height: 35px;
        padding: 0px 10px;
    }
    #multiple_txt {
        margin-left: 18px;
    }
    .amount-box{
        width: 11%;
    }
    .status-box{
        width: 15%;
    }
    .payment-box{
        width: 11%;
    }
    .due-box{
        width: 20%;
    }
    .add-box{
        width: 1%;
    }
    .description-box{
        width: 20%;   
    }
    #amount{
        text-align: right;
    }
</style>

@endsection
@section('footer')
@push('footer_script')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ url('public/js/index.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    /* load default */
    var branchId = $("#branch").val();
    var olb_branch = $("#old_branch").val();
    var old_class = $("#old_class").val();
    var old_class_brn = '';
    var old_section = $("#old_section").val();
    var old_student_id = $("#old_student_id").val();
    var idAndClass = $("#class").val();

    if(branchId!='' && branchId!="none"){
        getClasses(branchId,old_class);
    }
    // if(old_student_id!='' && old_student_id!='none'){
    //     getStudent();
    // }
    // if(old_class!=0 && old_class!='none'){
    //     getSections();
    // }

    $("#submitId").on('click',function(e){
        var branchId = $("#branch").val();
        var classId = $("#class").val();
        var sectionId = $("#section").val();
        var studentId = $("#student").val();
        var status = $("#status").val();
        var datepicker = $("#datepicker").val();
        var amount = $("#amount").val();

        if(branchId=="none" || branchId==""){
            alert("Please select branch");
            e.preventDefault();
        } else if(classId=="none" || classId==""){
            alert("Please select class");
            e.preventDefault();
        } else if(sectionId=="none" || sectionId==""){
            alert("Please select section");
            e.preventDefault();
        } else if(studentId=="none" || studentId==""){
            alert("Please select student");
            e.preventDefault();
        } else if(status=="1" && datepicker.trim()==""){
            alert("Please select due date");
            e.preventDefault();
        } else{
            $("input:text[name='amount[]']").each(function( key, value ){
                if($(this).val().trim()=='' || $(this).val().trim()==0){
                    $(this).css("border","1px solid red");
                    $(this).focus();
                    alert("Please enter amount");
                    e.preventDefault();
                    return false;
                } else{
                    $(this).css("border","");
                    if($(".date_"+$(this).attr('data-id')).val().trim() == ''){
                        $(".date_"+$(this).attr('data-id')).css("border","1px solid red");
                        $(".date_"+$(this).attr('data-id')).focus();
                        alert("Please select date");
                        e.preventDefault();
                        return false;
                    } else{
                        $(".date_"+$(this).attr('data-id')).css("border","");
                    }
                }
            });
        }
    });

    $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault();
            return false;
        }else{
            if(class_val=="none")
            {
                alert("Please Select  Class...");
                e.preventDefault();
            return false;
                
            }else{
                if(section=="none"){
                    alert("Please Select  Section...");
                    e.preventDefault();
            return false;
                }
            }
        }
    });

    $("#parent_autocomplete").autocomplete({
        source : "{{ url('student/getParent') }}"
    });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        getClasses(branchId);
    });
    $("#class").on('change',function(){
        getSections();
    });
    $("#section").on('change',function(){
        getStudent();
            // var branchId = $("#branch").val();
            // var idAndClass = $("#class").val();
            // var classIdName = idAndClass.split(",");
            // var sectionId = $("#section").val();
            // $.ajax({
            //     type : "POST",
            //     url : "{{ url('get_student_list') }}",
            //     data : {
            //         _token:     '{{ csrf_token() }}',
            //         branchId : branchId,
            //         className : classIdName[1],
            //         sectionId : sectionId
            //     },
            //     dataType : "JSON",
            //     success : function(data){
            //         $(".student-class option").each(function() {
            //             $(this).remove();
            //         });
            //         var items = [];
            //          $.each( data, function( key, val ) {
            //             items.push( "<option value='" + this['Std_Id'] + "'>" + this['Std_Name'] + "</option>" );
            //          });
            //         $("#student").append('<option value="none">-- Select --</option>');
            //         $("#student").append(items);
            //     },
            //     error :function(error){
            //         console.log(error);
            //     }
            // });
    });

    $(".datepicker-cls").datepicker({
        dateFormat: "dd/mm/yy",
    });

    $(document).on('click', '.datepicker-cls', function(){
        $(this).datepicker({
            dateFormat: "dd/mm/yy",
        }).focus();
    });

    $(document).on('click', '.delete-row', function(){
        var row_id = $(this).attr('data-id');
        $("#row_"+row_id).remove();
    });    

    $(document).on('click', '.number', function(){
        $($(this)).keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
});

function getClasses(branchId,old_class=''){
    $.ajax({
        type : "POST",
        url : "{{ url('student/getClass') }}",
        data : {
            _token:     '{{ csrf_token() }}',
            Cla_Bra_Id : branchId
        },
        dataType : "JSON",
        success : function(data){
                $(".branch-cls option").each(function() {
                    $(this).remove();
                });
                $(".class-sec option").each(function() {
                    $(this).remove();
                });
                var items = [];
                $.each( data, function( key, val ) {
                    if(old_class == this['Cla_Id'].toString()){
                        old_class_brn = this['Cla_Bra_Id']+","+this['Cla_Class'];
                        $("#old_class").val(old_class_brn);
                        getSections();
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "' selected='true'>" + this['Cla_Class'] + "</option>" );
                    } else{
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                    }
                });
                $("#class").append('<option value="none">-- Select--</option>');
                $("#section").append('<option value="none">-- Select --</option>');
                $("#class").append(items);
                
            },
        error : function(error){
            console.log(error);
        }
    });
}

function getSections(){
    var branchId = $("#branch").val();
    var idAndClass = $("#old_class").val();
    if(idAndClass==''){
        idAndClass =  $("#class").val();
    }
    var classIdName = idAndClass.split(",");
    var sectionId = $("#section").val();
    var selectedStd = $("#student_id").val();
    var old_section = $("#old_section").val();
    $.ajax({
        type : "POST",
        url : "{{ url('student/getSection') }}",
        data : {
            _token:     '{{ csrf_token() }}',
            Cla_Class : classIdName[1],
            Cla_Bra_Id : classIdName[0]
        },
        dataType : "JSON",
        success : function(data){
                $(".class-sec option").each(function() {
                    $(this).remove();
                });
                var items = [];
                 $.each( data, function( key, val ) {
                    if( old_section == this['Cla_Section']){
                        $("#old_section").val(this['Cla_Section']);
                        getStudent();
                        items.push( "<option value='" + this['Cla_Section'] + "' selected='true'>" + this['Cla_Section'] + "</option>" );
                    } else{
                        items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                    }
                 });
                $("#section").append('<option value="none">-- Select --</option>');
                $("#section").append(items);
        },
        error :function(error){
            console.log(error);
        }
    });
}

function getStudent(){
    var branchId = $("#branch").val();
    var idAndClass = $("#old_class").val();
    if(idAndClass==''){
        idAndClass =  $("#class").val();
    }
    var classIdName = idAndClass.split(",");
    var sectionId = $("#old_section").val();
    if(sectionId==""){
        sectionId = $("#section").val();
    }
    var selectedStd = $("#old_student_id").val();
    $.ajax({
        type : "POST",
        url : "{{ url('get_student_list') }}",
        data : {
            _token:     '{{ csrf_token() }}',
            branchId : branchId,
            className : classIdName[1],
            sectionId : sectionId
        },
        dataType : "JSON",
        success : function(data){
            $(".student-class option").each(function() {
                $(this).remove();
            });
            var items = [];
             $.each( data, function( key, val ) {
                if(selectedStd==this['Std_Id']){
                    items.push( "<option selected='true' value='" + this['Std_Id'] + "'>" + this['Std_Name'] + "</option>" );
                } else{
                    items.push( "<option value='" + this['Std_Id'] + "'>" + this['Std_Name'] + "</option>" );
                }
             });
            $("#student").append('<option value="none">-- Select --</option>');
            $("#student").append(items);
        },
        error :function(error){
            console.log(error);
        }
    });
}

function deleteRow(pay_id,row_id){
    $.ajax({
        type : "POST",
        url : "{{ url('payment/delete') }}",
        data : {
            _token:     '{{ csrf_token() }}',
            pay_id : pay_id
        },
        success : function(data){
            if(data == "true"){
                $("#row_"+row_id).remove();
            }
        },error : function(error){
            console.log(error);
        }
    });
}

$(function() {
    $('#add').on('click', function( e ) {
        e.preventDefault();
        var row_id = $("#fee_count").val();
        $("#multiple_txt").append('<div class="row" id="row_'+row_id+'"><div class="col-lg-12">'+
            '<div class="form-box amount-box">'+
                '<div class="form-text">'+
                    '<h5>Amount <span>*</span> :</h5>'+
                '</div>'+
                '<div class="form-typ-box"> '+
                    '<input type="text" name="amount[]" class="number amount_'+row_id+'" maxlength="6" id="amount" data-id="'+row_id+'">'+
                '</div>'+
            '</div>'+
            '<div class="form-box status-box">'+
                '<div class="form-text">'+
                  '<h5>Status <span>*</span> :</h5>'+
                '</div>   '+
                '<div class="form-typ-box">'+
                '<div class="">'+
                '<select class="form-control status_'+row_id+'" name="status[]" id="status" required id="status">'+
                    '<option value="remaining" >Pending</option>'+
                    '<option value="complate" >Paid</option>'+
                '</select>'+
                '</div>'+
                '</div>'+
            '</div>'+
            '<div class="form-box payment-box">'+
                '<div class="form-text">'+
                    '<h5>Type <span>*</span> :</h5>'+
                '</div>'+
                '<div class=""> '+
                   '<select class="form-control type_'+row_id+'" name="pay_type[]" required >'+
                        '<option value="cash">Cash</option>'+
                        '<option value="cheque">Cheque</option>'+
                    '</select>'+
                '</div>'+
            '</div>'+
            '<div class="form-box due-box" id="due_date_id">'+
                '<div class="form-text ">'+
                    '<h5>Due Date/Date <span>*</span> :</h5>'+
                '</div>'+
                '<div class="form-typ-box"> '+
                   '<input type="text" class="datepicker-cls date_'+row_id+'" name="due_date[]" placeholder="dd/mm/yyyy" readonly="true">'+
                '</div>'+
            '</div>'+
            '<div class="form-box description-box" id="payment_date_id">'+
                '<div class="form-text ">'+
                    '<h5>Payment Date <span style="font-size: 22px;">:</span></h5>'+
                '</div>'+
                '<div class="form-typ-box"> '+
                   '<input type="text" class="datepicker-cls date_'+row_id+'" name="payment_date[]" placeholder="dd/mm/yyyy" readonly="true">'+
                '</div>'+
            '</div>'+
            '<div class="form-box description-box">'+
                '<div class="form-text">'+
                  '<h5>Description <span style="font-size: 22px;">:</span> </h5>'+
                '</div>'+
                '<div class="form-typ-box description_'+row_id+'">'+
                '<textarea name="description[]" class="" rows="2" cols="40" ></textarea>'+
                '</div>'+
            '</div>'+
            '<div class="form-box add-box">'+
                '<div class="form-text ">'+
                    '<h5></h5>'+
                '</div>'+
                '<div class="form-typ-box" style="margin-top: 24px;">'+
                    '<a href="JavaScript:void(0);" class="delete-row" data-id="'+row_id+'">Delete</a>'+
                '</div>'+
            '</div>'+
            '</div>'+
        '</div>');
        row_id = (parseInt(row_id) + 1);
        $("#fee_count").val(row_id);
    });
    $(document).on('click', 'button.remove', function( e ) {
        e.preventDefault();
    });
});
</script>
@endpush
@section('footer_link_and_scripts')