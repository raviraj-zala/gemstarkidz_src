@extends('auth.Master.master')

@section('title','Import Excel Report')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/export_img.png') }}" alt=""> Form Mgmt.</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>

<div class="form-section">

<div class="left-form">
    <!-- <div class="branch-form">  -->
    <form  id="excel-import" role="form" method="POST" action="{{ url('import_form') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <!-- <div class="branch-form"> -->
            

            <div class="form-box">
                <div class="form-text">
                    <h5>Select Type <span>*</span> :</h5>
                </div>
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('importType') ? 'is-invalid' : '' }}" tabindex="1"  name="importType" required id="importType">
                        <option value="none">--Select--</option>
                        <option value="1">Student</option>
                         <option value="2">Progress Report</option>
                         <option value="3">Parent</option>
                         <option value="4">HomeWork</option>
                         <option value="5">Event</option>
                         <option value="6">Attedence</option>
                         <!--
                        <option value="3">Teacher</option>
                        <option value="4">Driver</option> -->
                    </select>
                    <div id="errorImportType"></div>
                    @if (Session::has('importType'))
                    <label class="error" for="importType">
                        <span class="text-danger">
                            {{ Session::get('importType') }}
                        </span>
                    </label>
                    @endif
                </div>
                </div>
            </div>
            <div class="form-box">
                <div class="form-text">
                    <h5>File <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                    <input name="excelFile" class=" browser-btn" tabindex="" value="" accept="" type="file" required>
                    <div id="errorFile"></div>
                </div>
            </div>

            @if(Session::has('studentNotImport'))
            <div class="form-text">
                <label style="color:white;">{{ Session::get('studentNotImport')." Gr No Student Not Exists Please Insert Student First"  }}</label>
            </div>
            @endif

            @if(Session::has('studentClassNotFound'))
            <div class="form-text">
                <label style="color:white;">{{ Session::get('studentClassNotFound')  }}</label>
            </div>
            @endif
            
            @if(Session::has('studentUpdateDone'))
            <div class="form-text">
                <label style="color:white;">{{ Session::get('studentUpdateDone')  }}</label>
            </div>
            @endif

            @if(Session::has('importError'))
            <div class="form-text">
                <label style="color:white;">{!! Session::get('importError')  !!}</label>
            </div>
            @endif

            

        <div class="form-btn branch-form-btn">
            <input type="submit" value="save" tabindex="3"><a href="{{ URL::to('/home') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
        </div>
        <!-- </div> -->
        <div class="form-text">
        <font style="color:white;">
            <legend style="color:white;">Sample File.</legend>
            <a href="{{url('public/sampleFile/Update_Student_Class.xls')}}" style="color: #17202a;" download>Student Class Update</a><br><br>
            <a href="{{url('public/sampleFile/student_subject_json.json')}}" style="color: #17202a;" download>Progress Report</a><br><br>
            <a href="{{url('public/sampleFile/parent.xlsx')}}" style="color: #17202a;" download>Parent Details Import</a><br><br>
            <a href="{{url('public/sampleFile/Homework.xlsx')}}" style="color: #17202a;" download>HomeWork List Import</a><br><br>
            <a href="{{url('public/sampleFile/Event.xlsx')}}" style="color: #17202a;" download>Event List Import</a><br><br>
            <a href="{{url('public/sampleFile/Attedence.xlsx')}}" style="color: #17202a;" download>Attedence List Import</a>
            </font>
        </div>
</form>
    <!-- </div> -->



</div>


<div class="right-form">


</div>
@endsection

@section('footer')

@push('footer_script')
<script type="text/javascript">
$(document).ready(function(){
    $("#excel-import").on('submit',function(e){
        var type = $("#importType").val();
        if($("#excelFile").val()==""){
            $("#errorFile").html('<label class="error" for="excelFile"><span class="text-danger">Please Select Excel File.</span></label>');
            e.preventDefault();
        }
        if(type=="none"){
            $("#errorImportType").html('<label class="error" for="importType"><span class="text-danger">Please Select Import type.</span></label>');
            e.preventDefault();
        }
    })
})

</script>
@endpush

@section('footer_link_and_scripts')