    <div class="row">
        <div id="teacehr_class_id" @if(old('userType')==2) style="display: block;" @else style="display: none;" @endif>
        <div class="col-lg-12">
        <?php $i=1; $cid=""; $cids=""; $abc = "<script>document.write(jvalue)</script>"; ?>   
        @foreach($class as $data)
                <div class="box box-primary">
                @if($data->Brn_Id==$data->Brn_Zon_Id && $data->Brn_Id!=$cid)
                    <?php $cid=$data->Brn_Id; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
                            <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class=" display-class css-checkbox {{ 'brn'.$data->Cla_Id }}" value="{{ $data->Cla_Id }}"
                           @if(old('branch'))
                                @foreach(old('branch') as $b)
                                    @if($b == $data->Cla_Bra_Id )
                                        checked="true"
                                    @endif
                                @endforeach
                           @endif
                           ><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Class }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
        </div>
    </div>