@extends('auth.Master.master')

@section('title','Edit User')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="">Edit User</h2>      
    </div>
</div>
@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="form-section">
    <form name="edit-user" role="form" method="POST" action="{{ url('update_user') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="left-form">
        <input type="hidden" name="id" value="{{ $user->Use_Id }}">
        <input type="hidden" id="user_type" value="{{ $user->Use_Type }}">

    <div class="form-box">
        <div class="form-text ">
            <h5>Name <span>*</span> :</h5>
        </div>
        <div class="form-typ-box">
            <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $user->Use_Name }}" required autofocus maxlength="50" tabindex="1">
        </div>
        @if ($errors->has('name'))
        <span class="text-danger">
            {{ $errors->first('name') }}
        </span>
        @endif
    </div>

    <div class="form-box">
        <div class="form-text">
            <h5>Email <span>*</span> :</h5>
        </div>
        <div class="form-typ-box"> 
            <input type="text" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" required tabindex="2" value="{{ $user->Use_Email }}" maxlength="60" readonly>
        @if ($errors->has('email'))
        <label class="error" for="email">
            <span class="text-danger">
                {{ $errors->first('email') }}
            </span>
        </label>
        @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
          <h5>user type <span>*</span> :</h5>
        </div>
        <div class="form-typ-box select-text">
            <div class="btn-group bootstrap-select show-tick form-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('userType') ? 'is-invalid' : '' }}" name="userType" tabindex="3" id="userType">
                    @if(Auth::user()->Use_Type=="1" )<option value="1" @if($user->Use_Type=='1') selected="true" @endif>Admin</option>@endif
                    @if(Auth::user()->Use_Type=="1" )<option value="5" @if($user->Use_Type=='5') selected="true" @endif>Branch Admin</option>@endif
                    <option value="2" @if($user->Use_Type=='2') selected="true" @endif>Teacher</option>
                    <option value="3" @if($user->Use_Type=='3') selected="true" @endif>Driver</option>
                    @if($user->Use_Type==4)
                    <option value="4" selected disabled>Parent</option>
                    @endif          
                </select>
                @if ($errors->has('userType'))
                    <span class="text-danger">
                        {{ $errors->first('userType') }}
                    </span>
                @endif 
            </div>
        </div>
    </div>

    <div id="teacherDetail" style="display: none;">
            <div class="form-box">
                    <div class="form-text">
                      <h5>Branch * :</h5>
                    </div>   
                    <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">

                    <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="teacherBranch" required id="teacherBranch">
                        <option value="none">-- Select --</option>
                                @foreach($branch as $data)
                                    <option value="{{ $data->Brn_Id }}" >{{ $data->Brn_Name }}</option>
                                @endforeach
                            </select>
                    </div>
                    @if (Session::has('branch'))
                    <label class="error" for="teacherBranch">
                        <span class="text-danger">
                            {{ Session::get('branch') }}
                        </span>
                        </label>
                    @endif
                    </div>    
            </div>

            <div class="form-box">
                    <div class="form-text">
                      <h5>Class <span>*</span> :</h5>
                    </div>   
                    <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                    <div class="select-box" >
                      <select class="branch-cls" name="class" id="class" >
                        <option value="none">-- Select--</option>
                      </select>
                    </div>
                    </div>
                    @if (Session::has('class'))
                        <label class="error" for="class">
                        <span class="text-danger">
                            {{ Session::get('class') }}
                        </span>
                        </label>
                    @endif
                    </div>
            </div>
            
            <div class="form-box">
                    <div class="form-text">
                      <h5>Section <span>*</span> :</h5>
                    </div>   
                    <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick form-control">
                    <div class="select-box" >
                      <select class="class-sec" name="section" id="section">
                        <option value="none">-- Select--</option>
                      </select>
                    </div>
                    </div>
                    @if (Session::has('section'))
                    <label class="error" for="section">
                        <span class="text-danger">
                            {{ Session::get('section') }}
                        </span>
                    </label>
                    @endif
                    </div>
            </div>

            <div class="form-text">
                <h5>Permission :</h5><br>
                <input name="permissionMobile" value="1" tabindex="5" type="checkbox" @if($user->Use_Show_Mobile==1) checked="true" @endif><font style="color:white; padding: 10px;">Show Mobile No.</font>
            </div>
    </div>

    @if($user->Use_Type==2)
    <div id="teacher_old_id">
        <div class="form-box">
            <div class="form-text">
              <h5>Branch * :</h5>
            </div>   
            <div class="form-typ-box">
            <div class="btn-group bootstrap-select show-tick form-control">

            <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="teacherBranch" required id="teacherBranchId">
                <option value="none">-- Select --</option>
                        @foreach($branch as $data)
                            <option value="{{ $data->Brn_Id }}" @if($teacherData->Cla_Bra_Id == $data->Brn_Id) selected @endif>{{ $data->Brn_Name }}</option>
                        @endforeach
                    </select>
            </div>
            </div>    
            @if (Session::has('branch'))
                <span class="text-danger">
                    {{ Session::get('branch') }}
                </span>
            @endif
        </div>

        <div class="form-box">
            <div class="form-text">
              <h5>Class <span>*</span> :</h5>
            </div>   
            <div class="form-typ-box">
            <div class="btn-group bootstrap-select show-tick form-control">
            <div class="select-box" >
              <select class="branch-cls" name="class" id="classId" tabindex="2">
                @foreach($tec_class as $data)
                    <option value="{{ $data->Brn_Id.','.$data->Cla_Class }}" @if($data->Cla_Class==$teacherData->Cla_Class) selected @endif>{{ $data->Cla_Class }}</option>
                @endforeach
              </select>
            </div>
            </div>
            @if ($errors->has('class'))
                <span class="text-danger">
                    {{ $errors->first('class') }}
                </span>
            @endif
            </div>
        </div>

        <div class="form-box">
        <div class="form-text">
          <h5>Section <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box">
        <div class="btn-group bootstrap-select show-tick form-control">
        <div class="select-box" >
          <select class="class-sec" name="section" id="sectionId" tabindex="3">
                @foreach($tec_section as $data)
                    <option value="{{ $data->Cla_Section }}" @if($teacherData->Cla_Section==$data->Cla_Section) selected @endif>{{ $data->Cla_Section }}</option>
                @endforeach
          </select>
        </div>
        </div>
        @if ($errors->has('section'))
            <span class="text-danger">
                {{ $errors->first('section') }}
            </span>
        @endif
        </div>
        </div>
        
        <div class="form-text">
            <h5>Permission :</h5><br>
            <input name="permissionMobile" value="1" tabindex="5" type="checkbox" @if($user->Use_Show_Mobile==1) checked="true" @endif><font style="color:white; padding: 10px;">Show Mobile No.</font>
        </div>
    </div>
    @endif

    <div class="form-box">
        <div class="form-text ">
          <h5>Address  :</h5>
        </div>
        <div class="form-typ-box">
        <textarea name="address" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" maxlength="500" tabindex="4">{{ $user->Use_Address }}</textarea>
        @if ($errors->has('address'))
            <label class="error" for="address">
            <span class="text-danger">
                {{ $errors->first('address') }}
            </span>
            </label>
        @endif
        </div>
    </div>

 </div>

  <div class="right-form">

  <div class="form-box">
        <div class="form-text h">
          <h5>City  :</h5>
        </div>   
            <div class="form-typ-box">
              <input type="text" name="city" class="{{ $errors->has('city') ? ' is-invalid' : '' }}" value="{{ $user->Use_City }}"  maxlength="20"  tabindex="5">
                @if ($errors->has('city'))
                    <label class="error" for="city">
                    <span class="text-danger">
                        {{ $errors->first('city') }}
                    </span>
                    </label>
                @endif
            </div>    
    </div>

   <div class="form-box">
        <div class="form-text">
          <h5>State :</h5>
        </div>  
        <div class="form-typ-box">
            <input type="text" name="state" class="{{ $errors->has('state') ? ' is-invalid' : '' }}" value="{{ $user->Use_State }}"  maxlength="20" tabindex="6">
            @if ($errors->has('state'))
                <label class="error" for="state">
                <span class="text-danger">
                    {{ $errors->first('state') }}
                </span>
                </label>
            @endif
        </div>
    </div>
       
    <div class="form-box">
        <div class="form-text ">
          <h5>Country :</h5>
        </div>
        <div class="form-typ-box">
            <input type="text" name="country" class="{{ $errors->has('country') ? ' is-invalid' : '' }}" value="{{ $user->Use_Country }}"  tabindex="7">
            @if ($errors->has('country'))
                <label class="error" for="country">
                <span class="text-danger">
                    {{ $errors->first('country') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
          <h5>Mobile No <span>*</span>:</h5>
        </div> 
        <div class="form-typ-box">
                <input type="text" class="{{ $errors->has('mobile') ? 'is-invalid' : '' }}" name="mobile" value="{{ $user->Use_Mobile_No }}" required onblur="" maxlength="10" id="mobile" tabindex="8" >
            @if($errors->has('mobile'))
                <label class="error" for="mobile">
                <span class="text-danger">
                    {{ $errors->first('mobile') }}
                </span>
                </label>
            @endif
        </div>
    </div>

    <div class="form-box">
        <div class="form-text">
            <h5>Phone No :</h5>
        </div>
            <div class="form-typ-box">
                <input type="text" class=" {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" value="{{ $user->Use_Phone_No }}" maxlength="10" tabindex="9">
                @if($errors->has('phone'))
                 <label class="error" for="phone">
                    <span class="text-danger">
                        {{ $errors->first('phone') }}
                    </span>
                 </label>
                @endif
            </div>
    </div>
    </div>
</div>

<!-- Single Branch and It multiple Class Start-->
    <div class="row">
        <div id="teacehr_class_id" @if(old('userType')==2) style="display: block;" @else style="display: none;" @endif>
        <div class="col-lg-12">
    
        @php $i=0; @endphp
        @foreach($classes as $data)
            <div class="box box-primary">
                <div class="line-1 branch-line">        
                     <div class="mangement-btn user-text">
                      <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                    </div>
                    <div class="user-checkall checkAll {{ 'checkAll_'.$data->Brn_Id }}">
                        <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
                        <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
                    </div>
                </div>
            </div>

            @php
                $classes_details = \App\Model\ClassTbl::where('Cla_Bra_Id',$data->Brn_Id)->get();
            @endphp

            @foreach($classes_details as $detail)
                <div class="branch-box col-md-3">
                   <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class=" display_class css-checkbox {{ 'branch'.$detail->Cla_Id }} {{ 'brn'.$detail->Cla_Bra_Id }} " value="{{ $detail->Cla_Id }}"
                        @foreach($class_select as $right)
                            @if($right->Tac_Cla_Id == $detail->Cla_Id)
                                {{ "checked" }}
                            @endif
                        @endforeach
                        @if($detail->Cla_Bra_Id==$branchAccess)
                        @else
                            disabled="true"
                        @endif
                   ><label class="css-label" for="chckbox{{ $i }}">{{ $detail->Cla_Class }}</label>
                </div>
                @php $i++; @endphp
            @endforeach            

        @endforeach     

        </div>
        </div>
    </div>



<!-- Branch and it class Start End -->
<div style="@if($user->Use_Type=='5') display: block; @else display: none; @endif" id="branch_admin_id">
    <div class="row">
        <div class="col-lg-12">
        <?php $i=1; $zid=""; ?>   
        @foreach($branch as $data)
                <div class="box box-primary">
                @if($data->Zon_Id==$data->Brn_Zon_Id && $data->Zon_Id!=$zid)
                    <?php $zid=$data->Zon_Id; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="zone[]" value="{{ $data->Zon_Id }}">{{ $data->Zon_Name }}</h2>      
                         </div>
                         <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Zon_Id }})">Check All </a>|
                            <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Zon_Id }})">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="branch[]" id="adminchckbox{{ $i }}" class="bra_class css-checkbox {{ 'brn'.$data->Zon_Id }}" value="{{ $data->Brn_Id }}" @foreach($user_right as $right)
                                        @if($right->Usr_Bra_Id == $data->Brn_Id)
                                        {{ "checked" }}
                                        @endif
                                    @endforeach
                           ><label class="css-label" for="adminchckbox{{ $i }}">{{ $data->Brn_Name }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
    </div>
</div>

@if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5 || Auth::user()->Use_Type==2)
    <div class="row">
        <div class="col-lg-12">
                <div class="box box-primary">
                    @foreach($menu as $data)
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title"><b><label><input type="hidden" name="menu[]" value="{{ $data->Mod_Id }}">{{ $data->Mod_Name }}</label></b></h3>
                    </div> -->
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                        <h2 class="box-title"><b><input type="hidden" name="menu[]" value="{{ $data->Mod_Id }}">{{ $data->Mod_Name }}</b></h2>
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAllMod(true,{{ $data->Mod_Id }})">Check All</a>|
                            <a href="javascript:void(0);" onclick="unselectAllMod(false,{{ $data->Mod_Id }})">UnCheck All</a>
                        </div>
                    </div>
                    @endforeach
                    <div class="branch-box">
                    <?php $i=1; ?>
                    @foreach($module as $data)
                        <div class="col-md-3">
                            <input type="checkbox" name="module[]" id="ckbox{{ $i }}" class="mod_class css-checkbox {{ 'mod'.$data->Mod_Parent_Id }}" value="{{ $data->Mod_Id }}" @foreach($user_right as $right)
                                @if($right->Usr_Mod_Id == $data->Mod_Id)
                                {{ "checked" }}
                                @endif
                            @endforeach
                                ><label for="ckbox{{ $i }}" class="css-label" >{{ $data->Mod_Name }}</label>
                        </div>
                    <?php $i++; ?>
                    @endforeach
                    </div>
                </div>
            </div>
    </div>
    @endif
    <div class="row">
            <div class="form-btn">
                <input type="submit" value="save" tabindex="10" id="submitBtn"></input><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="11"  ></a>
            </div>
    </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
    $("#submitBtn").on('click',function(e){
        var userType = $("#userType").val();
        var n =$('.mod_class:checkbox:checked').length;
        var editUserType = $("#user_type").val();
        if(userType=="none"){
            alert("Please select user type");
            $("#userType").focus();
            e.preventDefault();
        }else if(type == 2 && editUserType!=2){
            var branchVal = $("#teacherBranch").val();
            var classVal = $("#class").val();
            var sectionVal = $("#section").val();
            if(branchVal == "none"){
                alert("Please select branch");
                $("#teacherBranch").focus();
                e.preventDefault();
            }else if(classVal == "none"){
                alert("Please select class");
                $("#class").focus();
                e.preventDefault();
            }else if(sectionVal == "none"){
                alert("Please select section");
                $("#section").focus();
                e.preventDefault();
            }else if(n==0){
                alert("Please select atleast one master group.");
                e.preventDefault();
            }
        }else if(userType == 2){
            var branchVal = $("#teacherBranchId").val();
            var classVal = $("#classId").val();
            var sectionVal = $("#sectionId").val();
            if(branchVal == "none"){
                alert("Please select branch");
                $("#teacherBranch").focus();
                e.preventDefault();
            }else if(classVal == "none"){
                alert("Please select class");
                $("#class").focus();
                e.preventDefault();
            }else if(sectionVal == "none"){
                alert("Please select section");
                $("#section").focus();
                e.preventDefault();
            }else if(n==0){
                alert("Please select atleast one master group.");
                e.preventDefault();
            }
        }else if(userType == 5){
            var brnRight =$('.bra_class:checkbox:checked').length;
            if(brnRight==0){
                alert("Please select atleast one branch.");
                e.preventDefault();
            }else if(n==0){
                alert("Please select atleast one master group.");
                e.preventDefault();
            }
        }
    })
});
function selectAll(bool,ids)
{
    $("input.brn"+ids).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAll(bool,ids)
{
    $("input.brn"+ids).each(function(i){
        $(this).prop('checked', false);
    });   
}
function selectAllMod(bool,ids)
{
    $("input.mod"+ids).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAllMod(bool,ids)
{
    $("input.mod"+ids).each(function(i){
        $(this).prop('checked', false);
    });   
}
function checkSpace(txtval){
    if(txtval.value.indexOf(' ')>=0){
        txtval.value="";
        txtval.focus();
        alert("Space Not Allow In "+txtval.name);
    }
}
function checkMobileNo()
  {
    var mobileNo = $("#mobile").val();
    if(mobileNo.length<10){
      if(mobileNo.indexOf(' ')>=0){
        alert("Space Not Allow in mobile number.");
      }else{
        alert("Please enter valid mobile number.");
      }
      $("#mobile").val("");
      $("#mobile").focus();
      return false;
    }else{
        if(mobileNo.indexOf(' ')>=0){
            alert("Space Not Allow in mobile number.");
            $("#mobile").val("");
            $("#mobile").focus();
        }
        if(isNaN(mobileNo)){
            alert("Allow Only Digits.");
            $("#mobile").val("");
            $("#mobile").focus();
            return false;
        }
    }
  }

$(document).ready(function(){
    var type = $("#userType").val();
    if(type==2){
            $("#teacherDetail").css('display','none');
            $("#branch_admin_id").css('display','none');
            $("#teacehr_class_id").css('display','block');
        }else if(type==5){
            $("#branch_admin_id").css('display','block');
            $("#teacherDetail").css('display','none');
            $("#teacehr_class_id").css('display','none');
            $("#teacher_old_id").css('display','none');
        }else{
            $("#branch_admin_id").css('display','none');
            $("#teacherDetail").css('display','none');
            $("#teacehr_class_id").css('display','none');
            $("#teacher_old_id").css('display','none');
        }

    $("#userType").on('change',function(){
        var type = $("#userType").val();
        if(type==2){
            $("#teacherDetail").css('display','block');
            $("#teacehr_class_id").css('display','block');
            $("#branch_admin_id").css('display','none');
        }else if(type==5){
            $("#branch_admin_id").css('display','block');
            $("#teacherDetail").css('display','none');
            $("#teacehr_class_id").css('display','none');
            $("#teacher_old_id").css('display','none');
        }else{
            $("#branch_admin_id").css('display','none');
            $("#teacherDetail").css('display','none');
            $("#teacehr_class_id").css('display','none');
            $("#teacher_old_id").css('display','none');
        }
    });

        $("#teacherBranch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#class").append('<option value="none">-- Select--</option>');
                     $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
            });
        });

        $("#teacherBranchId").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                    $("#classId option").each(function() {
                        $(this).remove();
                    });
                    $("#sectionId option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#classId").append('<option value="none">-- Select--</option>');
                     $("#sectionId").append('<option value="none">-- Select --</option>');
                    $("#classId").append(items);
                },
            error : function(error){
                console.log(error);
            }
            });
        });

        $("#class").on('change',function(){
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            var userType = $("#userType").val();
            var editUserType = $("#user_type").val();
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
            });
        }); 

        $("#classId").on('change',function(){
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            var userType = $("#userType").val();
            var editUserType = $("#user_type").val();
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $("#sectionId option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#sectionId").append('<option value="none">-- Select --</option>');
                        $("#sectionId").append(items);
                },
                error :function(error){
                    console.log(error);
                }
            });
        }); 

        $("#sectionId").on('change',function(){
            console.log($("#sectionId").val());
        var branchId = $("#teacherBranchId").val();
        var classIdName = $("#classId").val();
        var section = $("#sectionId").val();
        if(type == 2){
            $.ajax({
                url : "{{ url('getClassId') }}",
                type : "POST",
                data : {
                    _token: '{{ csrf_token() }}',
                    branch : branchId,
                    classIdName : classIdName,
                    section : section
                },success : function(data){
                    var cls_id = data;
                    $(".branch"+cls_id).prop("checked", true);
                    $(".branch"+cls_id).prop("readonly", true);
                },error : function(error){

                }
            });
        }
        // console.log("branch = "+branchId);
        // console.log("classIdName = "+classIdName);
        // console.log("section = "+section);
    });
});

</script>
<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>

@endsection

@section('footer')

@section('footer_link_and_scripts')