<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Student;
use App\Model\Event;
use App\Model\Count;
use Auth;
use Input;
use Validator;
use DB;

class EventservicesController extends Controller
{
  //Event Creation
	public function create_Event(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if($request->file('image')){
	            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $isUpload = $request->file('image')->move(public_path('images/event'), $imageName);
	        }else{
	            $imageName = "";
	        }

           $flag = Input::get('flag');

	        $unique_id = "EVENT".time();

	        $event = new Event;
	       	//$event->Eve_Use_Id = Input::get('user_id');
	       	$event->Eve_Unique_Id = $unique_id;
			$event->Eve_Name = Input::get('title');
			$event->Eve_Date = Input::get('start_date');
			$event->Eve_End_Date = Input::get('end_date');
			$event->Eve_Description = Input::get('description');
			$event->Eve_Image = $imageName;
            $event->Eve_Rsvp = $flag;
			$event->Eve_Brn_Id = Input::get('branch_id');
			$event->Eve_Cla_Id = Input::get('class_id');
			$event->Eve_Cla_Section	= Input::get('section');
			$event->Eve_CreatedBy = Input::get('user_id');
			$event->Eve_CreatedAt = date('Y-m-d');
			$event->Eve_UpdatedBy = Input::get('user_id');
			$event->Eve_UpdatedAt = date('Y-m-d');
			$event->save();
			$event->Eve_Id;
			
			if($event){
				ResponseMessage::success("Event Create Successfully",$event);
			}else{
				ResponseMessage::error("Failed to Event create");
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Event Updation
	public function update_Event(Request $request)
	{
		try{
			$rules = [
				'user_id' => 'required',
				'event_id' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$image = Event::where('Eve_Id',Input::get('event_id'))->first();

			if($request->file('image')){
	            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $isUpload = $request->file('image')->move(public_path('images/event'), $imageName);
	        }else{
	            $imageName = $image->Eve_Image;
	        }

           $flag = Input::get('flag');
	        	
	        $unique_id = Event::where('Eve_Id',Input::get('event_id'))->first()->Eve_Unique_Id;

	        //$event['Eve_Use_Id'] = Input::get('user_id');
			$event['Eve_Name'] = Input::get('title');
			$event['Eve_Unique_Id'] = $unique_id;
			$event['Eve_Date'] = Input::get('start_date');
			$event['Eve_End_Date'] = Input::get('end_date');
			$event['Eve_Description'] = Input::get('description');
            $event['Eve_Rsvp'] = $flag;
			$event['Eve_Image'] = $imageName;
			$event['Eve_UpdatedBy'] = Input::get('user_id');
			$event['Eve_UpdatedAt'] = date('Y-m-d');

			$update = Event::where('Eve_Unique_Id',$unique_id)->update($event);
			
			if($update){
				ResponseMessage::success("Event Updated Successfully",$event);
			}else{
				ResponseMessage::error("No Event Any Changes");
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	/**
	   * 
	   * In this event_List api to dispaly all event.which approved by admin 
	   *
	   * @author Jayesh Sukhadiya
	   *
	   * @param integer user_id =  User Id
	   * @return JSON Event List Display ,Count Display and Accept and Decline flag
	   */

	//Event List For COUNT
	public function event_List(Request $request)
	{
		try{

			$rules = [
				'user_id' => 'required',
				'branch_id' => 'required',
				'class_id' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$user_id = Input::get('user_id');
			$branch_id = Input::get('branch_id');
			$class_id = Input::get('class_id');

			if(Student::where('Std_Parent_Id',Input::get('user_id'))->exists()){

			$event = DB::table('event_tbl')
					->select(array(
					DB::raw("event_tbl.*"),
					DB::raw("(SELECT COUNT(count_tbl.Cnt_Eve_Id) FROM count_tbl WHERE count_tbl.Cnt_Eve_Id = event_tbl.Eve_Id AND Cnt_Acc_Dec = 1 GROUP BY count_tbl.Cnt_Eve_Id) as Eve_Count"),
					DB::raw("(SELECT max(count_tbl.Cnt_Acc_Dec) FROM count_tbl WHERE count_tbl.Cnt_Eve_Id = event_tbl.Eve_Id AND count_tbl.Cnt_User_Id = '".$user_id."' GROUP BY count_tbl.Cnt_Eve_Id) as Eve_Accept")
					))
					->where('event_tbl.Eve_Approve',1)
					->where('event_tbl.Eve_Brn_Id',$branch_id)
					->where('event_tbl.Eve_Cla_Id',$class_id)
					->orderBy('Eve_Id', 'DESC')
	               	->get();

				if($event)
				{
					ResponseMessage::success("Event List",$event);
				}else
				{
					ResponseMessage::error("Events Not Found");
				}
			}else if(User::where('Use_Id',Input::get('user_id'))->where('Use_Type',2)->exists())
			{	
				$event = DB::table('event_tbl')
					->select(array(
					DB::raw("event_tbl.*"),
					DB::raw("(SELECT COUNT(count_tbl.Cnt_Eve_Id) FROM count_tbl WHERE count_tbl.Cnt_Eve_Id = event_tbl.Eve_Id AND Cnt_Acc_Dec = 1 GROUP BY count_tbl.Cnt_Eve_Id) as Eve_Count"),
					DB::raw("(SELECT max(count_tbl.Cnt_Acc_Dec) FROM count_tbl WHERE count_tbl.Cnt_Eve_Id = event_tbl.Eve_Id AND count_tbl.Cnt_User_Id = '".$user_id."' GROUP BY count_tbl.Cnt_Eve_Id) as Eve_Accept")
					))
					->where('event_tbl.Eve_Approve',1)
					->where('event_tbl.Eve_Brn_Id',$branch_id)
					->where('event_tbl.Eve_Cla_Id',$class_id)
					->orderBy('Eve_Id', 'DESC')
	               	->get();

				if($event)
				{
					ResponseMessage::success("Event List",$event);
				}else
				{
					ResponseMessage::error("Events Not Found");
				}
			}
			else{
				ResponseMessage::error("Events Not Found");	
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//User Profile Acceptance 
	public function event_accept_profile(Request $request)
	{
		try{
			$eve_user = Count::with('Cnt_user')
				->where(function($query){
						$query->where('Cnt_Eve_Id',Input::get('event_id'));
						$query->where('Cnt_Acc_Dec',1);
					})->get();
				
				if($eve_user)
				{
					ResponseMessage::success("User Event List Record",$eve_user);
				}else
				{
					ResponseMessage::error("No Event Available");
				}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	/**
	   * 
	   * Accept/Decline Event by user 
	   *
	   * @author Jayesh Sukhadiya
	   *
	   * @param integer user_id =  User Id
	   * @param integer event_id = Event Id
	   * @return JSON Event Accept / Decline 
	   * Note : Accept = 0 and Decline = 1
	   */

	//User Accept/ Decline Event
	public function accept_decline_event(Request $request)
	{
		try {
			$rules = [
				'user_id' => 'required',
				'event_id' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$event = Count::where('Cnt_User_Id',Input::get('user_id'))->where('Cnt_Eve_Id',Input::get('event_id'))->first();

			if($event){
				if($event->Cnt_Acc_Dec == 1){
					ResponseMessage::success("Accept",$event);
				}else if($event->Cnt_Acc_Dec == 0)
					ResponseMessage::success("Decline",$event);
			}else
			{
				ResponseMessage::error("Accept");
			}
		}catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Event Accept
	public function event_Accept(Request $request)
	{
		try{
			$rules = [
				'event_id' => 'required',
				'user_id' => 'required',
				'flag' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$cnt = Count::get();
			$user_id = Count::where('Cnt_User_Id',Input::get('user_id'))->get();
			$event_id = Count::where('Cnt_Eve_Id',Input::get('event_id'))->get();

			if($user_id->isEmpty() || $event_id->isEmpty()){
				$count = new Count;
		        $count->Cnt_User_Id = Input::get('user_id');
				$count->Cnt_Eve_Id = Input::get('event_id');
				$count->Cnt_Acc_Dec = Input::get('flag');
				$count->Cnt_CreatedAt = date('Y-m-d');
				$count->Cnt_UpdatedAt = date('Y-m-d');

				if($count->save()){
					ResponseMessage::success("Event Accepte Successfully",$count);
				}else{
					ResponseMessage::error("Event Acceptance Fail");
				}
			}else if($user_id->isEmpty() && $event_id->isEmpty()){
				$count = new Count;
		        $count->Cnt_User_Id = Input::get('user_id');
				$count->Cnt_Eve_Id = Input::get('event_id');
				$count->Cnt_Acc_Dec = Input::get('flag');
				$count->Cnt_CreatedAt = date('Y-m-d');
				$count->Cnt_UpdatedAt = date('Y-m-d');

				if($count->save()){
					ResponseMessage::success("Event Accepte Successfully",$count);
				}else{
					ResponseMessage::error("Event Acceptance Fail");
				}
			}else{
				foreach ($cnt as $value){
					if(Input::get('user_id') == $value['Cnt_User_Id'] && Input::get('event_id') == $value['Cnt_Eve_Id']){
						if($value['Cnt_Acc_Dec'] == 1  && Input::get('flag') == 1){
							ResponseMessage::success("Event Already Accepted",$value);
							break;
						}

						}else if($value['Cnt_Acc_Dec'] == 0 && Input::get('flag') == 1){
							$count['Cnt_Acc_Dec'] = Input::get('flag');
							$count['Cnt_UpdatedAt']  = date('Y-m-d');
							$update = Count::where('Cnt_User_Id','=',Input::get('user_id'))->Where('Cnt_Eve_Id','=',Input::get('event_id'))->update($count);

							if($update){
								ResponseMessage::success("Event Accepted",$value);
							}else{
								ResponseMessage::error("Event Already Accepted");
								break;
							}
						}
					}
				}
		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
	}

	//Event Decline
	public function event_Decline(Request $request)
	{
		try{
			$rules = [
				'event_id' => 'required',
				'user_id' => 'required',
				'flag' => 'required',
				'reason' => 'required',
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$cnt = Count::get();
			$user_id = Count::where('Cnt_User_Id',Input::get('user_id'))->get();
			$event_id = Count::where('Cnt_Eve_Id',Input::get('event_id'))->get();

			if($user_id->isEmpty() || $event_id->isEmpty()){
				$count = new Count;
		        $count->Cnt_User_Id = Input::get('user_id');
				$count->Cnt_Eve_Id = Input::get('event_id');
				$count->Cnt_Acc_Dec = Input::get('flag');
				$count->Cnt_Reason = Input::get('reason');
				$count->Cnt_CreatedAt = date('Y-m-d');
				$count->Cnt_UpdatedAt = date('Y-m-d');

				if($count->save()){
					ResponseMessage::success("Event Declined Successfully",$count);
				}else{
					ResponseMessage::error("Event Declined Fail");
				}
			}else if($user_id->isEmpty() && $event_id->isEmpty()){
				$count = new Count;
		        $count->Cnt_User_Id = Input::get('user_id');
				$count->Cnt_Eve_Id = Input::get('event_id');
				$count->Cnt_Acc_Dec = Input::get('flag');
				$count->Cnt_Reason = Input::get('reason');
				$count->Cnt_CreatedAt = date('Y-m-d');
				$count->Cnt_UpdatedAt = date('Y-m-d');

				if($count->save()){
					ResponseMessage::success("Event Declined Successfully",$count);
				}else{
					ResponseMessage::error("Event Declined Fail");
				}
			}else{
				foreach ($cnt as $value){
					if(Input::get('user_id') == $value['Cnt_User_Id'] && Input::get('event_id') == $value['Cnt_Eve_Id']){
						if($value['Cnt_Acc_Dec'] == 1  && Input::get('flag') == 1){
							ResponseMessage::success("Event Already Accepted",$value);
						}

						}else if($value['Cnt_Acc_Dec'] == 0 && Input::get('flag') == 0){
							ResponseMessage::success("Event Already Decline",$value);

						}else if($value['Cnt_Acc_Dec'] == 1 && Input::get('flag') == 0){
							$count['Cnt_Acc_Dec'] = Input::get('flag');
							$count['Cnt_UpdatedAt']  = date('Y-m-d');
							$count['Cnt_Reason']  = Input::get('reason');
							$update = Count::where('Cnt_User_Id','=',Input::get('user_id'))->Where('Cnt_Eve_Id','=',Input::get('event_id'))->update($count);

							if($update){
								ResponseMessage::success("Event Declined",$value);
							}else{
								ResponseMessage::error("Event Declination Fail");
							}

						}else if($value['Cnt_Acc_Dec'] == 0 && Input::get('flag') == 1){
							$count['Cnt_Acc_Dec'] = Input::get('flag');
							$count['Cnt_UpdatedAt']  = date('Y-m-d');
							$count['Cnt_Reason']  = "";
							$update = Count::where('Cnt_User_Id','=',Input::get('user_id'))->Where('Cnt_Eve_Id','=',Input::get('event_id'))->update($count);

							if($update){
								ResponseMessage::success("Event Declined",$value);
							}else{
								ResponseMessage::error("Event Declined Fail");
							}
						}
					}
				}
		}catch(\Exception $e) {
			Exceptions::exception($e);
		}
	}
  
    public function event_Monthwise_List(Request $request)
	{
		try 
		{
			$rules = [
				'month' => 'required',
				'user_id' => 'required',
				'branch_id' => 'required',
				'class_id' => 'required'
				];

			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$month = Input::get('month');
			$user_id = Input::get('user_id');
			$branch_id = Input::get('branch_id');
			$class_id = Input::get('class_id');

			$eventList = DB::table('event_tbl')
					->select(array(
					DB::raw("event_tbl.*"),
					DB::raw("(SELECT COUNT(count_tbl.Cnt_Eve_Id) FROM count_tbl WHERE count_tbl.Cnt_Eve_Id = event_tbl.Eve_Id AND Cnt_Acc_Dec = 1 GROUP BY count_tbl.Cnt_Eve_Id) as Eve_Count"),
					DB::raw("(SELECT max(count_tbl.Cnt_Acc_Dec) FROM count_tbl WHERE count_tbl.Cnt_Eve_Id = event_tbl.Eve_Id AND count_tbl.Cnt_User_Id = '".$user_id."' GROUP BY count_tbl.Cnt_Eve_Id) as Eve_Accept")
					))
					->where('event_tbl.Eve_Approve',1)
					->whereRaw('MONTH(Eve_Date) = ?',$month)
					->where('event_tbl.Eve_Brn_Id',$branch_id)
					->where('event_tbl.Eve_Cla_Id',$class_id)
					->orderBy('Eve_Id', 'DESC')
	               	->get();


			if($eventList)
			{
				ResponseMessage::success("Event Monthwise List",$eventList);
			}else{
				ResponseMessage::error("Record Not Found");
			}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
}
