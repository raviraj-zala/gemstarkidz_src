@extends('auth.Master.master')
@section('title','Create User')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="">Create User</h2>      
    </div>
</div>
@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif
<div class="form-section">
    <form id="createUser" name="create-user" role="form" method="POST" action="{{ url('save_user') }}" enctype="multipart/form-data" >
            {{ csrf_field() }}
 <div class="left-form">
    <div class="form-box">
        <div class="form-text">
            <h5>Name <span>*</span> :</h5>
        </div>
        <div class="form-typ-box">
            <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus tabindex="1" value="{{ old('name') }}" maxlength="50">
        </div>
            @if ($errors->has('name'))
                <span class="text-danger">
                    {{ $errors->first('name') }}
                </span>
            @endif
    </div>
     <div class="form-box">
                <div class="form-text">
                    <h5>Email <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" required tabindex="2" value="{{ old('email') }}" maxlength="60">
                @if ($errors->has('email'))
                    <label class="error" for="email">
                    <span class="text-danger">
                        {{ $errors->first('email') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>
    <div class="form-box">
            <div class="form-text">
              <h5>Password <span>*</span> :</h5>
            </div> 
            <div class="form-typ-box">
                <input type="password" name="password" class=" {{ $errors->has('password') ? ' is-invalid' : '' }}" required tabindex="3" id="password" maxlength="20">
            <div id="passwordError"></div>
            @if ($errors->has('password'))
                <label class="error" for="password">
                    <span class="text-danger">
                    {{ $errors->first('password') }}
                </span>
                </label>
            @endif
            </div>
        </div>
        <div class="form-box">
            <div class="form-text">
              <h5>user type <span>*</span> :</h5>
            </div>   
            <div class="form-typ-box select-text">
        <div class="btn-group bootstrap-select show-tick form-control">
                <select tabindex="4" class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('userType') ? 'is-invalid' : '' }}" name="userType" required id="userType">
                    <option value="none">-- Select -- </option>
                    @if(Auth::user()->Use_Type=="1" )<option value="1" @if(old('userType')==1) selected="true" @endif>Admin</option>@endif
                    @if(Auth::user()->Use_Type=="1" )<option value="5" @if(old('userType')==5) selected="true" @endif>Branch Admin</option>@endif
                    <option value="2" @if(old('userType')==2) selected="true" @endif>Teacher</option>
                    <option value="3" @if(old('userType')==3) selected="true" @endif>Driver</option>
                </select>
        </div>
                @if (Session::has('userType'))
                <label class="error" for="email">
                    <span class="text-danger">
                        {{ Session::get('userType') }}
                    </span>
                    </label>
                @endif  
        </div>
        </div>
        
        <div id="teacherDetail" @if(old('userType')==2) style="display: block;" @else style="display: none;" @endif>
        <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}"  name="teacherBranch" required id="teacherBranch">
                    <option value="none">-- Select --</option>
                            @foreach($branch as $data)
                                <option value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</option>
                            @endforeach
                        </select>
                </div>
                @if (Session::has('teacherBranch'))
                    <label class="error" for="teacherBranch">
                    <span class="text-danger">
                        {{ Session::get('teacherBranch') }}
                    </span>
                    </label>
                @endif
                </div>    
        </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" >
                    <option value="none">-- Select--</option>
                  </select>
                </div>
                </div>
                @if (Session::has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        {{ Session::get('class') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>
            <div class="form-box">
                <div class="form-text">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="class-sec" name="section" id="section" >
                    <option value="none">-- Select--</option>
                  </select>
                </div>
                </div>
                @if (Session::has('section'))
                    <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>
            <div class="form-text">
                <h5>Permission :</h5><br>
                <input name="permissionMobile" value="1" tabindex="5" type="checkbox" @if(old('permissionMobile')) checked="true" @endif><font style="color:white; padding: 10px;">Show Mobile No.</font>
            </div>
        </div>
    </div>
    <div class="right-form">
        <div class="form-box">
            <div class="form-text">
              <h5>City :</h5>
            </div>  
            <div class="form-typ-box">
                <input type="text" name="city" class=" {{ $errors->has('city') ? ' is-invalid' : '' }}"  tabindex="5" value="{{ old('city') }}" maxlength="20">
                @if ($errors->has('city'))
                    <label class="error" for="city">
                    <span class="text-danger">
                        {{ $errors->first('city') }}
                    </span>
                    </label>
                @endif
            </div>
        </div>
        <div class="form-box">
            <div class="form-text">
              <h5>State :</h5>
            </div>  
            <div class="form-typ-box">
                <input type="text" name="state" class=" {{ $errors->has('state') ? ' is-invalid' : '' }}" tabindex="6" value="{{ old('state') }}" maxlength="20">
                @if ($errors->has('state'))
                    <label class="error" for="state">
                    <span class="text-danger">
                        {{ $errors->first('state') }}
                    </span>
                    </label>
                @endif
            </div>
        </div>
        <div class="form-box">
            <div class="form-text ">
              <h5>Country :</h5>
            </div>
            <div class="form-typ-box">
                <input type="text" name="country" class="{{ $errors->has('country') ? ' is-invalid' : '' }}" tabindex="7" value="{{ old('country') }}"  maxlength="20">
            @if ($errors->has('country'))
                <label class="error" for="country">
                <span class="text-danger">
                    {{ $errors->first('country') }}
                </span>
                </label>
            @endif
            </div>
        </div>
        <div class="form-box">
            <div class="form-text ">
              <h5>Mobile No <span>*</span>:</h5>
            </div> 
            <div class="form-typ-box">
                    <input type="text" class="{{ $errors->has('mobile') ? 'is-invalid' : '' }}" name="mobile" tabindex="8" value="{{ old('mobile') }}" required id="mobile" onblur="" maxlength="10" >
                @if($errors->has('mobile'))
                <label class="error" for="mobile">
                    <span class="text-danger">
                        {{ $errors->first('mobile') }}
                    </span>
                </label>
                @endif
            </div>
        </div>
        <div class="form-box">
            <div class="form-text ">
                <h5>Phone No :</h5>
            </div>
                <div class="form-typ-box">
                    <input type="text" class="{{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" tabindex="9" value="{{ old('phone') }}" maxlength="10">
                    @if($errors->has('phone'))
                        <label class="error" for="phone">
                        <span class="text-danger">
                            {{ $errors->first('phone') }}
                        </span>
                        </label>
                    @endif
                </div>
        </div>
        <div class="form-box">
            <div class="form-text">
              <h5>Address :</h5>
            </div>
            <div class="form-typ-box">
            <textarea name="address" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" rows="5" tabindex="10" maxlength="500">{{ old('address') }}</textarea></div>
            @if ($errors->has('address'))
            <label class="error" for="address">
                <span class="text-danger">
                    {{ $errors->first('address') }}
                </span>
                </label>
            @endif
        </div>
    </div>
</div>

    <!-- Single Branch and It multiple Class Start-->
    <div class="row">
        <div id="teacehr_class_id" @if(old('userType')==2) style="display: block;" @else style="display: none;" @endif>
        <div class="col-lg-12">
    
        @php $i=0; @endphp
        @foreach($classes as $data)
            <div class="box box-primary">
                <div class="line-1 branch-line">        
                     <div class="mangement-btn user-text">
                      <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                    </div>
                    <div class="user-checkall checkAll {{ 'checkAll_'.$data->Brn_Id }}">
                        <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
                        <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
                    </div>
                </div>
            </div>

            @php
                $classes_details = \App\Model\ClassTbl::where('Cla_Bra_Id',$data->Brn_Id)->get();
            @endphp

            @foreach($classes_details as $detail)
                <div class="branch-box col-md-3">
                    <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class=" display_class css-checkbox {{ 'branch'.$detail->Cla_Id }} {{ 'brn'.$detail->Cla_Bra_Id }} " value="{{ $detail->Cla_Id }}" @if(old('branch')) @foreach(old('branch') as $b) @if($b == $detail->Cla_Bra_Id ) checked="true" @endif @endforeach @endif >
                    <label class="css-label" for="chckbox{{ $i }}">{{ $detail->Cla_Class }}</label>
                </div>
                @php $i++; @endphp
            @endforeach            
        @endforeach     

        </div>
        </div>
    </div>
    <!-- Single Branch and It multiple Class End-->
    <!-- Admin Branch Select Multiple Start-->
    <div class="row">
        <div id="branch_admin_id" @if(old('userType')==5) style="display: block;" @else style="display: none;" @endif>
        <div class="col-lg-12">
        <?php $i=1; $zid=""; $bid=""; ?>   
        @foreach($branch as $data)
                <div class="box box-primary">
                @if($data->Zon_Id==$data->Brn_Zon_Id && $data->Zon_Id!=$zid)
                    <?php $zid=$data->Zon_Id; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="zone[]" value="{{ $data->Zon_Id }}">{{ $data->Zon_Name }}</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Zon_Id }})">Check All </a>|
                            <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Zon_Id }})">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="branch[]" id="chckboxadmin{{ $i }}" class="bra_class css-checkbox {{ 'brn'.$data->Zon_Id }}" value="{{ $data->Brn_Id }}"
                           @if(old('branch'))
                                @foreach(old('branch') as $b)
                                    @if($b == $data->Brn_Id )
                                        checked="true"
                                    @endif
                                @endforeach
                           @endif
                           ><label class="css-label" for="chckboxadmin{{ $i }}">{{ $data->Brn_Name }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
        </div>
    </div>
 <!-- Admin Branch Select Multiple End-->
    <div class="row">
        <div class="col-lg-12">
                <div class="box box-primary">
                    @foreach($menu as $data)
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                        <h2 class="box-title"><b><input type="hidden" name="menu[]" value="{{ $data->Mod_Id }}">{{ $data->Mod_Name }}</b></h2>
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAllMod(true,{{ $data->Mod_Id }})">Check All</a>|
                            <a href="javascript:void(0);" onclick="unselectAllMod(false,{{ $data->Mod_Id }})">UnCheck All</a>
                        </div>
                    </div>
                    @endforeach
                    <div class="branch-box">
                    <?php $i=1; ?>
                    @foreach($module as $data)
                        <div class="col-md-3">
                            <input type="checkbox" name="module[]" id="ckbox{{ $i }}" class="mod_class css-checkbox {{ 'mod'.$data->Mod_Parent_Id }}" value="{{ $data->Mod_Id }}"
                            @if(old('module'))
                                @foreach(old('module') as $m)
                                    @if($m == $data->Mod_Id )
                                        checked="true"
                                    @endif
                                @endforeach
                           @endif
                            ><label for="ckbox{{ $i }}" class="css-label">{{ $data->Mod_Name }}</label>
                        </div>
                    <?php $i++; ?>
                    @endforeach
                    </div>
                </div>
        </div>
    </div>

    <div class="row">
            <div class="form-btn">
                <input type="submit" value="save" tabindex="10" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="11"  ></a>
            </div>
    </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
    $("#submitBtn").on('click',function(e){
        var userType = $("#userType").val();
        var pwd = $("#password").val();
        var n =$('.mod_class:checkbox:checked').length;
        if(userType=="none"){
            alert("Please select user type");
            $("#userType").focus();
            e.preventDefault();
        }else if(userType == 2){
            var branchVal = $("#teacherBranch").val();
            var classVal = $("#class").val();
            var sectionVal = $("#section").val();
            if(branchVal == "none"){
                alert("Please select branch");
                $("#teacherBranch").focus();
                e.preventDefault();
            }else if(classVal == "none"){
                alert("Please select class");
                $("#class").focus();
                e.preventDefault();
            }else if(sectionVal == "none"){
                alert("Please select section");
                $("#section").focus();
                e.preventDefault();
            }else if(n==0){
                alert("Please select atleast one master group.");
                e.preventDefault();
            }
        }else if(userType == 5){
            var brnRight =$('.bra_class:checkbox:checked').length;
            if(brnRight==0){
                alert("Please select atleast one branch.");
                e.preventDefault();
            }else if(n==0){
                alert("Please select atleast one master group.");
                e.preventDefault();
            }
        }else if(pwd.length<8){
            alert("Password leangth menimum 8 character");
            $("#password").focus();
            e.preventDefault();
        }
    })
    $("#password").on("blur",function(){
        var pwd = $("#password").val();
        if(pwd.length<8){
            $("#passwordError").html("<label class='error'><span class='text-danger'>Password leangth menimum 8 character.</span></label>");
            $("#password").focus();
        }else{
            $("#passwordError").html("");
        }
    });
    $("#section").on('change',function(){
        var branchId = $("#teacherBranch").val();
        var classIdName = $("#class").val();
        var section = $("#section").val();
        $.ajax({
            url : "{{ url('getClassId') }}",
            type : "POST",
            data : {
                _token: '{{ csrf_token() }}',
                branch : branchId,
                classIdName : classIdName,
                section : section
            },success : function(data){
                var cls_id = data;
                $(".branch"+cls_id).prop("checked", true);
                // $(".branch"+cls_id).prop("disabled", true);
            },error : function(error){

            }
        });
        // console.log("branch = "+branchId);
        // console.log("classIdName = "+classIdName);
        // console.log("section = "+section);
    });
});

function selectAll(bool,ids)
{
    $("input.brn"+ids).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAll(bool,ids)
{
    $("input.brn"+ids).each(function(i){
        $(this).prop('checked', false);
    });   
}
function selectAllMod(bool,ids)
{
    $("input.mod"+ids).each(function(i){
        $(this).prop('checked', true);
    });
}
function unselectAllMod(bool,ids)
{
    $("input.mod"+ids).each(function(i){
        $(this).prop('checked', false);
    });   
}
function checkSpace(txtval){
    if(txtval.value.indexOf(' ')>=0){
        txtval.value="";
        txtval.focus();
        alert("Space Not Allow In "+txtval.name);
    }
}
function checkLength(pass) {
  if (pass.value.length < 8) {
    document.getElementById("password").value="";
    document.getElementById("password").focus();
    alert("Password Length Minimum 8 characters..");
  }else{
    if(pass.value.indexOf(' ')>=0){
        document.getElementById("password").value="";
        alert("Space Not Allow In Password.");
    }
  }
}
function checkMobileNo()
  {
    var mobileNo = $("#mobile").val();
    if(mobileNo.length<10){
      if(mobileNo.indexOf(' ')>=0){
        alert("Space Not Allow in mobile number.");
      }else{
        alert("Please enter valid mobile number.");
      }
      $("#mobile").val("");
      $("#mobile").focus();
      return false;
    }else{
        if(mobileNo.indexOf(' ')>=0){
            alert("Space Not Allow in mobile number.");
            $("#mobile").val("");
            $("#mobile").focus();
        }
        if(isNaN(mobileNo)){
            alert("Allow Only Digits.");
            $("#mobile").val("");
            $("#mobile").focus();
            return false;
        }
    }
  }
  
$(document).ready(function(){
    var type = $("#userType").val();
    if(type==2){
            $("#teacherDetail").css('display','none');
            $("#branch_admin_id").css('display','none');
            $("#teacehr_class_id").css('display','block');
        }else if(type==5){
            $("#branch_admin_id").css('display','block');
            $("#teacherDetail").css('display','none');
            $("#teacehr_class_id").css('display','none');
            $("#teacher_old_id").css('display','none');
        }else{
            $("#branch_admin_id").css('display','none');
            $("#teacherDetail").css('display','none');
            $("#teacehr_class_id").css('display','none');
            $("#teacher_old_id").css('display','none');
        }

        $("#userType").on('change',function(){
            var type = $("#userType").val();
            if(type==2){
                $("#teacherDetail").css('display','block');
                $("#teacehr_class_id").css('display','block');
                $("#branch_admin_id").css('display','none');
            }else if(type==5){
                $("#branch_admin_id").css('display','block');
                $("#teacherDetail").css('display','none');
                $("#teacehr_class_id").css('display','none');
                $("#teacher_old_id").css('display','none');
            }else{
                $("#branch_admin_id").css('display','none');
                $("#teacherDetail").css('display','none');
                $("#teacehr_class_id").css('display','none');
                $("#teacher_old_id").css('display','none');
            }
        });

  $("#userType").on('change',function(){
    var type = $("#userType").val();
    if(type==2){
        $("#teacherDetail").css('display','block');
        $("#teacherBranch").on('change',function(){
        var branchId = $(this).val();
        $.each($(".display_class:checkbox"), function(){
            var bid = $("#teacherBranch").val();
            var myObje = $(this);
            if(myObje[0].classList.contains('brn'+bid)){
                $(this).prop("disabled", false);
            }else{
                $(this).prop("checked", false);
                $(this).prop("disabled", true);
            }
        });
      
        $.each($(".checkAll"), function(){
            $(this).css('pointer-events','none');
        });
      
        var bid = $("#teacherBranch").val();
        $(".checkAll_"+bid).css('pointer-events','auto');
            
        if(branchId!=""){

            $.ajax({
                type : "POST",
                url : "{{ url('student/getClass') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Bra_Id : branchId
                },
                dataType : "JSON",
                success : function(data){
                        $(".branch-cls option").each(function() {
                            $(this).remove();
                        });
                        $(".class-sec option").each(function() {
                                $(this).remove();
                            });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                         });
                         $("#class").append('<option value="none">-- Select--</option>');
                         $("#section").append('<option value="none">-- Select --</option>');
                        $("#class").append(items);
                    },
                error : function(error){
                    console.log(error);
                }
            });
            var jvalue = branchId ;
        }
    });
        $("#class").on('change',function(){
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
        });
    });    
    }else{
        $("#teacherDetail").css('display','none');
    }
  });
});
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')