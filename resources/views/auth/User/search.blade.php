<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
        <label for="selectall" class="css-label table-ckeckbox">
        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">User Name</th>
      <th class="td-left">User Type</th>
      <th class="td-left">City</th>
      <th class="td-left">Mobile</th>
      <th class="td-left">Email</th>
      <th>Status</th>
      <th>Created By</th>
      <th>Edit</th>

      @if(Auth::user()->Use_Type == 5)
      <th>Password</th>
      @endif

      @if(Auth::user()->Use_Type == 1)
      <th>Password</th>
      @endif
    </tr>
@if($user)
  <?php $i=1; ?>
  @foreach($user as $data)
    <tr>
        <td><input id="user{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Use_Id }}" class="css-checkbox check-all"><label for="user{{ $i }}"" class="css-label table-ckeckbox"></label></td>
        <td class="td-left">{{ $data->Use_Name }}</td>
        <td class="td-left">
          @if($data->Use_Type == 1)
            Admin
          @elseif($data->Use_Type == 2)
            Teacher
          @elseif($data->Use_Type == 3)
            Driver
          @elseif($data->Use_Type == 5)
            Branch Admin
          @endif
        </td>
        <td class="td-left">{{ $data->Use_City }}</td>
        <td class="td-left">{{ $data->Use_Mobile_No }}</td>
        <td class="td-left">{{ $data->Use_Email }}</td>
        <td>
          @if($data->Use_Status == 1)
        Active
      @elseif($data->Use_Status == 0)
        In-Active
      @endif
        </td>
        <td>{{ $data->createdByName }}</td>
        <td><a href="{{ url('edit_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
         @if(Auth::user()->Use_Type == 5)
        <td><a href="{{ url('password_change_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Password"></a></td>
        @endif
        @if(Auth::user()->Use_Type == 1)
        <td><a href="{{ url('password_change_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Password"></a></td>
        @endif
    </tr>
  <?php $i++; ?>   
    @endforeach
@else
  No Data Found
  @endif
  </tbody>
</table>
