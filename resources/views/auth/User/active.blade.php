<table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th>User Name</th>
      <th>User Type</th>
      <th>City</th>
      <th>Status</th>
      <th>Created By</th>
      <th>Edit</th>
    </tr>


@if($user)
	<?php $i=1; ?>
	@foreach($user as $data)
    <tr>
      	<td><input id="user{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Use_Id }}" class="css-checkbox check-all"><label for="user{{ $i }}"" class="css-label table-ckeckbox"></label></td>
      	<td>{{ $data->Use_Name }}</td>
      	<td>
      		@if($data->Use_Type == 1)
      			Admin
      		@elseif($data->Use_Type == 2)
      			Teacher
      		@elseif($data->Use_Type == 3)
      			Driver
      		@endif
      	</td>

      	<td>{{ $data->Use_City }}</td>
      	<td>
      		@if($data->Use_Status == 1)
				Active
			@elseif($data->Use_Status == 0)
				In-Active
			@endif
      	</td>
      	<td>{{ $data->User_CreatedBy }}</td>
      	<td><a href="{{ url('edit_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
    </tr>
	<?php $i++; ?>   
    @endforeach
@else
	No Data Found
	@endif

  </tbody></table>