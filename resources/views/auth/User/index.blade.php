@extends('auth.Master.master')

@section('title','User')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/user-managment.png') }}" alt="branch-img"> User List</h2>      
	</div>
</div>
<div class="clearfix"></div>

<div class="row">
@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif
	    <div class="col-md-4 col-sm-4 col-xs-12">
	      <div class="search-box">
					<input id="btnSearch" name="Use_Name" placeholder="Teacher,Driver,Name..." type="search" />
	      </div>
	    </div>

	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
	      	<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_user') }}'">
	      		</li>
         		<li>
				<input type="button" id="active"  value="Active" class="active btnActInact" >
				</li>
	            <li>
	            <input type="button" id="inactive" value="In-Active" class="in-active btnActInact">
				</li>
	      		<li>
					<input type="submit" value="Delete" class="delete" id="btnDelete">
	      		</li>
	      	</ul>
	      </div>
	    </div>
	  </div>

<div class="table-form">   
    <table id="example2">
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      	<label for="selectall" class="css-label table-ckeckbox">
      	<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
      <th class="td-left">User Name</th>
      <th class="td-left">User Type</th>
      <th class="td-left">City</th>
      <th class="td-left">Mobile</th>
      <th class="td-left">Email</th>
      <th>Status</th>
      <th>Created By</th>
      <th>Edit</th>

      @if(Auth::user()->Use_Type == 5)
      <th>Password</th>
      @endif

      @if(Auth::user()->Use_Type == 1)
      <th>Password</th>
      @endif
    </tr>
@if($user)
	<?php $i=1; ?>
	@foreach($user as $data)
    <tr>
      	<td><input id="user{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Use_Id }}" class="css-checkbox check-all"><label for="user{{ $i }}"" class="css-label table-ckeckbox"></label></td>
      	<td class="td-left">{{ $data->Use_Name }}</td>
      	<td class="td-left">
      		@if($data->Use_Type == 1)
      			Admin
      		@elseif($data->Use_Type == 2)
      			Teacher
      		@elseif($data->Use_Type == 3)
      			Driver
      		@elseif($data->Use_Type == 5)
      			Branch Admin
      		@endif
      	</td>
        <td class="td-left">{{ $data->Use_City }}</td>
        <td class="td-left">{{ $data->Use_Mobile_No }}</td>
        <td class="td-left">{{ $data->Use_Email }}</td>
      	<td>
      		@if($data->Use_Status == 1)
				Active
			@elseif($data->Use_Status == 0)
				In-Active
			@endif
      	</td>
      	<td>{{ $data->createdByName }}</td>
      	<td><a href="{{ url('edit_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
      	 @if(Auth::user()->Use_Type == 5)
      	<td><a href="{{ url('password_change_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Password"></a></td>
      	@endif
      	@if(Auth::user()->Use_Type == 1)
      	<td><a href="{{ url('password_change_user',$data->Use_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Password"></a></td>
      	@endif
    </tr>
	<?php $i++; ?>   
    @endforeach
@else
	No Data Found
	@endif
  </tbody>
</table>
</div>

<div class="paggination-section">
@if ($user->lastPage() > 1)
    <ul>
        @if ($user->currentPage() != 1 && $user->lastPage() >= 5)
            <li><a href="{{ $user->url($user->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($user->currentPage() != 1)
            <li>
                <a  href="{{ $user->url($user->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($user->currentPage()-2, 1); $i <= min(max($user->currentPage()-2, 1)+4,$user->lastPage()); $i++)
                <li>
                    <a class="{{ ($user->currentPage() == $i) ? 'active' : '' }}" href="{{ $user->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($user->currentPage() != $user->lastPage())
            <li>
                <a href="{{ $user->url($user->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($user->currentPage() != $user->lastPage() && $user->lastPage() >= 5)
            <li>
                <a href="{{ $user->url($user->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});

$('#btnSearch').on('change',function(){
	var Use_Name = $(this).val();
	if(Use_Name!=""){
		$.ajax({
	        type : "POST",
	        url : "{{ url('search_user') }}",
	        data : { 
		            _token:     '{{ csrf_token() }}',
		            Use_Name : Use_Name
		        },
	        success : function(data){
	        	$(".paggination-section").html('');
	        	$('table#example2').html(data);
	        },
	        error: function(error){
	        }
	    });
    }else{
    	window.location = "{{ url('User Mgmt.') }}";
    }
});


$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		
			$.ajax({
                type : "POST",
                url : "{{ url('delete_user') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Use_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});


$("input.btnActInact").on('click',function(){
	var n = $("input:checked").length;
	var val = $(this).val();
	var ids = [];
	var ch_sts=false;
	var status=null;
	if( n >0 )
	{
		$('input:checked').each(function(i){ ids[i] = $(this).val(); });
		if(val == 'Active'){ status = 1; }else if(val == 'In-Active'){ status = 0; }
    	if(status==1) { ch_sts=confirm('Activate selected records???'); }
    	else if(status==0) { ch_sts=confirm('In-activate selected records???'); }
    	if(ch_sts){
	    	$.ajax({
		        type: "POST",
		        url: "{{ url('userStatus') }}",
		        data: { 
		            id: ids, 
		            status: status,
		            _token:     '{{ csrf_token() }}'
		        },
		        success: function(data) {
		        	$("input[type='checkbox']").each(function(i){
							$(this).prop('checked', false);
					});
		        	window.location.reload();
		        },
		        error: function(result) {
		            console.log(result.responseText);
		        }
		    });
    	}
    }
    else
    {
    	alert('Please select atleast one record!');
    }
});
</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')