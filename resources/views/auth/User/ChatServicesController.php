<?php

namespace App\Http\Controllers\RESTAPIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Helper;
use App\Helper\ResponseMessage;
use App\Helper\Exceptions;
use App\User;
use App\Model\Users;
use App\Model\Attendence;
use App\Model\Student;
use App\Model\RouteTbl;
use App\Model\GroupMaster;
use App\Model\GroupDetail;
use App\Model\Chat;
use App\Model\ClassTbl;
use Auth;
use Input;
use Validator;
use DB;
use Hash;

class ChatServicesController extends Controller
{
    public function uploadFile(Request $request)
    {
    	try {
    		$rules = [
    			'sender_id' => 'required',
    			'receiver_id' => 'required'
    		];

	    	$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			if($request->flag == 2){
		       
		    	if(Users::where('Use_Id',$request->sender_id)->exists() && Users::where('Use_Id',$request->receiver_id)->exists()){

		    	$isUpload = false;	
		    	if($request->file('file')){
		            $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
		            $isUpload = $request->file('file')->move(public_path('chat/image'), $imageName);
		            $chat_type = 2;
		        }else{
		        	$isUpload = false;	
		            $imageName = "";
		            $chat_type = 2;
		        }

	    		$image = array('image'=>$imageName,'chat_type'=>$chat_type);
	    		
	    		if($isUpload){
	    			ResponseMessage::success('Image Added',$image);
	    		}else{
	    			ResponseMessage::error('Image Not Added');	
	    		}
		    	}else{
		    		ResponseMessage::error('User Not Exist');
		    	}
			}else if($request->flag == 1){
		       
		    	if(Users::where('Use_Id',$request->sender_id)->exists() && Users::where('Use_Id',$request->receiver_id)->exists()){

		    	$isUpload = false;	
		    	if($request->file('file')){
		            $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
		            $isUpload = $request->file('file')->move(public_path('chat/video'), $imageName);
		            $chat_type = 1;
		        }else{
		        	$isUpload = false;	
		            $imageName = "";
		            $chat_type = 1;
		        }
	    		$image = array('image'=>$imageName,'chat_type'=>$chat_type);

		    		if($isUpload){

		    			ResponseMessage::success('Video Added',$image);
		    		}else{
		    			ResponseMessage::error('Video Not Added');	
		    		}
		    	}else{
		    		ResponseMessage::error('User Not Exist');
		    	}
			}else if($request->flag == 3){
		       
		    	if(Users::where('Use_Id',$request->sender_id)->exists() && Users::where('Use_Id',$request->receiver_id)->exists()){

		    	$isUpload = false;	
		    	if($request->file('file')){
		            $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
		            $isUpload = $request->file('file')->move(public_path('chat/audio'), $imageName);
		            $chat_type = 3;
		        }else{
		        	$isUpload = false;	
		            $imageName = "";
		            $chat_type = 3;
		        }
	    		$image = array('image'=>$imageName,'chat_type'=>$chat_type);

		    		if($isUpload){

		    			ResponseMessage::success('Audio Added',$image);
		    		}else{
		    			ResponseMessage::error('Audio Not Added');	
		    		}
		    	}else{
		    		ResponseMessage::error('User Not Exist');
		    	}
			}else if($request->flag == 4){
		       
		    	if(Users::where('Use_Id',$request->sender_id)->exists() && Users::where('Use_Id',$request->receiver_id)->exists()){

		    	$isUpload = false;	
		    	if($request->file('file')){
		            $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
		            $isUpload = $request->file('file')->move(public_path('chat/docs'), $imageName);
		            $chat_type = 4;
		        }else{
		        	$isUpload = false;	
		            $imageName = "";
		            $chat_type = 4;
		        }
	    		$image = array('image'=>$imageName,'chat_type'=>$chat_type);

		    		if($isUpload){

		    			ResponseMessage::success('Document Added',$image);
		    		}else{
		    			ResponseMessage::error('Document Not Added');	
		    		}
		    	}else{
		    		ResponseMessage::error('User Not Exist');
		    	}
			}
    	} catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    //User Chat List
	public function chat_user_List(Request $request)
	{
		try 
		{
			$user = Users::select('Use_Id','Use_Name','Use_Type','Use_Mobile_No','Use_Status','Use_Email','Use_Image','Use_Cla_Id')->where('Use_Type',2)->orwhere('Use_Type',4)->get();

			if($user)
			{
				ResponseMessage::success('Users List Successfully',$user);
			}else{
				ResponseMessage::error('Users List Not Found');
			}	
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}

	public function chat_parent_List(Request $request)
	{
		try 
		{	
			$rules = [
			'parent_id' => 'required',
			'class_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(Student::where('Std_Parent_Id',Input::get('parent_id'))->exists())
			{

				if(User::where('Use_Id',Input::get('parent_id'))->where('Use_Type',4)->exists())
				{
					$mobile_no = Input::get('mobile_no');
					$number = str_replace(' ','', $mobile_no);
					$user_mobile_no = explode(',', $number);
					$phone = preg_replace('/\D+91/', '', $user_mobile_no);
					$parent_id = Input::get('parent_id');

					$parent_class_id = User::join('student_tbl','student_tbl.Std_Parent_Id','=','user_tbl.Use_Id')->where('Use_Id','<>',$parent_id)->where('Std_Cla_Id',Input::get('class_id'))->whereIn('Use_Mobile_No',$phone)->select('user_tbl.*')->get()->toArray();

					$teacher = User::where('Use_Cla_Id',Input::get('class_id'))->where('Use_Type',2)->select('user_tbl.*')->get()->toArray();	

					$parentData = array();
					foreach ($parent_class_id as $parent) {
						array_push($parentData, $parent);
					}

					foreach ($teacher as $t) {
						array_push($parentData, $t);
						
					}
					
					if($parentData)
					{
						ResponseMessage::success('Parents List',$parentData);
					}else{
						ResponseMessage::error('Parents List Not Found');
					}
				}
				else{
				ResponseMessage::error('Record Not Found');
			}		
			}else{
				ResponseMessage::error('Record Not Found');
			}		
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}
         
    // Teacher Chat List
	public function chat_teacher_List(Request $request)
	{
		try 
		{	
			$rules = [
			'user_id' => 'required',
			'branch_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			$user_id = Input::get('user_id');

			$teacherClassId = User::where('Use_Id',Input::get('user_id'))->first()->Use_Cla_Id;

			$studentParentId = Student::where('Std_Cla_Id',$teacherClassId)->select('Std_Parent_Id')->get()->toArray();
			$class_id = ClassTbl::where('Cla_Bra_Id',Input::get('branch_id'))->select('Cla_Id')->get()->toArray();

			$teacher = Users::select('Use_Id','Use_Name','Use_Type','Use_Mobile_No','Use_Email','Use_Image')->whereIn('Use_Id',$studentParentId)->OrwhereIn('Use_Cla_Id',$class_id)->where('Use_Id','<>',$user_id)->get();

			if($teacher)
			{
				ResponseMessage::success('Teacher List',$teacher);
			}else{
				ResponseMessage::error('Teacher List Not Found');
			}			
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}

	public function chat_Group_List(Request $request)
	{
		try 
		{	
			$rules = [
			'user_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			$groupChat = GroupMaster::where('Grp_Mas_CreatedBy',Input::get('user_id'))->get();

			if($groupChat)
			{
				ResponseMessage::success('Group Chat List',$groupChat);
			}else{
				ResponseMessage::error('Record Not Found');
			}			
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}
	//User wise group list Api
	public function chat_user_group_List(Request $request)
	{
		try 
		{	
			$rules = [
			'user_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->exists())
			{

				$member_id = GroupDetail::where('Grp_Det_Mem_Id',Input::get('user_id'))->select('Grp_Det_CreatedBy')->get()->toArray();

				$groupChat = GroupMaster::where('Grp_Mas_CreatedBy',Input::get('user_id'))->OrwhereIn('Grp_Mas_CreatedBy',$member_id)->get();

				if($groupChat)
				{
					ResponseMessage::success('Group Chat List',$groupChat);
				}else{
					ResponseMessage::error('Record Not Found');
				}
			}else{
					ResponseMessage::error('User Not Found');
				}		
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}

	/**
	   * 
	   * Create Chat Group  
	   *
	   * @author Ravirajsinh Zala
	   *
	   * @param integer $user_id  Teacher Id
	   * @param String $group_name  Group Name
	   * @param String $group_image  Group Icon Image
	   * @param String $group_member  List Of Member You Want to add this group with comma sepreted
	   * @return JSON Created Group Information Like Group name,member,image,etc
	   */
	//user create group chat
	public function create_group_Chat(Request $request)
	{
		try
		{
			$rules = [
				'user_id' => 'required',	
				'group_name' => 'required',
				'group_member' => 'required'
				];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
					exit;
				}
			}

			if(User::where('Use_Id',Input::get('user_id'))->exists())
			{	
				if($request->file('group_image')){
		            $imageName = time().'.'.$request->file('group_image')->getClientOriginalExtension();
		            $isUpload = $request->file('group_image')->move(public_path('group/images'), $imageName);
		        }else{
		            $imageName = "";
		        }

		        $member_id = Input::get('group_member');

				$member = explode(',', $member_id);

				if($member_id == null)
				{
						$groupMaster = new GroupMaster;
						$groupMaster->Grp_Mas_Name  = Input::get('group_name');
						$groupMaster->Grp_Mas_Status = 1;
						$groupMaster->Grp_Mas_Image = $imageName;
						$groupMaster->Grp_Mas_CreatedBy = Input::get('user_id');
						$groupMaster->Grp_Mas_CreatedAt = date('Y-m-d H:i:s');
						$groupMaster->Grp_Mas_UpdatedBy = Input::get('user_id');
						$groupMaster->Grp_Mas_UpdatedAt = date('Y-m-d H:i:s');
						$groupMaster->save();
						$group_master_last_id = $groupMaster->Grp_Mas_Id;

						$groupDetail = new GroupDetail;
						$groupDetail->Grp_Det_Mas_Id = $group_master_last_id;
						$groupDetail->Grp_Det_Mem_Id = Input::get('group_member');
						$groupDetail->Grp_Det_CreatedBy = Input::get('user_id');
						$groupDetail->Grp_Det_CreatedAt = date('Y-m-d H:i:s');
						$groupDetail->Grp_Det_UpdatedBy = Input::get('user_id');
						$groupDetail->Grp_Det_UpdatedAt = date('Y-m-d H:i:s');
						$groupDetail->save();


						if($groupDetail)
						{
							$groupData = GroupMaster::with('Group_Member')->where('Grp_Mas_Id',$group_master_last_id)->get();
							ResponseMessage::success('Group Create Successfully',$groupData);
						}else{
							ResponseMessage::error('Not Creation Group');
						}
				}else{
						
						$groupMaster = new GroupMaster;
						$groupMaster->Grp_Mas_Name =Input::get('group_name');
						$groupMaster->Grp_Mas_Status = 1;
						$groupMaster->Grp_Mas_Image = $imageName;
						$groupMaster->Grp_Mas_CreatedBy = Input::get('user_id');
						$groupMaster->Grp_Mas_CreatedAt = date('Y-m-d H:i:s');
						$groupMaster->Grp_Mas_UpdatedBy = Input::get('user_id');
						$groupMaster->Grp_Mas_UpdatedAt = date('Y-m-d H:i:s');
						$groupMaster->save();
						$group_master_last_id = $groupMaster->Grp_Mas_Id;

						foreach ($member as $value)
						{
							$groupDetail = new GroupDetail;
							$groupDetail->Grp_Det_Mas_Id = $group_master_last_id;
							$groupDetail->Grp_Det_Mem_Id = $value;
							$groupDetail->Grp_Det_CreatedBy = Input::get('user_id');
							$groupDetail->Grp_Det_UpdatedBy = Input::get('user_id');
							$groupDetail->Grp_Det_CreatedAt = date("Y-m-d H:i:s");
							$groupDetail->Grp_Det_UpdatedAt = date("Y-m-d H:i:s");
							$groupDetail->save();
						}

					if($groupDetail)
					{
						$groupData = GroupMaster::with('Group_Member')->where('Grp_Mas_Id',$group_master_last_id)->get();
						ResponseMessage::success('Group Create Successfully',$groupData);
					}else{
						ResponseMessage::error('Not Creation Group');
					}
				}
			}else{
				ResponseMessage::error('User Not Found');
			}
		} 
		catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}

     /**
	   * 
	   * In this api to display all teacher record for staff chat
	   *
	   * @author Jayesh Sukhadiya Date: 17-05-2018
	   *
	   * @param integer user_id =  User Id
	   * @return JSON All teacher list display
	   */

    //Staff Chat List (All Tacaher List)

    public function staff_Chat_List(Request $request)
    {
    	try 
    	{
    		$rules = [
    			'user_id' => 'required',
    			'branch_id' => 'required'
    		];

	    	$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
					ResponseMessage::error($message);
				}
			}

			$user_id = Input::get('user_id');

			$class_id = ClassTbl::where('Cla_Bra_Id',Input::get('branch_id'))->select('Cla_Id')->get()->toArray();

    		$staffList = User::whereIn('Use_Cla_Id',$class_id)->where('Use_Type',2)->where('Use_Id','<>',$user_id)->get();	

    		if($staffList)
    		{
    			ResponseMessage::success("School Staff List",$staffList);
    		}else{
    			ResponseMessage::error("Record Not Found");
    		}
    	}catch (\Exception $e) {
    		Exceptions::exception($e);
    	}
    }

    public function group_Member_List(Request $request)
    {
    try 
		{	
			$rules = [
			'group_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}

			if(GroupDetail::where('Grp_Det_Mas_Id',Input::get('group_id'))->exists())
			{

				$member_id = GroupDetail::where('Grp_Det_Mas_Id',Input::get('group_id'))->select('Grp_Det_Mem_Id')->get()->toArray();

				$member_admin = GroupDetail::where('Grp_Det_Mas_Id',Input::get('group_id'))->first()->Grp_Det_CreatedBy;

				$groupChat = Users::select('Use_Id','Use_Name','Use_Type','Use_Mobile_No','Use_Image')->with(['GrpAdmin' => function($query) use($member_admin){
					$query->select('Grp_Mas_CreatedBy','Grp_Mas_Status as Use_Admin');
				$query->where('Grp_Mas_CreatedBy',$member_admin);
		 		
		 	}])->whereIn('Use_Id',$member_id)->orderBy('Use_Id', 'DESC')->get();

				if($groupChat)
				{
					ResponseMessage::success('Group Member List',$groupChat);
				}else{
					ResponseMessage::error('Record Not Found');
				}
			}else{
					ResponseMessage::error('Group Not Found');
				}		
		} 
		catch (\Exception $e) {
		Exceptions::exception($e);
		}
	}

	public function group_Member_Remove(Request $request)
	{
		try
		{
		 	$rules = [
			'group_id' => 'required',
			'member_id' => 'required'
			];
			$validator = Validator::make($request->all(), $rules);

			if($validator->fails()){
				$errors = $validator->errors();
				foreach ($errors->all() as $message) {                
				ResponseMessage::error($message);
				}
			}
			if(GroupDetail::where('Grp_Det_Mem_Id',Input::get('member_id'))->exists())
			{

				$member_remove = GroupDetail::where('Grp_Det_Mem_Id',Input::get('member_id'))->where('Grp_Det_Mas_Id',Input::get('group_id'))->delete();

				if($member_remove)
				{
					ResponseMessage::success('Group Member Remove Successfully',$member_remove);
				}else{
					ResponseMessage::error('Group Member Not Remove');
				}
			}else{
					ResponseMessage::error('Group Member Not Exist');
				}
		} catch (\Exception $e) {
			Exceptions::exception($e);
		}
	}
}
