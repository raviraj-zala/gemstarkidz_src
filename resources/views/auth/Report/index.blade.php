@extends('auth.Master.master')

@section('title','Report Management')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/report_img.png') }}" alt="">Report Management</h2>      
    </div>
</div>

<div class="clearfix"></div>
<div class="row">

    @if(Session::has('success'))
    <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ Session::get('success') }}
    </div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ Session::get('error') }}
    </div>
    @endif
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="search-box">
            <input id="btnSearch" name="Std_Name" placeholder="Search" type="search" />
        </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="main-btn">
            <ul>
                <li>
                    <input class="create" value="create" type="button" onClick="document.location.href ='{{ url('create_report') }}'">
                </li>
                <li>
                    <input type="submit" value="Delete" class="delete" id="btnDelete" >
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="table-form">   
    <table id="example2">
        <tbody>
            <tr>
                <th>
                    <input type="checkbox" id="selectall" class="checked css-checkbox" />
                    <label for="selectall" class="css-label table-ckeckbox">
                        <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white">
                        </th>
                        <th class="td-left">Report Type</th>
                        <th class="td-left">Gr No.</th>
                        <th class="td-left">Student Name</th>
                        <th class="td-left">Class</th>
                        <th class="td-left">Section</th>
                        <th class="td-left">Date</th>
                        <th>Edit</th>
            </tr>
            <?php $i = 1; ?>
            @forelse($students as $student)
            <tr>
                <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $student->Rpt_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
                <td class="td-left">@if($student->Rpt_Type=="1") Progress @endif @if($student->Rpt_Type=="2") Assessment @endif</td>
                <td class="td-left">{{ $student->Std_Gr_No }}</td>
                <td class="td-left">{{ $student->Std_Name }}</td>
                <td class="td-left">{{ $student->Cla_Class }}</td>
                <td class="td-left">{{ $student->Cla_Section }}</td>
                <td>{{ date("d-m-Y", strtotime($student->Rpt_Date)) }}</td>
                <td>@if($student->Rpt_Type=="2")<a href="{{ url('edit_report',$student->Rpt_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif
                    @if($student->Rpt_Type=="1")<a href="{{ url('edit_progress_report',$student->Rpt_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif
                </td>
            </tr>
            <?php $i++; ?>
            @empty
            <tr><td colspan="7">No Data Found.</td></tr>
            @endforelse


        </tbody></table>
</div>
<div class="paggination-section">
    @if ($students->lastPage() > 1)
    <ul>
        @if ($students->currentPage() != 1 && $students->lastPage() >= 5)
        <li><a href="{{ $students->url($students->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($students->currentPage() != 1)
        <li>
            <a  href="{{ $students->url($students->currentPage()-1) }}" >
                <
            </a>
        </li>
        @endif
        @for($i = max($students->currentPage()-2, 1); $i <= min(max($students->currentPage()-2, 1)+4,$students->lastPage()); $i++)
        <li>
            <a class="{{ ($students->currentPage() == $i) ? 'active' : '' }}" href="{{ $students->url($i) }}">{{ $i }}</a>
        </li>
        @endfor
        @if ($students->currentPage() != $students->lastPage())
        <li>
            <a href="{{ $students->url($students->currentPage()+1) }}" >
                >
            </a>
        </li>
        @endif
        @if ($students->currentPage() != $students->lastPage() && $students->lastPage() >= 5)
        <li>
            <a href="{{ $students->url($students->lastPage()) }}" >
                >>
            </a>
        </li>
        @endif
    </ul>
    @endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>    
$(function () {
    $("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
});
$('#btnSearch').on('change', function () {
    var Std_Name = $(this).val();
    if (Std_Name != "") {
        $.ajax({
            type: "POST",
            url: "{{ url('search_report') }}",
            data: {
                _token: '{{ csrf_token() }}',
                Std_Name: Std_Name
            },
            success: function (data) {
                $(".paggination-section").html('');
                $('table#example2').html(data);
            },
            error: function (error) {
            }
        });
    } else {
        window.location = "{{ url('Report Mgmt.') }}";
    }
});

$('#btnDelete').on('click', function () {
    var n = $("input:checked").length;
    if (n > 0)
    {
        var ch_delete = confirm('Delete selected records???');
        if (ch_delete) {
            var ids = [];
            $('input:checked').each(function (i) {
                ids[i] = $(this).val();
            });
            $.ajax({
                type: "POST",
                url: "{{ url('delete_report') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    Rpt_Id: ids
                },
                success: function (data) {
                    $("input[type='checkbox']").each(function (i) {
                        $(this).prop('checked', false);
                    });
                    $(".eve_sts").each(function (i) {
                        $(this).prop('checked', false);
                    });
                    window.location.reload();
                }
            });
        }
    } else {
        alert('Please select atleast one record!');
    }
});
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')