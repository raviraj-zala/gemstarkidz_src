@extends('auth.Master.master')

@section('title','Edit Report')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/report_img.png') }}" alt="zone-img">Edit Report</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>

<form name="update-report" role="form" method="POST" action="{{ url('update_report') }}" enctype="multipart/form-data">
    <div class="form-section">

        {{ csrf_field() }}
        <div class="branch-form">
            <input type="hidden" id="student_id" name="student_id" value="{{ $report->Std_Id }}" >
            <input type="hidden" id="id" name="id" value="{{ $report->Rpt_Id }}" >
            <input type="hidden" id="student_gr_no" name="student_gr_no" value="{{ $report->Std_Gr_No }}" >

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Report Type <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('reportType') ? 'is-invalid' : '' }}" tabindex="1"  name="reportType" id="reportType" readonly="true">
                            <option value="none">--Select--</option>
                            <option value="1" @if($report->Rpt_Type == "1") selected @endif>Progress</option>
                            <option value="2" @if($report->Rpt_Type == "2") selected @endif>Assessment</option>
                        </select>
                        @if (Session::has('reportType'))
                        <label class="error" for="reportType">
                            <span class="text-danger">
                                {{ Session::get('reportType') }}
                            </span>
                        </label>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div id="assessmentReport">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="branch-form">
                        <div class="form-box">
                            <div class="form-text form-box-width">
                                <h5>Date <span>*</span> :</h5>
                            </div>
                            <div class="form-typ-box zone-box"> 
                                <input type="text" name="date" id="datepicker"  placeholder="dd/mm/yyyy" readonly="true" value="{{ date('d/m/Y', strtotime($report->Rpt_Date)) }}">
                                @if($errors->has('date'))
                                <span class="text-danger">
                                    {{ $errors->first('date') }}
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Student Name :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <input type="text" name="studentName" class="branch-control {{ $errors->has('studentName') ? ' is-invalid' : '' }}" readonly="true" id="studentName" value="{{ $report->Std_Name }}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Class :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <input type="text" name="studentClass" class="branch-control {{ $errors->has('studentClass') ? ' is-invalid' : '' }}" readonly="true" id="studentClass" value="{{ $report->Cla_Class }}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Section :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <input type="text" name="studentSection" class="branch-control {{ $errors->has('studentSection') ? ' is-invalid' : '' }}" readonly="true" id="studentSection" value="{{ $report->Cla_Section }}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-lg-offset-3 col-md-4 col-sm-4 col-xs-4"><div class="form-text form-box-width">
                        <b>ES = Established</b></div></div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b >Dv = Developing</b></div></div></div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b >Em = Emergent</b></div></div></div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="bs-example">

                        <div class="panel-group" id="accordion">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> Maths</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <?php $data = json_decode($report->Rpt_Data); ?>

                                            <tbody>

                                                <tr>
                                                    <td class="td-left">Numbers names 1-100</td>
                                                    <td><input type="radio" name="number1to100" value="1" @if($data->Maths->number1to100==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="number1to100" value="2" @if($data->Maths->number1to100==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="number1to100" value="3" @if($data->Maths->number1to100==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Numbers 1-1000</td>
                                                    <td><input type="radio" name="number1to1000" value="1" @if($data->Maths->number1to1000==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="number1to1000" value="2" @if($data->Maths->number1to1000==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="number1to1000" value="3" @if($data->Maths->number1to1000==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Before/After/Between</td>
                                                    <td><input type="radio" name="beforeAfterBeteen" value="1" @if($data->Maths->beforeAfterBeteen==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="beforeAfterBeteen" value="2" @if($data->Maths->beforeAfterBeteen==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="beforeAfterBeteen" value="3" @if($data->Maths->beforeAfterBeteen==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Biggest/Smallest Number</td>
                                                    <td><input type="radio" name="biggestSmallestNumber" value="1" @if($data->Maths->biggestSmallestNumber==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="biggestSmallestNumber" value="2" @if($data->Maths->biggestSmallestNumber==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="biggestSmallestNumber" value="3" @if($data->Maths->biggestSmallestNumber==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Expanded form</td>
                                                    <td><input type="radio" name="expandedForm" value="1" @if($data->Maths->expandedForm==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="expandedForm" value="2" @if($data->Maths->expandedForm==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="expandedForm" value="3" @if($data->Maths->expandedForm==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Ascending/Descending order</td>
                                                    <td><input type="radio" name="ascDesOrder" value="1" @if($data->Maths->ascDesOrder==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="ascDesOrder" value="2" @if($data->Maths->ascDesOrder==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="ascDesOrder" value="3" @if($data->Maths->ascDesOrder==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Greater/Lesser Than</td>
                                                    <td><input type="radio" name="greLesThan" value="1" @if($data->Maths->greLesThan==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="greLesThan" value="2" @if($data->Maths->greLesThan==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="greLesThan" value="3" @if($data->Maths->greLesThan==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Equal/not equal to</td>
                                                    <td><input type="radio" name="equalNotEqual" value="1" @if($data->Maths->equalNotEqual==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="equalNotEqual" value="2" @if($data->Maths->equalNotEqual==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="equalNotEqual" value="3" @if($data->Maths->equalNotEqual==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"><b>Addition</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardAddition" value="1" @if($data->Maths->montessoriBoardAddition==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardAddition" value="2" @if($data->Maths->montessoriBoardAddition==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardAddition" value="3" @if($data->Maths->montessoriBoardAddition==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 2. Tally lines</td>
                                                    <td><input type="radio" name="tallyLinesAddition" value="1" @if($data->Maths->tallyLinesAddition==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="tallyLinesAddition" value="2" @if($data->Maths->tallyLinesAddition==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="tallyLinesAddition" value="3" @if($data->Maths->tallyLinesAddition==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 3. Word and pictures problems</td>
                                                    <td><input type="radio" name="wordPicturesProblemsAddition" value="1" @if($data->Maths->wordPicturesProblemsAddition==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="wordPicturesProblemsAddition" value="2" @if($data->Maths->wordPicturesProblemsAddition==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="wordPicturesProblemsAddition" value="3" @if($data->Maths->wordPicturesProblemsAddition==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 4. 4 digit addition without carry</td>
                                                    <td><input type="radio" name="fourDigitAdditionWithout" value="1" @if($data->Maths->fourDigitAdditionWithout==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitAdditionWithout" value="2" @if($data->Maths->fourDigitAdditionWithout==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitAdditionWithout" value="3" @if($data->Maths->fourDigitAdditionWithout==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 5. 4 digit addition with carry</td>
                                                    <td><input type="radio" name="fourDigitAdditionWith" value="1" @if($data->Maths->fourDigitAdditionWith==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitAdditionWith" value="2" @if($data->Maths->fourDigitAdditionWith==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitAdditionWith" value="3" @if($data->Maths->fourDigitAdditionWith==3) checked="true" @endif></td>
                                                </tr>

                                                <tr>
                                                    <td><b>Subtraction</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                <tr>
                                                    <td> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardSub" value="1" @if($data->Maths->montessoriBoardSub==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardSub" value="2" @if($data->Maths->montessoriBoardSub==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardSub" value="3" @if($data->Maths->montessoriBoardSub==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 2. Tally lines</td>
                                                    <td><input type="radio" name="tallyLinesSub" value="1" @if($data->Maths->tallyLinesSub==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="tallyLinesSub" value="2" @if($data->Maths->tallyLinesSub==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="tallyLinesSub" value="3" @if($data->Maths->tallyLinesSub==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 3. Word and pictures problems</td>
                                                    <td><input type="radio" name="wordPicturesProblemsSub" value="1" @if($data->Maths->wordPicturesProblemsSub==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="wordPicturesProblemsSub" value="2" @if($data->Maths->wordPicturesProblemsSub==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="wordPicturesProblemsSub" value="3" @if($data->Maths->wordPicturesProblemsSub==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 4. 4 digit subtraction without carry</td>
                                                    <td><input type="radio" name="fourDigitSubWithout" value="1" @if($data->Maths->fourDigitSubWithout==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitSubWithout" value="2" @if($data->Maths->fourDigitSubWithout==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitSubWithout" value="3" @if($data->Maths->fourDigitSubWithout==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td> 5. 4 digit subtraction with carry</td>
                                                    <td><input type="radio" name="fourDigitSubWith" value="1" @if($data->Maths->fourDigitSubWith==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitSubWith" value="2" @if($data->Maths->fourDigitSubWith==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="fourDigitSubWith" value="3" @if($data->Maths->fourDigitSubWith==3) checked="true" @endif></td>
                                                </tr>

                                                <tr>
                                                    <td class="td-left"><b>Multiplication</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardMul" value="1" @if($data->Maths->montessoriBoardMul==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardMul" value="2" @if($data->Maths->montessoriBoardMul==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardMul" value="3" @if($data->Maths->montessoriBoardMul==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 2. Multiplay with help of pictures</td>
                                                    <td><input type="radio" name="mulWithPic" value="1" @if($data->Maths->mulWithPic==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="mulWithPic" value="2" @if($data->Maths->mulWithPic==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="mulWithPic" value="3" @if($data->Maths->mulWithPic==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"><b>Division</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardDiv" value="1" @if($data->Maths->montessoriBoardDiv==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardDiv" value="2" @if($data->Maths->montessoriBoardDiv==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="montessoriBoardDiv" value="3" @if($data->Maths->montessoriBoardDiv==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 2. Division with help of pictures</td>
                                                    <td><input type="radio" name="divWithPic" value="1" @if($data->Maths->divWithPic==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="divWithPic" value="2" @if($data->Maths->divWithPic==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="divWithPic" value="3" @if($data->Maths->divWithPic==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Money</td>
                                                    <td><input type="radio" name="money" value="1" @if($data->Maths->money==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="money" value="2" @if($data->Maths->money==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="money" value="3" @if($data->Maths->money==3) checked="true" @endif></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> English</a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left"><b>Sounds</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 1. sh,ch,ck</td>
                                                    <td><input type="radio" name="shChCk" value="1" @if($data->english->shChCk==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="shChCk" value="2" @if($data->english->shChCk==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="shChCk" value="3" @if($data->english->shChCk==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">2. a-e,o-e,u-e</td>
                                                    <td><input type="radio" name="aeOeUe" value="1" @if($data->english->aeOeUe==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="aeOeUe" value="2" @if($data->english->aeOeUe==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="aeOeUe" value="3" @if($data->english->aeOeUe==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">3. ow,ou,ea,ing</td>
                                                    <td><input type="radio" name="owOuEaIng" value="1" @if($data->english->owOuEaIng==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="owOuEaIng" value="2" @if($data->english->owOuEaIng==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="owOuEaIng" value="3" @if($data->english->owOuEaIng==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">One and many</td>
                                                    <td><input type="radio" name="oneAndMany" value="1" @if($data->english->oneAndMany==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="oneAndMany" value="2" @if($data->english->oneAndMany==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="oneAndMany" value="3" @if($data->english->oneAndMany==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">A and an</td>
                                                    <td><input type="radio" name="aAndAn" value="1" @if($data->english->aAndAn==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="aAndAn" value="2" @if($data->english->aAndAn==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="aAndAn" value="3" @if($data->english->aAndAn==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Preposition in,on,under,in front,behind, next to</td>
                                                    <td><input type="radio" name="preposition" value="1"@if($data->english->preposition==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="preposition" value="2"@if($data->english->preposition==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="preposition" value="3"@if($data->english->preposition==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">This and that</td>
                                                    <td><input type="radio" name="thisAndThat" value="1"@if($data->english->thisAndThat==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="thisAndThat" value="2"@if($data->english->thisAndThat==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="thisAndThat" value="3"@if($data->english->thisAndThat==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Those and these</td>
                                                    <td><input type="radio" name="thoseAndThese" value="1"@if($data->english->thoseAndThese==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="thoseAndThese" value="2"@if($data->english->thoseAndThese==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="thoseAndThese" value="3"@if($data->english->thoseAndThese==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Yes and no</td>
                                                    <td><input type="radio" name="yesAndNo" value="1"@if($data->english->yesAndNo==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="yesAndNo" value="2"@if($data->english->yesAndNo==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="yesAndNo" value="3"@if($data->english->yesAndNo==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Articles</td>
                                                    <td><input type="radio" name="articles" value="1" @if($data->english->articles==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="articles" value="2" @if($data->english->articles==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="articles" value="3" @if($data->english->articles==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Write answers to questions (What)</td>
                                                    <td><input type="radio" name="writeAnsWhat" value="1" @if($data->english->writeAnsWhat==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="writeAnsWhat" value="2" @if($data->english->writeAnsWhat==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="writeAnsWhat" value="3" @if($data->english->writeAnsWhat==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Nouns</td>
                                                    <td><input type="radio" name="nouns" value="1" @if($data->english->nouns==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="nouns" value="2" @if($data->english->nouns==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="nouns" value="3" @if($data->english->nouns==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Verbs</td>
                                                    <td><input type="radio" name="verbs" value="1" @if($data->english->verbs==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="verbs" value="2" @if($data->english->verbs==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="verbs" value="3" @if($data->english->verbs==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Adverbs</td>
                                                    <td><input type="radio" name="adverbs" value="1" @if($data->english->adverbs==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="adverbs" value="2" @if($data->english->adverbs==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="adverbs" value="3" @if($data->english->adverbs==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Pronouns</td>
                                                    <td><input type="radio" name="pronouns" value="1" @if($data->english->pronouns==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="pronouns" value="2" @if($data->english->pronouns==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="pronouns" value="3" @if($data->english->pronouns==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Adjectives</td>
                                                    <td><input type="radio" name="adjectives" value="1" @if($data->english->adjectives==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="adjectives" value="2" @if($data->english->adjectives==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="adjectives" value="3" @if($data->english->adjectives==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Masculine and feminine</td>
                                                    <td><input type="radio" name="masculineFeminine" value="1" @if($data->english->masculineFeminine==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="masculineFeminine" value="2" @if($data->english->masculineFeminine==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="masculineFeminine" value="3" @if($data->english->masculineFeminine==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Singular and plurals</td>
                                                    <td><input type="radio" name="singularPlurals" value="1" @if($data->english->singularPlurals==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="singularPlurals" value="2" @if($data->english->singularPlurals==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="singularPlurals" value="3" @if($data->english->singularPlurals==3) checked="true" @endif></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> Sensorial</a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>   
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left">Family</td>
                                                    <td><input type="radio" name="family" value="1" @if($data->sensorial->family==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="family" value="2" @if($data->sensorial->family==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="family" value="3" @if($data->sensorial->family==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My amazing body</td>
                                                    <td><input type="radio" name="myAmazingBody" value="1" @if($data->sensorial->myAmazingBody==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="myAmazingBody" value="2" @if($data->sensorial->myAmazingBody==3) checked="true" @endif></td>
                                                    <td><input type="radio" name="myAmazingBody" value="3" @if($data->sensorial->myAmazingBody==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My home</td>
                                                    <td><input type="radio" name="myHome" value="1" @if($data->sensorial->myHome==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="myHome" value="2" @if($data->sensorial->myHome==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="myHome" value="3" @if($data->sensorial->myHome==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My School</td>
                                                    <td><input type="radio" name="mySchool" value="1" @if($data->sensorial->mySchool==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="mySchool" value="2" @if($data->sensorial->mySchool==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="mySchool" value="3" @if($data->sensorial->mySchool==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My neighbourhood</td>
                                                    <td><input type="radio" name="myNeighbourhood" value="1" @if($data->sensorial->myNeighbourhood==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="myNeighbourhood" value="2" @if($data->sensorial->myNeighbourhood==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="myNeighbourhood" value="3" @if($data->sensorial->myNeighbourhood==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Types of metals</td>
                                                    <td><input type="radio" name="typesOfMetals" value="1" @if($data->sensorial->typesOfMetals==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="typesOfMetals" value="2" @if($data->sensorial->typesOfMetals==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="typesOfMetals" value="3" @if($data->sensorial->typesOfMetals==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Living and non-living things</td>
                                                    <td><input type="radio" name="livingAndNonLiving" value="1" @if($data->sensorial->livingAndNonLiving==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="livingAndNonLiving" value="2" @if($data->sensorial->livingAndNonLiving==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="livingAndNonLiving" value="3" @if($data->sensorial->livingAndNonLiving==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Community helpers</td>
                                                    <td><input type="radio" name="communityHelpers" value="2" @if($data->sensorial->communityHelpers==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="communityHelpers" value="1" @if($data->sensorial->communityHelpers==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="communityHelpers" value="3" @if($data->sensorial->communityHelpers==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Types of animals and birds</td>
                                                    <td><input type="radio" name="typeAnimalsBirds" value="1" @if($data->sensorial->typeAnimalsBirds==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="typeAnimalsBirds" value="2" @if($data->sensorial->typeAnimalsBirds==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="typeAnimalsBirds" value="3" @if($data->sensorial->typeAnimalsBirds==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Colours</td>
                                                    <td><input type="radio" name="colours" value="1" @if($data->sensorial->colours==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="colours" value="2" @if($data->sensorial->colours==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="colours" value="3" @if($data->sensorial->colours==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Transport</td>
                                                    <td><input type="radio" name="transport" value="1" @if($data->sensorial->transport==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="transport" value="2" @if($data->sensorial->transport==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="transport" value="3" @if($data->sensorial->transport==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Emotions</td>
                                                    <td><input type="radio" name="emotions" value="1" @if($data->sensorial->emotions==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="emotions" value="2" @if($data->sensorial->emotions==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="emotions" value="3" @if($data->sensorial->emotions==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Good and bad habits</td>
                                                    <td><input type="radio" name="goodBadHabits" value="1" @if($data->sensorial->goodBadHabits==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="goodBadHabits" value="2" @if($data->sensorial->goodBadHabits==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="goodBadHabits" value="3" @if($data->sensorial->goodBadHabits==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Electricity</td>
                                                    <td><input type="radio" name="electricity" value="1" @if($data->sensorial->electricity==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="electricity" value="2" @if($data->sensorial->electricity==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="electricity" value="3" @if($data->sensorial->electricity==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Seasons</td>
                                                    <td><input type="radio" name="seasons" value="1" @if($data->sensorial->seasons==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="seasons" value="2" @if($data->sensorial->seasons==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="seasons" value="3" @if($data->sensorial->seasons==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>People of india</td>
                                                    <td><input type="radio" name="peopleOfIndia" value="1" @if($data->sensorial->peopleOfIndia==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="peopleOfIndia" value="2" @if($data->sensorial->peopleOfIndia==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="peopleOfIndia" value="3" @if($data->sensorial->peopleOfIndia==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Good habits in school</td>
                                                    <td><input type="radio" name="goodHabitsSchools" value="1" @if($data->sensorial->goodHabitsSchools==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="goodHabitsSchools" value="2" @if($data->sensorial->goodHabitsSchools==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="goodHabitsSchools" value="3" @if($data->sensorial->goodHabitsSchools==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Festivals</td>
                                                    <td><input type="radio" name="festivals" value="1" @if($data->sensorial->festivals==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="festivals" value="2" @if($data->sensorial->festivals==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="festivals" value="3" @if($data->sensorial->festivals==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Sporst</td>
                                                    <td><input type="radio" name="sporst" value="1" @if($data->sensorial->sporst==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="sporst" value="2" @if($data->sensorial->sporst==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="sporst" value="3" @if($data->sensorial->sporst==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Insects</td>
                                                    <td><input type="radio" name="insects" value="1" @if($data->sensorial->insects==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="insects" value="2" @if($data->sensorial->insects==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="insects" value="3" @if($data->sensorial->insects==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Vegetables and fruits</td>
                                                    <td><input type="radio" name="vegetablesFruits" value="1" @if($data->sensorial->vegetablesFruits==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="vegetablesFruits" value="2" @if($data->sensorial->vegetablesFruits==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="vegetablesFruits" value="3" @if($data->sensorial->vegetablesFruits==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>Birds</td>
                                                    <td><input type="radio" name="birds" value="1" @if($data->sensorial->birds==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="birds" value="2" @if($data->sensorial->birds==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="birds" value="3" @if($data->sensorial->birds==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td>National symbols</td>
                                                    <td><input type="radio" name="nationalSymbols" value="1" @if($data->sensorial->nationalSymbols==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="nationalSymbols" value="2" @if($data->sensorial->nationalSymbols==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="nationalSymbols" value="3" @if($data->sensorial->nationalSymbols==3) checked="true" @endif></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> Hindi</a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left">अ-ह</td>
                                                    <td><input type="radio" name="aH" value="1" @if($data->hindi->aH==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="aH" value="2" @if($data->hindi->aH==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="aH" value="3" @if($data->hindi->aH==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Letter words ल, ब, त, न</td>
                                                    <td><input type="radio" name="letterLBTN" value="1" @if($data->hindi->letterLBTN==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterLBTN" value="2" @if($data->hindi->letterLBTN==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterLBTN" value="3" @if($data->hindi->letterLBTN==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">3 Letter words</td>
                                                    <td><input type="radio" name="letterWord3" value="1" @if($data->hindi->letterWord3==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterWord3" value="2" @if($data->hindi->letterWord3==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterWord3" value="3" @if($data->hindi->letterWord3==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">4 Letter words</td>
                                                    <td><input type="radio" name="letterWord4" value="1" @if($data->hindi->letterWord4==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterWord4" value="2" @if($data->hindi->letterWord4==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterWord4" value="3" @if($data->hindi->letterWord4==3) checked="true" @endif></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">2 Letter mixed words</td>
                                                    <td><input type="radio" name="letterMixedWord" value="1" @if($data->hindi->letterMixedWord==1) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterMixedWord" value="2" @if($data->hindi->letterMixedWord==2) checked="true" @endif></td>
                                                    <td><input type="radio" name="letterMixedWord" value="3" @if($data->hindi->letterMixedWord==3) checked="true" @endif></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="form-btn branch-form-btn">
                <input type="submit" value="save" tabindex="3" id="submitForm"><a href="{{ url('Report Mgmt.') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
            </div>
        </div>    

    </div>

</form>

@endsection

@section('footer')

@push('footer_script')
<script type="text/javascript">
    $(document).ready(function () {

        var stdNotExists;
        $("#submitForm").click(function (event) {
            if ($("#reportType").val() === "2") {
                var nameGr = $("#nameOrGrNo").val();
                if (nameGr === "") {
                    $("#studentNotFound").html("<label for='nameOrGr' class='error'> <span class='text-danger'>Please Enter Student Name Or Gr No.</span> </label> ");
                    event.preventDefault();
                } else if ($("#datepicker").val() === "") {
                    $("#dateError").html("<label for='date' class='error'> <span class='text-danger'>Please Enter Date.</span> </label> ");
                    $("#datepicker").focus();
                    event.preventDefault();
                } else {
                    if (stdNotExists === true) {
                        $("#studentNotFound").html("<label for='nameOrGr' class='error'> <span class='text-danger'>Student Not Found.</span> </label> ");
                        $("#nameOrGrNo").focus();
                        event.preventDefault();
                    }
                }
            }
        });

        $("#reportType").on('change', function () {
            var type = $("#reportType").val();
            if (type === "2") {
                $("#assessmentReport").css('display', 'block');
            } else {
                $("#assessmentReport").css('display', 'none');
            }
        });


        $("#nameOrGrNo").autocomplete({
            source: "{{ url('report/getStudentNameOrGr') }}"
        });

        $("#nameOrGrNo").on('focusout', function () {
            var Std_Name_Gr = $("#nameOrGrNo").val();
            $.ajax({
                type: "POST",
                url: "{{ url('report/getStudentData') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    Std_Name_Gr: Std_Name_Gr
                },
                success: function (data) {
                    if (data === "false" && $("#nameOrGrNo").val() !== "")
                    {
                        $("#studentNotFound").html("<label for='nameOrGrNo' class='error'><span class='text-danger'>Student Not Found.</span></label>");
                        stdNotExists = true;
                    } else {
                        stdNotExists = false;
                        $("#studentNotFound").html("");
                        $("#studentName").val(data.Std_Name);
                        $("#studentClass").val(data.Cla_Class);
                        $("#studentSection").val(data.Cla_Section);
                        $("#student_id").val(data.Std_Id);
                        $("#student_gr_no").val(data.Std_Gr_No);
                    }
                }, error: function (error) {
                    console.log(error);
                }
            });
        });

        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
        });
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    });
</script>
@endpush

@section('footer_link_and_scripts')