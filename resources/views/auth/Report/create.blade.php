@extends('auth.Master.master')
@section('title','Insert Report')
@section('site_header')
@section('sidebar')
@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/report_img.png') }}" alt="zone-img">Insert Report</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>

<div class="form-section">
<form name="insert-report" role="form" method="POST" action="{{ url('save_report') }}" enctype="multipart/form-data">
    

        {{ csrf_field() }}
        <div class="branch-form">
            <input type="hidden" id="student_id" name="student_id" >
            <input type="hidden" id="student_gr_no" name="student_gr_no" >

            <div class="form-box">
                <div class="form-text form-box-width">
                    <h5>Report Type <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                        <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('reportType') ? 'is-invalid' : '' }}" tabindex="1"  name="reportType" required id="reportType">
                            <option value="none">--Select--</option>
                            <option value="1">Progress</option>
                            <option value="2">Assessment</option>
                        </select>
                        @if (Session::has('reportType'))
                        <label class="error" for="reportType">
                            <span class="text-danger">
                                {{ Session::get('reportType') }}
                            </span>
                        </label>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div id="assessmentReport" style="display:none;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="branch-form">
                        <div class="form-box">
                            <div class="form-text form-box-width">
                                <h5>Gr No./Student Name <span>*</span> :</h5>
                            </div>
                            <div class="form-typ-box zone-box">
                                <input type="text" name="nameOrGr" class="branch-control {{ $errors->has('nameOrGr') ? ' is-invalid' : '' }}" id="nameOrGrNo">
                                <div id="studentNotFound"></div>
                            </div>
                            @if ($errors->has('nameOrGr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('nameOrGr') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-box">
                            <div class="form-text form-box-width">
                                <h5>Date <span>*</span> :</h5>
                            </div>
                            <div class="form-typ-box zone-box"> 
                                <input type="text" name="date" id="datepicker" value="{{ old('date') }}"  placeholder="dd/mm/yyyy">
                                @if($errors->has('date'))
                                <span class="text-danger">
                                    {{ $errors->first('date') }}
                                </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Student Name :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <input type="text" name="studentName" class="branch-control {{ $errors->has('studentName') ? ' is-invalid' : '' }}" readonly="true" id="studentName" >
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Class :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <input type="text" name="studentClass" class="branch-control {{ $errors->has('studentClass') ? ' is-invalid' : '' }}" readonly="true" id="studentClass" >
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Section :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <input type="text" name="studentSection" class="branch-control {{ $errors->has('studentSection') ? ' is-invalid' : '' }}" readonly="true" id="studentSection" >
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-lg-offset-3 col-md-4 col-sm-4 col-xs-4"><div class="form-text form-box-width">
                        <b>ES = Established</b></div></div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b >Dv = Developing</b></div></div></div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b >Em = Emergent</b></div></div></div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="bs-example">

                        <div class="panel-group" id="accordion">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> Maths</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left">Numbers names 1-100</td>
                                                    <td><input type="radio" name="number1to100" value="1"></td>
                                                    <td><input type="radio" name="number1to100" value="2"></td>
                                                    <td><input type="radio" name="number1to100" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Numbers 1-1000</td>
                                                    <td><input type="radio" name="number1to1000" value="1"></td>
                                                    <td><input type="radio" name="number1to1000" value="2"></td>
                                                    <td><input type="radio" name="number1to1000" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Before/After/Between</td>
                                                    <td><input type="radio" name="beforeAfterBeteen" value="1"></td>
                                                    <td><input type="radio" name="beforeAfterBeteen" value="2"></td>
                                                    <td><input type="radio" name="beforeAfterBeteen" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Biggest/Smallest Number</td>
                                                    <td><input type="radio" name="biggestSmallestNumber" value="1"></td>
                                                    <td><input type="radio" name="biggestSmallestNumber" value="2"></td>
                                                    <td><input type="radio" name="biggestSmallestNumber" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Expanded form</td>
                                                    <td><input type="radio" name="expandedForm" value="1"></td>
                                                    <td><input type="radio" name="expandedForm" value="2"></td>
                                                    <td><input type="radio" name="expandedForm" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Ascending/Descending order</td>
                                                    <td><input type="radio" name="ascDesOrder" value="1"></td>
                                                    <td><input type="radio" name="ascDesOrder" value="2"></td>
                                                    <td><input type="radio" name="ascDesOrder" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Greater/Lesser Than</td>
                                                    <td><input type="radio" name="greLesThan" value="1"></td>
                                                    <td><input type="radio" name="greLesThan" value="2"></td>
                                                    <td><input type="radio" name="greLesThan" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Equal/not equal to</td>
                                                    <td><input type="radio" name="equalNotEqual" value="1"></td>
                                                    <td><input type="radio" name="equalNotEqual" value="2"></td>
                                                    <td><input type="radio" name="equalNotEqual" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"><b>Addition</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardAddition" value="1"></td>
                                                    <td><input type="radio" name="montessoriBoardAddition" value="2"></td>
                                                    <td><input type="radio" name="montessoriBoardAddition" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 2. Tally lines</td>
                                                    <td><input type="radio" name="tallyLinesAddition" value="1"></td>
                                                    <td><input type="radio" name="tallyLinesAddition" value="2"></td>
                                                    <td><input type="radio" name="tallyLinesAddition" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 3. Word and pictures problems</td>
                                                    <td><input type="radio" name="wordPicturesProblemsAddition" value="1"></td>
                                                    <td><input type="radio" name="wordPicturesProblemsAddition" value="2"></td>
                                                    <td><input type="radio" name="wordPicturesProblemsAddition" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 4. 4 digit addition without carry</td>
                                                    <td><input type="radio" name="fourDigitAdditionWithout" value="1"></td>
                                                    <td><input type="radio" name="fourDigitAdditionWithout" value="2"></td>
                                                    <td><input type="radio" name="fourDigitAdditionWithout" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 5. 4 digit addition with carry</td>
                                                    <td><input type="radio" name="fourDigitAdditionWith" value="1"></td>
                                                    <td><input type="radio" name="fourDigitAdditionWith" value="2"></td>
                                                    <td><input type="radio" name="fourDigitAdditionWith" value="3"></td>
                                                </tr>

                                                <tr>
                                                    <td><b>Subtraction</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                <tr>
                                                    <td> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardSub" value="1"></td>
                                                    <td><input type="radio" name="montessoriBoardSub" value="2"></td>
                                                    <td><input type="radio" name="montessoriBoardSub" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 2. Tally lines</td>
                                                    <td><input type="radio" name="tallyLinesSub" value="1"></td>
                                                    <td><input type="radio" name="tallyLinesSub" value="2"></td>
                                                    <td><input type="radio" name="tallyLinesSub" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 3. Word and pictures problems</td>
                                                    <td><input type="radio" name="wordPicturesProblemsSub" value="1"></td>
                                                    <td><input type="radio" name="wordPicturesProblemsSub" value="2"></td>
                                                    <td><input type="radio" name="wordPicturesProblemsSub" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 4. 4 digit subtraction without carry</td>
                                                    <td><input type="radio" name="fourDigitSubWithout" value="1"></td>
                                                    <td><input type="radio" name="fourDigitSubWithout" value="2"></td>
                                                    <td><input type="radio" name="fourDigitSubWithout" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td> 5. 4 digit subtraction with carry</td>
                                                    <td><input type="radio" name="fourDigitSubWith" value="1"></td>
                                                    <td><input type="radio" name="fourDigitSubWith" value="2"></td>
                                                    <td><input type="radio" name="fourDigitSubWith" value="3"></td>
                                                </tr>

                                                <tr>
                                                    <td class="td-left"><b>Multiplication</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardMul" value="1"></td>
                                                    <td><input type="radio" name="montessoriBoardMul" value="2"></td>
                                                    <td><input type="radio" name="montessoriBoardMul" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 2. Multiplay with help of pictures</td>
                                                    <td><input type="radio" name="mulWithPic" value="1"></td>
                                                    <td><input type="radio" name="mulWithPic" value="2"></td>
                                                    <td><input type="radio" name="mulWithPic" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"><b>Division</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 1. Montessori board</td>
                                                    <td><input type="radio" name="montessoriBoardDiv" value="1"></td>
                                                    <td><input type="radio" name="montessoriBoardDiv" value="2"></td>
                                                    <td><input type="radio" name="montessoriBoardDiv" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 2. Division with help of pictures</td>
                                                    <td><input type="radio" name="divWithPic" value="1"></td>
                                                    <td><input type="radio" name="divWithPic" value="2"></td>
                                                    <td><input type="radio" name="divWithPic" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Money</td>
                                                    <td><input type="radio" name="money" value="1"></td>
                                                    <td><input type="radio" name="money" value="2"></td>
                                                    <td><input type="radio" name="money" value="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> English</a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left"><b>Sounds</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left"> 1. sh,ch,ck</td>
                                                    <td><input type="radio" name="shChCk" value="1"></td>
                                                    <td><input type="radio" name="shChCk" value="2"></td>
                                                    <td><input type="radio" name="shChCk" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">2. a-e,o-e,u-e</td>
                                                    <td><input type="radio" name="aeOeUe" value="1"></td>
                                                    <td><input type="radio" name="aeOeUe" value="2"></td>
                                                    <td><input type="radio" name="aeOeUe" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">3. ow,ou,ea,ing</td>
                                                    <td><input type="radio" name="owOuEaIng" value="1"></td>
                                                    <td><input type="radio" name="owOuEaIng" value="2"></td>
                                                    <td><input type="radio" name="owOuEaIng" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">One and many</td>
                                                    <td><input type="radio" name="oneAndMany" value="1"></td>
                                                    <td><input type="radio" name="oneAndMany" value="2"></td>
                                                    <td><input type="radio" name="oneAndMany" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">A and an</td>
                                                    <td><input type="radio" name="aAndAn" value="1"></td>
                                                    <td><input type="radio" name="aAndAn" value="2"></td>
                                                    <td><input type="radio" name="aAndAn" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Preposition in,on,under,in front,behind, next to</td>
                                                    <td><input type="radio" name="preposition" value="1"></td>
                                                    <td><input type="radio" name="preposition" value="2"></td>
                                                    <td><input type="radio" name="preposition" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">This and that</td>
                                                    <td><input type="radio" name="thisAndThat" value="1"></td>
                                                    <td><input type="radio" name="thisAndThat" value="2"></td>
                                                    <td><input type="radio" name="thisAndThat" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Those and these</td>
                                                    <td><input type="radio" name="thoseAndThese" value="1"></td>
                                                    <td><input type="radio" name="thoseAndThese" value="2"></td>
                                                    <td><input type="radio" name="thoseAndThese" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Yes and no</td>
                                                    <td><input type="radio" name="yesAndNo" value="1"></td>
                                                    <td><input type="radio" name="yesAndNo" value="2"></td>
                                                    <td><input type="radio" name="yesAndNo" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Articles</td>
                                                    <td><input type="radio" name="articles" value="1"></td>
                                                    <td><input type="radio" name="articles" value="2"></td>
                                                    <td><input type="radio" name="articles" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Write answers to questions (What)</td>
                                                    <td><input type="radio" name="writeAnsWhat" value="1"></td>
                                                    <td><input type="radio" name="writeAnsWhat" value="2"></td>
                                                    <td><input type="radio" name="writeAnsWhat" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Nouns</td>
                                                    <td><input type="radio" name="nouns" value="1"></td>
                                                    <td><input type="radio" name="nouns" value="2"></td>
                                                    <td><input type="radio" name="nouns" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Verbs</td>
                                                    <td><input type="radio" name="verbs" value="1"></td>
                                                    <td><input type="radio" name="verbs" value="2"></td>
                                                    <td><input type="radio" name="verbs" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Adverbs</td>
                                                    <td><input type="radio" name="adverbs" value="1"></td>
                                                    <td><input type="radio" name="adverbs" value="2"></td>
                                                    <td><input type="radio" name="adverbs" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Pronouns</td>
                                                    <td><input type="radio" name="pronouns" value="1"></td>
                                                    <td><input type="radio" name="pronouns" value="2"></td>
                                                    <td><input type="radio" name="pronouns" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Adjectives</td>
                                                    <td><input type="radio" name="adjectives" value="1"></td>
                                                    <td><input type="radio" name="adjectives" value="2"></td>
                                                    <td><input type="radio" name="adjectives" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Masculine and feminine</td>
                                                    <td><input type="radio" name="masculineFeminine" value="1"></td>
                                                    <td><input type="radio" name="masculineFeminine" value="2"></td>
                                                    <td><input type="radio" name="masculineFeminine" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Singular and plurals</td>
                                                    <td><input type="radio" name="singularPlurals" value="1"></td>
                                                    <td><input type="radio" name="singularPlurals" value="2"></td>
                                                    <td><input type="radio" name="singularPlurals" value="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> Sensorial</a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>   
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left">Family</td>
                                                    <td><input type="radio" name="family" value="1"></td>
                                                    <td><input type="radio" name="family" value="2"></td>
                                                    <td><input type="radio" name="family" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My amazing body</td>
                                                    <td><input type="radio" name="myAmazingBody" value="1"></td>
                                                    <td><input type="radio" name="myAmazingBody" value="2"></td>
                                                    <td><input type="radio" name="myAmazingBody" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My home</td>
                                                    <td><input type="radio" name="myHome" value="1"></td>
                                                    <td><input type="radio" name="myHome" value="2"></td>
                                                    <td><input type="radio" name="myHome" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My School</td>
                                                    <td><input type="radio" name="mySchool" value="1"></td>
                                                    <td><input type="radio" name="mySchool" value="2"></td>
                                                    <td><input type="radio" name="mySchool" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">My neighbourhood</td>
                                                    <td><input type="radio" name="myNeighbourhood" value="1"></td>
                                                    <td><input type="radio" name="myNeighbourhood" value="2"></td>
                                                    <td><input type="radio" name="myNeighbourhood" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Types of metals</td>
                                                    <td><input type="radio" name="typesOfMetals" value="1"></td>
                                                    <td><input type="radio" name="typesOfMetals" value="2"></td>
                                                    <td><input type="radio" name="typesOfMetals" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Living and non-living things</td>
                                                    <td><input type="radio" name="livingAndNonLiving" value="1"></td>
                                                    <td><input type="radio" name="livingAndNonLiving" value="2"></td>
                                                    <td><input type="radio" name="livingAndNonLiving" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Community helpers</td>
                                                    <td><input type="radio" name="communityHelpers" value="2"></td>
                                                    <td><input type="radio" name="communityHelpers" value="1"></td>
                                                    <td><input type="radio" name="communityHelpers" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Types of animals and birds</td>
                                                    <td><input type="radio" name="typeAnimalsBirds" value="1"></td>
                                                    <td><input type="radio" name="typeAnimalsBirds" value="2"></td>
                                                    <td><input type="radio" name="typeAnimalsBirds" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Colours</td>
                                                    <td><input type="radio" name="colours" value="1"></td>
                                                    <td><input type="radio" name="colours" value="2"></td>
                                                    <td><input type="radio" name="colours" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Transport</td>
                                                    <td><input type="radio" name="transport" value="1"></td>
                                                    <td><input type="radio" name="transport" value="2"></td>
                                                    <td><input type="radio" name="transport" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Emotions</td>
                                                    <td><input type="radio" name="emotions" value="1"></td>
                                                    <td><input type="radio" name="emotions" value="2"></td>
                                                    <td><input type="radio" name="emotions" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Good and bad habits</td>
                                                    <td><input type="radio" name="goodBadHabits" value="1"></td>
                                                    <td><input type="radio" name="goodBadHabits" value="2"></td>
                                                    <td><input type="radio" name="goodBadHabits" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Electricity</td>
                                                    <td><input type="radio" name="electricity" value="1"></td>
                                                    <td><input type="radio" name="electricity" value="2"></td>
                                                    <td><input type="radio" name="electricity" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Seasons</td>
                                                    <td><input type="radio" name="seasons" value="1"></td>
                                                    <td><input type="radio" name="seasons" value="2"></td>
                                                    <td><input type="radio" name="seasons" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>People of india</td>
                                                    <td><input type="radio" name="peopleOfIndia" value="1"></td>
                                                    <td><input type="radio" name="peopleOfIndia" value="2"></td>
                                                    <td><input type="radio" name="peopleOfIndia" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Good habits in school</td>
                                                    <td><input type="radio" name="goodHabitsSchools" value="1"></td>
                                                    <td><input type="radio" name="goodHabitsSchools" value="2"></td>
                                                    <td><input type="radio" name="goodHabitsSchools" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Festivals</td>
                                                    <td><input type="radio" name="festivals" value="1"></td>
                                                    <td><input type="radio" name="festivals" value="2"></td>
                                                    <td><input type="radio" name="festivals" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Sporst</td>
                                                    <td><input type="radio" name="sporst" value="1"></td>
                                                    <td><input type="radio" name="sporst" value="2"></td>
                                                    <td><input type="radio" name="sporst" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Insects</td>
                                                    <td><input type="radio" name="insects" value="1"></td>
                                                    <td><input type="radio" name="insects" value="2"></td>
                                                    <td><input type="radio" name="insects" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Vegetables and fruits</td>
                                                    <td><input type="radio" name="vegetablesFruits" value="1"></td>
                                                    <td><input type="radio" name="vegetablesFruits" value="2"></td>
                                                    <td><input type="radio" name="vegetablesFruits" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>Birds</td>
                                                    <td><input type="radio" name="birds" value="1"></td>
                                                    <td><input type="radio" name="birds" value="2"></td>
                                                    <td><input type="radio" name="birds" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>National symbols</td>
                                                    <td><input type="radio" name="nationalSymbols" value="1"></td>
                                                    <td><input type="radio" name="nationalSymbols" value="2"></td>
                                                    <td><input type="radio" name="nationalSymbols" value="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-plus"></span> Hindi</a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="td-left">अ-ह</td>
                                                    <td><input type="radio" name="aH" value="1"></td>
                                                    <td><input type="radio" name="aH" value="2"></td>
                                                    <td><input type="radio" name="aH" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">Letter words ल, ब, त, न</td>
                                                    <td><input type="radio" name="letterLBTN" value="1"></td>
                                                    <td><input type="radio" name="letterLBTN" value="2"></td>
                                                    <td><input type="radio" name="letterLBTN" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">3 Letter words</td>
                                                    <td><input type="radio" name="letterWord3" value="1"></td>
                                                    <td><input type="radio" name="letterWord3" value="2"></td>
                                                    <td><input type="radio" name="letterWord3" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">4 Letter words</td>
                                                    <td><input type="radio" name="letterWord4" value="1"></td>
                                                    <td><input type="radio" name="letterWord4" value="2"></td>
                                                    <td><input type="radio" name="letterWord4" value="3"></td>
                                                </tr>
                                                <tr>
                                                    <td class="td-left">2 Letter mixed words</td>
                                                    <td><input type="radio" name="letterMixedWord" value="1"></td>
                                                    <td><input type="radio" name="letterMixedWord" value="2"></td>
                                                    <td><input type="radio" name="letterMixedWord" value="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="form-btn branch-form-btn">
                <input type="submit" value="save" tabindex="3" id="submitForm"><a href="{{ url('Report Mgmt.') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
            </div>
        </div>    

</form>

<div id="progress_report_id" style="display: none;">
    <form action="{{ url('save_progress_report') }}" method="POST" enctype="multipart/form-data" >
        {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="branch-form">
                        <div class="form-box">
                            <div class="form-text form-box-width">
                                <h5>Select Branch <span>*</span> :</h5>
                            </div>
                            <div class="form-typ-box zone-box">
                                <div class="btn-group bootstrap-select show-tick branch-control">
                                    <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('reportType') ? 'is-invalid' : '' }}" tabindex="1"  name="branch_id" required id="branch_id">
                                        <option value="none">--Select--</option>
                                        @foreach($branch as $data)
                                        <option value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</option>
                                        @endforeach
                                    </select>
                                    @if (Session::has('branch_code'))
                                    <label class="error" for="branch_code">
                                        <span class="text-danger">
                                            {{ Session::get('branch_code') }}
                                        </span>
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-box">
                            <div class="form-text form-box-width">
                                <h5>Class <span>*</span> :</h5>
                            </div>   
                            <div class="form-typ-box zone-box">
                                <div class="btn-group bootstrap-select show-tick branch-control">
                                    <div class="select-box" >
                                        <select class="branch-cls" name="class" id="class" tabindex="2">
                                            <option value="none">-- Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="classError"></div>
                                @if (Session::has('class'))
                                <label class="error" for="class">
                                    <span class="text-danger">
                                        {{ Session::get('class') }}
                                    </span>
                                </label>
                                @endif
                            </div>
                        </div>

                        <div class="form-box">
                            <div class="form-text form-box-width">
                              <h5>Section <span>*</span> :</h5>
                            </div>   
                            <div class="form-typ-box zone-box">
                            <div class="btn-group bootstrap-select show-tick form-control">
                            <div class="select-box" >
                              <select class="class-sec" name="section" id="section" >
                                <option value="none">-- Select--</option>
                              </select>
                            </div>
                            </div>
                            @if (Session::has('section'))
                                <label class="error" for="section">
                                <span class="text-danger">
                                    {{ Session::get('section') }}
                                </span>
                                </label>
                            @endif
                            </div>
                        </div>

                        <div class="form-box">
                            <div class="form-text form-box-width">
                                <h5>Month - Year <span>*</span> :</h5>
                            </div>   
                            <div class="form-typ-box zone-box">
                                <div class="btn-group bootstrap-select show-tick branch-control">
                                    <div class="select-box" >
                                        <select class="month-year" name="month_year" id="month_year_id" tabindex="3">
                                            <option value="none">-- Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="monthYearError"></div>
                                @if (Session::has('month_year'))
                                <label class="error" for="month_year">
                                    <span class="text-danger">
                                        {{ Session::get('month_year') }}
                                    </span>
                                </label>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="progress_report_data"></div>
    </form>
</div>
</div>
</div>

@endsection

@section('footer')

@push('footer_script')

<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            dateFormat: "dd/mm/yy"
        });

        $("#progress_report_id").css("display", "none");
        $("#assessmentReport").css('display', 'none');
        var stdNotExists;
        $("#submitForm").click(function (event) {
            if ($("#reportType").val() === "2") {
                var nameGr = $("#nameOrGrNo").val();
                if (nameGr === "") {
                    $("#studentNotFound").html("<label for='nameOrGr' class='error'> <span class='text-danger'>Please Enter Student Name Or Gr No.</span> </label> ");
                    event.preventDefault();
                } else if ($("#datepicker").val() === "") {
                    $("#dateError").html("<label for='date' class='error'> <span class='text-danger'>Please Enter Date.</span> </label> ");
                    $("#datepicker").focus();
                    event.preventDefault();
                } else {
                    if (stdNotExists === true) {
                        $("#studentNotFound").html("<label for='nameOrGr' class='error'> <span class='text-danger'>Student Not Found.</span> </label> ");
                        $("#nameOrGrNo").focus();
                        event.preventDefault();
                    }
                }
            }
        });

        $("#reportType").on('change', function () {
            var type = $("#reportType").val();
            if (type === "2") {
                $("#assessmentReport").css('display', 'block');
                $("#progress_report_id").css("display", "none");
            } else {
                $("#assessmentReport").css('display', 'none');
            }
            if (type === "1") {
                $("#assessmentReport").css('display', 'none');
                $("#progress_report_id").css("display", "block");
            } else {
                $("#progress_report_id").css("display", "none");
            }
        });


        $("#nameOrGrNo").autocomplete({
            source: "{{ url('report/getStudentNameOrGr') }}"
        });

        $("#nameOrGrNo").on('focusout', function () {
            var Std_Name_Gr = $("#nameOrGrNo").val();
            $.ajax({
                type: "POST",
                url: "{{ url('report/getStudentData') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    Std_Name_Gr: Std_Name_Gr
                },
                success: function (data) {
                    if (data === "false" && $("#nameOrGrNo").val() !== "")
                    {
                        $("#studentNotFound").html("<label for='nameOrGrNo' class='error'><span class='text-danger'>Student Not Found.</span></label>");
                        stdNotExists = true;
                    } else {
                        stdNotExists = false;
                        $("#studentNotFound").html("");
                        $("#studentName").val(data.Std_Name);
                        $("#studentClass").val(data.Cla_Class);
                        $("#studentSection").val(data.Cla_Section);
                        $("#student_id").val(data.Std_Id);
                        $("#student_gr_no").val(data.Std_Gr_No);
                    }
                }, error: function (error) {
                    console.log(error);
                }
            });
        });

        $("#branch_id").on('change', function () {
            var bid = $(this).val();
            // alert(bid);
            $.ajax({
                url: "{{ url('raport/getclass') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    bid: bid,
                },
                dataType: "JSON",
                success: function (data) {
                    $(".branch-cls option").each(function () {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function () {
                        $(this).remove();
                    });
                    var items = [];
                    $.each(data, function (key, val) {
                        items.push("<option value='" + this['Cla_Bra_Id'] + "," + this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>");
                    });
                    $("#class").append('<option value="none">-- Select--</option>');
                    $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
                error(error) {
                }
            });
        });

        $("#class").on('change', function () {
            var cls = $(this).val();
            var bid = $("#branch_id").val();
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token: "{{ csrf_token() }}",
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
        });
        });

        $("#section").on('change',function(){
            var sec= $("#section").val();
            var cls= $("#class").val();
            var bid = $("#branch_id").val();
            if (sec === "none") {
            } else {
                $.ajax({
                    url: "{{ url('report/getmonthyear') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        bid : bid,
                        cls : cls,
                        section : sec 
                    },
                    success: function (data) {
                        if (data === "false") {
                            $("#monthYearError").html("<span class='error'><label class='text-danger'>This Class Subject Not Added Please Add Subject First.</label></span>");
                            $("#month_year_id").html('<option value="none">-- Select--</option>');
                        } else {
                            $("#monthYearError").html("");
                            $("#month_year_id").html("");
                            var items = [];
                            $.each(data, function (key, val) {
                                items.push("<option value='" + this['Sub_Id'] + "'>" + this['Sub_Month']+"-"+this['Sub_Year']+ "</option>");
                            });
                            $("#month_year_id").append('<option value="none">-- Select--</option>');
                            $("#month_year_id").append(items);
                        }
                    }, error: function (error) {

                    }
                });
            }
        });

        $("#month_year_id").on('change',function(){
            var sec= $("#section").val();
            var bid = $("#branch_id").val();
            var cls = $("#class").val();
            var subid = $(this).val();
            if(subid!="none"){
                $.ajax({
                   url : "{{ url('report/getstudentlist') }}",
                   type : "POST",
                   data : {
                       _token: "{{ csrf_token() }}",
                       bid : bid,
                       cls : cls,
                       subid : subid,
                       section : sec 
                   },
                   success : function(data){
                       $("#progress_report_data").html(data);
                   },error : function(error){
                   }
                });
            }else{
                $("#progress_report_data").html("");
            }
        });

        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
        });
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    });
</script>
@endpush

@section('footer_link_and_scripts')