@extends('auth.Master.master')

@section('title','Edit Progress report')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/report_img.png') }}" alt="">Edit Report</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif

<div class="clearfix"></div>

<?php $json_data = json_decode($rData, true); ?>
<div class="form-section">
    <div class="row">
        <div class="col-lg-3 col-lg-offset-3 col-md-4 col-sm-4 col-xs-4"><div class="form-text form-box-width">
                <b>ES = Established</b></div></div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b>Dv = Developing</b></div></div></div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="for    m-text form-box-width"><b>Em = Emergent</b></div></div></div>
        </        div>

        <form action="{{ url('update_progress_report') }}" method="POST" enctype="multipart/form-data" >
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="bs-example">
                        <input type="hidden" name="subid" value="{{ $sub_id }}">
                        <div class="panel-group" id="accordion">
                            <input type="hidden" name="reportId" value="{{ $reportId }}">
                            <input type="hidden" name="studentId" value="{{ $json_data['student_id'] }}">
                            <input type="hidden" name="studentGrNo" value="{{ $json_data['student_gr_no'] }}">
                            <input type="hidden" name="studentName" value="{{ $json_data['student_name'] }}">
                            <input type="hidden" name="studentClass" value="{{ $json_data['student_class'] }}">
                            <input type="hidden" name="studentSection" value="{{ $json_data['student_section'] }}">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $json_data['student_id'] }}" aria-expanded="true"><span class="glyphicon glyphicon-plus"></span> {{ $json_data['student_name'] }}</a>
                                    </h4>
                                </div>
                                <div id="collapse{{ $json_data['student_id'] }}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="td-left">Criteria</th>
                                                    <th>Es</th>
                                                    <th>Dv</th>
                                                    <th>Em</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $subidcount = 1; ?>
                                                <?php $subcount = 0; ?>
                                                @forelse($sub_data["subject"] as $subject)
                                                <tr>
                                                    <td class="td-left"><b>{{{ $subject["title"] }}}</b></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                @forelse($subject["criteria"] as $key=>$value)
                                                @if($json_data["subject"][$subcount]["subject_title"] == $subject["title"])
                                                
                                                <tr>
                                                    <td class="td-left">{{ $value }}</td>
                                                    <td><input name="{{ $key }}-{{ $subidcount }}" value="3" type="radio" @if($json_data["subject"][$subcount][$key]=="3") checked="true" @endif></td>
                                                    <td><input name="{{ $key }}-{{ $subidcount }}" value="2" type="radio" @if($json_data["subject"][$subcount][$key]=="2") checked="true" @endif></td>
                                                    <td><input name="{{ $key }}-{{ $subidcount }}" value="1" type="radio" @if($json_data["subject"][$subcount][$key]=="1") checked="true" @endif></td>
                                                </tr>
                                                @endif
                                                @empty
                                                criteria no found.
                                                @endforelse
                                                <?php $subcount++; ?>
                                                <?php $subidcount++; ?>
                                                @empty
                                                No Subject Found.
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="form-btn branch-form-btn">
                    <input type="submit" value="save" tabindex="3" id="progressSubmitForm"><a href="{{ url('Report Mgmt.') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
                </div>        
            </div>
        </form>
    </div>

    @endsection

    @section('footer')

    @section('footer_link_and_scripts')