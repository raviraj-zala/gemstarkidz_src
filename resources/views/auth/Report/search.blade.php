<table id="example2">
    <tbody>
        <tr>
            <th>
                <input type="checkbox" id="selectall" class="checked css-checkbox" />
                <label for="selectall" class="css-label table-ckeckbox">
                    <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white">
                    </th>
                    <th class="td-left">Report Type</th>
                    <th class="td-left">Gr No.</th>
                    <th class="td-left">Student Name</th>
                    <th class="td-left">Class</th>
                    <th class="td-left">Section</th>
                    <th class="td-left">Date</th>
                    <th>Edit</th>
        </tr>
        <?php $i = 1; ?>
        @forelse($students as $student)
        <tr>
            <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $student->Rpt_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
            <td class="td-left">@if($student->Rpt_Type=="1") Progress @endif @if($student->Rpt_Type=="2") Assessment @endif</td>
            <td class="td-left">{{ $student->Std_Gr_No }}</td>
            <td class="td-left">{{ $student->Std_Name }}</td>
            <td class="td-left">{{ $student->Cla_Class }}</td>
            <td class="td-left">{{ $student->Cla_Section }}</td>
            <td>{{ date("d-m-Y", strtotime($student->Rpt_Date)) }}</td>
            <td>@if($student->Rpt_Type=="2")<a href="{{ url('edit_report',$student->Rpt_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif
                @if($student->Rpt_Type=="1")<a href="{{ url('edit_progress_report',$student->Rpt_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif
            </td>
        </tr>
        <?php $i++; ?>
        @empty
        <tr>
            <td colspan="7">No Data Found.</td>
        </tr>
        @endforelse


    </tbody></table>