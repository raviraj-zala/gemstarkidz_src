<?php $json_data = json_decode($rData["Sub_Json_Data"], true); ?>

@if(count($student_data)>0)
<div class="row">
    <div class="col-lg-3 col-lg-offset-3 col-md-4 col-sm-4 col-xs-4"><div class="form-text form-box-width">
            <b>ES = Established</b></div></div>
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b>Dv = Developing</b></div></div></div>
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4"><div class="form-box"><div class="form-text form-box-width"><b>Em = Emergent</b></div></div></div>
</div>
@endif

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="bs-example">
            <input type="hidden" name="subid" value="{{ $subid }}">
            <?php $i = 1; ?>
            <?php $stdcount = 1; ?>
            @forelse($student_data as $student)
            <input type="checkbox" name="std_check[]" id="chk" value="{{ $student->Std_Id }}" >
            <div class="panel-group" id="accordion">
                <input type="hidden" name="studentId[]" value="{{ $student->Std_Id }}">
                <input type="hidden" name="studentGrNo[]" value="{{ $student->Std_Gr_No }}">
                <input type="hidden" name="studentName[]" value="{{ $student->Std_Name }}">
                <input type="hidden" name="studentClass[]" value="{{ $student->Cla_Class }}">
                <input type="hidden" name="studentSection[]" value="{{ $student->Cla_Section }}">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $student->Std_Id }}"><span class="glyphicon glyphicon-plus"></span> {{  $student->Std_Name }}</a>
                        </h4>
                    </div>
                    <div id="collapse{{ $student->Std_Id }}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="td-left">Criteria</th>
                                        <th>Es</th>
                                        <th>Dv</th>
                                        <th>Em</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $subidcount = 1; ?>
                                @forelse($json_data["subject"] as $subject)
                                <input type="hidden" name="sub_title[{{ $student->Std_Id }}][{{ $subidcount }}]" value="{{{ $subject["title"] }}}" >
                                <tr>
                                    <td class="td-left"><b>{{{ $subject["title"] }}}</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php $careacount = 1; ?>
                                @forelse($subject["criteria"] as $key=>$value)
                                <tr>
                                    <td class="td-left">{{ $value }}</td>
                                    <td><input name="{{ $key }}-{{ $subidcount }}-{{ $student->Std_Id }}" value="3" type="radio"></td>
                                    <td><input name="{{ $key }}-{{ $subidcount }}-{{ $student->Std_Id }}" value="2" type="radio"></td>
                                    <td><input name="{{ $key }}-{{ $subidcount }}-{{ $student->Std_Id }}" value="1" type="radio"></td>
                                </tr>
                                <?php $careacount++; ?>
                                @empty
                                criteria no found.
                                @endforelse

                                <?php $subidcount++; ?>
                                <?php $i++; ?>
                                @empty
                                No Subject Found.
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php $stdcount++; ?>
            @empty
            No Data Found.
            @endforelse
        </div>
    </div>
</div>

@if(count($student_data)>0)
<div>
    <div class="form-btn branch-form-btn">
        <input type="submit" value="save" tabindex="3" onClick="return setText()" id="progressSubmitForm"><a href="{{ url('Report Mgmt.') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
    </div>        
</div>
@endif

<script>
    function setText(){
    var chkArray = [];
    
    /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
    $("#chk:checked").each(function() {
        chkArray.push($(this).val());
    });
    
    /* we join the array separated by the comma */
    var selected;
    selected = chkArray.join(',') ;
    
    /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
    if(selected.length > 0){
        return true;
    }else{
        alert("Please at least check one of the student"); 
        return false;
    }
    }
</script>