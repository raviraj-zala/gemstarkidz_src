<?php
	header("Pragma: no-cache");
	header("Cache-Control: no-cache");
	header("Expires: 0");

?>

@extends('auth.Master.master')

@section('title','Payment Add')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/event-management.png') }}" alt="">Payment Add</h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<!-- <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div> -->
    {!! Session::get('error') !!}
@endif

<div class="form-section">
<form name="add-student" role="form" method="POST" action="{{ url('save_paytm') }}" enctype="multipart/form-data" id="studentForm">
    {{ csrf_field() }}
    <div class="left-form">

     		<div class="form-box">
                <div class="form-text ">
                    <h5>ORDER_ID<span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="ORDER_ID" class="{{ $errors->has('ORDER_ID') ? ' is-invalid' : '' }}" required tabindex="1" value="<?php echo  "ORDS" . rand(10000,99999999)?>" maxlength="20" autocomplete="off">
                </div>
                @if ($errors->has('ORDER_ID'))
                    <label class="error" for="ORDER_ID">
                    <span class="text-danger">
                        {{ $errors->first('ORDER_ID') }}
                    </span>
                    </label>
                @endif
            </div>
            <div class="form-box">
                <div class="form-text ">
                    <h5>CUSTID <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="CUST_ID" class="{{ $errors->has('CUST_ID') ? ' is-invalid' : '' }}" required tabindex="2" value="{{ old('CUST_ID') }}" maxlength="15" autocomplete="off" >
                </div>
                @if ($errors->has('CUST_ID'))
                    <label class="error" for="CUST_ID">
                    <span class="text-danger">
                        {{ $errors->first('CUST_ID') }}
                    </span>
                    </label>
                @endif
            </div>
            <div class="form-box">
                <div class="form-text ">
                    <h5>INDUSTRY_TYPE_ID <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="INDUSTRY_TYPE_ID" class="{{ $errors->has('INDUSTRY_TYPE_ID') ? ' is-invalid' : '' }}" required tabindex="3" value="{{ old('INDUSTRY_TYPE_ID') }}" maxlength="50" >
                </div>
                @if ($errors->has('INDUSTRY_TYPE_ID'))
                    <label class="error" for="INDUSTRY_TYPE_ID">
                    <span class="text-danger">
                        {{ $errors->first('INDUSTRY_TYPE_ID') }}
                    </span>
                    </label>
                @endif
            </div>
            <div class="form-box">
                <div class="form-text ">
                    <h5>Channel  <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="CHANNEL_ID" class="{{ $errors->has('CHANNEL_ID') ? ' is-invalid' : '' }}" required tabindex="6" value="{{ old('CHANNEL_ID') }}" maxlength="50" >
                </div>
                @if ($errors->has('CHANNEL_ID'))
                    <label class="error" for="CHANNEL_ID">
                    <span class="text-danger">
                        {{ $errors->first('CHANNEL_ID') }}
                    </span>
                    </label>
                @endif
            </div>
            <div class="form-box">
                <div class="form-text ">
                    <h5>txnAmount <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="TXN_AMOUNT" class="{{ $errors->has('TXN_AMOUNT') ? ' is-invalid' : '' }}" required tabindex="6" value="{{ old('TXN_AMOUNT') }}" maxlength="50" >
                </div>
                @if ($errors->has('TXN_AMOUNT'))
                    <label class="error" for="TXN_AMOUNT">
                    <span class="text-danger">
                        {{ $errors->first('TXN_AMOUNT') }}
                    </span>
                    </label>
                @endif
            </div>
            
    </div>
					
	 <div class="form-btn branch-form-btn">
            <input value="CheckOut" type="submit" tabindex="8"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="9" ></a>
            </div>
</form>
</div>

@endsection

@section('footer')