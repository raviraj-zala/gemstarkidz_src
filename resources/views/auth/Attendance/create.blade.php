@extends('auth.Master.master')
@section('title','Insert Report')
@section('site_header')
@section('sidebar')
@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/report_img.png') }}" alt="zone-img">Insert Attendance</h2>      
    </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif


@if(Session::has('error'))
<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('error') }}
</div>
@endif
<div class="clearfix"></div>

<div class="form-section">
    <form name="insert-report" role="form" method="POST" action="{{ url('attendance/getstudentlist') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="branch-form">
            <input type="hidden" id="student_id" name="student_id" >
            <input type="hidden" id="student_gr_no" name="student_gr_no" >
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="branch-form">
                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Select Branch <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box">
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('branch_id') ? 'is-invalid' : '' }}" tabindex="1"  name="branch_id" required id="branch_id" >

                                    <option value="none">--Select--</option>
                                    @foreach($branch as $data)
                                    <option value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</option>
                                    @endforeach
                                </select>
                                @if (Session::has('branch_code'))
                                <label class="error" for="branch_code">
                                    <span class="text-danger">
                                        {{ Session::get('branch_code') }}
                                    </span>
                                </label>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                            <h5>Class <span>*</span> :</h5>
                        </div>   
                        <div class="form-typ-box zone-box">
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <div class="select-box" >
                                    <select class="branch-cls" name="class" id="class" tabindex="2">
                                        <option value="none">-- Select--</option>
                                    </select>
                                </div>
                            </div>
                            <div id="classError"></div>
                            @if (Session::has('class'))
                            <label class="error" for="class">
                                <span class="text-danger">
                                    {{ Session::get('class') }}
                                </span>
                            </label>
                            @endif
                        </div>
                    </div>

                    <div class="form-box">
                        <div class="form-text form-box-width">
                          <h5>Section <span>*</span> :</h5>
                        </div>   
                        <div class="form-typ-box zone-box">
                         <div class="btn-group bootstrap-select show-tick form-control">
                            <div class="select-box" >
                               <select class="class-sec" name="section" id="section" >
                                  <option value="none">-- Select--</option>
                                </select>
                            </div>
                        </div>
                        @if (Session::has('section'))
                            <label class="error" for="section">
                            <span class="text-danger">
                                {{ Session::get('section') }}
                            </span>
                            </label>
                        @endif
                        </div>
                    </div>
                    @php
                        $today_date = date('d-m-Y');
                    @endphp
                    <div class="form-box">
                        <div class="form-text form-box-width">
                        <h5>Date <span>*</span> :</h5>
                        </div>
                        <div class="form-typ-box zone-box"> 
                            <div class="btn-group bootstrap-select show-tick branch-control">
                                <input type="text" name="date" id="datepicker" tabindex="3" value="@if(isset($branch->created_at)){{date('d-m-Y',strtotime($branch->created_at))}}@else{{$today_date}}@endif">
                            </div>
                            <!-- <div id="monthYearError"></div> -->
                            @if (Session::has('month_year'))
                            <label class="error" for="month_year">
                                <span class="text-danger">
                                    {{ Session::get('month_year') }}
                                </span>
                            </label>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </form>
</div>              
<div id="progress_report_data"></div>  
<!-- <div id="attendancereport" style="display: none;">
     <form action="{{ url('store_attendance') }}" method="POST" enctype="multipart/form-data" >
            {{ csrf_field() }}
        <div class="clearfix"></div>
        <div class="row">

            @if(Session::has('success'))
            <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ Session::get('success') }}
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ Session::get('error') }}
            </div>
            @endif
        </div>
        <div class="table-form">   
            <table id="example2">
                <tbody>
                    <tr>
                        <th>
                            <input type="checkbox" id="selectall" class="checked css-checkbox" />
                            <label for="selectall" class="css-label table-ckeckbox">
                            <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white">
                        </th>
                        <th class="td-left">Student Name</th>
                        <th class="td-left">Present</th>
                        <th class="td-left">Absent</th>
                    </tr>
                    <?php $i = 1; ?>

                    @forelse($students as $student)

                    <tr>
                        <td><input id="class{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $student->Rpt_Id }}" class="css-checkbox check-all"><label for="class{{ $i }}" class="css-label table-ckeckbox"></label></td>
                        <td class="td-left">{{ $student->Std_Name }}</td>
                            @if($student->Rpt_Type=="1")<a href="{{ url('edit_Attendance',$student->Rpt_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @empty
                    <tr><td colspan="7">No Data Found.</td></tr>
                    @endforelse

                    @if(count($students)>0)
                        <div>
                            <div class="form-btn branch-form-btn">
                                <input type="submit" value="save" tabindex="3" onClick="return setText()" id="progressSubmitForm"><a href="{{ url('Attendance Mgmt.') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
                            </div>        
                        </div>
                    @endif
                </tbody>
            </table>
        </div>
    </form>
</div> -->

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script> -->

<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            autoclose: true,  
            dateFormat: "dd/mm/yy",
            todayHighlight: true,
        });
        $("#attendancereport").css('display', 'none');

        $("#section").on('change', function () {
            var type = $("#branch_id").val();
            // alert(type);
            if (type === "1") {
                $("#attendancereport").css('display', 'none');
            } else {
                $("#attendancereport").css("display", "block");
            }
        });
        $("#nameOrGrNo").autocomplete({
            source: "{{ url('attendance/getStudentNameOrGr') }}"
        });

        $("#nameOrGrNo").on('focusout', function () {
            var Std_Name_Gr = $("#nameOrGrNo").val();
            $.ajax({
                type: "POST",
                url: "{{ url('attendance/getstudentdata') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    Std_Name_Gr: Std_Name_Gr
                },
                success: function (data) {
                    if (data === "false" && $("#nameOrGrNo").val() !== "")
                    {
                        $("#studentNotFound").html("<label for='nameOrGrNo' class='error'><span class='text-danger'>Student Not Found.</span></label>");
                        stdNotExists = true;
                    } else {
                        stdNotExists = false;
                        $("#studentNotFound").html("");
                        $("#studentName").val(data.Std_Name);
                        $("#studentClass").val(data.Cla_Class);
                        $("#studentSection").val(data.Cla_Section);
                        $("#student_id").val(data.Std_Id);
                        $("#student_gr_no").val(data.Std_Gr_No);
                    }
                }, error: function (error) {
                    console.log(error);
                }
            });
        });

        $("#branch_id").on('change', function () {
            var bid = $(this).val();
            // alert(bid);
            $.ajax({
                url: "{{ url('attendance/getclass') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    bid: bid,
                },
                dataType: "JSON",
                success: function (data) {
                    $(".branch-cls option").each(function () {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function () {
                        $(this).remove();
                    });
                    var items = [];
                    $.each(data, function (key, val) {
                        items.push("<option value='" + this['Cla_Bra_Id'] + "," + this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>");
                    });
                    $("#class").append('<option value="none">-- Select--</option>');
                    $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
                error(error) {
                }
            });
            $("#progress_report_data").html("");
            
        });

        $("#class").on('change', function () {
            var cls = $(this).val();
            var bid = $("#branch_id").val();
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            // alert(cls);
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token: "{{ csrf_token() }}",
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
            });
            $("#progress_report_data").html("");

        });
        $("#section").on('change',function(){
            var sec= $("#section").val();
            var cls= $("#class").val();
            var bid = $("#branch_id").val();
            // var subid = $(this).val();
            if (sec === "none") {
            } else {
                // console.log(sec);
                $.ajax({
                    url: "{{ url('attendance/getstudentlist') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        bid : bid,
                        cls : cls,
                        // subid : subid,
                        section : sec 
                    },
                    success: function (data) {
                    console.log(data);

                        $("#progress_report_data").html(data);

                    }, error: function (error) {
                    }

                });
            }
            $("#progress_report_data").html("");
        });
         
         /*function setText()
        {
            var chkArray = [];
            
             look for all checkboes that have a class 'chk' attached to it and check if it was checked 
            $("#chk:checked").each(function() {
                chkArray.push($(this).val());
            });
            
             we join the array separated by the comma 
            var selected;
            selected = chkArray.join(',') ;
            
             check if there is selected checkboxes, by default the length is 1 as it contains one single comma 
            if(selected.length > 0){
                return true;
            }else{
                alert("Please at least check one of the student"); 
                return false;
            }
        }*/

    });
</script>

@endsection
@section('footer_link_and_scripts')
@endsection

@section('footer')

@push('footer_script')
