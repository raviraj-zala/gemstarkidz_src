<table id="example2"> 
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      <label for="selectall" class="css-label table-ckeckbox">
      <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label></th>
      <th>Zone</th>
      <th>Branch Name</th>
      <th>Branch Code</th>
      <th>Address</th>
      <th>Status</th>
      <th>Edit</th>
    </tr> 

@if($branch)
		<?php $i=1; ?>
	@foreach($branch as $data)
        <tr>
          	<td><input id="branch{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Brn_Id }}" class="css-checkbox check-all"><label for="branch{{ $i }}" class="css-label table-ckeckbox"></label></td>

          	<td>{{ $data->Zon_Name }}</td>
          	<td>{{ $data->Brn_Name }}</td>
          	<td>{{ $data->Brn_Code }}</td>
          	<td>{{ $data->Brn_Address }}</td>
          	<td>
          		@if($data->Brn_Status == 1)
					Active
				@elseif($data->Zon_Status == 0)
					In-Active
				@endif
          	</td>
          	<td><a href="{{ url('edit_branch',$data->Brn_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a></td>
        </tr>
	    <?php $i++; ?>
    @endforeach
@else
	No Data Found
@endif
</tbody></table>