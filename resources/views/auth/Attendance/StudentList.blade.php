<form name="insert-report" role="form" method="GET" action="{{ url('save_attendance') }}" enctype="multipart/form-data" id="formData">

    <div class="line-1">        
        <div class="mangement-btn user-text">
            <h2><img src="{{ asset('public/images/report_img.png') }}" alt="">Student List</h2>      
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">

        @if(Session::has('success'))
        <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('error') }}
        </div>
        @endif

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="main-btn">
               
            </div>
        </div>
    </div>
    <div class="table-form">   
        <table id="example2">
            <tbody>
                <tr>
                    <th class="td-left">Student GR No.</th>   
                    <th class="td-left">Student Name</th>
                    <th class="td-left">Present &nbsp;<input type="checkbox" id="presentall" class="checked css-checkbox" />
                    <label for="presentall" class="css-label table-ckeckbox">
                    <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
                    <th class="td-left">Absent &nbsp;<input type="checkbox" id="absentall" class="checked css-checkbox" />
                    <label for="absentall" class="css-label table-ckeckbox">
                    <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></th>
                </tr>
                <?php $i = 1; ?>
                @forelse($student_data as $student)
                <tr>
                    <input type="hidden" name="Att_Cla_Id" value="{{ $student->Std_Cla_Id }}" >
                    <td>{{$student->Std_Gr_No}}</td>
                    <td class="td-left">{{ $student->Std_Name }}</td>
                        <!-- @if($student->Rpt_Type=="1")<a href="{{url('edit_progress_report',$student->Rpt_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="notepad"></a>@endif
                    </td> -->
                    <td><input id="class{{ $i }}presentall" type="checkbox" onClick="checkbox_is_checked()" name="present_id[]" value="{{ $student->Std_Id }}" class="css-checkbox present-all present-all{{$student->Std_Id}}"><label for="class{{ $i }}presentall" class="css-label table-ckeckbox"></label></td>
                    <td><input id="class{{ $i }}absentall" type="checkbox" onClick="checkbox_is_checked()" name="absent_id[]" value="{{ $student->Std_Id }}" class="css-checkbox absent-all absent-all{{$student->Std_Id}}">
                        <label for="class{{ $i }}absentall" class="css-label table-ckeckbox"></label></td>
                </tr>
                <?php $i++; ?>
                @empty
                <tr><td colspan="7">No Data Found.</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
    @if(count($student_data)>0)
        <div>
            <div class="form-btn branch-form-btn">
                <input type="submit" value="save" tabindex="3" onClick="return setText()" id="progressSubmitForm"><a href="{{ url('save_attendance') }}"><input value="Cancel" type="button"  tabindex="5" ></a>
            </div>    
        </div>
    @endif
</form>

<script>    
$(function () {
    /*$("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });*/
    $("#presentall").click(function () {
        if ($("#presentall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.present-all').prop('checked', true);
                $('.absent-all').prop('checked', false);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.present-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
    $("#absentall").click(function () {
        if ($("#absentall").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $('.absent-all').prop('checked', true);
                $('.present-all').prop('checked', false);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function () {
                $('.absent-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
    $('.present-all').click(function(){
        // var presentid = $('input[name="present_id[]"]').val();
         var presentid = this.value;
         if(presentid == 'checked' ){
            $('.absent-all'+presentid).attr('checked',true);
         } else {
            $('.absent-all'+presentid).attr('checked',false);
         }
         // console.log(id);
    });
    $('.absent-all').click(function(){
        var absentid = this.value;
        if(absentid == 'checked'){
            $('.present-all'+absentid).attr('checked',true);
         } else {
            $('.present-all'+absentid).attr('checked',false);
         }
        // console.log(absentid);
        // var absentid = $('input[name="absent_id[]"]').val();
    });
});

    function setText()
    {
        var chkArray = [];
        
         // look for all checkboes that have a class 'chk' attached to it and check if it was checked 
        $(".present-all:checked").each(function() {

            console.log($(this).val());
            chkArray.push($(this).val());
        });
         // we join the array separated by the comma 
        var selected;
        selected = chkArray.join(',') ;
        
         // check if there is selected checkboxes, by default the length is 1 as it contains one single comma 
        if(selected.length > 0)
        {
            return true;
        }else{
             alert("Please at least check one of the student"); 
             return false;
        }
    }
    /* $("#section").on('change', function () {
            var type = $("#section").val();
            if (type === "2") {
                $("#attendancereport").css('display', 'none');
            } else {
                $("#attendancereport").css("display", "block");
            }
        });*/
</script>