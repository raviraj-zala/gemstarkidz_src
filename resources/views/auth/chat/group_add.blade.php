@extends('auth.Master.master')
@section('title','Create Group')
@section('site_header')
@section('sidebar')
@section('content')
<form id="editGroup" name="edit-group" role="form" method="get" action="{{ route('chat.edit.save_group') }}" enctype="multipart/form-data" >
<div class="box box-primary">
    <div class="line-1 branch-line">
        <div class="mangement-btn user-text">
        <h2 class="box-title"><b><input type="hidden" name="menu[]" value="">Teacher</b></h2>
        </div>
        <div class="user-checkall">
            <a href="javascript:void(0);" onclick="selectAllMod(true,'checkAllTeacher')">Check All</a>|
            <a href="javascript:void(0);" onclick="unselectAllMod(false,'unCheckAllTeacher')">UnCheck All</a>
        </div>
    </div>
    <input type="hidden" name="group_id" value="{{$master->Grp_Mas_Id}}">
        @php $i=0; @endphp
        @foreach($teacher as $data)
            <div class="branch-box col-md-3" style="width: 100%;">
                <input type="checkbox"  name="teacher[]" id="tckbox{{ $i }}" class="css-checkbox teacher" value="{{ $data->Use_Id }}" @foreach($details as $detail)  @if($detail->Use_Id==$data->Use_Id) title="{{$detail->id_member}}" checked="checked" @endif @endforeach>
                <label for="tckbox{{ $i }}" class="css-label">{{ $data->Use_Name }}</label>
            </div>            
        @php $i++; @endphp
        @endforeach
        <div class="box box-primary">
            <div class="line-1 branch-line">
                <div class="mangement-btn user-text">
                <h2 class="box-title"><b><input type="hidden" name="menu[]" value="">Parent</b></h2>
                </div>
                <div class="user-checkall">
                    <a href="javascript:void(0);" onclick="selectAllMod(true,'checkAllParent')">Check All</a>|
                    <a href="javascript:void(0);" onclick="unselectAllMod(false,'unCheckAllParent')">UnCheck All</a>
                </div>
            </div>
        @php $i=0; @endphp
        @foreach($parent as $data)
            <div class="branch-box" style="width: 100%;">
                <div class="col-md-3">
                    <input type="checkbox" name="parent[]" id="pckbox{{ $i }}" class="css-checkbox parent" value="{{ $data->Use_Id }}" @foreach($details as $detail)  @if($detail->Use_Id==$data->Use_Id) title="{{$detail->id_member}}" checked="checked" @endif @endforeach >
                    <label for="pckbox{{ $i }}" class="css-label">{{ $data->Use_Name }}</label>
                </div>
            </div>
            @php $i++; @endphp
        @endforeach
        </div>
</div>

<div class="row">
    <div class="form-btn">
        <input type="submit" value="save" tabindex="3" >
        <a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="4"  ></a>
    </div>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
            }
            else if($(this).prop("checked") == false){
                var Grp_Det_Mem_Id = $(this).attr("title");
                if(Grp_Det_Mem_Id!="")
                {
                    $.ajax({
                        type : "POST",
                        url : "{{ url('delete_grp_member') }}",
                        data : { 
                                _token:     '{{ csrf_token() }}',
                                Grp_Det_Mem_Id : Grp_Det_Mem_Id
                            },
                        success : function(data){
                        },
                        error: function(error){
                        }

                    });
                }
            }
        });
    });
    
// $("#submitBtn").on('click',function(e){
//     var gn = $("#name_id").val();
//     var checkCount = 0;
//     $("#userError").html('');
//     $("#branchError").html('');
//     if(gn.trim().length==0){
//         $("#groupError").html('<label class="text-danger">Please enter group name.</label>');
//         $("#name_id").focus();
//         e.preventDefault();
//     } 
//     $("input.parent").each(function(i){
//         if($(this).prop('checked')==true){
//             checkCount++;
//         }
//     });
//     $("input.teacher").each(function(i){
//         if($(this).prop('checked')==true){
//             checkCount++;   
//         }
//     });
//     if(checkCount==0){
//         $("#userError").html('<br><br><label>Please select atleast one user.</label>');
//         e.preventDefault();
//     }
// });
function selectAllMod(bool,type)
{
    if(type == "checkAllTeacher"){
        $("input.teacher").each(function(i){
            $(this).prop('checked', true);
        });
    } else if(type == "checkAllParent"){
        $("input.parent").each(function(i){
            $(this).prop('checked', true);
        });
    }
}
function unselectAllMod(bool,type)
{
    if(type == "unCheckAllTeacher"){
        $("input.teacher").each(function(i){
            $(this).prop('checked', false);
        });   
    } else if(type == "unCheckAllParent"){
        $("input.parent").each(function(i){
            $(this).prop('checked', false);
        });
    }
}
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')