@extends('auth.Master.master')
@section('title','Class Management')
@section('site_header')
@section('sidebar')
@section('content')
	<div class="line-1">        
	    <div class="mangement-btn user-text">
	        <h2><img src="{{ asset('public/images/') }}" alt="">Group Detail</h2>      
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-12">
		    <div class="search-box">	
			 	{{-- <input id="btnSearch" name="Zon_Name" placeholder="Search" type="search" /> --}}
		    </div>
	    </div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">   
	      		<ul>     
		      		<li>
		            	<input class="create" value="Add" type="button" onClick="document.location.href='{{ url('group/add/'.$master->Grp_Mas_Id) }}'">
		            </li>
		            <li>
						<input type="submit" value="Delete" class="delete" id="btnDelete">
		            </li>
	            </ul>
	          </div>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="table-form">
				<table id="example2">
					<thead>
						<tr>
							<th>
								<input type="checkbox" id="selectall" class="checked css-checkbox" />
	      						<label for="selectall" class="css-label table-ckeckbox">
	      						<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label>
	      					</th>
	      					<th class="td-left">Member Name</th>
	      					{{-- <th>Action</th> --}}
	      				</tr>
	      			</thead>
					<tbody>
						@php $i=0; @endphp
						@forelse($detail as $data)
							<tr>
		      					<td>
		      						<input id="group{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{$data->Grp_Det_Id}}" class="css-checkbox check-all">
									<label for="group{{ $i }}" class="css-label table-ckeckbox"></label>
		      					</td>
		      					<td class="td-left">{{ $data->Use_Name }}</td>
		      					{{-- <td>
		      						<a href="">
		      							<img src="http://intellewitinfotech.com/public/images/notepad.png" alt="Edit">
		      						</a>
		      					</td> --}}
		      				</tr>
	      				@php
	      				$i++;
	      				@endphp
	      				@empty
		      				<tr>
		      					<td colspan="5" align="center">Group not found.</td>
		      				</tr>
	      				@endforelse
	      			</tbody>
	      		</table>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#selectall").click(function () {
				if ($("#selectall").is(':checked')) {
					$("input[type=checkbox]").each(function(){
						$('.check-all').prop('checked', true);
					});
					$("#active").show();
				} else {
					$("input[type=checkbox]").each(function(){
						$('.check-all').prop('checked', false);
					});
					$("#active").show();
				}
			});
		});
		$('#btnDelete').on('click',function(){
	    var n = $("input:checked").length;
	    if (n > 0)
	    {
	        var group_member_delete=confirm('Delete selected records???');
	        if(group_member_delete){
	            var ids = [];
	            $('input:checked').each(function(i){
	                ids[i] = $(this).val();
	            });     
	            $.ajax({
	                type : "POST",
	                url : "{{ url('delete_group_member') }}",
	                data : { 
	                        _token:     '{{ csrf_token() }}',
	                        Grp_Det_Mem_Id : ids
	                    },
	                success : function(data){
	                    $("input[type='checkbox']").each(function(i){
	                        $(this).prop('checked', false);
	                    });
	                    window.location.reload();
	                }
	            });
	        }
	    }else{
	        alert('Please select atleast one record!');
	    }
		});
	</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')