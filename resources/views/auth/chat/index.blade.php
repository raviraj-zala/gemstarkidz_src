@extends('auth.Master.master')
@section('title','Class Management')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt="">Chat Management</h2>      
	</div>
</div>
<div class="clearfix"></div>
<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
	    <div class="search-box">	
		 	<input id="btnSearch" name="Zon_Name" placeholder="Search" type="search" />
	    </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="main-btn">   
      		<ul>     
	      		<li>    
	            	<input class="create" value="create" type="button" onClick="document.location.href='{{ route('chat.create.group') }}'">
	            </li>
	            <li>
					<input type="button" id="active" value="Active" class="active btnActInact">
				</li>
	            <li>
					<input type="submit" value="Delete" class="delete" id="btnDelete">
	            </li>
            </ul>
          </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="table-form">
			<table id="example2">
				<thead>
					<tr>
						<th width="10%">
							<input type="checkbox" id="selectall" class="checked css-checkbox" />
                            <label for="selectall" class="css-label table-ckeckbox">
      						<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label>
      					</th>
      					<th class="td-left" width="45%">Group Name</th>
      					<!-- <th class="td-left"></th> -->
      					<th width="20%">Created By</th>
      					<th width="15%">Status</th>
                <th>Chat</th>
      					<th width="10%">View</th>
      				</tr>
      			</thead>
				<tbody>
                    <?php $i=0; ?>
					@forelse($group as $data)
          				<tr>
          					<td>
          						<input id="group{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{$data->Grp_Mas_Id}}" class="css-checkbox check-all">
    					        <label for="group{{ $i }}" class="css-label table-ckeckbox"></label>
          					</td>
          					<td class="td-left">{{ $data->Grp_Mas_Name }}</td>
          					<!-- <td></td> -->
          					<td>{{ $data->Use_Name }}{{ "(".date('d-m-Y',strtotime($data->Grp_Mas_CreatedAt)).")" }}</td>
          					<td>
          						@if($data->Grp_Mas_Status == "1")
            						Active
        							@else
        								In-Active
          						@endif
          					</td>
                    <td>
                      <a href="">
                        <a href="{{ route('chat.group.open',['id'=>$data->Grp_Mas_Id]) }}"><img src="{{ url('public/images/chat.png') }}" alt="Chat"></a>
                      </a>
                    </td>
                    <td>
                      <a href="">
                        <a href="{{ url('/group_detail/'.$data->Grp_Mas_Id) }}"><img src="http://intellewitinfotech.com/public/images/notepad.png" alt="Detail"></a>
                      </a>
                    </td>
          				</tr>
                    <?php $i++; ?>
                    @empty
          				<tr>
          					<td colspan="5" align="center">Group not found.</td>
          				</tr>
                    @endforelse
      			</tbody>
  			</table>
		</div>
	</div>
</div>
<div class="paggination-section">
@if ($group->lastPage() > 1)
    <ul>
        @if ($group->currentPage() != 1 && $group->lastPage() >= 5)
            <li><a href="{{ $group->url($group->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($group->currentPage() != 1)
            <li>
                <a  href="{{ $group->url($group->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($group->currentPage()-2, 1); $i <= min(max($group->currentPage()-2, 1)+4,$group->lastPage()); $i++)
                <li>
                    <a class="{{ ($group->currentPage() == $i) ? 'active' : '' }}" href="{{ $group->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($group->currentPage() != $group->lastPage())
            <li>
                <a href="{{ $group->url($group->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($group->currentPage() != $group->lastPage() && $group->lastPage() >= 5)
            <li>
                <a href="{{ $group->url($group->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
    $("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function(){
            $('.check-all').prop('checked', true);
        });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function(){
              $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
});
$('#btnDelete').on('click',function(){
    var n = $("input:checked").length;
    if (n > 0)
    {
        var ch_delete=confirm('Delete selected records???');
        if(ch_delete){
            var ids = [];
            $('input:checked').each(function(i){
                ids[i] = $(this).val();
            });     
            $.ajax({
                type : "POST",
                url : "{{ url('delete_group') }}",
                data : { 
                        _token:     '{{ csrf_token() }}',
                        Chat_Id : ids
                    },
                success : function(data){
                    console.log(data);
                    $("input[type='checkbox']").each(function(i){
                        $(this).prop('checked', false);
                    });
                    window.location.reload();
                }
            });
        }
    }else{
        alert('Please select atleast one record!');
    }
});

</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')