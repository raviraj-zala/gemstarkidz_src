@extends('auth.Master.master')

@section('title','Create Group')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt="">Create Group</h2>      
	</div>
</div>

<div class="clearfix"></div>

<form id="createGroup" name="create-group" role="form" method="POST" action="{{ route('chat.create.save_group') }}" enctype="multipart/form-data" >
	{{ csrf_field() }}
	<div class="form-section">
		<div class="branch-form">
			
			<div class="form-box">
				<div class="form-text form-box-width">
	                <h5>Branch<span>*</span> :</h5>
	            </div>   
				<div class="form-typ-box zone-box">
					<div class="btn-group bootstrap-select show-tick branch-control">
	                	<select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('branch') ? 'is-invalid' : '' }}" tabindex="1"  name="branch" id="branch_id" required>
	                		@if(count($branch)>0)
	                			<option value="none">SELECT BRANCH</option>
	                		@endif
	                		@forelse($branch as $data)
	                    		<option value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</option>
	                    	@empty
	                    		<option value="none">BRANCH NOT FOUND</option>
	                    	@endforelse
	                	</select>
	                	<div id="branchError" style="float: left;"></div>
	                </div>

	                @if ($errors->has('branch'))
	                    <span class="invalid-feedback">
	                        <label class="error">{{ $errors->first('branch') }}</label>
	                    </span>
	                @endif
	            </div>    
	        </div>

			<div class="form-box">
				<div class="form-text form-box-width">
	                <h5>Group Name <span>*</span> :</h5>
	            </div>
				<div class="form-typ-box zone-box">
	                <input type="text" name="name" class="branch-control {{ $errors->has('name') ? ' is-invalid' : '' }}" tabindex="2" maxlength="50" id="name_id">
					<div id="groupError"></div>
				</div>
	            @if ($errors->has('name'))
	                <span class="invalid-feedback">
	                    <label class="error">{{ $errors->first('name') }}</label>
	                </span>
	            @endif
			</div>

			<div class="form-box">
				<div class="form-text form-box-width">
					<h5>Status<span>*</span> :</h5>
				</div>   
				<div class="form-typ-box zone-box">
					<div class="btn-group bootstrap-select show-tick branch-control">
						<select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="3"  name="status" required>
							<option value="1">Active</option>
							<option value="0">In-Active</option>
						</select>
					</div>
					@if ($errors->has('status'))
					<span class="invalid-feedback">
						<label class="error">{{ $errors->first('status') }}</label>
					</span>
					@endif
				</div>    
			</div>

		</div>
	</div>

	

	<div class="row">
	    <div class="col-lg-12">
	    	<div id="userError"></div>
			<div id="user_list_id">
			</div>
		</div>
	</div>

</form>

<script type="text/javascript">
$(document).ready(function(){
	$("#branch_id").on('change',function(){
		$("#branchError").html('');
		$("#user_list_id").html("Loading...");
		var branch_id = $(this).val();
		if(branch_id!="none"){
			$.ajax({
				type : "POST",
				url : "{{ route('chat.create.get_teacher_parent') }}",
				data : { id:branch_id,"_token":"{{ csrf_token() }}" },
				success : function(data){
					$("#user_list_id").html("Loading...");
					if(data=="false"){
						$("#user_list_id").html("Something went wrong please try again later.");
					} else{
						$("#user_list_id").html(data);
					}
				}, error : function(error){
					console.log(error);
				}
			});
		} else{
			$("#branchError").html('<label class="text-danger">Please select branch.<label>');
			$("#user_list_id").html('Please select branch.');
		}
	});
});
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')