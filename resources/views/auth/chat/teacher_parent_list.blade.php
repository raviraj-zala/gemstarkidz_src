<div class="box box-primary">
    <div class="line-1 branch-line">
        <div class="mangement-btn user-text">
        <h2 class="box-title"><b><input type="hidden" name="menu[]" value="">Teacher</b></h2>
        </div>
        <div class="user-checkall">
            <a href="javascript:void(0);" onclick="selectAllMod(true,'checkAllTeacher')">Check All</a>|
            <a href="javascript:void(0);" onclick="unselectAllMod(false,'unCheckAllTeacher')">UnCheck All</a>
        </div>
    </div>

    @php $i=0; @endphp
    <div class="branch-box" style="width: 100%;">
        @forelse($teacher as $data)
            <div class="col-md-3">
                <input type="checkbox" name="teacher[]" id="tckbox{{ $i }}" class="css-checkbox teacher" value="{{ $data->Use_Id }}">
                <label for="tckbox{{ $i }}" class="css-label">{{ $data->Use_Name }}</label>
            </div>
            @php $i++; @endphp
        @empty
            <div class="user-checkall"><a href="javascript:void(0);">Teacher not found.</a></div>
        @endforelse
    </div>
</div>

<div class="box box-primary">
    <div class="line-1 branch-line">
        <div class="mangement-btn user-text">
        <h2 class="box-title"><b><input type="hidden" name="menu[]" value="">Parent</b></h2>
        </div>
        <div class="user-checkall">
            <a href="javascript:void(0);" onclick="selectAllMod(true,'checkAllParent')">Check All</a>|
            <a href="javascript:void(0);" onclick="unselectAllMod(false,'unCheckAllParent')">UnCheck All</a>
        </div>
    </div>

    @php $i=0; @endphp
    <div class="branch-box" style="width: 100%;">
        @forelse($parent as $data)
            <div class="col-md-3">
                <input type="checkbox" name="parent[]" id="pckbox{{ $i }}" class="css-checkbox parent" value="{{ $data->Use_Id }}">
                <label for="pckbox{{ $i }}" class="css-label">{{ $data->Use_Name }}</label>
            </div>
            @php $i++; @endphp
        @empty
            <div class="user-checkall"><a href="javascript:void(0);">Parent not found.</a></div>
        @endforelse
    </div>
</div>

<div class="row">
    <div class="form-btn">
        <input type="submit" value="save" tabindex="3" id="submitBtn">
        <a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="4"  ></a>
    </div>
</div>

<script type="text/javascript">
$("#submitBtn").on('click',function(e){
    var gn = $("#name_id").val();
    var checkCount = 0;
    $("#userError").html('');
    $("#branchError").html('');
    if(gn.trim().length==0){
        $("#groupError").html('<label class="text-danger">Please enter group name.</label>');
        $("#name_id").focus();
        e.preventDefault();
    } 
    $("input.parent").each(function(i){
        if($(this).prop('checked')==true){
            checkCount++;
        }
    });
    $("input.teacher").each(function(i){
        if($(this).prop('checked')==true){
            checkCount++;   
        }
    });
    if(checkCount==0){
        $("#userError").html('<br><br><label>Please select atleast one user.</label>');
        e.preventDefault();
    }
});
function selectAllMod(bool,type)
{
    if(type == "checkAllTeacher"){
        $("input.teacher").each(function(i){
            $(this).prop('checked', true);
        });
    } else if(type == "checkAllParent"){
        $("input.parent").each(function(i){
            $(this).prop('checked', true);
        });
    }
}
function unselectAllMod(bool,type)
{
    if(type == "unCheckAllTeacher"){
        $("input.teacher").each(function(i){
            $(this).prop('checked', false);
        });   
    } else if(type == "unCheckAllParent"){
        $("input.parent").each(function(i){
            $(this).prop('checked', false);
        });
    }
}
</script>