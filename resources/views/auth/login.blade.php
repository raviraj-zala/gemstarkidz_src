<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GemStarKidz - Log In</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/menu-style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/qry_new.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,600,700,800" rel="stylesheet">
<!--     <link rel="stylesheet" href="{{ asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>
<body>
<!--    <div class="login-box">
         <div class="login-logo">
            <a href=""><b>Admin</b>LTE</a>
        </div> -->

<form action="{{ url('postLogin') }}" method="POST">
  {{ csrf_field() }}
<div class="main-back">
  <div class="back-clr">
    <div class="login-screen">
      <div class="logo">
       <a href="#"><img src="{{ asset('public/images/logo_new.png') }}" alt="logo"></a>
      </div>
      <div class="login-form">
      @if(Session::has('success'))
        <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('success') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ Session::get('error') }}
        </div>
        @endif
        <input placeholder="Email" type="text" name="username" required autofocus>
        @if ($errors->has('username'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
        <input placeholder="Password" type="Password" name="password" required>
        @if ($errors->has('password'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <input class="login-btn" value="login" type="submit">
      </div>
    </div>
  </div>    
</div>  
</form>
       <!--  <div class="login-box-body">
            <form action="{{ url('postLogin') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
                    @if ($errors->has('username'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" required> 
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="box-footer">
                    <center><button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 20%;">Sign In</button></center>
                </div>
            </form>
        </div>
    </div> -->

    <script src="{{ asset('public/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/plugins/iCheck/icheck.min.js') }}"></script>

</body>
</html>







<!-- <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gemstarkidz</title>
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">
    <link href="public/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,600,700,800" rel="stylesheet"> 
  </head>
  <body>
  <div class="main-back">
  <div class="back-clr">
    <div class="login-screen">
      <div class="logo">
       <a href="#"><img src="public/images/logo.png" alt="logo"></a>
      </div>
      <div class="login-form">
        <form method="POST" action="{{ url('login') }}">
            {{ csrf_field() }}
            <input placeholder="Username" type="text" name="username" required autofocus>
             @if ($errors->has('username'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
            <input placeholder="Password" type="Password" name="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <input class="login-btn" value="login" type="submit">
        </form>
      </div>
    </div>
  </div>    
  </div>  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  </body>
</html> -->