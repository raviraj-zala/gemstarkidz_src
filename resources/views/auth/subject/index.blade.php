@extends('auth.Master.master')
@section('title','Subject Management')
@section('site_header')
@section('sidebar')
@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt=""> Subject Management</h2>      
	</div>
</div>


<div class="clearfix"></div>
	<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif
	<div class="col-md-4 col-sm-4 col-xs-12">
		</div>
	    <div class="col-md-8 col-sm-8 col-xs-12">
	      <div class="main-btn">
	      	<ul>
	      		<li>
			 		<input type="submit" value="Delete" class="delete" id="btnDelete" >
	      		</li>
	      	</ul>
	      </div>
	    </div>
	  </div>

<div class="table-form">   
<table id="example2"> 
    <tbody><tr>
      <th><input type="checkbox" id="selectall" class="checked css-checkbox" />
      <label for="selectall" class="css-label table-ckeckbox">
      <img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label></th>
      <th class="td-left">Branch</th>
      <th class="td-left">Class</th>
      <th class="td-left">Subject</th>
      <!-- <th class="td-left">Section</th> -->
      <th class="td-left">Month</th>
    </tr> 

	<?php $i=1; ?>
	@forelse($subject as $data)
        <tr>
          	<td><input id="branch{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Sub_Id }}" class="css-checkbox check-all"><label for="branch{{ $i }}" class="css-label table-ckeckbox"></label></td>
          	<td class="td-left">{{ $data->Brn_Name }}</td>
          	<td class="td-left">{{ $data->Sub_Class }}</td>
          	<td class="td-left"><a href="{{ url('view/subject/'.$data->Sub_Id) }}" style="color: blue;">View File</a></td>
          	<td class="td-left">{{ $data->Sub_Month."-".$data->Sub_Year }}</td>
        </tr>
    <?php $i++; ?>
    @empty
    <tr><td colspan="7" align="center"> No Data Found</td></tr>
    @endforelse
  
  </tbody></table>
  <!-- </form> -->
  </div>


<div class="paggination-section">
@if ($subject->lastPage() > 1)
    <ul>
        @if ($subject->currentPage() != 1 && $subject->lastPage() >= 5)
            <li><a href="{{ $subject->url($subject->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($subject->currentPage() != 1)
            <li>
                <a  href="{{ $subject->url($subject->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($subject->currentPage()-2, 1); $i <= min(max($subject->currentPage()-2, 1)+4,$subject->lastPage()); $i++)
                <li>
                    <a class="{{ ($subject->currentPage() == $i) ? 'active' : '' }}" href="{{ $subject->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($subject->currentPage() != $subject->lastPage())
            <li>
                <a href="{{ $subject->url($subject->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($subject->currentPage() != $subject->lastPage() && $subject->lastPage() >= 5)
            <li>
                <a href="{{ $subject->url($subject->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
	$("#selectall").click(function () {
		if ($("#selectall").is(':checked')) {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', true);
			});
			$("#active").show();
		} else {
			$("input[type=checkbox]").each(function(){
				$('.check-all').prop('checked', false);
			});
			$("#active").show();
		}
	});
});

$('#btnDelete').on('click',function(){
	var n = $("input:checked").length;
	if (n > 0)
	{
		var ch_delete=confirm('Delete selected records???');
		if(ch_delete){
			var ids = [];
			$('input:checked').each(function(i){
				ids[i] = $(this).val();
			});		

			$.ajax({
                type : "POST",
                url : "{{ url('delete_subject') }}",
                data : { 
			            _token:     '{{ csrf_token() }}',
			            Sub_Id : ids
			        },
                success : function(data){
                	$("input[type='checkbox']").each(function(i){
						$(this).prop('checked', false);
					});
                	window.location.reload();
                }
            });
		}
	}else{
		alert('Please select atleast one record!');
	}
});
</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')