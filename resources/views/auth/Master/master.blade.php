<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    {{-- <link rel="icon" href="{{ asset('public/favicon.png') }}" type="image/gif">  --}}
    {{-- FAVICON --}}
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('public/images/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('public/images/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('public/images/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('public/images/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('public/images/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('public/images/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('public/images/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('public/images/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('public/images/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('public/images/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('public/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('public/images/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('public/images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('public/images/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('public/images/favicon//ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <title>GemStarKidz - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/menu-style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/qry_new.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="/resources/demos/style.css">
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
      }
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<div class="wrapper">
<nav id="sidebar">
@section('site_header')
    <div class="sidebar-header">
      <a href="#"><img src="{{ asset('public/images/logo_new.png') }}" alt="logo"></a>
    </div>    
@section('sidebar') 

    <div class="mm-page">
                <div class="main">
                   <div class="middle">
                    <div id="left">                       
                     <ul id="nav">
                        @foreach($menu as $data)
                            @if($data->children->count() > 0)
                                    <li>
                                        <a class="link active" href="#">
                                        <span id="Left1_rpt_Menu_l_GroupName_0">{{ $data->Mod_Name }}</span></a>
                                                <ul style="display: none;">
                                                @foreach($data->children as $submenu)
                                                    @foreach($CURight as $rig)    
                                                        @if($submenu->Mod_Id==$rig || $rig=="Admin")
                                                    <li>
                                                        <a class="link" href="{{ url($submenu->Mod_Name) }}">{{ $submenu->Mod_Name }}</a>
                                                    </li>
                                                        @endif
                                                    @endforeach
                                                    
                                                @endforeach
                                                <li>
                                                    <a class="link" href="{{url('list_Attendance')}}">Attendance mgmt.</a>
                                                </li>
                                            </ul>
                                    </li>

                            @else
                                <li><a href="#">{{ $data->Mod_Name }}</a></li>

                            @endif
                        @endforeach
                    </ul>
                    @if(Auth::user()->Use_Type == "1") {{-- || Auth::user()->Use_Type == "5" --}}
                    <ul id="nav">
                        <li>
                            <a class="link" href="#">Report</a>
                            <ul>
                                <li>
                                    <a href="{{route('report.dashboard')}}" class="link">Attendance</a>
                                </li>
                                <li>
                                    <a href="{{route('fees_report.index')}}" class="link">Fees</a>
                                </li>
                                <li>
                                    <a href="{{route('report.user')}}" class="link">User</a>
                                </li>
                                <li>
                                    <a href="{{route('report.student')}}" class="link">Student</a>
                                </li>
                                <li>
                                    <a href="{{route('report.social_activity')}}" class="link">Social Activity</a>
                                </li>
                                <li>
                                    <a href="{{route('report.event')}}" class="link">Event</a>
                                </li>
                                <li>
                                    <a href="{{route('report.assignment')}}" class="link">Diary Note</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    @endif
                   </div>
                  </div>
                 </div>
    </div>
</nav>
@show



<div class="right-screen-box">
            <div id="content">
            <div class="navbar-header">
                <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                    <i class="fas fa-bars"></i>
                </button>
            </div>
        <div class="top-icon">
           <ul>
                <li>
                    <a href="{{ asset('home') }}"><img src="{{ asset('public/images/dashboard.png') }}" alt="dashboard"></a><p>dashboard</p>
                </li>
                <li>
                    <a class="report-icon" href="#"><img src="{{ asset('public/images/graph.png') }}" alt="graph"></a><p>report</p>
                </li>
                <li>
                    <a class="user-icon" href="{{ url('changepassword') }}"><img src="{{ asset('public/images/user.png') }}" alt="user"></a><p>change password</p>
                </li>
                <li>
                    <a class="support-icon" href="{{ url('Help Mgmt.') }}"><img src="{{ asset('public/images/support.png') }}" alt="support"></a><p>support</p>
                </li>
           </ul>
         </div> 
         <div style="float: right;" id="google_translate_element"></div>
         <div class="navbar-custom-menu" style="float: right; padding-left:35%;">
            <ul class="nav navbar-nav">
                <?php 
                    if(Auth::user()){
                ?>
                    <li class="dropdown user user-menu">
                        <a href="{{ url('logout') }}" style="color:#ff975b;">Welcome {{ Auth::user()->Use_Name }} | Logout</a>    
                    </li>
                <?php
                    }else{
                ?>
                    <li class="dropdown user user-menu">
                        <a href="{{ url('login') }}" style="color:#ff975b;">Login</a>    
                    </li>
                <?php } ?>
            </ul>
        </div>
    <div class="content-wrapper">
        @yield('content')  
    </div>
</div>

  
@section('footer')
@show
<style type="text/css">
    .dropdown {
      position: relative;
      display: inline-block;
    }
</style>

<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('public/js/jquery_003.js') }}"></script>
<script src="{{ asset('public/js/jquery_002.js') }}"></script>
<script src="{{ asset('public/js/jquery.js') }}"></script>
<script src="{{ asset('public/js/Menu.js') }}"></script>
  <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                 });
             });
         </script>
        <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#content').toggleClass('active-2');
                 });
             });
</script>       



<script src="{{ asset('public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- <script type="text/javascript" src="{{ asset('public/validation/jquery.validate.js') }}"></script> -->
<script src="{{ URL::asset('public/validation/validation.js') }}"></script>
@section('footer_link_and_scripts') 
@show
@stack('footer_script')
</body>
</html>