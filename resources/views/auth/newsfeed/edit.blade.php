@extends('auth.Master.master')

@section('title','Newsfees Edit')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt="">Edit Newsfeed</h2>
	</div>
</div>
<div class="clearfix"></div>

<form name="create-leave" role="form" method="POST" action="{{ url('update_newsfeed') }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="hidden" id="user_type" value="{{{ Auth::user()->Use_Type }}}">
	<input type="hidden" name="newsfeed_unique_id" value="{{{ $newsfeed->Nfd_Unique_Id }}}">
	<div class="form-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-10">
							<div class="form-box">
								<div class="form-text form-box-width">
									<h5>Title<span>*</span> :</h5>
								</div>
								<div class="form-typ-box zone-box"> 
									<input type="text" name="title" class="branch-control" id="title" value="{{ $newsfeed->Nfd_Title }}" >
									<div id="titleError"></div>
									@if ($errors->has('title'))
										<label class="error" for="imageOrVideo">
											<span class="text-danger">
												{{ $errors->first('title') }}
											</span>
										</label>
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-10">
							<div class="form-box">
								<div class="form-text form-box-width">
									<h5>Description<span>*</span> :</h5>
								</div>
								<div class="form-typ-box zone-box"> 
									<textarea class="branch-control" name="description" id="description" rows="5">{{ $newsfeed->Nfd_Description }}</textarea>
									<div id="descriptionError"></div>
									@if ($errors->has('description'))
										<label class="error" for="imageOrVideo">
											<span class="text-danger">
												{{ $errors->first('description') }}
											</span>
										</label>
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-10">

							<div class="form-box">
								<div class="form-text form-box-width">
								<h5>Image/Video :</h5>
								</div>
								<div class="form-typ-box zone-box browser-box"> 
									<input name="imageOrVideo[]" class="browser-btn {{ $errors->has('imageOrVideo') ? ' is-invalid' : '' }}" type="file" 	accept="image/*,video/*" multiple="true">
								@if ($errors->has('imageOrVideo'))
									<label class="error" for="imageOrVideo">
										<span class="text-danger">
											{{ $errors->first('imageOrVideo') }}
										</span>
									</label>
								@endif
								</div>
							</div>
						</div>
					</div>

					<input type="hidden" name="status" value="{{ $newsfeed->Nfd_Status }}">
					<input type="hidden" name="tag_type" value="{{ $newsfeed->Nfd_Tag_Type }}">
					@if(Auth::user()->Use_Type == "2")
						<div class="row">
							<div class="col-lg-10">
								<div class="form-box">
						            <div class="form-text form-box-width">
						              <h5>Tag <span>*</span> :</h5>
						            </div>   
						            <div class="form-typ-box zone-box">
						                <div class="btn-group bootstrap-select show-tick branch-control">
						                <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('tag_type') ? 'is-invalid' : '' }}" name="tag_type" id="tagType">
						                    <option value="class" @if($newsfeed->Nfd_Tag_Type == 'class') selected="" @endif>Class</option>
						                    <option value="student" @if($newsfeed->Nfd_Tag_Type == 'student') selected="" @endif>Student</option>
						                </select>
						                @if ($errors->has('tag_type'))
						                    <label class="error" for="tag_type">
						                    <span class="text-danger">
						                        {{ $errors->first('tag_type') }}
						                    </span>
						                    </label>
						                @endif
						                </div>
						            </div>    
						        </div>
							</div>
						</div>
					@else
						<div class="row">
							<div class="col-lg-10">
								<div class="form-box">
						            <div class="form-text form-box-width">
						              <h5>Status <span>*</span> :</h5>
						            </div>   
						            <div class="form-typ-box zone-box">
						                <div class="btn-group bootstrap-select show-tick branch-control">
						                <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="13">
						                    <option value="1" @if($newsfeed->Nfd_Status == "1" ) selected="true" @endif>Active</option>
						                    <option value="0" @if($newsfeed->Nfd_Status == "0" ) selected="true" @endif>In-Active</option>
						                </select>
						                @if ($errors->has('status'))
						                    <label class="error" for="status">
						                    <span class="text-danger">
						                        {{ $errors->first('status') }}
						                    </span>
						                    </label>
						                @endif
						                </div>
						            </div>    
						        </div>
						    </div>
						</div>
					@endif
				</div>
				<div class="col-lg-6">
					<div class="form-text form-box-width">
						<h5>Images / Videos </h5>
					</div>   
					<div class="row">
					<?php $i = 1; ?>
					@forelse($newsfeed_info as $info)
						@if($info->Nfi_Type == "1")
							<div class="col-lg-5">
								<img src="{{ url('public/attechment/newsfeed/'.$info->Nfi_Path) }}" width="100%" height="100%">
							</div>
						@elseif($info->Nfi_Type == "2")
							<div class="col-lg-5">
								<video width="100%" controls>
								  <source src="{{ url('public/attechment/newsfeed/'.$info->Nfi_Path) }}">
								  Your browser does not support HTML5 video.
								</video>
							</div>
						@endif
						@if($i%2==0)
							</div>
							<br>
							<div class="row">
						@endif
						<?php $i++; ?>
					@empty
						<div class="form-text form-box-width">
							<h5>Not any image or video.</h5>
						</div>	
					@endforelse
					</div>
				</div>
			</div>

		</div>
	</div>

	@if($created_user_type == "branchAdmin" || $created_user_type == "admin" )
		<div class="row" id="classList">
	        <div class="col-lg-12">
	        <?php $i=1; $cid=""; $cids=""; ?>   
	        @foreach($branch as $data)
	                <div class="box box-primary">
	                @if($data->Brn_Id==$data->Brn_Zon_Id && $data->Brn_Id!=$cid)
	                    <?php $cid=$data->Brn_Id; ?>
	                    <div class="line-1 branch-line">        
	                         <div class="mangement-btn user-text">
	                          <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
	                        </div>
	                        <div class="user-checkall">
	                            <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
	                            <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
	                        </div>
	                    </div>
	                @endif
	                        <div class="branch-box col-md-3">
	                           <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class="class_id css-checkbox {{ 'brn'.$data->Brn_Id }}" value="{{ $data->Cla_Id }}"
                                @foreach($newsfeed_tag as $tag)
                                    @if($tag->Nft_Cla_Id == $data->Cla_Id )
                                        checked="true"
                                    @endif
                                @endforeach
	                           ><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Class }}</label>
	                        </div>
	                </div>
	            <?php $i++; ?>
	        @endforeach
	        </div>
	    </div>
    @elseif($created_user_type == "teacher")
		<div class="row" id="classList" style="@if($tag_type == 'class') display: block; @else display: none; @endif ">
		<div class="col-lg-12">
		<?php $i=1; $cid=""; $cids=""; ?>   
		@foreach($branch as $data)
		        <div class="box box-primary">
		        @if($data->Brn_Id==$data->Brn_Zon_Id && $data->Brn_Id!=$cid)
		            <?php $cid=$data->Brn_Id; ?>
		            <div class="line-1 branch-line">        
		                 <div class="mangement-btn user-text">
		                  <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
		                </div>
		                <div class="user-checkall">
		                    <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
		                    <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
		                </div>
		            </div>
		        @endif
		                <div class="branch-box col-md-3">
		                   <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class="class_id css-checkbox {{ 'brn'.$data->Brn_Id }}" value="{{ $data->Cla_Id }}"
		                   	@foreach($newsfeed_tag as $tag)
		                        @if($tag->Nft_Cla_Id == $data->Cla_Id )
		                            checked="true"
		                        @endif
		                    @endforeach
		                   ><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Class }}</label>
		                </div>
		        </div>
		    <?php $i++; ?>
		@endforeach
		</div>
		</div>
		<div class="row" id="studentList" style="@if($tag_type == 'student') display:block; @else display: none; @endif">
		<div class="col-lg-12">
			<div class="box box-primary">
		            <div class="line-1 branch-line">        
		                 <div class="mangement-btn user-text">
		                  <h2><input type="hidden" name="" value="">Students</h2>      
		                </div>
		                <div class="user-checkall">
		                    <a href="javascript:void(0);" onclick="selectAll(true,{{ "" }})">Check All </a>|
		                    <a href="javascript:void(0);" onclick="unselectAll(false,{{ "" }})">UnCheck All</a>
		                </div>
		            </div>
		            <?php $i=1; ?>
		            @forelse($students as $data)
		            <div class="branch-box col-md-3">
		               <input type="checkbox" name="student_id[]" id="chckboxstd{{ $i }}" class="student_cls css-checkbox {{ 'std'.$data->Std_Id }}" value="{{ $data->Std_Id }}" 
		               @foreach($newsfeed_tag as $tag)
		                    @if($tag->Nft_Std_Id == $data->Std_Id )
		                        checked="true"
		                    @endif
		                @endforeach
		               ><label class="css-label" for="chckboxstd{{ $i }}">{{ $data->Std_Name }}</label>
		            </div>
		            <?php $i++; ?>
		            @empty
		           		<label class="css-label" for="chckbox{{ $i }}">Student Not Found</label>
		       		@endforelse
		        </div>
		</div>
		</div>
    @endif

	<div class="row">
	    <div class="col-lg-12">
	        <div class="form-btn">
	            <input value="Update" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" ></a>
	        </div>
	    </div>
	</div>

</form>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitBtn").on('click',function(e){
		var title = $("#title").val();
		var description = $("#description").val();
		var user_type = $("#user_type").val();
		if(user_type=="1" || user_type=="5"){
			var checkedNum = $('input[name="class_id[]"]:checked').length;
			$("#titleError").html("");
			$("#descriptionError").html("");
			if(title=="" || title.trim()=="" ){
				$("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
				$("#title").focus();
				return false;
				e.preventDefault();
			}else if(description=="" || description.trim()=="" ){
				$("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
				$("#description").focus();
				return false;
				e.preventDefault();
			}else if(checkedNum=="0"){
				alert('Please at least check one of the class');
				return false;
				e.preventDefault();
			}
		}else if(user_type=="2"){
			var tag_type = $("#tagType").val();
			$("#titleError").html("");
			$("#descriptionError").html("");
			if(title=="" || title.trim()=="" ){
				$("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
				$("#title").focus();
				return false;
				e.preventDefault();
			}else if(description=="" || description.trim()=="" ){
				$("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
				$("#description").focus();
				return false;
				e.preventDefault();
			}else if(tag_type == "class"){
				var checkedClass = $('input[name="class_id[]"]:checked').length;
				if(checkedClass=="0")
				{
					alert('Please at least check one of the class');
					return false;
					e.preventDefault();
				}
			}else if(tag_type == "student"){
				var checkedStudent = $('input[name="student_id[]"]:checked').length;
				if(checkedStudent=="0")
				{
					alert('Please at least check one of the student');
					return false;
					e.preventDefault();
				}
			}
		}
	});
	$("#tagType").on('change',function(e){
		var tag_type = $("#tagType").val();
		if(tag_type=="class")
		{
			$("#studentList").css('display','none');
			$("#classList").css('display','block');
		}else if(tag_type=="student"){
			$("#classList").css('display','none');
			$("#studentList").css('display','block');
		}
	})
});
</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')