@extends('auth.Master.master')

@section('title','Newsfees Create')

@section('site_header')

@section('sidebar')

@section('content')
<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt="">Create Newsfeed</h2>
	</div>
</div>
<div class="clearfix"></div>

<form name="create-leave" role="form" method="POST" action="{{ url('save_newsfeed') }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="hidden" id="user_type" value="{{{ Auth::user()->Use_Type }}}">
	<div class="form-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<div class="form-box">
						<div class="form-text form-box-width">
							<h5>Title<span>*</span> :</h5>
						</div>
						<div class="form-typ-box zone-box"> 
							<input type="text" name="title" class="branch-control" id="title" value="{{ old('title') }}">
							<div id="titleError"></div>
							@if ($errors->has('title'))
								<label class="error" for="imageOrVideo">
									<span class="text-danger">
										{{ $errors->first('title') }}
									</span>
								</label>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5">
					<div class="form-box">
						<div class="form-text form-box-width">
							<h5>Description<span>*</span> :</h5>
						</div>
						<div class="form-typ-box zone-box"> 
							<textarea class="branch-control" name="description" id="description" rows="5">{{ old('description') }}</textarea>
							<div id="descriptionError"></div>
							@if ($errors->has('description'))
								<label class="error" for="imageOrVideo">
									<span class="text-danger">
										{{ $errors->first('description') }}
									</span>
								</label>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5">

					<div class="form-box">
						<div class="form-text form-box-width">
						<h5>Image/Video :</h5>
						</div>
						<div class="form-typ-box zone-box browser-box"> 
							<input name="imageOrVideo[]" class="browser-btn {{ $errors->has('imageOrVideo') ? ' is-invalid' : '' }}" type="file" 	accept="image/*,video/*" multiple="true">
						@if ($errors->has('imageOrVideo'))
							<label class="error" for="imageOrVideo">
								<span class="text-danger">
									{{ $errors->first('imageOrVideo') }}
								</span>
							</label>
						@endif
						</div>
					</div>

				</div>
			</div>

			@if(Auth::user()->Use_Type == "2")
			<input type="hidden" name="status" value="0">
			<div class="row">
				<div class="col-lg-5">
					<div class="form-box">
			            <div class="form-text form-box-width">
			              <h5>Tag <span>*</span> :</h5>
			            </div>   
			            <div class="form-typ-box zone-box">
			                <div class="btn-group bootstrap-select show-tick branch-control">
			                <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('tag_type') ? 'is-invalid' : '' }}" name="tag_type" id="tagType">
			                    <option value="class" selected="">Class</option>
			                    <option value="student">Student</option>
			                </select>
			                @if ($errors->has('tag_type'))
			                    <label class="error" for="tag_type">
			                    <span class="text-danger">
			                        {{ $errors->first('tag_type') }}
			                    </span>
			                    </label>
			                @endif
			                </div>
			            </div>    
			        </div>
				</div>
			</div>
			@else
			<input type="hidden" name="tag_type" value="class">
			<div class="row">
				<div class="col-lg-5">
					<div class="form-box">
			            <div class="form-text form-box-width">
			              <h5>Status <span>*</span> :</h5>
			            </div>   
			            <div class="form-typ-box zone-box">
			                <div class="btn-group bootstrap-select show-tick branch-control">
			                <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="13">
			                    <option value="1" >Active</option>
			                    <option value="0">In-Active</option>
			                </select>
			                @if ($errors->has('status'))
			                    <label class="error" for="status">
			                    <span class="text-danger">
			                        {{ $errors->first('status') }}
			                    </span>
			                    </label>
			                @endif
			                </div>
			            </div>    
			        </div>
			    </div>
			</div>
			@endif
		</div>
	</div>

	<div class="row" id="classList">
        <div class="col-lg-12">
        <?php $i=1; $cid=""; $cids=""; ?>   
        @foreach($branch as $data)
                <div class="box box-primary">
                @if($data->Brn_Id==$data->Brn_Zon_Id && $data->Brn_Id!=$cid)
                    <?php $cid=$data->Brn_Id; ?>
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="branch_id[]" value="{{ $data->Brn_Id }}">{{ $data->Brn_Name }}</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Brn_Id }})">Check All </a>|
                            <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Brn_Id }})">UnCheck All</a>
                        </div>
                    </div>
                @endif
                        <div class="branch-box col-md-3">
                           <input type="checkbox" name="class_id[]" id="chckbox{{ $i }}" class="class_id css-checkbox {{ 'brn'.$data->Brn_Id }}" value="{{ $data->Cla_Id }}"
                           @if(old('branch'))
                                @foreach(old('branch') as $b)
                                    @if($b == $data->Cla_Bra_Id )
                                        checked="true"
                                    @endif
                                @endforeach
                           @endif
                           ><label class="css-label" for="chckbox{{ $i }}">{{ $data->Cla_Class }}</label>
                        </div>
                </div>
            <?php $i++; ?>
        @endforeach
        </div>
    </div>

    @if(Auth::user()->Use_Type == "2" )
    <div class="row" id="studentList" style="display: none;">
    	<div class="col-lg-12">
			<div class="box box-primary">
                    <div class="line-1 branch-line">        
                         <div class="mangement-btn user-text">
                          <h2><input type="hidden" name="" value="">Students</h2>      
                        </div>
                        <div class="user-checkall">
                            <a href="javascript:void(0);" onclick="selectAll(true,{{ $data->Std_Id }})">Check All </a>|
                            <a href="javascript:void(0);" onclick="unselectAll(false,{{ $data->Std_Id }})">UnCheck All</a>
                        </div>
                    </div>
                    <?php $i=1; ?>
                    @forelse($students as $data)
                    <div class="branch-box col-md-3">
                       <input type="checkbox" name="student_id[]" id="chckboxstd{{ $i }}" class="student_cls css-checkbox {{ 'std'.$data->Std_Id }}" value="{{ $data->Std_Id }}" ><label class="css-label" for="chckboxstd{{ $i }}">{{ $data->Std_Name }}</label>
                    </div>
                    <?php $i++; ?>
                    @empty
                   		<label class="css-label" for="chckbox{{ $i }}">Student Not Found</label>
               		@endforelse
                </div>
    	</div>
    </div>
    @endif

	<div class="row">
	    <div class="col-lg-12">
	        <div class="form-btn">
	            <input value="save" type="submit" id="submitBtn"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" ></a>
	        </div>
	    </div>
	</div>

</form>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitBtn").on('click',function(e){
		var title = $("#title").val();
		var description = $("#description").val();
		var user_type = $("#user_type").val();
		if(user_type=="1" || user_type=="5"){
			var checkedNum = $('input[name="class_id[]"]:checked').length;
			$("#titleError").html("");
			$("#descriptionError").html("");
			if(title=="" || title.trim()=="" ){
				$("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
				$("#title").focus();
				return false;
				e.preventDefault();
			}else if(description=="" || description.trim()=="" ){
				$("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
				$("#description").focus();
				return false;
				e.preventDefault();
			}else if(checkedNum=="0"){
				alert('Please at least check one of the class');
				return false;
				e.preventDefault();
			}
		}else if(user_type=="2"){
			var tag_type = $("#tagType").val();
			$("#titleError").html("");
			$("#descriptionError").html("");
			if(title=="" || title.trim()=="" ){
				$("#titleError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter title</span></label>");
				$("#title").focus();
				return false;
				e.preventDefault();
			}else if(description=="" || description.trim()=="" ){
				$("#descriptionError").html("<label class='error' for='imageOrVideo'><span class='text-danger'>Please enter description</span></label>");
				$("#description").focus();
				return false;
				e.preventDefault();
			}else if(tag_type == "class"){
				var checkedClass = $('input[name="class_id[]"]:checked').length;
				if(checkedClass=="0")
				{
					alert('Please at least check one of the class');
					return false;
					e.preventDefault();
				}
			}else if(tag_type == "student"){
				var checkedStudent = $('input[name="student_id[]"]:checked').length;
				if(checkedStudent=="0")
				{
					alert('Please at least check one of the student');
					return false;
					e.preventDefault();
				}
			}
		}
	});
	$("#tagType").on('change',function(e){
		var tag_type = $("#tagType").val();
		if(tag_type=="class")
		{
			$("#studentList").css('display','none');
			$("#classList").css('display','block');
		}else if(tag_type=="student"){
			$("#classList").css('display','none');
			$("#studentList").css('display','block');
		}
	})
});
</script>

@endsection

@section('footer')

@section('footer_link_and_scripts')