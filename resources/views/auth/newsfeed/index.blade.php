@extends('auth.Master.master')
@section('title','Newsfeed Management')
@section('site_header')
@section('sidebar')
@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/') }}" alt=""> Newsfeed Management</h2>      
	</div>
</div>
<div class="clearfix"></div>

<div class="row">
	@if(Session::has('success'))
	<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('success') }}
	</div>
	@endif

	@if(Session::has('error'))
	<div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">×</span>
	    </button>
	    {{ Session::get('error') }}
	</div>
	@endif

	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="search-box">
			<input id="btnSearch" name="Hmw_Subject" placeholder="Search" type="search" />
		</div>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-12">
	    <div class="main-btn">
	      	<ul>
	      		<li>
	      			<input class="create" value="create" type="button" onClick="document.location.href='{{ url('create_newsfeed') }}'">
	      		</li>
	      		@if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
	      		<li>
					<input id="active" value="Active" class="active btnActInact" type="button">
				</li>
				<li>
	            	<input id="inactive" value="In-Active" class="in-active btnActInact" type="button">
				</li>
				@endif
	      		<li>
					<input type="submit" value="Delete" class="delete" id="btnDelete">
	      		</li>			 		
	      	</ul>
	    </div>
	</div>
</div>

<div class="table-form">

	<table id="example2"> 
	    <tbody>
	    	<tr>
	      		<th><input type="checkbox" id="selectall" class="checked css-checkbox"/>
		      		<label for="selectall" class="css-label table-ckeckbox">
		      		<img src="{{ asset('public/images/checkbox-white.jpg') }}" alt="checkbox-white"></label>
		      	</th>
		      	<th class="td-left">Title</th>
		      	@if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
		      	<th class="td-left">Branch Name</th>
		      	@endif
		      	<th class="td-left">Created By</th>
		      	<th>Status</th>
		      	<th>Action</th>
	    	</tr> 
			<?php $i=1; ?>
			<?php $newsfeed_unique_id = ""; $newsfeed_unique_id1 = ""; $newsfeed_unique_id2 = ""; $newsfeed_unique_id3 = ""; $newsfeed_unique_id4 = ""; $newsfeed_unique_id5 = ""; ?>
			@forelse($newsfeed as $data)
	        <tr>
	          	<td>
	          		@if($newsfeed_unique_id != $data->Nfd_Unique_Id )
	          			<?php $newsfeed_unique_id = $data->Nfd_Unique_Id ?>
	          				@if(Auth::user()->Use_Type == "1")
          						<input id="leave{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Nfd_Id }}" class="css-checkbox check-all">
	          						<label for="leave{{ $i }}" class="css-label table-ckeckbox"></label>
	          				@elseif(Auth::user()->Use_Type == "2")
          						@if($data->Nfd_CreatedBy == Auth::user()->Use_Id)
	          						<input id="leave{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Nfd_Id }}" class="css-checkbox check-all">
	          						<label for="leave{{ $i }}" class="css-label table-ckeckbox"></label>
          						@endif
          					@elseif(Auth::user()->Use_Type == "5")
          						<input id="leave{{ $i }}" type="checkbox" onClick="checkbox_is_checked()" name="id[]" value="{{ $data->Nfd_Id }}" class="css-checkbox check-all">
	          						<label for="leave{{ $i }}" class="css-label table-ckeckbox"></label>
      						@endif

	          		@endif
	          	</td>
	          	<td class="td-left">
	          		@if($newsfeed_unique_id1 != $data->Nfd_Unique_Id )
	          			<?php $newsfeed_unique_id1 = $data->Nfd_Unique_Id ?>
	          			{{{ $data->Nfd_Title }}}
          			@endif
	          	</td>
	          	@if(Auth::user()->Use_Type=="1" || Auth::user()->Use_Type=="5")
	          		<td class="td-left">
		          		{{ $data->Brn_Name }}
	          		</td>
	          	@endif
	          	<td class="td-left">
	          		@if($newsfeed_unique_id2 != $data->Nfd_Unique_Id )
		          		<?php $newsfeed_unique_id2 = $data->Nfd_Unique_Id ?>
	          				{{ $data->Use_Name }}
		          	@endif
	          	</td>
	          	<td>
      				@if($newsfeed_unique_id3 != $data->Nfd_Unique_Id )
          				<?php $newsfeed_unique_id3 = $data->Nfd_Unique_Id ?>
		          			@if(Auth::user()->Use_Type == "1")
			          			@if($data->Nfd_Status=="0")
			          				<label style="color:  #ff975b;"><input type="checkbox" id="newsfeed_sts" value="{{ $data->Nfd_Unique_Id }}"></label>
			          			@elseif($data->Nfd_Status=="1")
			          				<label style="color:  #ff975b;">Approve</label>
			          			@elseif($data->Nfd_Status=="2")
			          				<label style="color:  #ff975b;">In-Active</label>
			          			@endif
			          		@elseif(Auth::user()->Use_Type == "2")
			          			@if($data->Nfd_Status=="0")
		          					<label style="color:  #ff975b;">Pending</label>	
			          			@elseif($data->Nfd_Status=="1")
			          				<label style="color:  #ff975b;">Approve</label>
			          			@elseif($data->Nfd_Status=="2")
			          				<label style="color:  #ff975b;">In-Active</label>
			          			@endif
			          		@elseif(Auth::user()->Use_Type == "5")
			          			@if($data->Nfd_Status=="0")
			          				@if($data->Nfd_User_Type!="1")
			          					<label style="color:  #ff975b;"><input type="checkbox" id="newsfeed_sts" value="{{ $data->Nfd_Unique_Id }}"></label>
		          					@else
		          						<label style="color:  #ff975b;">Pending</label>	
			          				@endif
			          			@elseif($data->Nfd_Status=="1")
			          				<label style="color:  #ff975b;">Approve</label>
			          			@elseif($data->Nfd_Status=="2")
			          				<label style="color:  #ff975b;">In-Active</label>
			          			@endif
			          		@endif
          			@endif
	          	</td>
	          	<td>
	          		@if($newsfeed_unique_id5 != $data->Nfd_Unique_Id )
          				<?php $newsfeed_unique_id5 = $data->Nfd_Unique_Id ?>
          					@if(Auth::user()->Use_Type == "1")
          						<a href="{{ url('edit_newsfeed',$data->Nfd_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Edit"></a>
          					@elseif(Auth::user()->Use_Type == "2")
          						@if($data->Nfd_CreatedBy == Auth::user()->Use_Id)
          							<a href="{{ url('edit_newsfeed',$data->Nfd_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Edit"></a>
      							@endif
          					@elseif(Auth::user()->Use_Type == "5")
	          					@if($data->Nfd_User_Type!="1")
		          					<a href="{{ url('edit_newsfeed',$data->Nfd_Unique_Id) }}"><img src="{{ asset('public/images/notepad.png') }}" alt="Edit"></a>
		          				@endif
          					@endif
      				@endif
	          	</td>
	          
	        </tr>
		    <?php $i++; ?>
		    @empty
		    	<tr><td colspan="10" align="center"> Newsfeed Not Found.</td></tr>
		    @endforelse
	  	</tbody>
	</table>
</div>

<div class="paggination-section">
@if ($newsfeed->lastPage() > 1)
    <ul>
        @if ($newsfeed->currentPage() != 1 && $newsfeed->lastPage() >= 5)
            <li><a href="{{ $newsfeed->url($newsfeed->url(1)) }}" ><i class="fa fa-angle-double-left"></i></a></li>
        @endif
        @if($newsfeed->currentPage() != 1)
            <li>
                <a  href="{{ $newsfeed->url($newsfeed->currentPage()-1) }}" >
                    <
                </a>
            </li>
        @endif
        @for($i = max($newsfeed->currentPage()-2, 1); $i <= min(max($newsfeed->currentPage()-2, 1)+4,$newsfeed->lastPage()); $i++)
                <li>
                    <a class="{{ ($newsfeed->currentPage() == $i) ? 'active' : '' }}" href="{{ $newsfeed->url($i) }}">{{ $i }}</a>
                </li>
        @endfor
        @if ($newsfeed->currentPage() != $newsfeed->lastPage())
            <li>
                <a href="{{ $newsfeed->url($newsfeed->currentPage()+1) }}" >
                    >
                </a>
            </li>
        @endif
        @if ($newsfeed->currentPage() != $newsfeed->lastPage() && $newsfeed->lastPage() >= 5)
            <li>
                <a href="{{ $newsfeed->url($newsfeed->lastPage()) }}" >
                    >>
                </a>
            </li>
        @endif
    </ul>
@endif
</div>

<script type="text/javascript">
$(document).ready(function(e){
	$("#newsfeed_sts").on('change',function(){
		if ($("#newsfeed_sts").is(':checked')) {
            var confirm_leave = confirm("Are you sure ?");
            if(confirm_leave == true){
            	window.location.href = "/confirm_newsfeed/"+$("#leave_sts").val();
            }
        }
	});
	$('#btnDelete').on('click',function(){
	    var n = $("input:checked").length;
	    if (n > 0)
	    {
	        var ch_delete=confirm('Delete selected records???');
	        if(ch_delete==true){
		    	var ids = [];
		    	$('input:checked').each(function(i){
	                ids[i] = $(this).val();
	            });
	            $.ajax({
	            	type : "POST",
	            	url : "{{ url('newsfeed_delete') }}",
	            	data : { 
                        _token:     '{{ csrf_token() }}',
                        ids : ids
                    },success : function(data){
                    	$("input[type='checkbox']").each(function(i){
	                        $(this).prop('checked', false);
	                    });
	                    window.location.reload();
                    }
	            });
	        }
	    }else{
	        alert('Please select atleast one record!');
	    }
	});
	$("#selectall").click(function () {
        if ($("#selectall").is(':checked')) {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', true);
            });
            $("#active").show();
        } else {
            $("input[type=checkbox]").each(function(){
                $('.check-all').prop('checked', false);
            });
            $("#active").show();
        }
    });
})
</script>
@endsection
@section('footer')
@section('footer_link_and_scripts')