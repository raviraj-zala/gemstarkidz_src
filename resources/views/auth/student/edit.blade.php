@extends('auth.Master.master')

@section('title','Add Student')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/event-management.png') }}" alt="">Edit Student</h2>      
     </div>
</div>

<div class="form-section">
<form role="form" method="POST" action="{{ url('update_student') }}" enctype="multipart/form-data" id="studentForm" name="edit-student">
    {{ csrf_field() }}

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<!-- <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div> -->
    {!! Session::get('error') !!}
@endif
    
    <div class="left-form">
            <input type="hidden" name="id" value="{{$student->Std_Id}}">

            <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                    <option value="none">-- Select --</option>
                            @foreach($branch as $data)
                                <option value="{{ $data->Brn_Id }}" @if($student->Cla_Bra_Id==$data->Brn_Id) selected @endif>{{ $data->Brn_Name }}</option>
                            @endforeach
                        </select>
                    @if (Session::has('branch'))
                      <label class="error" for="branch">
                        <span class="text-danger">
                            {{ Session::get('branch') }}
                        </span>
                        </label>
                    @endif
                </div>
                </div>    
            </div>


            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" tabindex="2">
                    @foreach($std_class as $data)
                        <option value="{{ $data->Cla_Bra_Id.','.$data->Cla_Class }}" @if($student->Cla_Class==$data->Cla_Class) selected @endif>{{ $data->Cla_Class }}</option>
                    @endforeach
                  </select>
                </div>
                </div>
                    @if (Session::has('class'))
                      <label class="error" for="class">
                        <span class="text-danger">
                            {{ Session::get('class') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

    <div class="form-box">
        <div class="form-text">
          <h5>Section <span>*</span> :</h5>
        </div>   
        <div class="form-typ-box ">
        <div class="btn-group bootstrap-select show-tick form-control">
        <div class="select-box" >
          <select class="class-sec" name="section" id="section" tabindex="3">
                @foreach($std_section as $data)
                    <option value="{{ $data->Cla_Section }}" @if($student->Cla_Section==$data->Cla_Section) selected @endif>{{ $data->Cla_Section }}</option>
                @endforeach
          </select>
        </div>
        </div>
         @if (Session::has('section'))
            <label class="error" for="section">
            <span class="text-danger">
                {{ Session::get('section') }}
            </span>
            </label>
        @endif
        </div>
    </div>

          <div class="form-box">
                <div class="form-text">
                  <h5>Status<span>*</span> :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick branch-control">
                <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="4">
                    <option value="1" @if($student->Std_Status=='1') selected @endif >Active</option>
                    <option value="0" @if($student->Std_Status=='0') selected @endif>In-Active</option>
                </select>
                </div>
                </div>    
                @if ($errors->has('status'))
                <label class="error" for="status">
                    <span class="text-danger">
                        {{ $errors->first('status') }}
                    </span>
                    </label>
                @endif
            </div> 
            <div class="form-box">
                <div class="form-text">
                    <h5>Transport :</h5><br>
                <input name="transport" value="1" type="checkbox" tabindex="5" @if($student->Std_Transport==1) checked="true" @endif><font style="color:white; padding: 10px;">School Bus</font>
                </div>
            </div>           
</div>

<div class="right-form">
<div class="form-box">
                <div class="form-text ">
                    <h5>G.R No <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="grno" class="{{ $errors->has('grno') ? ' is-invalid' : '' }}" required tabindex="6" value="{{ $student->Std_Gr_No }}" >
                @if ($errors->has('grno'))
                    <label class="error" for="grno">
                    <span class="text-danger">
                        {{ $errors->first('grno') }}
                    </span>
                    </lable>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Student Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="studentname" class="{{ $errors->has('studentname') ? ' is-invalid' : '' }}" required tabindex="7" value="{{ $student->Std_Name }}" >
                @if ($errors->has('studentname'))
                <label class="error" for="studentname">
                    <span class="text-danger">
                        {{ $errors->first('studentname') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Parent Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="parentname" class="{{ $errors->has('parentname') ? ' is-invalid' : '' }}" required tabindex="8" value="{{ $student->parentName }}" id="parent_autocomplete" >
                    @if ($errors->has('parentname'))
                    <label class="error" for="name">
                        <span class="text-danger">
                            {{ $errors->first('parentname') }}
                        </span>
                    </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                    <h5>Image :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%">
                    <input type="file" class="browser-btn" name="image" accept="image/*" tabindex="9" style=>
                    @if ($errors->has('image'))
                      <label class="error" for="image">
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        </label>
                    @endif
                </div>
                <img src="{{ url('public/images/profile/'.$student->Std_Image) }}" width="50px" height="50px" style="position: absolute; top: 511px; left: 1256px;">
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Student Fee <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="number" name="studentfee" class="{{ $errors->has('studentfee') ? ' is-invalid' : '' }}" required tabindex="10" value="{{ $student->Std_Fee }}" 
                    pattern="^[0-9]*$" >
                </div>
                @if ($errors->has('studentfee'))
                    <label class="error" for="studentfee">
                    <span class="text-danger">
                        {{ $errors->first('studentfee') }}
                    </span>
                    </label>
                @endif
            </div>
            
</div>
            <div class="form-btn branch-form-btn">
            <input value="save" type="submit" tabindex="11"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="12" ></a>
            </div>
</form>
</div>

@endsection

@section('footer')

@push('footer_script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ url('public/js/index.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
        $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault()    
        }
        if(class_val=="none"){
            alert("Please Select  Class...");
            e.preventDefault()    
        }
        if(section=="none"){
            alert("Please Select  Section...");
            e.preventDefault()    
        }
    });

    $("#parent_autocomplete").autocomplete({
        source : "{{ url('student/getParent') }}"
    });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#class").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });
        $("#class").on('change',function(){
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
        });

    });
});
</script>
@endpush
@section('footer_link_and_scripts')