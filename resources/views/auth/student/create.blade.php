@extends('auth.Master.master')

@section('title','Add Student')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
     <div class="mangement-btn user-text">
       <h2><img src="{{ asset('public/images/event-management.png') }}" alt="">Add Student</h2>      
     </div>
</div>

@if(Session::has('success'))
<div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    {{ Session::get('success') }}
</div>
@endif

@if(Session::has('error'))
<!-- <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div> -->
    {!! Session::get('error') !!}
@endif

<div class="form-section">
<form name="add-student" role="form" method="POST" action="{{ url('save_student') }}" enctype="multipart/form-data" id="studentForm">
    {{ csrf_field() }}
    
    <div class="left-form">
            <div class="form-box">
                <div class="form-text">
                  <h5>Branch * :</h5>
                </div>   
                <div class="form-typ-box">
                <div class="btn-group bootstrap-select show-tick form-control">
                <select class="selectpicker show-tick branch-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" tabindex="1" name="branch" required id="branch">
                    <option value="none">-- Select --</option>
                            @foreach($branch as $data)
                                <option value="{{ $data->Brn_Id }}" >{{ $data->Brn_Name }}</option>
                            @endforeach
                        </select>
                @if (Session::has('branch'))
                <label class="error" for="branch">
                    <span class="text-danger">
                        {{ Session::get('branch') }}
                    </span>
                </label>
                @endif
                </div>
                <div id="branchError"></div>
                </div>    
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Class <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="branch-cls" name="class" id="class" tabindex="2">
                    <option value="none">-- Select--</option>
                  </select>
                </div>
                </div>
                <div id="classError"></div>
                @if (Session::has('class'))
                <label class="error" for="class">
                    <span class="text-danger">
                        {{ Session::get('class') }}
                    </span>
                    </label>
                @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text">
                  <h5>Section <span>*</span> :</h5>
                </div>   
                <div class="form-typ-box ">
                <div class="btn-group bootstrap-select show-tick form-control">
                <div class="select-box" >
                  <select class="class-sec" name="section" id="section" tabindex="3">
                    <option value="none">-- Select--</option>
                  </select>
                </div>
                </div>
                <div id="sectionError"></div>
                @if (Session::has('section'))
                <label class="error" for="section">
                    <span class="text-danger">
                        {{ Session::get('section') }}
                    </span>
                </label>
                @endif
                </div>
            </div>

              <div class="form-box">
                    <div class="form-text">
                      <h5>Status<span>*</span> :</h5>
                    </div>   
                    <div class="form-typ-box">
                    <div class="btn-group bootstrap-select show-tick branch-control">
                    <select class="selectpicker show-tick form-control select2 select2-hidden-accessible {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" required tabindex="4">
                        <option value="1" >Active</option>
                        <option value="0">In-Active</option>
                    </select>
                    </div>
                    @if ($errors->has('status'))
                        <label class="error" for="status">
                        <span class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </span>
                        </label>
                    @endif
                    </div>    
                </div>    

                <div class="form-box">
                    <div class="form-text">
                        <h5>Transport :</h5><br>
                    <input name="transport" value="1" type="checkbox" tabindex="5"><font style="color:white; padding: 10px;">School Bus</font>
                    </div>
                </div>        
        </div>

<div class="right-form">
<div class="form-box">
                <div class="form-text ">
                    <h5>G.R No <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="grno" class="{{ $errors->has('grno') ? ' is-invalid' : '' }}" required tabindex="6" value="{{ old('grno') }}" maxlength="15">
                @if ($errors->has('grno'))
                    <label class="error" for="grno">
                    <span class="text-danger">
                        {{ $errors->first('grno') }}
                    </span>
                    </label>
                @endif
                </sdiv>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Student Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="studentname" class="{{ $errors->has('studentname') ? ' is-invalid' : '' }}" required tabindex="7" value="{{ old('studentname') }}" maxlength="50" >
                </div>
                @if ($errors->has('studentname'))
                    <label class="error" for="studentname">
                    <span class="text-danger">
                        {{ $errors->first('studentname') }}
                    </span>
                    </label>
                @endif
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Parent Name <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="text" name="parentname" class="{{ $errors->has('parentname') ? ' is-invalid' : '' }}" required tabindex="8" value="{{ old('parentname') }}" id="parent_autocomplete" maxlength="50" >
                    @if ($errors->has('parentname'))
                      <label class="error" for="parentname">
                        <span class="text-danger">
                            {{ $errors->first('parentname') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>
            <div class="form-box">
                <div class="form-text">
                    <h5>Image <span>*</span> :</h5>
                </div>
                <div class="form-typ-box zone-box browser-box" style="width: 75%"> 
                    <input name="image" class=" browser-btn{{ $errors->has('image') ? ' is-invalid' : '' }}" type="file" tabindex="9" value="{{ old('image') }}" accept="image/*">
                    @if ($errors->has('image'))
                        <label class="error" for="image">
                        <span class="text-danger">
                            {{ $errors->first('image') }}
                        </span>
                        </label>
                    @endif
                </div>
            </div>

            <div class="form-box">
                <div class="form-text ">
                    <h5>Student Fee <span>*</span> :</h5>
                </div>
                <div class="form-typ-box"> 
                    <input type="number" name="studentfee" class="{{ $errors->has('studentfee') ? ' is-invalid' : '' }}" required tabindex="10" value="{{ old('studentfee') }}" >
                </div>
                @if ($errors->has('studentfee'))
                    <label class="error" for="studentfee">
                    <span class="text-danger">
                        {{ $errors->first('studentfee') }}
                    </span>
                    </label>
                @endif
            </div>

            
</div>
            <div class="form-btn branch-form-btn">
            <input value="save" type="submit" tabindex="11" id="submitForm"><a href="{{ URL::previous() }}"><input value="Cancel" type="button" tabindex="12" ></a>
            </div>
</form>
</div>

@endsection

@section('footer')

@push('footer_script')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{ url('public/js/index.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
    var brnErr,clsErr,secErr;
    $('#studentForm').on('submit',function(e){
        var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        
        if(branch=="none"){
            alert("Please Select  Branch...");
            e.preventDefault();
            return false;
        }else{
            if(class_val=="none")
            {
                alert("Please Select  Class...");
                e.preventDefault();
            return false;
                
            }else{
                if(section=="none"){
                    alert("Please Select  Section...");
                    e.preventDefault();
            return false;
                }
            }
        }
    });

    $("#submitForm").click(function(e){
    	var branch = $("#branch").val();
        var class_val = $("#class").val();
        var section = $("#section").val();
        
        if(branch=="none"){
            $("#branchError").html("<label for='branch' class='error'><span class='text-danger'>Please select branch.</span></label>");
            e.preventDefault();
            return false;
        }else{
	        $("#branchError").html("");
            if(class_val=="none")
            {
                $("#classError").html("<label for='class' class='error'><span class='text-danger'>Please Class branch.</span></label>");
                e.preventDefault();
            return false;
                
            }else{
                if(section=="none"){
                 $("#sectionError").html("<label for='branch' class='error'><span class='text-danger'>Please Select Section.</span></label>");
                    e.preventDefault();
            return false;
                }
            }
        }
    });

    $("#parent_autocomplete").autocomplete({
        source : "{{ url('student/getParent') }}"
    });

    $("#branch").on('change',function(){
        var branchId = $(this).val();
        $.ajax({
            type : "POST",
            url : "{{ url('student/getClass') }}",
            data : {
                _token:     '{{ csrf_token() }}',
                Cla_Bra_Id : branchId
            },
            dataType : "JSON",
            success : function(data){
                    $(".branch-cls option").each(function() {
                        $(this).remove();
                    });
                    $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                    var items = [];
                     $.each( data, function( key, val ) {
                        items.push( "<option value='" +this['Cla_Bra_Id']+","+this['Cla_Class'] + "'>" + this['Cla_Class'] + "</option>" );
                     });
                     $("#class").append('<option value="none">-- Select--</option>');
                     $("#section").append('<option value="none">-- Select --</option>');
                    $("#class").append(items);
                },
            error : function(error){
                console.log(error);
            }
        });
    });
        $("#class").on('change',function(){
            var idAndClass = $(this).val();
            var classIdName = idAndClass.split(",");
            $.ajax({
                type : "POST",
                url : "{{ url('student/getSection') }}",
                data : {
                    _token:     '{{ csrf_token() }}',
                    Cla_Class : classIdName[1],
                    Cla_Bra_Id : classIdName[0]
                },
                dataType : "JSON",
                success : function(data){
                        $(".class-sec option").each(function() {
                            $(this).remove();
                        });
                        var items = [];
                         $.each( data, function( key, val ) {
                            items.push( "<option value='" + this['Cla_Section'] + "'>" + this['Cla_Section'] + "</option>" );
                         });
                        $("#section").append('<option value="none">-- Select --</option>');
                        $("#section").append(items);
                },
                error :function(error){
                    console.log(error);
                }
        });

    });
});
</script>
@endpush
@section('footer_link_and_scripts')