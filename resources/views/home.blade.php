@extends('auth.Master.master')

@section('title','Home')

@section('site_header')

@section('sidebar')

@section('content')

<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/event-management.png') }}" alt="branch-img">Daily Event List</h2>      
  </div>
</div>

<div class="clearfix"></div>

  <div class="row">

  @if(Session::has('success'))
  <div class="alert alert-success alert-icon alert-close alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
      {{ Session::get('success') }}
  </div>
  @endif

  @if(Session::has('error'))
  <div class="alert alert-danger alert-icon alert-close alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
      {{ Session::get('error') }}
  </div>
  @endif
      <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="">
        </div>
        </br> 
      </div>
    </div>

@if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5 || Auth::user()->Use_Type==2 )
<div class="table-form">   
@if(count($event)>0)
<table id="example2" align="left">
    <tbody>
      <tr>
      <th class="td-left">Event</th>
      <th>Date</th>
      <th>Created By</th>
      <th>Approve</th>
    </tr>
  <?php $i=1; $eve1 = ""; $eve2 = ""; $eve3 = ""; $eve4 = ""; $eve5 = ""; $eve6 = "";  ?>
  @foreach($event as $data)
    
        @if($eve2 != $data->Eve_Unique_Id)
        <tr>
        <td class="td-left">
           <a href="{{ url('Event Mgmt.') }}">  
            <?php $eve2 = $data->Eve_Unique_Id; ?>
              <font color="#f4862c"> {{ $data->Eve_Name }} </font>
            </a>
        </td>
        @endif
        
        @if($eve3 != $data->Eve_Unique_Id)
        <td>
            <?php $eve3 = $data->Eve_Unique_Id; ?>
            {{ date("d-m-Y", strtotime($data->Eve_Date)) }}
        </td>
        @endif
        
        @if($eve4 != $data->Eve_Unique_Id)
        <td>
            <?php $eve4 = $data->Eve_Unique_Id; ?>
            {{ $data->createdByName}}
        </td>
        @endif
        
        @if($eve5 != $data->Eve_Unique_Id)
        <td>
            <?php $eve5 = $data->Eve_Unique_Id; ?>
            @if($data->Eve_Approve==1 ) 
              <label style="color:  #ff975b;">Approve</label>
            @else
                @if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5 )
                <label style="color:  #ff975b;"><input type="checkbox" class="eve_sts" value="{{ $data->Eve_Unique_Id }}"></label>
                @else(Auth::user()->Use_Type==2 || Auth::user()->Use_Type==3)
                <label style="color:  #ff975b;">Pendding...</label>
                @endif
            @endif
        </td>    
        </tr>
        @endif
  <?php $i++; ?>
  @endforeach
</tbody>
</table>
@else
  <h6 style="color: #ff975b" >Not any event today.</h6>
@endif


<div class="line-1">        
    <div class="mangement-btn user-text">
        <h2><img src="{{ asset('public/images/event-management.png') }}" alt="branch-img">Daily Social Activity</h2>      
  </div>
</div>
<div class="clearfix"></div>
<br>
@if(count($social)>0)
<table id="example2" align="left">
    <tbody><tr>
      <th class="td-left">Title</th>
      <th class="td-left">Created By </th>
      <th class="td-left">Approve</th>
    </tr>
  @if($social)
  <?php $i=1; $soa1 = ""; $soa2 = ""; $soa3 = ""; $soa4 = ""; $soa5 = ""; $soa6 = ""; $soa7 = ""; $soa8 = ""; ?>
  @foreach($social as $data)
        @if($soa2 != $data->Soa_Unique_Id)
        <tr>
        <td class="td-left">
          <?php $soa2 = $data->Soa_Unique_Id; ?>
          {{ $data->Soa_Title }}
        </td>
        <td class="td-left">
        {{ $data->createdByName }}
        </td>
       <td>
            @if($data->Soa_Status==1) 
              <label style="color:  #ff975b;">Approve</label>
            @else
                @if(Auth::user()->Use_Type==1 || Auth::user()->Use_Type==5)
                <label style="color:  #ff975b;"><input type="checkbox" class="social_sts" value="{{ $data->Soa_Unique_Id }}"></label>
                @else(Auth::user()->Use_Type==2 || Auth::user()->Use_Type==3)
                <label style="color:  #ff975b;">Pendding...</label>
                @endif
            @endif
        </td>
        </tr>
        @endif
    <?php $i++; ?>
    @endforeach
@else
  <tr><td colspan="8">No Data Found</td></tr>
@endif
@else
  <h6 style="color: #ff975b" >Not any social activity.</h6>
@endif
</tbody>
</table>
</div>



</div>
@endif
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script>
$(function () {
  $("#selectall").click(function () {
    if ($("#selectall").is(':checked')) {
      $("input[type=checkbox]").each(function(){
        $('.check-all').prop('checked', true);
      });
      $("#active").show();
    } else {
      $("input[type=checkbox]").each(function(){
        $('.check-all').prop('checked', false);
      });
      $("#active").show();
    }
  });
});

$(".eve_sts").click(function(){
  var id = $(this).val();
  var ch_sts=confirm('Aprrove Event ???');
    if(ch_sts){
    $.ajax({
      type : "POST",
      url : "{{ url('approve_event') }}",
      data : {
        _token : '{{ csrf_token() }}',
        id : id
      },
      success : function(data){
        $('table#example2').html(data);
        $(".eve_sts").each(function(i){
          $(this).prop('checked', false);
        });
        window.location.reload();
      }
    });
  }
});

$(".social_sts").click(function(){
  var id = $(this).val();
  var ch_sts=confirm('Aprrove Social Activate ???');
    if(ch_sts){
    $.ajax({
      type : "POST",
      url : "{{ url('approve_social') }}",
      data : {
        _token : '{{ csrf_token() }}',
        id : id
      },
      success : function(data){
        $('table#example2').html(data);
        $(".social_sts").each(function(i){
          $(this).prop('checked', false);
        });
              window.location.reload();
      }
    });
  }
});


</script>
@endsection

@section('footer')

@section('footer_link_and_scripts')