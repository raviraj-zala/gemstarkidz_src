<!doctype html>
<html lang="en">
   <head>
      Explain permissions in our Applications:
      <title>Explain permissions in our Applications:</title>
   </head>
   <body style="background-color:white; ">
      <header style="background-color:white;">
         <p>I.&nbsp;<strong>Access Internet</strong></p>
         <ul >
            <li class="line number1 index0 alt2">
            	<code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code>&nbsp;<code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.INTERNET"</code>&nbsp;<code class="xml plain">&gt;</code></li>
            <li class="line number1 index0 alt2">
               <div class="line number3 index2 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.ACCESS_NETWORK_STATE"</code></div>
            </li>
            <li class="line number1 index0 alt2">
               <div class="line number3 index2 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.ACCESS_WIFI_STATE"</code><code class="xml plain">/&gt;</code></div>
            </li>
         </ul>
         <p>These permissions can help us get resources from our server.</p>



         <p>II.&nbsp;<strong>Location Permission</strong></p>
         <ul >
            <li class="line number1 index0 alt2">
            	<code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code>&nbsp;<code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.ACCESS_FINE_LOCATION"</code>&nbsp;<code class="xml plain">&gt;</code></li>
            <li class="line number1 index0 alt2">
               <div class="line number3 index2 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.ACCESS_COARSE_LOCATION"</code></div>
            </li>
         </ul>
         <p>Using this permission application can access the location of the device to it's core functionality.</p>
      </header>
   </body>
</html>