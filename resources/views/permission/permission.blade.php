<!doctype html>
<html lang="en">
   <head>
      Explain permissions in our Applications:
      <title>Explain permissions in our Applications:</title>
   </head>
   <body style="background-color:white; ">
      <header style="background-color:white;">
         <p>I.&nbsp;<strong>Access Internet</strong></p>
         <ul >
            <li class="line number1 index0 alt2">
            	<code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code>&nbsp;<code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.INTERNET"</code>&nbsp;<code class="xml plain">&gt;</code></li>
            <li class="line number1 index0 alt2">
               <div class="line number3 index2 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.ACCESS_NETWORK_STATE"</code></div>
            </li>
            <li class="line number1 index0 alt2">
               <div class="line number3 index2 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.ACCESS_WIFI_STATE"</code><code class="xml plain">/&gt;</code></div>
            </li>
         </ul>
         <p>These permissions can help us get resources from our server.</p>
         <p>II.&nbsp;<strong>Access Storage</strong></p>
         <div class="container">
            <ul>
               <li class="line number1 index0 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.WRITE_EXTERNAL_STORAGE"</code><code class="xml plain">/&gt;</code></li>
               <li class="line number1 index0 alt2"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.READ_EXTERNAL_STORAGE"</code><code class="xml plain">/&gt;</code></li>
            </ul>
            <p>These permissions can help us access your storage for save photos, then for share to all your friends.</p>
            <p>III.&nbsp;<strong>Read Phone Permission</strong></p>
            <ul>
               
               <li class="line number2 index1 alt1"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.READ_PHONE_STATE"</code>&nbsp;<code class="xml plain">/&gt;</code></li>
            </ul>
            <p>These permission can help us access your read phone state.</p>
           
         </div>
         </ul>
         <p>Iv.&nbsp;<strong>Call Phone Permission</strong></p>
         <ul>
            <li class="line number2 index1 alt1"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.CALL_PHONE"</code>&nbsp;<code class="xml plain">/&gt;</code></li>
         </ul>
         <p>These permission can help us access your calling facility.</p>
         
         <p>V.&nbsp;<strong>Contact List Permission</strong></p>
         <ul>
            <li class="line number2 index1 alt1"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.READ_CONTACTS"</code>&nbsp;<code class="xml plain">/&gt;</code></li>
            <li class="line number2 index1 alt1"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.WRITE_CONTACTS"</code>&nbsp;<code class="xml plain">/&gt;</code></li>
            <li class="line number2 index1 alt1"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.READ_PHONE_NUMBERS"</code>&nbsp;<code class="xml plain">/&gt;</code></li>
         </ul>
         <p>These permission can help us access your contact for chat.</p>
         
         <p>VI.&nbsp;<strong>Microphone Use Permission</strong></p>
         <ul>
            <li class="line number2 index1 alt1"><code class="xml plain">&lt;</code><code class="xml keyword">uses-permission</code><code class="xml color1">android:name</code><code class="xml plain">=</code><code class="xml string">"android.permission.RECORD_AUDIO"</code>&nbsp;<code class="xml plain">/&gt;</code></li>
         </ul>
         <p>These permission can help us access your Microphone for chat.</p>
         


      </header>
   </body>
</html>